<?php
if(isset($_POST['email'])){
	$_POST['email'] = $_SESSION['user'];
}
?>
<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />
		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>
			.row{
				margin: 0px !important;
			}

			
		</style>
	</head>
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="gftnow-landing-video" style="height:100%;">
				<span class="mask" style="height:100%;">
					<br/>
					<div class="landing-nav">
						<div class="landing-nav-header">
							<div class="row">
								<div class="col-md-5 col-sm-12 text-center"></div>
								<div class="col-md-2 col-sm-12 text-center"><img src="assets/img/logo-lg.png"><br/></div>
								<div class="col-md-5 col-sm-12 text-center"></div>
							</div>
							
						</div>
					</div>
					<div class="section" id="landing-video">
						<div class="row">
							<div class="col-md-9 text-center subheader" style="margin-left:40px;">
								<div class="gftnow-signbox">
									<form method="post" id="loginform">
										<input type="hidden" name="key" value="<?php echo uniqid();?>" />
										<div class="form-group"><p class="gftnow-font-medium">Player Login</p></div>
										<div class="form-group">
											<input class="form-control" type="text" name="email" id="email" placeholder="Email"/>
										</div>
										<div class="form-group">
											<input class="form-control" type="password" name="password" id="password" placeholder="Password"/>
										</div>
										<br/>
										<div class="form-group">
											<button type="submit" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg">Sign In</button>
										</div>
										<div class="gftnow-hide" style="padding: 5px;" id="errorCode">
											<div class="alert alert-danger">
											  
											</div>
										</div>
										<br/>
									</form>
								</div>
							</div>
						</div>
					</div>

				</span>
			</div>

		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>


		<script type="text/javascript">
			$(document).ready(function() {	
				init();

				$("#mask2").css("z-index", "9999");
				$(".gftnow-signbox").show();

				$("#loginform").validate({
					errorPlacement: function(){
						return false;
					},
					rules:{
						email: "required",
						password: "required"
					},
					submitHandler: function(form){
						console.log("submit");
						
						console.log($(form.email).val());
						console.log($(form.password).val());

						var email = $(form.email).val();
						var password = $(form.password).val();
						var haserror = 0;

						firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
						  // Handle Errors here.
						  var errorCode = error.code;
						  var errorMessage = error.message;

						  if(errorCode=="auth/wrong-password"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
						  }
						  if(errorCode=="auth/invalid-email"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
						  }
						  if(errorCode=="auth/user-not-found"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
						  }
						  
						  // ...
						}).then(function(user){

							if(haserror==0)
							{
								console.log(user);

								var user = firebase.auth().currentUser;

								user.getToken().then(function(data) {
				      
					    			var token = data;
	  	  							console.log(token);
	  	  							jQuery.ajax({
										url: "mvc/controller/ajaxController.php",
										type: "post",
										dataType : "json",
										data: { func: "loginAdmin",token:token },
										success: function(data){
											console.log(data);
											if(data.result.result.userInfo.identifier){
												window.location = "index.php?p=admin/realtime_map";
											}else{
												console.log("cant login");
											}
										},error: function(err){
											console.log(err.responseText);
										}
									});
					  	  			

				    			});

							}
						});

						
					}
				});
			});

			function init(){
				var width = $(window).width();
				console.log(width);
				var panelWidth = 0.5;

				if(width<=414)
					panelWidth = 0.95;
				else if(width<=320)
					panelWidth = 1;

				

				$('#slider-one').movingBoxes({
				startPanel   : 2,      // start with this panel
				reducedSize  : 0.5,    // non-current panel size: 80% of panel size
				wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
				buildNav     : false,   // if true, navigation links will be added
				navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
				,panelWidth   : panelWidth,
				initAnimation: true,
				easing       : 'swing',
				completed: function(e, slider, tar){
					
					$("#movingbox-label").text(slider.curPanel+"/3");
				}
				});

				var mb = $('#slider-one').data('movingBoxes');

				$("#movingbox-next").bind("click", function(){
					mb.goForward();
				});

				$("#movingbox-prev").bind("click", function(){
					mb.goBack();
				});
			}


		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>