<?php ob_start();session_start();
	
	
	if(!isset($_SESSION['user'])){
		header("Location:index.php");
	}else{

		if($_SESSION['role']=="Admin"){

			if(isset($_GET['p'])){
				include_once $_GET['p'].".php";

				if(!isset($content))
						header("Location:dashboard.php?p=admin/realtime_map");
			}else{
				header("Location:dashboard.php?p=admin/realtime_map");
			}


		}else{
			header("Location:index.php");
		}

	}

	if(isset($_GET['loggedOut'])){
		session_destroy();
		header("Location:index.php");
	}

	function trim_text($string, $repl, $limit) {
		if(strlen($string) > $limit){
			return substr($string, 0, $limit) . $repl;
		}
		else {
			return $string;
		}
	}
?>

<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/Croppie-2.4.1/croppie.css" />
		
		<link rel="stylesheet" type="text/css" media="screen" href="assets/jQuery-File-Upload-9.18.0/css/jquery.fileupload.css" />

		<!-- DATA TABLES -->
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
	</head>
	<body style="max-height: 600px;">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
				<img src="assets/img/logo.png">	<span id="gftnowtitle">GiftDrop</span>
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->

			<?php
				echo $content['menu'];
			?>
			<!-- <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span> -->

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- <div class="arrow_box_top" id="notifications-balloon">
				<div class="body">
					<div class="row">
						<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">Your Recent Campaign Performing Very Well!!</div>
						<div class="col-md-2 col-sm-2 col-lg-2 col-xs-2 text-center">
							<a href="#"><i class="fa fa-arrow-circle-right text-success"></i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">Please confirm your email</div>
						<div class="col-md-2 col-sm-2 col-lg-2 col-xs-2 text-center">
							<a href="#"><i class="fa fa-exclamation-triangle text-warning"></i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">Welcome to GFTnow! Start with learning about dashboard</div>
						<div class="col-md-2 col-sm-2 col-lg-2 col-xs-2 text-center">
							<a href="#"><i class="fa fa-arrow-circle-right text-success"></i></a>
						</div>
					</div>
				</div>
			</div> -->
			
			<!-- RIBBON -->
			<div id="ribbon" style="background-color:#ffffff;">
				<div class="pull-left">
					<span class="minifyme minify2"> <i class="fa fa-angle-left hit"></i> </span>
				</div>
				

				<div class="pull-right notif">
					<ul>
						<li><?php echo isset( $content['ribbon_image'])?$content['ribbon_image']:"".'&nbsp;&nbsp;'.trim_text(ucfirst($_SESSION['user']['name']), "...", 10);?></li>
						<li onclick='showballon()' id='notif-bell'>
							<img src="assets/img/bell.png"/>
							<b class="badge">1</b>
						</li>
						<li id="logout" style="display:none;">
							<i class="fa fa-sign-out icon-gray" style="font-size:22px;"></i>
						</li>
					</ul>
				</div>
				
				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content" style="background-color:#ffffff;">
				<br/><br/><br/><br/>
				<div class="row">
					<div id="title" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<h1 class="gftnow-font-medium">&nbsp;<?php echo $content['title'];?> </h1>
					</div>
					<div id="right-of-title" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<?php
							echo $content['right-of-title'];
						?>
					</div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-12">
						<?php
							echo $content['content'];
						?>
						</div>
					</div>
				</div>

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		

		
		<?php include "footer.php";?>
		<script>
			$(document).ready(function(){
				// LOGOUT
				$("#logout").click(function(){
					var url = window.location.href;
					window.location.href = url+'&loggedOut';
				});

				// COLLAPSE LEFT NAV
				$('.minifyme, .hide-menu').click(function(e) {
					$('body').toggleClass("minified");
					$(this).effect("highlight", {}, 500);
					e.preventDefault();
				});

				
				<?php echo $content['script'];?>

				$(".drop-rounded-header").click(function(){
				 	$(this).parent().find(".drop-rounded-body").show();
				 });

				 $(".drop-rounded-body a").click(function(){
				 	var text = $(this).text();
				 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
				 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
					$(this).parent().parent().hide();
				 });
			});
		</script>

	</body>

</html>