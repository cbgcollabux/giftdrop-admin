<?php
if(isset($_POST['email'])){
	$_POST['email'] = $_SESSION['user'];
}
?>
<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>

			.container-full{
				background-color: black;
			}
			.row{
				margin: 0px !important;
			}
			.gftnow-signbox{
				left:0 !important;
				right:0 !important;
				margin:0 auto !important;
				width: 100%;
				max-width: 400px;

			}
			#particles-js{
				background-position: center center;
				background-repeat: no-repeat;
				background-size: cover;
				position: absolute;
				height: 100%;
				width: 100%;
				overflow: hidden;
				z-index: 
			}

			#particles-js2{
				background-image: url(assets/img/parallax3.jpg);
				background-position: center center;
				background-repeat: no-repeat;
				background-size: cover;
				position: absolute;
				height: 100%;
				width: 100%;
				overflow: hidden;
			}
		</style>
	</head>
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		<div style="position:absolute;color:#fff;bottom: 10px;right: 40px;z-index:999;">Photo by <span id="by">Neal Grosskopt</span></div>
		
		<div class="container-full">
			<div id="particles-js"></div>
				<span class="mask" style="top:0;position:absolute;left:0;right:0;margin:0;">
					<br/>
					<div class="landing-nav">
						<div class="landing-nav-header">
							<div class="row">
								<div class="col-md-5 col-sm-12 text-center"></div>
								<div class="col-md-2 col-sm-12 text-center"><img src="assets/img/logo-lg.png"><br/></div>
								<div class="col-md-5 col-sm-12 text-center"></div>
							</div>
						</div>
					</div>
					<div class="section">
						<center>
						<div class="gftnow-signbox" style="display:inline;">
							<form method="post" id="loginform">
								<input type="hidden" name="key" value="<?php echo uniqid();?>" />
								<div class="form-group"><p class="gftnow-font-medium">Admin Login</p></div>
								
								<div class="form-group">
									<input class="form-control" type="text" name="email" id="email" placeholder="Email"/>
								</div>
								<div class="form-group">
									<input class="form-control" type="password" name="password" id="password" placeholder="Password"/>
								</div>
								<br/>
								<div class="form-group">
									<a href="javascript:void(0);" onclick="$(this).closest('form').submit();" id="submit" type="submit" class="bttn-rounded">Sign In</a>
								</div>
								<div style="padding: 5px;display:none;" id="errorCode">
									<div class="alert alert-danger">
									</div>
								</div>
								<br/>
							</form>
						</div>
						</center>
					</div>
				</span>

		</div>


		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script src="assets/particles/particles.js"></script>
		<script src="assets/particles/demo/js/app.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {

			    var imgArr =[ {"img":"parallax.jpg","by":"Cody Sumter"},{"img":"parallax1.jpg","by":"Trey Ratcliff"},{"img":"parallax2.jpg","by":"Bjorn Kleemann"},{"img":"parallax3.jpg","by":"Dave Morrow"},{"img":"parallax4.jpg","by":"Neal Grosskopt"},{"img":"parallax5.jpg","by":"Andreas Klose"},{"img":"parallax6.jpg","by":"+AnushElangonvan"},{"img":"parallax7.jpg","by":"Ray Bilcliff"}];
			    imgArr = shuffle(imgArr);

			    var initbg = imgArr[Math.floor(Math.random() * (imgArr.length)) ];
			    $("#particles-js").css("background-image", "url(assets/img/" + initbg.img + ")")
				$("#by").text(initbg.by);

			    animateBackground();

				function animateBackground() {
				    window.setTimeout(function(){
				        var url = imgArr[imgArr.push(imgArr.shift()) - 1];
				        $('#particles-js').delay(30000).fadeOut(500, function(){
				            $(this).css("background-image", "url(assets/img/" + url.img + ")")
				            $("#by").text(url.by);
				        }).fadeIn(500, animateBackground())
				    });
				}

				init();

				$("#mask2").css("z-index", "9999");

				$("#email").keypress(function(e){
					if(e.which == 13){
						$(this).blur();
            			$('#submit').focus().click();
					}
				});

				$("#password").keypress(function(e){
					if(e.which == 13){
						$(this).blur();
            			$('#submit').focus().click();
					}
				});

				$("#loginform").validate({
					errorPlacement: function(){
						return false;
					},
					rules:{
						email: "required",
						password: "required"
					},
					submitHandler: function(form){
						console.log("submit");
						
						console.log($(form.email).val());
						console.log($(form.password).val());

						var email = $(form.email).val();
						var password = $(form.password).val();
						var haserror = 0;

						firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
							// Handle Errors here.
							var errorCode = error.code;
							var errorMessage = error.message;

							if(errorCode=="auth/wrong-password"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
							}
							if(errorCode=="auth/invalid-email"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
							}
							if(errorCode=="auth/user-not-found"){
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
							}
						}).then(function(user){

							if(haserror==0)
							{
								
								var user = firebase.auth().currentUser;

								user.getToken().then(function(data) {
				      
					    			var token = data;

					    			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
									jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });
	  	  							
	  	  							jQuery.ajax({
										url: "mvc/controller/ajaxController.php",
										type: "post",
										dataType : "json",
										data: { func: "loginAdmin",token:token },
										success: function(data){

											//console.log(data);
											if(data.identifier){
												window.location = "dashboard.php?p=admin/realtime_map";
											}else{
												console.log("cant login");
												$("#errorCode .alert").html("User do not exist or not an admin.");
												$("#errorCode").show();
											}
										},error: function(err){
											console.log(err.responseText);
										}
									});
				    			});
							}
						});
					}
				});
			});

			function shuffle(array) {
				var currentIndex = array.length, temporaryValue, randomIndex;
				// While there remain elements to shuffle...
				while (0 !== currentIndex) {
					// Pick a remaining element...
					randomIndex = Math.floor(Math.random() * currentIndex);
					currentIndex -= 1;

					// And swap it with the current element.
					temporaryValue = array[currentIndex];
					array[currentIndex] = array[randomIndex];
					array[randomIndex] = temporaryValue;
				}
				return array;
			}

			function init(){
				var width = $(window).width();
				console.log(width);
				var panelWidth = 0.5;

				if(width<=414)
					panelWidth = 0.95;
				else if(width<=320)
					panelWidth = 1;

				$('#slider-one').movingBoxes({
				startPanel   : 2,      // start with this panel
				reducedSize  : 0.5,    // non-current panel size: 80% of panel size
				wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
				buildNav     : false,   // if true, navigation links will be added
				navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
				,panelWidth   : panelWidth,
				initAnimation: true,
				easing       : 'swing',
				completed: function(e, slider, tar){
					
					$("#movingbox-label").text(slider.curPanel+"/3");
				}
				});

				var mb = $('#slider-one').data('movingBoxes');

				$("#movingbox-next").bind("click", function(){
					mb.goForward();
				});

				$("#movingbox-prev").bind("click", function(){
					mb.goBack();
				});
			}
		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

	</body>

</html>