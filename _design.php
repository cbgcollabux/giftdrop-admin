<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />
		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>
			.row{
				margin: 0px !important;
			}

			
		</style>
	</head>
	<body style="background-color: rgb(244, 245, 248);padding:30px;margin:0;overflow: auto;height: 100%;margin: 0px;">

		<div class="container-full">
			<div class="row">
				<center>
					<div class="col-md-12">
						<img src="assets/img/logo.png">
						<p class="gftnow-font-bold">GiftDrop</p>
					</div>
					<br/>
					<div class="col-md-12">
						<div style="background-color:#D3D2D2;">
							<br/><br/><br/><br/><br/><br/><br/><br/>
							<p class="gftnow-font-bold">Email Title</p>
							<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
						</div>

						<div style="width:90%; background-color:#FFFFFF;padding:30px;margin-top:-100px;">
							<br/>
							<p class="gftnow-font-regular">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec elementum orci. Ut id venenatis quam. Duis vestibulum orci ac sem iaculis laoreet. Cras non egestas mi. Proin id convallis massa. Cras orci felis, vulputate id venenatis id, eleifend non nibh. Suspendisse potenti. Proin vel velit vitae eros dignissim rhoncus. Sed eleifend metus vitae ligula mollis, nec commodo lorem sollicitudin. Curabitur nec varius diam, sed lobortis purus. Duis aliquet vitae libero in dignissim.</p>
							<p class="gftnow-font-regular">Duis ornare faucibus lectus gravida volutpat. Donec vel massa fermentum, luctus sapien ultricies, rutrum mauris. Phasellus sit amet risus nec mauris dapibus pulvinar. Nunc ultricies, tortor ut congue blandit, sem eros mollis nibh, in venenatis magna velit sit amet diam. Suspendisse tellus est, tempus et placerat sed, ultricies eget magna. Integer et mollis ex. Sed posuere feugiat elit in imperdiet. Maecenas sit amet facilisis augue. Nam varius lacus in pretium aliquet. Vivamus vitae varius erat, id fringilla dolor. Aliquam quam ex, porta cursus iaculis suscipit, congue id urna. Fusce id lobortis dolor, vel ultricies ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam feugiat condimentum lacus et bibendum. Suspendisse ac porta neque. Sed ut erat consequat risus porttitor convallis.</p>
							<br/>
							<button class="btn btn-success gftnow-btn gftnow-btn-success">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
						</div>

						<div style="margin-top:15px;">
							<p style="font-size:12px;color:#D3D2D2;">GiftDrop Inc, PO Box 7510 Beaverton, OR</p>

							<img src="assets/img/social-medias.png"/>

						</div>
					</div>
				</center>
			</div>
		</div>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>
	</body>

</html>