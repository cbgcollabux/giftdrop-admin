<?php 
	include_once "header.php";
    include_once "mvc/model/user.php";

	if ($_SESSION['timeout'] + 30 * 60 < time()) {
		session_destroy();
		header('Location: onboarding.php');
	}

    echo "<input type='hidden' id='token' value='".$_SESSION['token']."' />";
?>


	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/template/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
					</div>
				</center>
			</div>
			
			<div class="row onboarding-container" style="height: 1300px;">
				<div class="col-md-3 onboarding-left-panel">
					<div class="onboarding-nav-container">
						<nav id="onboarding-nav">
							<ul>
								<li>
									<a href="onboarding.php" title="Service"> <div class="numberCircle">1</div> <span class="menu-item-parent">Service</span></a>
								</li>
								<li>
									<a href="onboarding-4.php" title="Campaign"> <div class="numberCircle active">2</div> <span class="menu-item-parent">Campaign</span></a>
								</li>
								<?php
								if(isset($_SESSION['drops'])){
									echo '<li><a href="onboarding-5.php" title="Drops"> <div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a href="onboarding-6.php" title="Detail"> <div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								} else {
									echo '<li class="inactive"><a><div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li class="inactive"><a><div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								}
								?>
							</ul>
						</nav>
					
						<br/><br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
										<p><center>
											<h3>Business Information</h3>
											<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
										</center></p>
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>	
				</div>

				
					<div class="col-md-9 onboarding-right-panel ">
						<form id="form1" method="post" action="onboarding-5.php">
							<div class="row">
								<div class="col-md-12">
									<center>
										<h2 class="gftnow-title">
											Create Campaign
										</h2>
										What would you like to promote?
									</center>

									<div class="onboarding" style="box-shadow:none;">
										<div class="onboarding-content">
											<div class="row">
												<div class="col-md-6">
													<br/>
													<p id="campaignname-placeholder" class="placeholder-effect"></p>
													<input type="text" class="form-control" id="campaignname" name="campaignname" placeholder="Campaigne Title" value="<?php if(isset($_SESSION['campaign']['name']))echo $_SESSION['campaign']['name']; ?>"/>
												</div>
												
												<div class="col-md-6">
													<p>Type</p>
													<div class="gftnow-inline">
														<div class="gftnow-form">
															<div class="gftnow-checkbox <?php if(isset($_SESSION['campaign']['type'])){ if($_SESSION['campaign']['type']=="gift-card"){echo "active";} }?>" data-name="type" data-value="gift-card"></div>
															<label>Gift Card</label>
														</div>

														<div class="gftnow-form">
															<div class="gftnow-checkbox <?php if(isset($_SESSION['campaign']['type'])){ if($_SESSION['campaign']['type']=="free-product"){echo "active";} }?>" data-name="type" data-value="free-product"></div>
															<label>Free Product / Service</label>
														</div>
													</div>
													<input name="type" id="type" value="<?php if(isset($_SESSION['campaign']['type'])){echo $_SESSION['campaign']['type'];}?>" type="hidden"/>
													<input type="hidden" name="giftcard" type="giftcard"/>
												</div>
											</div>

											<br/>

											<div class="row" id="image-preview">
												<div class="col-md-12">
													<p>Image Preview</p> <input type="hidden" name="campaignimage" id="campaignimage" value="<?php if(isset($_SESSION['campaign']['image'])){echo $_SESSION['campaign']['image'];}?>" />
													<input type="hidden" name="campaignimagetype" id="campaignimagetype" value="<?php if(isset($_SESSION['campaign']['imagetype'])){echo $_SESSION['campaign']['imagetype'];}?>" />
													<div class="gtfnow-uploader-slider">
														<div class="gftnow-fader">
															<img src="assets/img/chevron-left.png" />
														</div>
														<ul>
															<li>
																<div class="uploadLogo uploadLogo2 fileinput-button card-preview-small" id="custom-upload" style="display:none;">
																	<br/><br/><br/><br/>
																	<div class="label2">
																		<div id="custom-upload-content"></div>
																	</div>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>

											<input id='fileupload' type='file' name='files[]' style='display:none;'>

											<br/><br/>
											<div class="row">
												<div class="col-md-12">
													<p id="description-placeholder" class="placeholder-effect"></p>
													<textarea class="form-control" name="description" id="description" style="height: 180px;" placeholder="Campaign Description"><?php if(isset($_SESSION['campaign']['name']))echo $_SESSION['campaign']['description']; ?></textarea>
												</div>
											</div>

											<br/><br/>
											<div class="row">
												<div class="col-md-12">
													<p id="terms-placeholder" class="placeholder-effect"></p>
													
													<textarea class="form-control" name="terms" id="terms" style="height: 180px;" placeholder="Terms & Conditions"><?php if(isset($_SESSION['campaign']['name']))echo $_SESSION['campaign']['terms']; ?></textarea>
												</div>
											</div>

										</div>
										
										<input type="hidden" id="gftvendor" name="gftvendor" value="<?php if(isset($_SESSION['campaign']['vendor']))echo $_SESSION['campaign']['vendor']; else echo "-1"; ?>" />
										<input type="hidden" id="gftamount" name="gftamount" value="<?php if(isset($_SESSION['campaign']['gftamount']))echo $_SESSION['campaign']['gftamount']; else echo "-1"; ?>" />
										<div class="pull-left">
											<a href="onboarding-2.php" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
										</div>


										<div class="pull-right">
											<button type="submit" id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
										</div>

									</div>
								</div>
							</div>
						</form>
					</div>
				
			</div>

			<div class="gftnow-mask" id="other-brand" style="overflow-y: auto;">
				<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 740px;width: 700px;top:10px;left:0;right:0;margin:0 auto;">
					<div id="other-brand-content" align="center"></div>
				</div>
			</div>			
			
			<div class="gftnow-mask" id="mask1">
				<div class="gftnow-modal">
					<div class="gftnow-modal-header text-center">
						<b>Offer Image</b> <br/><br/>
						<p>Move your image around to fit the orange box</p>
					</div>
					<div class="gftnow-modal-body">
						<img id="croppie-crop-icon" src="assets/img/mini-croppie-icon.png"/>
						<div id="croppie"></div>
						<br/>
						<center>
							<img id="loader" src="assets/img/ring-alt.svg" alt="Loading..." style="width:40px;height:40px;margin-left:-40px;display:none;">
							<button class="gftnow-btn gftnow-btn-default" id="croppie-done">DONE</button>
						</center>
						<br/>
					</div>
				</div>
			</div>

			<div class="gftnow-mask" id="mask2">
				<div class="gftnowLoader">
					<center>
						<img src="assets/img/ring-alt.svg" alt="Loading...">
						<!-- <div class="uil-rolling-css" style="transform:scale(0.6);"><div><div></div><div></div></div></div> -->
					</center> 
				</div>
			</div>
			
			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">

			var basic;
			var images = new Array();
			var start = 0;
			var init = 0;

			$(document).ready(function() {	
				
				checkvalidation();
				
				$("#onboarding-nav .inactive").hover(function(){
					$(this).find(".numberCircle").removeClass("active");
					$(this).find(".menu-item-parent").css("font", "GothamRndRegular");
				}, function(){
					$(this).find(".numberCircle").removeClass("active");
					$(this).find(".menu-item-parent").css("font", "GothamRndRegular");
				});

				$("input, textarea").focus(function(){
					checkvalidation();
				});

				$("input, textarea").keyup(function(){
					checkvalidation();
				});

				$(".gftnow-checkbox").click(function(){
					$("#type").val( $(this).attr('data-value'));
					
				});

				/*images.push({location:"assets/img/sampleupload1.png",type: "image"});
				images.push({location:"assets/img/sampleupload2.png", type: "image"});
				*/
				
				if($("#campaignname").val != "" && $("#description").val() != "" && $("#terms").val() != ""){
					$("#campaignname").trigger("click");
					$("#description").trigger("click");
					$("#terms").trigger("click");
				}

				$(".gftnow-checkbox[data-name='type']").click(function(){
					$(".gftnow-checkbox[data-name='type']").removeClass("active");
					$(this).addClass("active");
					checkvalidation();
					console.log($(this).attr("data-value"));
					console.log($("#token").val());
					if($(this).attr("data-value")=="gift-card"){
						$("#custom-upload").show().unbind("click").bind("click", function(){
							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType: "json",
								data: { func: "listGiftcards", token: $("#token").val() },
								success: function(data){
									
									console.log("listGiftcards");
									console.log(data);
									var otherbrand_content = "<h1 style='font-size:25px;'>Gift Card <a href='javascript:void(0);' id='close-other-brand' class='pull-right' style='margin-top:-18px;margin-right:-4px;color:#D3D2D2;'><i class='fa fa-close'></i></a></h1><div style='width:280px;'><p style='font-family:GothamRndRegular;font-size:15px;'>Please choose the type of gift card you would like to purchase</p><br/></div>";
									
									otherbrand_content += "<center><img id='my-own' style='cursor:pointer;' src='assets/img/gift-card-1.png'/></center>"+
															"<hr/><div class='active-main-panel' style='margin-top:0px;'>"+
																	"<div class='row active-list-title'>"+
																		"<div class='col-md-3' align='center'><strong>Preview</strong></div>"+
																		"<div class='col-md-3' align='center'><strong>Name</strong></div>"+
																		"<div class='col-md-3' align='center'><strong>Details</strong></div>"+
																		"<div class='col-md-3' align='center'><strong>Amount</strong></div>"+
																	"</div>"+
														   "</div>";

									otherbrand_content += "<div class='active-main-panel' id='gftnow-scrollbar' style='margin-top:0px;overflow-y:auto;max-height:300px;'>";

									for(var i =0;i<data.listGiftcards1.length;i++)
									{
											console.log(data.listGiftcards1[i]);
											var values_str = data.listGiftcards1[i].valid_values;
											var values = values_str.split(',');
											console.log(values);

											otherbrand_content+="<div class='row active-list other-brand' data-status='"+data.listGiftcards1[i].catalog_status+"' data-terms='"+data.listGiftcards1[i].terms_and_conditions+"' data-name='"+data.listGiftcards1[i].name+"' data-description='"+data.listGiftcards1[i].description+"' data-amount='"+data.listGiftcards1[i].valid_values+"' data-id='"+data.listGiftcards1[i].identifier+"' data-logo='"+data.listGiftcards1[i].logo_url+"'>"+
												"<div class='col-md-3'><img style='height:50px;width:85px;' src='"+data.listGiftcards1[i].logo_url+"'></div>"+
												"<div class='col-md-3'>"+data.listGiftcards1[i].name+"</div>"+
												"<div class='col-md-3'><a class='read-disclaimer' href='javascript:void(0);' data-toggle='collapse' data-target='#desc"+data.listGiftcards1[i].identifier+"'>Read Disclaimer</a></div>"+
												"<div class='col-md-3'>$ "+( (values[0]) ? values[0]+" - "+values[values.length-1] : values[values.length-1] )+"</div>"+
												"<div id='desc"+data.listGiftcards1[i].identifier+"' class='col-md-11 collapse' align='left' style='font-family:GothamRndLight;top:20px;bottom:20px;left:30px;'>"+data.listGiftcards1[i].description+"<br/><br/></div>"+
											"</div>";
									}

									otherbrand_content+= "</div><br/><span id='otherbrand-error-msg' style='font-family:GothamRndMedium;color:red;'></span><br/>"+
														 "<br/><label id='choose-brand' type='button' class='btn btn-success gftnow-btn-rounded'>&nbsp;&nbsp;CHOOSE&nbsp;&nbsp;</label>";


									$("#gft-card-div").fadeOut("fast");

									$("#other-brand-content").html(otherbrand_content);

									$("#other-brand").fadeIn("fast");
									$("#close-other-brand").bind("click", function(){
										$("#other-brand").fadeOut("fast");
									});

									var selected = 0;
									var ob = false;
									var yo = false;

									$(".other-brand").bind("click", function(){
										$(".other-brand").removeClass("active");
										$("#my-own").attr("src", "assets/img/gift-card-1.png");
										$(this).addClass("active");
										if(selected == 0)
											selected++;
										ob = true;
										yo = false;
										console.log(ob);
										console.log(yo);
									});

									$("#my-own").bind("click", function(){
										$(this).attr("src", "assets/img/gift-card-1.1.png");
										$(".other-brand").removeClass("active");
										if(selected == 0)
											selected++;
										yo = true;
										ob = false;
										console.log(ob);
										console.log(yo);										
									});

									$("#choose-brand").bind("click", function(){
										//$("#campaignname").trigger("click");
										$("#description").trigger("click");
										$("#terms").trigger("click");
										if(selected == 0){
											$("#otherbrand-error-msg").html("Please select a type of gift card!");
										} else {
											if(yo == true){
												$("#fileupload").trigger("click");
												$("#gftvendor").val( null );
												$("#gftamount").val( null );
												$("#custom-upload-content").html("<h4>Upload</h4><p>5mb max size<br/>543 x 303 recommended size</p>");
												$("#other-brand").fadeOut("fast");
											}
											if(ob == true){
												var logolocation = $(".other-brand.active").attr('data-logo');
												$("#gftvendor").val( $(".other-brand.active").attr('data-id') );
												$("#gftamount").val( $(".other-brand.active").attr('data-amount') );
												$("#gftstatus").val( $(".other-brand.active").attr('data-status') );
												$("#gftterms").val( $(".other-brand.active").attr('data-terms') );
												$("#gftname").val( $(".other-brand.active").attr('data-name') );
												$("#gftdescription").val( $(".other-brand.active").attr('data-description') );

												$("#description").val( $(".other-brand.active").attr('data-description').replace(/<\/?[^>]+(>|$)/g, "") );
												$("#terms").val( $(".other-brand.active").attr('data-terms').replace(/<\/?[^>]+(>|$)/g, "") );
												//images[2] = {location:"assets/upload/php/files/amazon-lg.png"};
												images[0] = {location:logolocation};
												$("#campaignimagetype").val("image");
												$("#campaignimage").val(images[0].location);
												start=0;
												
												renderScroll();
												$("#other-brand").fadeOut("fast");
											}
										}
									});
							
									
								},error: function(err){
									console.log(err.responseText);
								}
							});
						});
						$("#custom-upload-content").html("<h4>Choose Gift Card</h4>");
						$("#campaignimagetype").attr("value", "");
						$("#campaignimage").attr("value", "");
						/*$("#gftnow-alert-modal-content").html("403 Forbidden<br/>User is not player");
						$("#gftnow-alert-modal").modal("show");
						$("#mask1").fadeOut("fast");
						$(".gftnow-checkbox.active[data-value='gift-card']").removeClass("active");*/
					}
					if($(this).attr("data-value")=="free-product"){
						$("#custom-upload").show().unbind("click").bind("click", function(){
							$("#fileupload").trigger("click");
						});
						$("#custom-upload-content").html("<h4>Upload</h4><p>5mb max size<br/>543 x 303 recommended size</p>");
						$("#gftvendor").val( -1 );
						$("#gftamount").val( -1 );
						$("#campaignimagetype").attr("value", "");
						$("#campaignimage").attr("value", "");
					}
				});

				$(".gftnow-fader img").bind("click",function(){
					start++;
					
					renderScroll();
				});

				$(".drop-rounded-header").click(function(){
					 	$(this).parent().find(".drop-rounded-body").show();
				 });

				 $(".drop-rounded-body a").click(function(){
				 	var text = $(this).text();
				 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
				 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
					$(this).parent().parent().hide();
				 });

				fileupload();

			    basic = $('#croppie').croppie({
				    viewport: {
				        width: 400,
				        height: 225
				    },
				    customClass: "gftnow-croppie-container",
				    showZoomer: false
				});

				$("#croppie-crop-icon, #croppie-done").click(function(){

					jQuery(document).ajaxStart(function(){
						$("#mask2").hide();
						$("#loader").show();
					});
					jQuery(document).ajaxComplete(function(){
						$("#loader").hide();
					});

					//fileupload();
					if($("#croppie-done").hasClass("gftnow-btn-success")){
						basic.croppie('result',{ type: 'base64', size: {width: 800, height: 450} }).then(function(base64){
							var cropted = base64;

							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType: "json",
								data: { func: "base64ToPNG", base64: base64 },
								success: function(data){

									if(data.result=="OK")
									{
										images[0] = {location:"assets/upload/php/files/"+data.file,type:"image"};
										$("#campaignimagetype").val("image");
										$("#campaignimage").val(images[0].location);
										start=0;
										
										renderScroll();
										$("#mask1").hide();

										if(images.length>3){
											$(".gftnow-fader").css("display","initial");
											$("#loader").hide();
										}
									}
									
								},error: function(err){
									console.log(err.responseText);
									$("#gftnow-alert-modal-content").html("The requested resource does not allow request data width GET requests, or the amount of data provided in the request exceeds the capacity limit.");
									$("#gftnow-alert-modal").modal("show");
									$("#mask1").fadeOut("fast");
								}
							});
							

						});
					}
				});

				$("#form1").validate(
				 {
				 	
				 	submitHandler: function(form){
				 		var type = $(".gftnow-checkbox.active[data-name='type']").attr("data-value");
				 		var image = $(".uploadLogo.uploadLogo2.active").find("img").attr("src");
				 		var err = 0;
				 		var message = "";
				 		if(!type){
				 			message+="Please select Gift type<br/>";
				 			err++;
				 		}

				 		if(!image){
				 			message+="Please select or upload image<br/>";
				 			err++;
				 		}

				 		if(err==0){
				 			form.submit();
				 		}else{
				 			$("#gftnow-alert-modal-content").html(message);
							$("#gftnow-alert-modal").modal("show");
				 		}
				 		
				 	},
				 	rules: {
				 		terms: "required",
				 		description: "required",
				 		campaignname: "required"
				 	}
				 });

				renderScroll();


			});
		
		function checkvalidation()
		{
			console.log("validate");
			var err = 0;

			if( $("input[name='campaignname']").val() == "" )
				err++;

			if( $("textarea[name='description']").val() == "" )
				err++;

			if( $("textarea[name='terms']").val() == "" )
				err++;

		    var type = $(".gftnow-checkbox.active[data-name='type']").attr("data-value");
	 		var image = $(".uploadLogo.uploadLogo2.active").find("img").attr("src");
	 		if(!type)
	 			err++;

	 		if(!image)
	 			err++;
	 		
	 		console.log(type);
	 		console.log(image);

	 		if(err==0){
				$("#submit").removeClass("gftnow-btn-default");
				$("#submit").addClass("gftnow-btn-success");
			}else{
				$("#submit").addClass("gftnow-btn-default");
				$("#submit").removeClass("gftnow-btn-success");
			}
		}

		function renderScroll()
		{
			var html = "";

			if(init==0&&$("#campaignimage").val()!="")
			{
				$(".uploadLogo2").each(function(){
					$(this).removeClass("active");
				});

				images.push({location:$("#campaignimage").val(), type: $("#campaignimagetype").val() });

				
			}
			
			$('.gtfnow-uploader-slider ul li:not(:last)').remove();
			for(var i=images.length-1;i>=0;i--)
			{
				if(images[i].isvideo){

					html = '<li class="image-selection card-preview-small">'+
							'<div class="card-preview-small uploadLogo uploadLogo2 active">'+

								'<video class="video"><source src="'+images[i].location+'"></video>'+
							'</div>'+
						'</li>';
					}else{
						html = '<li class="image-selection card-preview-small">'+
							'<div class="card-preview-small uploadLogo uploadLogo2 active">'+

								'<img src="'+images[i].location+'"/>'+
							'</div>'+
						'</li>';
					}
				
			
				$(".gtfnow-uploader-slider ul").prepend(html);
				$('.video').click(function(){this.paused?this.play():this.pause();});
			}

			

			

			init++;
			checkvalidation();
			

			$(".image-selection").bind("click",function(){
				$(".uploadLogo2").removeClass("active");
				$(this).find(".uploadLogo2").addClass("active");

				if($(this).find(".uploadLogo2 img").length>0){
					$("#campaignimage").val($(this).find(".uploadLogo2 img").attr("src"));
					$("#campaignimagetype").val("image");
				}
					
				if($(this).find(".uploadLogo2 video").length>0){
					$("#campaignimage").val($(this).find(".uploadLogo2 video").attr("src"));
					$("#campaignimagetype").val("video");
				}
					
				
				checkvalidation();
			});



		}
		function fileupload(){
			var url = "assets/upload/php/index.php";
			var cropted = "";

			$('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        add: function(e, data) {
		                var uploadErrors = [];
		                var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
		                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
		                    uploadErrors.push('Not an accepted file type. Accepted file types are as follows: GIF, JPG, and PNG.');
		                }
		                if(data.originalFiles[0]['size'] > 2000000) {
		                    uploadErrors.push('Filesize is too big. Import at least 2 MB.');
		                }
		                if(uploadErrors.length > 0) {
							$("#gftnow-alert-modal-content").html(uploadErrors.join("\n"));
							$("#gftnow-alert-modal").modal("show");				                    
		                } else {
		                    data.submit();
		                }
		                console.log(data.originalFiles[0]);
                	},
		        done: function (e, data) {
		        	if(data.result.files.length > 0){

		        		if(data.result.files[0]['type']=="video/mp4"){
		        			var obj = {};
		        			obj.location = "assets/upload/php/files/"+data.result.files[0]['name'];
		        			$("#campaignimagetype").val("video");
		        			obj.isvideo = true;
		        			images[0] = obj;
		        			renderScroll();
		        		}else{
		        			basic.croppie('bind', {
							    url: data.result.files[0].url
							});

							$("#mask1").show();
		        		}
		            	

		            	$("#mask2").hide();
		            	$("#croppie-done").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
						//$("#gift-card-1").attr("src", "assets/img/gift-card-1.1.png");

		            }
		            
		        },
		        error: function(err){
		        	console.log("error");
		        	console.log(err.responseText);
		        },
		        progressall: function (e, data) {
		        console.log(data);
		          
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');

		}


		</script>


	</body>

</html>