
<div class="gftnow-mask" id="mask2">
  <div class="gftnowLoader">
    <center>
      <img src="assets/img/ring-alt.svg" alt="Loading...">
    </center> 
  </div>
</div>
    
<div id="gftnow-alert-modal" class="gftnow-modal modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header gftnow-font-medium" align="center"></div>
      <div class="modal-body">
        <p id="gftnow-alert-modal-content"></p>
      </div>
      <div id="modal-footer" class="modal-footer text-center" style="height:80px;">
        <button id="closeBtn" type="button" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- <script src="http://code.jquery.com/jquery-1.8.0.min.js"></script> -->
<script src="assets/js/jquery-3.2.0.min.js"></script>
<!-- <script src="assets/js/jquery-easing.1.2.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="assets/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="assets/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="assets/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="assets/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="assets/js/plugin/fastclick/fastclick.js"></script>

<!-- Demo purpose only -->
<script src="assets/js/demo.js"></script>

<script src="assets/MovingBoxes-master/js/jquery.movingboxes.js"></script>

<!-- MAIN APP JS FILE -->
<script src="assets/js/app.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->

<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="assets/js/plugin/flot/jquery.flot.cust.js"></script>
<script src="assets/js/plugin/flot/jquery.flot.resize.js"></script>
<script src="assets/js/plugin/flot/jquery.flot.tooltip.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="assets/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Morris -->
<script src="assets/js/plugin/morris/raphael.2.1.0.min.js"></script>
<script src="assets/js/plugin/morris/morris.min.js"></script>

<script src="assets/js/plugin/Chart.js-2.5.0/dist/Chart.min.js"></script>
<script src="assets/js/plugin/bootstrap-select-1.12.2/dist/js/bootstrap-select.min.js"></script>
<script src="assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="assets/js/jsbarcode.min.js"></script>
<script src="assets/Croppie-2.4.1/croppie.min.js"></script>
<script src="assets/jQuery-File-Upload-9.18.0/js/vendor/jquery.ui.widget.js"></script>
<script src="assets/jQuery-File-Upload-9.18.0/js/jquery.fileupload.js"></script>
<script src="assets/js/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.9.0/firebase.js"></script>
<script src="assets/js/jquery.maskedinput.js"></script>
<script src="https://js.braintreegateway.com/web/dropin/1.3.0/js/dropin.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=lmlmlmyxrv9fsuuvdjnj9q484q56x2a4eqm8ouemwibmin3p"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
  var config = {
     apiKey: "AIzaSyD57sJlGpTwEjkX7tjsUqbpvYgQOuLDmOk",
     authDomain: "gftnow.firebaseapp.com",
     databaseURL: "https://gftnow.firebaseio.com",
     projectId: "gftnow",
     storageBucket: "gftnow.appspot.com",
     messagingSenderId: "656938776598"
   };

  firebase.initializeApp(config);
</script>
<script type="text/javascript">
  jQuery.ajaxSetup({async:true});

  jQuery(document).ajaxStart(function(){
    $("#mask2").show();
  });

  jQuery(document).ajaxComplete(function(){	
    $("#mask2").hide();
  });
</script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBNzVQeL9Qr7XHwGGtDSbT_slP5FGcKGmc&libraries=places"></script>
<script src="assets/typeahead-addresspicker-master/typeahead.bundle.min.js"></script>
<script src="assets/typeahead-addresspicker-master/typeahead-addresspicker.min.js"></script>
<script src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/gftnow.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>