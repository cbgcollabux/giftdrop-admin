<?php
include_once "header.php";
include_once "mvc/model/user.php";
include_once "mvc/model/swagger.php";

/*$user = new user();
$requestTotal = $user->requestTotal();*/

$requestTotal;
$swagger = new _swagger();

$api_client = $swagger->init($_SESSION['token']);
$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
$body = new Swagger\Client\Model\Body17; 

$quantities = array();
foreach($_SESSION['drops']['drops'] as $drop){
	array_push($quantities, $drop->quantity);

}
$total_qty = array_sum($quantities);


$body['amount'] = $total_qty * $_SESSION['drops']['offer']; 

try {
    $requestTotal = $api->requestTotal($body);

} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->requestTotal: ', $e->getMessage(), PHP_EOL;
}

if ($_SESSION['timeout'] + 30 * 60 < time()) {
	session_destroy();
	header('Location: onboarding.php');
}



?>
	
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/template/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
					</div>
				</center>
			</div>
			
			<div class="row onboarding-container" style="height:800px;">
				<div class="col-md-3 onboarding-left-panel">
					<div class="onboarding-nav-container">
						<nav id="onboarding-nav">
							<ul>
								<li>
									<a href="#" title="Service"> <div class="numberCircle">1</div> <span class="menu-item-parent">Service</span></a>
								</li>
								<li>
									<a href="#" title="Campaign"> <div class="numberCircle">2</div> <span class="menu-item-parent">Campaign</span></a>
								</li>
								<?php
								if(isset($_SESSION['drops'])){
									echo '<li><a href="onboarding-5.php" title="Drops"> <div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a href="onboarding-6.php" title="Detail"> <div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								} else {
									echo '<li><a class="inactive"><div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a class="inactive"><div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								}
								?>
							</ul>
						</nav>
					
						<br/><br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<p><center>
									<h3>Business Information</h3>
									<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
								</center></p>
							</div>
							<div class="col-md-1"></div>
							
						</div>
					</div>	
				</div>

				<div class="col-md-9 onboarding-right-panel ">
					<div class="row">
						<div class="col-md-11">
							<center>
								<h2 class="gftnow-title">
									Payment
								</h2>
								Provide your payment information
							</center>

							<div class="onboarding" style="box-shadow:none;">
								<div class="onboarding-content">
									
									 <div class="step3">
										<div class="paypal-visa">
											<div class="row">
												<div class="col-sm-4 col-md-4">
													<h3>Number of Drops</h3>
													<p style="margin-left:60px;"><b><?php echo $total_qty; ?></b></p>
												</div>
												<div class="col-sm-4 col-md-4">
													<h3>Per Drop Fee</h3>
													<p style="margin-left:40px;"><b>$ <?php if(isset($_SESSION['drops']['offer']) ){ echo $_SESSION['drops']['offer'];}else { echo "0"; }?></b></p>
												</div>
												<div class="col-sm-4 col-md-4">
													<h3>Total</h3>
													<p style="margin-left:6px;"><b>$<?php
														if(isset($_SESSION['drops']['offer'])){
															/*$fees = $requestTotal['fees'];
															$total = $total_qty * $_SESSION['drops']['offer'] + $fees;*/

									
															$total = $requestTotal['amount'] + $requestTotal['fees'];
															echo $total;
															echo "<input type='hidden' name='amount' id='amount' value='".$total."'/>";
														}
													?></b></p>
												</div>
											</div>
										</div>
										
									</div> 

									<hr/>

									<div id="dropin-container"></div>
  									
									
								</div>
								
								<div class="pull-left">
									<a href="onboarding-6.php" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
								</div>


								<div class="pull-right">
										<!--<a href="onboarding-7.php" class="btn btn-default gftnow-btn gftnow-btn-success">CONFIRM &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></a> -->
										<button type="button" id="submit-button" class="btn btn-default gftnow-btn gftnow-btn-success">Submit Payment</button>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div style="display:none">
					<form id="paymensubmit" action="onboarding-7.php" method="post">
						<input type='text' name='BTtransaction' id='BTtransaction'/>
						<input type='text' name='BTnonce' id='BTnonce' />
					</form>
				</div>
			</div>
			
			<div class="gftnow-infobox-wrapper">  
			    <div id="gftnow-infobox" data-zip="<?php echo $_SESSION['company']['zip'];?>" data-state="<?php echo $_SESSION['company']['state'];?>" data-address="<?php echo $_SESSION['company']['address'];?>" data-city="<?php echo $_SESSION['company']['city'];?>">  
					<?php echo $_SESSION['company']['address'];?> 
					<?php echo $_SESSION['company']['city'];?> 
					<?php echo $_SESSION['company']['state'];?> 
					<?php echo $_SESSION['company']['zip'];?>
				</div>  
			</div>  

			
			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		<?php include_once "footer.php";?>
		
		<script type="text/javascript">
			var map;

			$(document).ready(function() {

				var button = document.querySelector('#submit-button');

				braintree.dropin.create({
			      authorization: 'sandbox_3p8dmtg9_3zh5phbpsnqh9br2',
			      container: '#dropin-container'/*,
			      paypal: {
			      	flow: 'vault',
			      	amount: '10.00',
				    currency: 'USD'
			      }*/
			    }, function (createErr, instance) {
			      button.addEventListener('click', function () {
			        instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
			          // Submit payload.nonce to your server
			          console.log(payload);
			          if(payload)
			          {
			          	jQuery.ajax({
				          	url: "mvc/controller/ajaxController.php",
				          	type: "post",
				          	dataType: "json",
				          	data: { func: "braintree", payload: payload, amount: $("#amount").val() },
				          	success: function(data){
				          		console.log(data);

				          		if(data.result=="ok"){
				          			$("#BTnonce").val(data.payload);
				          			$("#BTtransaction").val(data.braintreeTranID);

				          			$("#paymensubmit").submit();
				          		}else{
				          			$("#gftnow-alert-modal-content").text(data.braintree);
									$("#gftnow-alert-modal").modal("show");
				          		}

				          	},error: function(err){
				          		console.log(err.responseText);
				          	}
				          });
			          }
			          
			        });
			      });
			    });

			});

			

			
			


		</script>

		

		

		

	</body>

</html>