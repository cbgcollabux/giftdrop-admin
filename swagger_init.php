<?php ob_start();session_start();
	
	include_once "mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	
	$identifier;
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	
	try {
	    $findMyCompanyProfile = $api->findMyCompanyProfile();

	    $identifier = $findMyCompanyProfile['identifier'];
	} catch (Exception $e) {
	    echo 'Exception when calling BusinessServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
	}

	
	/*// change company profile error 500
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	$body = new Swagger\Client\Model\Body10;


	$account = array();
	$account['identifier'] = $_SESSION['user']->userInfo->identifier;
	$account['created_at'] = gmdate("Y-m-d\TH:i:s\Z");
	$account['notes'] = "test notes";
	$account['logoURL'] = "http://www.gftnow.com/template/assets/upload/php/files/36e2418c89b2a3b8e276.png";
	$account['state'] = "PH";
	$account['title'] = "test title";
	$account['about'] = "test about";
	$account['website'] = "www.facebook.com";

	$body['identifier'] = $identifier;
	$body['name'] = "chris";
	$body['created'] = gmdate("Y-m-d\TH:i:s\Z");
	$body['notes'] = "chris notes";
	$body['state'] = "chris state";
	$body['address'] = "chris address";
	$body['category'] = "a02a6059-aaf4-4cc1-9494-a31a1715fbbd";
	$body['account'] =$account;

	try {
	    $changeMyCompany = $api->changeMyCompany($body);
	} catch (Exception $e) {
	    echo $e->getMessage();
	}


	//listGiftCards only accepting player user

	$api = new Swagger\Client\Api\GftServiceApi($api_client);

	try {
	    $listGiftCards = $api->listGiftCards($body);

	} catch (Exception $e) {
	   echo $e->getMessage();
	}*/

	//changeAccountState OK
	/*$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\Body4;

	$body['account_identifier'] = $_SESSION['user']->userInfo->identifier;

	$body['state'] ='Enabled';
	$body['security_alert'] = "alert";
	
	try {
	    $changeAccountState = $api->changeAccountState($body);

	    echo "<pre>";
	    print_r($changeAccountState);
	    echo "</pre>";

	} catch (Exception $e) {
	   echo $e->getMessage();
	}*/

	//sendEmail
	$sendEmail;
	$sendEmailBody = new Swagger\Client\Model\Body17();
	$sendEmailBody['from']['address'] = "company@gftdrop.com";
	$sendEmailBody['from']['name'] = "Company";
	$sendEmailBody['to']['address'] = "admin@gftdrop.com";
	$sendEmailBody['to']['name'] = "GiftDrop Admin";
	$sendEmailBody['title'] = "title";
	$sendEmailBody['body'] = "body";
	$sendEmailBody['content_type'] = "contentType";

	try{
		$sendEmail = $api->sendEmail($sendEmailBody);
	} catch(Exception $e){
		 echo 'Exception when calling BusinessServiceApi->sendEmail: ', $e->getMessage(), PHP_EOL;
	}

?>