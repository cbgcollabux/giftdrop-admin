<?php include_once "header.php"; ?>

<?php
		
		if(!isset($_SESSION['start'])){
			session_destroy();
			header('Location: business.php?expired_onboarding=true');
		}
	
		if(isset($_POST['businesscategory'])){
			$_SESSION['company']['businesscategory'] = $_POST['businesscategory'];
			$_SESSION['timeout'] = time();
		}

		if(isset($_SESSION['timeout'])){
			if ($_SESSION['timeout'] + 30 * 60 < time()) {
				session_destroy();
				header('Location: business.php?expired_onboarding=true');
			}
		}
		
	?>


	<body style="background-color: #F4F5F8;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">HOW IT WORKS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
			</div>
			
			<div class="text-center section row">
				<div class="col-md-12">
					
					<center>
						We are so glad you are here! Our desire is to tell more people about how greate You are and to<br/>do that we would love to know a bit more about who you are and what you do.
					</center>
					
					<br>

					<h2 class="gftnow-title">
						Tell Us About Your Business
					</h2>
					<br/><br/>
				</div>

				<div class="col-md-12">
					<center>

						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-3 gftnow-nopadding">
								<div class="onboarding onboarding-left onboarding-1-left" style="height:900px;min-height:900px;margin-top:0px;background-color:#fff;">
									<div class="onboarding-content">

										<br/>
										<div class="wizard">
												<div class="numberCircle">1</div>
												<div class="numberCircle active">2</div>
												<div class="numberCircle">3</div>
											</div>
											
											<center>
												<div  class="onboarding-icon">
													<img src="assets/img/cart.png" style="width: 80px;height:auto;">
												</div>
											</center>
											
											<p>
												<h3>More Information</h3>
												<span class="gftnow-font-14">The more people know about your<br/>business, the more confidant</span>
											</p>
											<br>
									</div>
								</div>
							</div>
							<div class="col-md-5 gftnow-nopadding">
								<form method="post" action="onboarding-2-1.php" id="form1">
									<div class="onboarding onboarding-right" style="margin-top:0px;background: url(assets/img/left-shadow.png) left;background-repeat:repeat-y;height:900px;min-height:900px;background-color: #fff;">
										<br/><br/>
										<div class="onboarding-content row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<p id="firstname-placeholder" class="placeholder-effect"></p>
												<input class="form-control" type="text" id="firstname" name="firstname" placeholder="First name"  value="<?php if(isset($_SESSION['company']['firstname']))echo $_SESSION['company']['firstname']; ?>">
												<br/>
												<p id="lastname-placeholder" class="placeholder-effect"></p>
												<input class="form-control" type="text" id="lastname" name="lastname" placeholder="Last name" value="<?php if(isset($_SESSION['company']['lastname']))echo $_SESSION['company']['lastname']; ?>">
												<br/>
												<p class="gftnow-font-medium pull-left" style="color:#000;">Business address</p>
												<br/><br/>
												<p id="website-placeholder" class="placeholder-effect"></p>
												<input class="form-control" type="text" id="website" name="website" placeholder="Website" value="<?php if(isset($_SESSION['company']['website']))echo $_SESSION['company']['website']; ?>">
												<br/>
												<p id="phone-placeholder" class="placeholder-effect"></p>
												<input class="form-control" type="text" id="phone" name="phone" placeholder="Phone" value="<?php if(isset($_SESSION['company']['phone']))echo $_SESSION['company']['phone']; ?>">
												<br/>
												<p id="address-placeholder" class="placeholder-effect"></p>
												<input class="form-control" type="text" id="address" name="address" placeholder="Address" value="<?php if(isset($_SESSION['company']['address']))echo $_SESSION['company']['address']; ?>">
							
												<div class="row">
													<div class="col-md-5 col-sm-12 col-xs-12 txt-rows" style="padding-left:0px;">
														<br/>
														<p id="city-placeholder" class="placeholder-effect"></p>
														<input class="form-control" type="text" name="city" id="city" placeholder="City" value="<?php if(isset($_SESSION['company']['city']))echo $_SESSION['company']['city']; ?>">
													</div>
													<div class="col-md-4 col-sm-12 col-xs-12 txt-rows">
														<br/>
														<p id="zip-placeholder" class="placeholder-effect"></p>
														<input class="form-control" type="text" name="zip" id="zip" placeholder="Zip Code" value="<?php if(isset($_SESSION['company']['zip']))echo $_SESSION['company']['zip']; ?>">
													</div>
													<div class="col-md-3 col-sm-12 col-xs-12 txt-rows" style="padding-right:0px;">
														<br/>
														<p id="state-placeholder" class="placeholder-effect"></p>
														<input class="form-control" name="state" id="state" placeholder="State" value="<?php if(isset($_SESSION['company']['state']))echo $_SESSION['company']['state']; ?>">
														<img id="state-loader" class="gftnow-hide" src="assets/img/ring-alt.svg" alt="Loading..." style="width:40px;height:40px;">
														<em id="state-invalid" class="invalid" style="display:none;">State not found.</em>
														<!-- <datalist id="states"></datalist> -->
													</div>
												</div>

												<br/>
												<p id="about-placeholder" class="placeholder-effect"></p>
												<textarea name="about" id="about" class="form-control" style="height: 100px;" placeholder="About Your Business"><?php if(isset($_SESSION['company']['about']))echo $_SESSION['company']['about']; ?></textarea>
												
												<br/>
												<div class="row gftnow-nopadding">
													<div class="col-md-1"><div class="gftnow-form"><div class="gftnow-checkbox"></div></div></div>
													<div class="col-md-11">
														<p class="gftnow-font-light" style="text-align:left;">
															You have read and agree to <b>GiftDrop Policy</b>, which provides that PayPal is an online sevice
															and that you will receive all account notices and information electronically via your primary
															email address. You have also read and agree to the <b>User Agreement</b> and <b>Privacy Policy</b>.
														</p>
													</div>
												</div>

												<br/>
												<div class="row">
													<!-- <div class="pull-left">
														<a href="onboarding-1.php" style="width:185px;" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
													</div> -->
													<div class="col-md-12">
														<button type="submit" style="width:100%;" id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">AGREE AND CREATE ACCOUNT</button>
													</div>
												</div>
											</div>
											<div class="col-md-1"></div>
											
										</div>
									</div>
								</form>

							</div>
							<div class="col-md-1"></div>
						</div>

						
					</center>
				</div>
			</div>
			
			<br/><br/><br/><br/>

			<div class="text-center section section-default row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>



		</div>
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">
			$(document).ready(function() {

					$(".gftnow-checkbox").click(function(){
						if($(this).hasClass("active")){
							$(this).removeClass("active");
						} else {
							$(this).addClass("active");
						}
						checkvalidation();
					});

				    $("#zip").keydown(function (e) {
				        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				            (e.keyCode >= 35 && e.keyCode <= 40)) {
				                 return;
				        }
				        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				            e.preventDefault();
				        }
				    });

				    $("#zip").on("keyup", function(){
				    	jQuery(document).ajaxStart(function(){
				    		$("#mask2").hide();
				    		$("#state-invalid").hide();
							$("#state").hide();
							$("#state-loader").show();
						});

						jQuery(document).ajaxComplete(function(){
							$("#mask2").hide();
							$("#state-loader").hide();
							$("#state").show();
						});

						$.ajax({
							url: "https://zip.getziptastic.com/v2/US/" + $(this).val(),
							cache: false,
							dataType: "json",
							type: "GET",
							success: function(result, success) {
								console.log("success");
								$("#state").val(result.state);
								$("#state-invalid").hide();
							},
							error: function() {
								console.log("error");
								$("#state").val();
								$("#state-invalid").show();
							}
						});
				    });

				    /*$('#zip').on('keyup', function() {
				        console.log($(this).val());	

				        								//City
				        if($(this).val() >= 35801 && $(this).val() <= 35816){
				        	$("#state").attr("value", "Alabama");	//Huntsville
				            $("#state-invalid").hide();
				        }
				        else if($(this).val() >= 99501 && $(this).val() <= 99524){
				        	$("#state").attr("value", "Alaska");	//Anchorage
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 85001 && $(this).val() <= 85005){
				        	$("#state").attr("value", "Arizona");	//Phoenix
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 72201 && $(this).val() <= 72217){
				        	$("#state").attr("value", "Arkansas");	//Little Rock
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 94203 && $(this).val() <= 94209){
				        	$("#state").attr("value", "California");	//Sacramento
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 90001 && $(this).val() <= 90089){
				        	$("#state").attr("value", "California");	//Los Angeles
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 90209 && $(this).val() <= 90213){
				        	$("#state").attr("value", "California");	//Beverly Hills
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 80201 && $(this).val() <= 80239){
				        	$("#state").attr("value", "Colorado");	//Denver
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 06101 && $(this).val() <= 06112){
				        	$("#state").attr("value", "Conneticut");	//Hartford
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 19901 && $(this).val() <= 19905){
				        	$("#state").attr("value", "Delaware");	//Dover
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 20001 && $(this).val() <= 20020){
				        	$("#state").attr("value", "District of Columbia");	//Washington
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 32501 && $(this).val() <= 32509){
				        	$("#state").attr("value", "Florida");	//Pensacola
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 33124 && $(this).val() <= 33190){
				        	$("#state").attr("value", "Florida");	//Miami
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 32801 && $(this).val() <= 32837){
				        	$("#state").attr("value", "Florida");	//Orlando
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 30301 && $(this).val() <= 30381){
				        	$("#state").attr("value", "Georgia");	//Atlanta
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 96801 && $(this).val() <= 96830){
				        	$("#state").attr("value", "Honlulu");	//Honlulu
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 83254){
				        	$("#state").attr("value", "Idaho");	//Montpelier
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 60601 && $(this).val() <= 60641){
				        	$("#state").attr("value", "Illinois");	//Chicago
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 62701 && $(this).val() <= 62709){
				        	$("#state").attr("value", "Illinois");	//Springfield
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 46201 && $(this).val() <= 46209){
				        	$("#state").attr("value", "Indiana");	//Indianapolis
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 52801 && $(this).val() <= 52809){
				        	$("#state").attr("value", "Iowa");	//Davenport
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 50301 && $(this).val() <= 50323){
				        	$("#state").attr("value", "Iowa");	//Des Moines
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 67201 && $(this).val() <= 67221){
				        	$("#state").attr("value", "Kansas");	//Wichita
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 41701 && $(this).val() <= 41702){
				        	$("#state").attr("value", "Kentucky");	//Hazard
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 70112 && $(this).val() <= 70119){
				        	$("#state").attr("value", "Lousiana");	//New Orleans
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 04032 && $(this).val() <= 04034){
				        	$("#state").attr("value", "Maine");	//Freeport
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 21201 && $(this).val() <= 21237){
				        	$("#state").attr("value", "Maryland");	//Baltimore
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 02101 && $(this).val() <= 02137){
				        	$("#state").attr("value", "Massachusetts");	//Boston
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 49036){
				        	$("#state").attr("value", "Michigan");	//Coldwater
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 49734 && $(this).val() <= 49735){
				        	$("#state").attr("value", "Michigan");	//Gaylord
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 55801 && $(this).val() <= 55808){
				        	$("#state").attr("value", "Minnesota");	//Duluth
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 39530 && $(this).val() <= 39535){
				        	$("#state").attr("value", "Mississippi");	//Biloxi
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 63101 && $(this).val() <= 63141){
				        	$("#state").attr("value", "Missouri");	//St. Louis
				        	$("#state-invalid").hide();
						}
				        else if($(this).val() == 59004){
				        	$("#state").attr("value", "Montana");	//Laurel
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 68901 && $(this).val() <= 68902){
				        	$("#state").attr("value", "Nebraska");	//Hastings
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 89501 && $(this).val() <= 89513){
				        	$("#state").attr("value", "Nevada");	//Reno
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 03217){
				        	$("#state").attr("value", "New Hampshire");	//Ashland
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 07039){
				        	$("#state").attr("value", "New Jersey");	//Livingston
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 87500 && $(this).val() <= 87506){
				        	$("#state").attr("value", "New Mexico");	//Santa Fe
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 10001 && $(this).val() <= 10048){
				        	$("#state").attr("value", "New York");	//New York
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 27565){
				        	$("#state").attr("value", "North Carolina");	//Oxford
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 58282){
				        	$("#state").attr("value", "North Dakota");	//Walhalla
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 44101 && $(this).val() <= 44179){
				        	$("#state").attr("value", "Ohio");	//Cleveland
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 74101 && $(this).val() <= 74110){
				        	$("#state").attr("value", "Oklahoma");	//Tulsa
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 97201 && $(this).val() <= 97225){
				        	$("#state").attr("value", "Oregon");	//Portland
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 15201 && $(this).val() <= 15244){
				        	$("#state").attr("value", "Pennsylvania");	//Pittsburgh
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 02840 && $(this).val() <= 02841){
				        	$("#state").attr("value", "Rhode Island");	//Newport
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 29020){
				        	$("#state").attr("value", "South Carolina");	//Camden
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 57401 && $(this).val() <= 57402){
				        	$("#state").attr("value", "South Dakota");	//Aberdeen
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 37201 && $(this).val() <= 37222){
				        	$("#state").attr("value", "Tenneessee");	//Nashville
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 78701 && $(this).val() <= 78705){
				        	$("#state").attr("value", "Texas");	//Austin
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 84321 && $(this).val() <= 84323){
				        	$("#state").attr("value", "Utah");	//Logan
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 05751){
				        	$("#state").attr("value", "Vermont");	//Killington
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 24517){
				        	$("#state").attr("value", "Virginia");	//Altavista
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() >= 98004 && $(this).val() <= 98009){
				        	$("#state").attr("value", "Washington");	//Bellevue
				        	$("#state-invalid").hide();
				        }
				       	else if($(this).val() == 25813){
				       		$("#state").attr("value", "Virginia");	//Beaver
				        	$("#state-invalid").hide();
				       	}
				        else if($(this).val() >= 53201 && $(this).val() <= 53228){
				        	$("#state").attr("value", "Wisconsin");	//Milwaukee
				        	$("#state-invalid").hide();
				        }
				        else if($(this).val() == 82941){
				        	$("#state").attr("value", "Wyoming");	//Pinedale
				        	$("#state-invalid").hide();
				        	
				        }
				        else{
				        	$("#states").html("");
				        	$("#state-invalid").show();
						}
				    });*/

					checkvalidation();

					jQuery.validator.addMethod("website", function(value, element) {
					  return this.optional(element) || /^(https?:\/\/)?([\w\d\-_]+\.)+\/?/.test(value);
					}, "Please provide a valid website.");
					
					jQuery.validator.addMethod("phone", function(value, element) {
					  return this.optional(element) || /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(value);
					}, "Please provide a valid phone number.");

					jQuery.validator.addMethod("zipcode", function(value, element) {
				  		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
					}, "Please provide a valid zipcode.");

					$("input,textarea").keyup(function(){
						checkvalidation();
					});

					$("input,textarea").focus(function(){
						checkvalidation();
					});

					 $("#form1").validate(
					 {
					 	submitHandler: function(form){
					 		form.submit();
					 	},
					 	rules: {
					 		firstname: {
					 			required: true
					 		},
					 		lastname: {
					 			required: true
					 		},
					 		website: {
					 			required: true,
					 			website: true
					 		},
					 		phone: {
					 			required: true,
					 			phone: true
					 		},
					 		address: {
					 			required: true
					 		},
					 		city: {
					 			required: true
					 		},
					 		zip: {
					 			required: true,
					 			zipcode: true
					 		},
					 		state: {
					 			required: true
					 		}
					 	}
					 });
			});

			function checkvalidation()
			{
				var err = 0;

				$("input").each(function(){
					if ( $(this).hasClass("invalid") )
						err++;
				});

				$("textarea").each(function(){
					if ( $(this).hasClass("invalid") )
						err++;
				});

				if( $("input[name='firstname']").val() == "" )
					err++;

				if( $("input[name='lastname']").val() == "" )
					err++;

				if( $("input[name='website']").val() == "" )
					err++;

				if( $("input[name='phone']").val() == "" )
					err++;

				if( $("input[name='address']").val() == "" )
					err++;

				if( $("input[name='city']").val() == "" )
					err++;

				if( $("input[name='zip']").val() == "" )
					err++;

				if( $("input[name='state']").val() == "" )
					err++;

				if( !$(".gftnow-checkbox").hasClass("active") )
					err++;

				if(err==0){
					$("#submit").removeClass("gftnow-btn-default");
					$("#submit").addClass("gftnow-btn-success");
					$("#submit").prop("disabled",false);
					$("#submit").css({color: "#fff"});
				}else{
					$("#submit").addClass("gftnow-btn-default");
					$("#submit").removeClass("gftnow-btn-success");
					$("#submit").prop("disabled",true);
					$("#submit").css({color: "#000"});
				}
			}
		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>