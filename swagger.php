<?php ob_start();session_start();
	
	require_once('lib/php-client/SwaggerClient-php/vendor/autoload.php');

	$host= 'https://gftnow.appspot.com/_ah/api';
	$provider = 'firebase';

	$config = new Swagger\Client\Configuration();
	$config->setHost($host);
	$config->setSSLVerification(false);
	$config->addDefaultHeader('X-GFT-AUTH-TOKEN',$_SESSION['token']);
	$config->addDefaultHeader('X-GFT-AUTH-PROVIDER',$provider);
	//$config->addDefaultHeader('Content-type','application/json');
	$config->setDebug(false);
	
	$api_client = new Swagger\Client\ApiClient($config);
	
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	
	try {
	    $result = $api->listCompanyCategories();
	    echo "<pre>";
	    print_r($result);
	    echo "</pre>";
	} catch (Exception $e) {
	    echo 'Exception when calling LoginServiceApi->createBusiness: ', $e->getMessage(), PHP_EOL;
		echo "<pre>";
		print_r($api->getApiClient());
		echo "</pre>";

	}


	
?>