<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />
		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>
			.row{
				margin: 0px !important;
			}
			#header-img{
				background-image: url("assets/img/brinker.jpg");
				background-size: cover;
    			background-repeat: no-repeat;
				height: 400px;
				opacity: 0.6;
				filter: alpha(opacity=20);

				-webkit-filter: blur(3px);
				-moz-filter: blur(3px);
				-o-filter: blur(3px);
				-ms-filter: blur(3px);
				filter: blur(3px);
			}
			#header-content{
				top:0px;
				position:absolute;
				width:100%;
				height:100%;
			}
			
		</style>
	</head>
	<body style="background-color: rgb(244, 245, 248);padding:0px;margin:0;overflow: auto;height: 100%;">

		<div class="container-full">
			<div class="row">
				<center>
					<div class="col-md-12">
						<p class="gftnow-font-regular" style="font-size:12px;margin-top:5px;">Buy a $50 Brinker Restaurants Gift Card and get a $10 bonus card.</p>
						<img src="assets/img/logo.png" style="margin-top:10px;">
						<p class="gftnow-font-bold" style="margin-bottom:10px;">GiftDrop</p>
					</div>
					<br/>
					<div class="col-md-12 gftnow-nopadding">
						<div style="overflow:hidden;position:relative;"><div id="header-img"></div></div>
						<div id="header-content">
							<br/><br/>
							<h1 class="gftnow-font-regular" style="color:#FFFFFF;">Take Dad Out for Dinner</h1>
							<br/>
							<p class="gftnow-font-regular" style="color:#FFFFFF;">
								This Sunday, take Dad out for a great dinner to any of the four Brinker<br/>
								restaurants - Chili's, Maggino's, On the Border, and Macaroni Grill.
							</p>
							<br/>
							<p class="gftnow-font-regular" style="color:#FFFFFF;">
								Buy a $50 Brinker Restaurants Gift Card (or any of the other cards) and<br/>
								get an extra $10 card! Use promo code <b>DINE2017</b> at checkout.
							</p>
							<br/>
							<button class="btn btn-success gftnow-btn gftnow-btn-success" style="border-radius:5px;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Buy Now&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</button>
						</div>
						<!-- <div style="margin-top:15px;">
							<p style="font-size:12px;color:#D3D2D2;">GiftDrop Inc, PO Box 7510 Beaverton, OR</p>
							<img src="assets/img/social-medias.png"/>
						</div> -->
					</div>
				</center>
			</div>
		</div>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>
	</body>

</html>