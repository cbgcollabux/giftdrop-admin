<?php
	include_once "header.php";

		

		if ($_SESSION['timeout'] + 30 * 60 < time()) {
			session_destroy();
			header('Location: onboarding.php');
		}
		
		if(isset($_POST['campaignname'])){
			$_SESSION['campaign'] = array(
				"gftdescription"=>$_POST['gftdescription'],
				"gftname"=>$_POST['gftname'],
				"gftstatus"=>$_POST['gftstatus'],
				"gftterms"=>$_POST['gftterms'],
				"gftamount"=>$_POST['gftamount'],
				"type"=>$_POST['type'],
				"vendor"=>$_POST['gftvendor'],
				"name"=>$_POST['campaignname'],
				"description"=>$_POST['description'],"terms"=>$_POST['terms'],
				"image"=>$_POST['campaignimage']
			);
		}

		if($_SESSION['campaign']['vendor'] != -1 && $_SESSION['campaign']['vendor'] != ''){
			include_once "mvc/model/user.php";
			$user = new user();
			$listGiftCards = $user->listGiftCards();
		}
	
		/*new start*/
		if(isset($_SESSION['drops'])){
			echo "<input type='hidden' name='sessiondrops' id='sessiondrops' value='".json_encode($_SESSION['drops'])."'>";
		}

		/*new end*/
		function offers_content(){
			if(isset($_SESSION['campaign']['type'])){
				if($_SESSION['campaign']['type'] == "free-product"){
				echo '<div class="col-md-1"><a href="javascript:void(0);" id="offer-minus" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-default gftnow-btn-nobg" style="height:37px;width:37px;border-radius:37px;padding:7px;margin-left:-15px;'.( ($_SESSION['campaign']['vendor']==-1 && $_SESSION['campaign']['vendor']=='')?"display:none":"" ).'"><i class="fa fa-minus"></i></a></div>
						<div class="col-md-2">
							<div class="inner-addon left-addon">
								<span class="addon-dollar"><i class="fa fa-dollar"></i></span>
						    	<input type="text" name="offer" id="offer" value="'.( ( $_SESSION['campaign']['gftamount'] != -1) ? $_SESSION['campaign']['gftamount'] : 0 ).'" class="form-control" style="text-align:center;height:38px;margin-left:-33px;"/>
							</div>
						</div>
						<div class="col-md-1"><a href="javascript:void(0);" id="offer-plus" class="btn btn-default gftnow-btn-circle gftnow-btn-success" style="height:37px;width:37px;border-radius:37px;padding:7px;margin-left:-48px;'.( ($_SESSION['campaign']['vendor']==-1 && $_SESSION['campaign']['vendor']=='')?"display:none":"" ).'"><i class="fa fa-plus"></i></a></div>
						<div class="col-md-6"></div>';
				}
				if($_SESSION['campaign']['type'] == "gift-card"){
				echo '<div class="col-md-1"><a href="javascript:void(0);" id="offer-minus" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-default gftnow-btn-nobg" style="height:37px;width:37px;border-radius:37px;padding:7px;margin-left:-15px;'.( ($_SESSION['campaign']['vendor']==-1 && $_SESSION['campaign']['vendor']=='')?"display:none":"" ).'"><i class="fa fa-minus"></i></a></div>
						<div class="col-md-2">
							<div class="inner-addon left-addon">
								<span class="addon-dollar"><i class="fa fa-dollar"></i></span>
						    	<input type="text" name="offer" id="offer" value="0" class="form-control" style="text-align:center;height:38px;margin-left:-33px;"/>
							</div>
						</div>
						<div class="col-md-1"><a href="javascript:void(0);" id="offer-plus" class="btn btn-default gftnow-btn-circle gftnow-btn-success" style="height:37px;width:37px;border-radius:37px;padding:7px;margin-left:-48px;'.( ($_SESSION['campaign']['vendor']==-1 && $_SESSION['campaign']['vendor']=='')?"display:none":"" ).'"><i class="fa fa-plus"></i></a></div>
						<div class="col-md-6"></div>';
				}
			}
		}

		function barcodes_val(){
			if(isset($_SESSION['drops']['barcodes'])){
				foreach($_SESSION['drops']['barcodes'] as $barcode){
					echo $barcode.',';
				}
			}
		}

		/*echo"<pre>";
		print_r($_SESSION);
		echo"</pre>";*/

		echo "<input type='hidden' id='type' value='".(isset($_POST['type'])?$_POST['type']:$_SESSION['campaign']['type'])."' />";

		echo "<input type='hidden' id='gftvendor' value='".( isset($_POST['vendor'])?$_POST['vendor']:$_SESSION['campaign']['vendor'] )."'/>";
		echo "<input type='hidden' id='gftamount' value='".( isset($_POST['gftamount'])?$_POST['gftamount']:$_SESSION['campaign']['gftamount'] )."'/>";
	?>

	</head>
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/template/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
					</div>
				</center>
			</div>
			
			<div class="row onboarding-container" style="height: 1500px;">
				<div class="col-md-3 onboarding-left-panel">
					<div class="onboarding-nav-container">
						<nav id="onboarding-nav">
							<ul>
								<li>
									<a href="onboarding.php" title="Service"> <div class="numberCircle">1</div> <span class="menu-item-parent">Service</span></a>
								</li>
								<li>
									<a href="onboarding-4.php" title="Campaign"> <div class="numberCircle">2</div> <span class="menu-item-parent">Campaign</span></a>
								</li>
								<?php
								if(isset($_SESSION['drops'])){
									echo '<li><a href="onboarding-5.php" title="Drops"> <div class="numberCircle active">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a href="onboarding-6.php" title="Detail"> <div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								} else {
									echo '<li><a class="inactive"><div class="numberCircle active">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a class="inactive"><div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								}
								?>
							</ul>
						</nav>
					
						<br/><br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
										<p><center>
											<h3>Business Information</h3>
											<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
										</center></p>
							</div>
							<div class="col-md-1"></div>
							
						</div>
					</div>	
				</div>

				<div class="col-md-9 onboarding-right-panel ">
					<div class="row">
						<div class="col-md-11">
							<center>
								<h2 class="gftnow-title">
									Drops Details
								</h2>
								Specify details about the gift drops you want to promote
							</center>

							<form method="post" action="onboarding-6.php" id="form">

								<input type="hidden" name="key5" value="<?php echo uniqid(); ?>" />
								<input type="hidden" id="barcodes" name="barcodes" value="<?php barcodes_val(); ?>" />
								<input type="hidden" id="drops" name="drops" />

								<input type="hidden" id="barcodetype" name="barcodetype" value="<?php if(isset($_SESSION['drops']['barcodetype'])){ echo $_SESSION['drops']['barcodetype']; } ?>">

								<div class="onboarding" style="box-shadow:none;">
									<div class="onboarding-content">
										<div class="row">
											<div class="col-md-12">
												<p>Location (s)</p>
												<div class="row">
													<div class="col-md-12 gftnow-nopadding">
														<div id="map" class="google_maps"></div>
													</div>
												</div>
											</div>
										</div>
										<br/>

										<div class="row">
											<div class="col-md-12">
												<br/>
												<p class="placeholder-effect active">
													<?php
													if($_SESSION['campaign']['vendor'] == -1 || $_SESSION['campaign']['vendor'] == ''){
														echo "Offer item value";
													} else {
														$totalGiftCards = count($listGiftCards['result']->vendors);
														for($i = 0; $i<$totalGiftCards; $i++){
															if($_SESSION['campaign']['vendor'] == $listGiftCards['result']->vendors[$i]->identifier){
																echo $listGiftCards['result']->vendors[$i]->name.' giftcard value';
															}
														}
													}
													?>
												</p>
												<div class="row">
													<div class="col-md-12 gftnow-nopadding">
														<div class="row">
															<div class="col-md-2" style="width: 12%;">
																<div style="width: 72px;height: 40px;background: url( <?php echo $_SESSION['campaign']['image']; ?> );background-size: cover; background-position: center center;" />
																
																</div>
															</div>
															<?php offers_content(); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<br/>

										<div id="tmp-add-drop">
											
										</div>
										<div class="row gftnow-hide" id="row-counter-container">
											<div class="col-md-7">
												<p style="display:inline-block;width: 29px;" class="rowcounter"></p>
												<div id="add-new-btn2" class="gftnow-container text-center" style="display:inline-block;width: 40px;">
													<img src="assets/img/icon-plus.png" style="height: 26px;width:auto;">
												</div>
											</div>
											
										</div>
										<div class="row gftnow-display-none" id="tmp-with-panel">

											<!-- <div class="col-md-12">

												<div class="row">
													<div class="col-md-7 gftnow-nopadding">

														<p style="display:inline-block;width: 30px;" class="rowcounter">1</p>
														<input type="text" class="form-control" placeholder="Type address here" id="txt-address-clicked"/>
													</div>
											
													<div class="col-md-2 text-right">
														<p>Quantity</p>
													</div>

													<div id="quantityDiv" class="col-md-3">
														<i id="minus_qty" class="fa fa-chevron-left"></i>
														<input style="display:inline-block;width:50px;" type="text" id="quantity" class="form-control text-center quantity"/ value="1">
														<i id="add_qty" class="fa fa-chevron-right"></i>
													</div>
												</div>
											</div>
											<br/><br/> -->
										</div>

										<div id="generateBarcodes" style="left:0;right:0;margin:0 auto;" class="gftnow-panel gftnow-hide barcodes-div">
											<div class="pull-right"><a href="javascript:void(0);" onclick="$('#generateBarcodes').hide();"><i class="fa fa-times"></i></a></div>
											<br/>
											<center>
												<b>Bar Codes</b> <br/>
												Enter your generated barcodes manually
												<br/><br/>
												<label type="button" style="display:none;" id="generateBarcodesBtn" class="btn btn-success gftnow-btn-rounded">Generate</label>
												<div id="generatedBarcodes"></div>
											</center>
										</div>

										<div id="uploadBarcodes" class="gftnow-panel gftnow-hide barcodes-div">
											<div class="pull-right"><a href="javascript:void(0);" onclick="$('#uploadBarcodes').hide();"><i class="fa fa-times"></i></a></div>
											<br/>
											<center>
												<b>Bar Codes</b> <br/>
												Enter your generated barcodes manually
												<br/><br/>
												<label type="button" id="uploadBarcodesBtn" class="btn btn-success gftnow-btn-rounded">Upload from file</label>
												<div id="uploadedBarcodessample">
													<br/><br/>
													<div style='overflow-y:auto;height:240px;'>
														<table class="table" id="gftnow-barcodes-table">
															<tr>
																<td>1</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>
															<tr>
																<td>2</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>
															<tr>
																<td>3</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>
															<tr>
																<td>4</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>
															<tr>
																<td>5</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>
															<tr>
																<td>6</td>
																<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
																<td></td>
															</tr>															
														</table>
													</div>
													<br/><br/>
													<a href='javascript:void(0)' class='doneBtn btn gftnow-btn-default gftnow-btn-rounded'>Done</a>													
												</div>
												<div id="uploadedBarcodestable"></div>
											</center>
										</div>

										<div class="row">
											<div class="col-md-12">
												<br/>
												<p id="date1-placeholder" class="placeholder-effect">Date</p>
												<div class="row">
													<div class="col-md-2 gftnow-nopadding">
														
															<input id='date1' name='date1' type="text" class="form-control" placeholder="Date" value="<?php if(isset($_SESSION['drops']['date1']))echo $_SESSION['drops']['date1']; ?>"/>
														
													</div>
													<div class="col-md-1">
														<div class="gftnow-container text-center">
															<img src="assets/img/icon-calendar.png" style="height: 18px;width:auto;"/>
														</div>
													</div>
													<div class="col-md-2 gftnow-nopadding">
														<input id="time1" name="time1" type="text" class="form-control" placeholder="07 : 00 AM"/>
													</div>
												</div>
											</div>
										</div>
										<br/>
										<div class="row">
											<div class="col-md-12">
												<br/>
												<p id="redemption_details-placeholder" class="placeholder-effect"></p>
												<textarea class="form-control" name="redemption_details" id="redemption_details" style="height: 180px;" placeholder="Redemption Details"><?php if(isset($_SESSION['drops']['redemption_details']))echo $_SESSION['drops']['redemption_details']; ?></textarea>
											</div>
										</div>
										<br/>


										<div class="row" <?php if($_SESSION['campaign']['vendor'] != -1 && $_SESSION['campaign']['vendor'] != ''){ echo 'style="display:none"'; }?> >
											<div class="col-md-12">
												<br/>
												<p class="placeholder-effect active">Codes</p>
												<div class="gftnow-form">
													<div id="checkboxBarcodes" class="gftnow-checkbox <?php if(isset($_SESSION['drops']['barcodetype'])){ if($_SESSION['drops']['barcodetype'] == 'Generated'){ echo 'active'; } } ?>"></div>
													<label>Generate Barcodes</label>
												</div>
												<div class="gftnow-form">
													<div id="checkboxAddManually" class="gftnow-checkbox <?php if(isset($_SESSION['drops']['barcodetype'])){ if($_SESSION['drops']['barcodetype'] == 'Manually Added'){ echo 'active'; } } ?>"></div>
													<label>Add Manually</label>
													<div class="gftnow-hide">
														<input id="fileupload" type="file" name="files[]">
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="pull-left">
										<a href="onboarding-4.php" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
									</div>


									<div class="pull-right">
										<button id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
									</div>

								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			

			
			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">
			var map;
			var markers = new Array();
			var barcodes = new Array();

			/*$(".barcodes-div").css({
				'position': 'absolute',
				'z-index': '999',
				'min-width': '500px',
				'width': '500px'
			});*/
			$("#offer-minus").click(function(){

				var vendor = $("#gftvendor").val();
				
				if(vendor!=-1 && vendor!=''){
					var gftamount = $("#gftamount").val();
					var amounts = gftamount.split(',');
					var dataIndex = Number($("#offer").attr("data-index"));

					if(dataIndex>0){
						dataIndex--;
						$("#offer").val(amounts[dataIndex]);
						$("#offer").attr("data-index", dataIndex);

						if(dataIndex==0){
							$("#offer-minus").removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
							$("#offer-plus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
						}
					}
					
				}else
				{
					$(this).blur();
			        var qty = $("#offer");
			        var currentVal = parseInt(qty.val());
			        var newVal='';
			       	if(currentVal>0){
			       		newVal = currentVal-1;
			            qty.val(newVal);
			        }
			        if(currentVal<=0){
						newVal = currentVal;
			            qty.val(newVal);			        	
			        	$(this).removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
			        }
				}

			 	
		      
		    });
			$("#offer-plus").click(function(){

				var vendor = $("#gftvendor").val();
				
				if(vendor!=-1 && vendor!=''){
					var gftamount = $("#gftamount").val();
					var amounts = gftamount.split(',');
					var dataIndex = Number($("#offer").attr("data-index"));

					if(dataIndex<amounts.length-1){
						dataIndex++;
						$("#offer").val(amounts[dataIndex]);
						$("#offer").attr("data-index", dataIndex);

						if(dataIndex==amounts.length-1){
							$("#offer-plus").removeClass("gftnow-btn-success").addClass("gftnow-btn-default");
							$("#offer-minus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
						}
					}
					
				}else{
					$(this).blur();
			 		var qty = $("#offer");
			 		if($("#offer").val() == ""){
			        	var currentVal = 0;
			 		}else {
			 			var currentVal = parseInt(qty.val());
			 		}
			        var newVal = currentVal+1;
			        qty.val(newVal);
			        if(currentVal>=0)
			        	$("#offer-minus").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
				}

			 	
		    });
			if($("#type").val() == "free-product"){
			    $("#offer").keydown(function (e) {
			        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
			            (e.keyCode >= 35 && e.keyCode <= 40)) {
			                 return;
			        }
			        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			            e.preventDefault();
			        }
			    });
			}
			if($("#type").val() == "gift-card"){
				$("#offer").attr("readonly", "readonly");
			}

			function generateQuickGuid() {
			    return Math.random().toString(36).substring(2, 15) +
			        Math.random().toString(36).substring(2, 15);
			}

			function executeDropEvent(id)
			{
					var qty = $("#quantity[data-id='"+id+"']").val();
					var address = $("#text-"+id).val();
					
					if(address==""){
						$("#gftnow-alert-modal-content").html("Please enter address");
						$("#gftnow-alert-modal").modal("show");
					}else{

						var rowcounter = $(".rowcounter").length;

						var row = '<div class="row"><div class="col-md-7 gftnow-nopadding txt-address-container"><p style="display:inline-block;width: 30px;" class="rowcounter">2</p><input class="drops" type="hidden" id="text-'+id+'" value="'+address+'"><input type="hidden" name="lat-'+id+'" id="lat-'+id+'"><input type="hidden" name="lng-'+id+'" id="lng-'+id+'"><input type="hidden" id="qty-'+id+'"><input class="form-control txt-address-clicked" data-id="'+id+'" disabled style="display:inline;width: 85%;" placeholder="Type address here" id="txt-address-clicked-'+id+'" type="text" value="'+address+'"></div><div class="col-md-1 text-left"><p style="padding-top: 5px;">Quantity</p></div><div id="quantityDiv" class="col-md-2">&nbsp;&nbsp;&nbsp;<input style="display:inline-block;width:50px;" id="quantity" data-id="'+id+'" class="form-control text-center quantity" disabled value="'+qty+'" type="text">&nbsp;&nbsp;</div><div class="col-md-2 text-right"> <i style="cursor:pointer;font-size: 28px; color: #E3B338;" class="fa fa-pencil edit-row" onclick="editRow(this)" data-id="'+id+'"></i> <i style="cursor:pointer;font-size: 28px; color: #E3B338;" class="fa fa-trash-o remove-row" data-id="'+id+'"></i></div><br><br></div>';

						$(".barcodes-div").remove();

						row += "<div class='row' style='margin-top:20px;'><div class='col-md-8'>"+
								"<div id='generateBarcodes' style='left:0;right:0;margin:0 auto;' class='gftnow-panel gftnow-hide barcodes-div'>"+
									"<div class='pull-right'><a href='javascript:void(0);' id='close-gB'><i class='fa fa-times'></i></a></div>"+
									"<br/>"+
									"<center>"+
										"<b>Bar Codes</b> <br/>"+
										"Enter your generated barcodes manually"+
										"<br/><br/>"+
										"<label type='button' style='display:none;' id='generateBarcodesBtn' class='btn btn-success gftnow-btn-rounded'>Generate</label>"+
										"<div id='generatedBarcodes'></div>"+
									"</center>"+
								"</div>"+

								"<div id='uploadBarcodes' class='gftnow-panel gftnow-hide barcodes-div'>"+
									"<div class='pull-right'><a href='javascript:void(0);' id='close-uB'><i class='fa fa-times'></i></a></div>"+
									"<br/>"+
									"<center>"+
										"<b>Bar Codes</b> <br/>"+
										"Enter your generated barcodes manually"+
										"<br/><br/>"+
										"<label type='button' id='uploadBarcodesBtn' class='btn btn-success gftnow-btn-rounded'>Upload from file</label>"+
										"<div id='uploadedBarcodessample'>"+
											"<br/><br/>"+
											"<div style='overflow-y:auto;height:240px;'>"+
												"<table class='table' id='gftnow-barcodes-table'>"+
													"<tr>"+
														"<td>1</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"+
													"<tr>"+
														"<td>2</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"+
													"<tr>"+
														"<td>3</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"+
													"<tr>"+
														"<td>4</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"+
													"<tr>"+
														"<td>5</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"+
													"<tr>"+
														"<td>6</td>"+
														"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"+
														"<td></td>"+
													"</tr>"	+														
												"</table>"+
											"</div>"+
											"<br/><br/>"+
											"<a href='javascript:void(0)' class='doneBtn btn btn-default gftnow-btn gftnow-btn-default'>Done</a>"+											
										"</div>"+
										"<div id='uploadedBarcodestable'></div>"+
									"</center>"+
								"</div></div></div>";

						$(".add-drop[data-id='"+id+"']").parent().parent().html(row);


						var addressPicker = new AddressPicker();

	        			$("#txt-address-clicked-"+id).typeahead(null, {
						  displayKey: 'description',
						  source: addressPicker.ttAdapter()
						});

	        			addressPicker.bindDefaultTypeaheadEvent( $("#txt-address-clicked-"+id) );

						$(addressPicker).on('addresspicker:selected', function (event, result) {
						  	$('#text-'+id).val(result.placeResult.formatted_address);
						});

						$("#txt-address-clicked-"+id).change(function(){
							$('#text-'+id).val($(this).val());
						});

						$("#row-counter-container").fadeIn("slow");

						$(".rowcounter").each(function(i,f){
							$(this).text(i+1);
						});

						$(".remove-row").unbind("click").bind("click", function(){
							$(this).parent().parent().parent().parent().parent().remove();
							
							for(var i =0;i<markers.length;i++){
								if(markers[i].get("id")==$(this).attr("data-id")){
									markers[i].setMap(null);
									markers.splice(i,1);
								}
							}

							$(".rowcounter").each(function(i,f){
								$(this).text(i+1);
							});

							checkvalidation();
						});


						$(".minus_qty").unbind("click").bind("click",function(){
					        var qty = $(this).parent().find("#quantity");
					        var currentVal = parseInt(qty.val());
					       	if(currentVal>1)
					            qty.val(currentVal - 1);
					      
					    });

						$(".add_qty").unbind("click").bind("click",function(){
						 	var qty = $(this).parent().find("#quantity");
						 	var currentVal = parseInt(qty.val());
					        qty.val(currentVal+1);
					        $(addb).attr("data-qty", currentVal+1);
					    });


						$("#close-gB").unbind("click").bind("click", function(){
							$("#generateBarcodes").hide();
						});
						$("#close-uB").unbind("click").bind("click", function(){
							$("#uploadBarcodes").hide();
						});

						console.log("address "+address);
						console.log("id "+id);
						codeAddress(address, id);

						if($("#type").val()=="gift-card"){
							generateBarcodes();
						}

					}

					setTimeout(function(){  checkvalidation(); }, 1000);
			}

			function addDropEvent(elem)
			{
				$(elem).click(function(){
					//var address = $(this).parent().parent().find(".txt-address-container").find("input[id*=text-]").val();
					var id = $(elem).attr('data-id');
					executeDropEvent(id);
				});
			}

			function editRow(elem)
			{
				var id = $(elem).attr("data-id");

				var qty = $("#quantity[data-id='"+id+"']").val();
				$("#quantity[data-id='"+id+"']").parent().html('<i class="fa fa-chevron-left minus_qty" data-id="'+id+'"></i>&nbsp;<input style="display:inline-block;width:50px;" id="quantity" data-id="'+id+'" class="form-control text-center quantity" value="'+qty+'" type="text">&nbsp;<i class="fa fa-chevron-right add_qty" data-id="'+id+'"></i>')
				
				$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
					var qty = $(this).parent().find("#quantity");
					var eid = $(this).attr("data-id");
					var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
					$(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

					checkvalidation();
				         
				});

				$(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
					var eid = $(this).attr("data-id");
					var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

					if(currentVal>1)
					$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

					checkvalidation();
				});

				$("#txt-address-clicked-"+id).prop("disabled",false);
				
				$(elem).parent().html('<button class="btn btn-success btn-block gftnow-hide edit-drop" onclick="editDrop(this)" data-qty="1" data-id="'+id+'" style="display: inline-block;">Save</button>');
				
				checkvalidation();
				
			}

			function editDrop(elem){
				var id = $(elem).attr("data-id");
				$("#txt-address-clicked[data-id='"+id+"']").prop("disabled",true);
				$("#quantity[data-id='"+id+"']").parent().html('&nbsp;&nbsp;&nbsp;<input style="display:inline-block;width:50px;" id="quantity" disabled data-id="'+id+'" class="form-control text-center quantity" value="'+$("#quantity[data-id='"+id+"']").val()+'" type="text">');
				$(elem).parent().html('<i style="cursor:pointer;font-size: 28px; color: #E3B338;" class="fa fa-pencil edit-row" onclick="editRow(this)" data-id="'+id+'"></i> <i style="cursor:pointer;font-size: 28px; color: #E3B338;" class="fa fa-trash-o remove-row" data-id="'+id+'"></i>');

				$(".add_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
					var qty = $(this).parent().find("#quantity");
					var eid = $(this).attr("data-id");
					var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );
					$(".quantity[data-id=\'"+eid+"\']").val( currentVal+1 );

					checkvalidation();
				         
				});

				$(".minus_qty[data-id=\'"+id+"\']").unbind("click").bind("click",function(){
					var eid = $(this).attr("data-id");
					var currentVal = parseInt( $(".quantity[data-id=\'"+eid+"\']").val() );

					if(currentVal>1)
					$(".quantity[data-id=\'"+eid+"\']").val(currentVal-1)

					checkvalidation();
				});

			    var qty = $("#quantity[data-id='"+id+"']").val();
				var address = $("#text-"+id).val();

				for(var i =0;i<markers.length;i++){
					if(markers[i].get("id")==id){
						markers[i].setMap(null);
						markers.splice(i,1);
					}
				}

				codeAddress(address, $(elem).attr("data-id"));

				setTimeout(function(){  checkvalidation(); }, 1000);

				if($("#type").val() == "gift-card"){
					generateBarcodes();
				}
			}
			function checkvalidation(){
				
				var x = 0;

				if($("input[name='offer']").val() == "0")
					x++;
				if($("input[name='offer']").val() == "")
					x++;
				if($("input[name='date1']").val() == "")
					x++;

				if($("input[name='time1']").val() == "")
					x++;

				if($("textarea[name='redemption_details']").val() == "")
		 			x++;

		 		if(markers.length==0)
		 			x++;


		 		if($("#sessiondrops").length>0){

		 		}else{
		 			if(barcodes.length==0)
		 				x++;
		 		}
		 		
		 		/*if( $("#gftvendor").val() == -1 ){
		 			
		 			if(barcodes.length==0)
		 				x++;
		 		}*/

		 	

		 		if($(".edit-drop").length > 0)
		 			x++;
		 		
		 		if(x==0){
					$("#submit").removeClass("gftnow-btn-default");
					$("#submit").addClass("gftnow-btn-success");
				}else{
					$("#submit").addClass("gftnow-btn-default");
					$("#submit").removeClass("gftnow-btn-success");
				}
			}

			checkvalidation();

			$(document).ready(function() {

				google.maps.event.addDomListener(window, 'load', init);

				var vendor = $("#gftvendor").val();
				
				if(vendor!=-1 && vendor!=''){
					var gftamount = $("#gftamount").val();
					var amounts = gftamount.split(',');
					$("#offer").val(amounts[0]);
					$("#offer").attr("data-index", 0);
					$("#offer").attr("data-max-index", amounts.length);
				}

				checkvalidation();

				$("input, textarea").keyup(function(){
					checkvalidation();
				});



				$("#form").validate({
					rules: {
						offer: "required",
						date1: "required",
						time1: "required",
						redemption_details: "required"
					},errorPlacement: function(){
						return false;
					},submitHandler: function(form){
						var drops = new Array();


						$(".drops").each(function()
						{
							if($(this).val()!=""){
								var drop = {};
								drop.address = $(this).val();
								drop.quantity = $(this).parent().parent().find("#quantity").val();
								drop.lat = $(this).attr("lat");
								drop.lng = $(this).attr("lng");
								drops.push(drop);
							}
								
						});

						console.log("drops");
						console.log(drops);

						console.log("barcodes");
						console.log(barcodes);
						

						var err=0;
						var errMessages="";
						if(barcodes.length==0){

							console.log("gftvendor");
							console.log($("#gftvendor").val());
							if($("#gftvendor").val()==-1){
								/*err++;
								errMessages+="You forgot to generate barcodes<br/>";*/

								console.log("debugging barcodes 1");

								var quantity = 0;
				
								 $(".quantity").each(function(){
									quantity += Number($(this).val());
								  });

								barcodes = new Array();
								
								for(var i =0; i<quantity; i++){
									var barcode = generateRandom(5)+"-"+generateRandom(5)+"-"+generateRandom(7);
									barcodes.push(barcode);
								}

								var totalb=0;
								$(".quantity").each(function(){
									totalb = totalb+Number($(this).val())						
								});

							}else{

								console.log("debugging barcodes 3");

								var totalb=0;
								$(".quantity").each(function(){
									totalb = totalb+Number($(this).val())						
								});

							}
							
						}else{
							console.log("debugging barcodes 2");
						}


						if($("#offer").val() == "0" || $("#offer").val() == ""){
							err++;
							errMessages+="Offer item value should be more than 0.<br/>";
						}
						if(drops.length==0){
							err++;
							errMessages+="At least create 1 drop.<br/>";
						}
						if($("#type").val() == "free-product"){
							if($("#checkboxBarcodes").hasClass("active") || $("#checkboxAddManually").hasClass("active")){
								
							}else{
								err++;
								errMessages+="Generate Barcodes or Add it Manually.<br/>";
							}
						}

						if(err!=0){
							$("#gftnow-alert-modal-content").html(errMessages);
							$("#gftnow-alert-modal").modal("show");
						}else{


							console.log(barcodes.toString());
							$("#drops").val( JSON.stringify(drops));
							$("#barcodes").val( barcodes.toString());

							if($("#submit").hasClass("gftnow-btn-default")){

							}else{
								form.submit();
							}
							
						}
					}
				});
				

				$("#time1").timepicker('setTime', new Date());

				/*new start*/
				$("#add-new-btn2").click(function()
				{
					addNewDrop();
				});
				/*new end*/
				
				

				

				$(".txt-address").focus(function(){
					$(this).parent().parent().find(".add-drop").fadeIn("slow",function(){
						
					});
				});

				$("#generateBarcodes").hide();

				$("#checkboxBarcodes").click(function(){
					$("#barcodetype").attr("value", "Generated");
					$("#uploadBarcodes").hide();
					$(this).addClass("active");
					$("#checkboxAddManually").removeClass("active");

					if(markers.length==0){
						$("#gftnow-alert-modal-content").html("At least create 1 drop to generate barcodes.");
						$("#gftnow-alert-modal").modal("show");

						$("#checkboxAddManually").removeClass("active");
						$("#checkboxBarcodes").removeClass("active");
					}else{
						generateBarcodes();
					}
				});

				$("#checkboxAddManually").click(function(){
					$("#barcodetype").attr("value", "Manually Added");
					$("#generateBarcodes").hide()
					$(this).addClass("active");
					$("#checkboxBarcodes").removeClass("active");

					if(markers.length==0){
						$("#gftnow-alert-modal-content").html("At least create 1 drop to generate barcodes.");
						$("#gftnow-alert-modal").modal("show");

						$("#checkboxAddManually").removeClass("active");
						$("#checkboxBarcodes").removeClass("active");
					}else{
						uploadBarCodes();
					}
				});

				$("#generateBarcodesBtn").click(function(){
				    generateBarcodes();
				});

				var date = new Date();
				date.setDate(date.getDate());

				$('#date1').datepicker({
					 startDate: date
				});

				$('#date1').on('changeDate', function(ev){
				    $(this).datepicker('hide');
				});

				
				
				
				$(".drop-rounded-header").click(function(){
					 	$(this).parent().find(".drop-rounded-body").show();
				 });

				 $(".drop-rounded-body a").click(function(){
				 	var text = $(this).text();
				 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
				 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
					$(this).parent().parent().hide();
				 });

			});
			
			function addNewDrop(drop){

				console.log("drops");
				console.log( drop );

				var id = generateQuickGuid();
				var rowcounter = $(".rowcounter").length;
				var row = '<div class="row">'+
								'<div class="col-md-12">'+
									'<div class="row">'+
										'<div class="col-md-7 gftnow-nopadding txt-address-container">'+
											'<p style="display:inline-block;width: 30px;" class="rowcounter">'+rowcounter+'</p>'+
											'<input type="hidden" class="drops" id="text-'+id+'" name="text-'+id+'" value="'+( (drop)?drop.address:"" )+'"/><input type="hidden" class="lat" id="lat-'+id+'" value="'+( ( drop )? drop.lat:'' )+'"/><input class="lng" type="hidden" id="lng-'+id+'" value="'+( (drop ) ? drop.lng:'' )+'"/><input style="display:inline;width: 85%;" class="form-control txt-address" placeholder="Type address here" id="'+id+'" type="text" value="'+( ( drop )? drop.address:'' )+'"/>'+
										'</div>'+
										'<div class="col-md-1 text-left">'+
											'<p class="qtyTxt" style="padding-top: 5px;">Quantity</p>'+
										'</div>'+
										'<div class="col-md-2">'+
											'<div id="quantityDiv" class="qtyDiv">'+
												'<i class="fa fa-chevron-left minus_qty" data-id="'+id+'"></i>&nbsp;<input style="display:inline-block;width:50px;" id="quantity" data-id="'+id+'" class="form-control text-center quantity" value="'+( ( drop )?drop.quantity:"1" ) +'" type="text">&nbsp;<i class="fa fa-chevron-right add_qty" data-id="'+id+'"></i>'+
											'</div>'+
										'</div>'+
										'<div class="col-md-2">'+
											'<button type="button" class="btn btn-success btn-block add-drop" data-id="'+id+'">Drop on Map</button>'+
										'</div>'+
									'</div>'+
								'</div>'+
									'<div class="col-md-3"></div>'+
									'<div class="col-md-1"></div>'+
								'<br><br>'+
							'</div>';

					$("#tmp-add-drop").append(row);

					if(drop){
						executeDropEvent(id);
					}

					var addressPicker = new AddressPicker();

        			$("#"+id).typeahead(null, {
					  displayKey: 'description',
					  source: addressPicker.ttAdapter()
					});

        			addressPicker.bindDefaultTypeaheadEvent($('#'+id));

					$(addressPicker).on('addresspicker:selected', function (event, result) {
					  	$('#text-'+id).val(result.placeResult.formatted_address);
					});

					$("#"+id).change(function(){
						$('#text-'+id).val($(this).val());
					});

					

					$(".minus_qty").unbind("click").bind("click",function(){
				        var qty = $(this).parent().parent().parent().parent().find("#quantity");
				        var currentVal = parseInt(qty.val());
				       	if(currentVal>1)
							qty.attr("value", currentVal - 1);
				      
				    });

					$(".add_qty").unbind("click").bind("click",function(){
					 	var qty = $(this).parent().parent().parent().parent().find("#quantity");
				        var currentVal = parseInt(qty.val());
				        qty.attr("value", currentVal+1);
				    });

					$(".txt-address").focus(function(){
						var qty = $(this).parent().parent().parent().parent().find("#quantity");
						$(this).parent().parent().parent().find(".qtyTxt").fadeIn("slow");
						$(this).parent().parent().parent().find(".qtyDiv").fadeIn("slow");
						$(this).parent().parent().parent().find(".add-drop").fadeIn("slow", function(){
							$(this).attr("data-qty", parseInt(qty.val()));
							$(this).attr("data-id", id);
						});
					});
					
					addDropEvent( $(".add-drop[data-id='"+id+"']") );

					$(".rowcounter").each(function(i,f){
						$(this).text(i+1);
					});

					$("#row-counter-container").fadeOut("fast");

					$("#text-"+id).keyup(function () {
					   if ($(this).val()) {
					      $(".add-drop").show();
					   }
					   else {
					      $(".add-drop").hide();
					   }
					});

					checkvalidation();

			}

			function fileupload(){
				var url = "assets/upload/php/index.php";
				var cropted = "";

			    $('#fileupload').fileupload({
			        url: url,
			        dataType: 'json',
			        add: function(e,data){
			        	var uploadErrors = [];
			        	var acceptFileTypes = /(\.|\/)(csv|plain)$/i;

			        	
			        	if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
				            uploadErrors.push('Not an accepted file type. Please upload csv or text file only');
				        }
				        if(uploadErrors.length > 0) {
				            $("#gftnow-alert-modal-content").html(uploadErrors.join("\n"));
							$("#gftnow-alert-modal").modal("show");
				        } else {
				            data.submit();
				        }
			        },
			        done: function (e, data) {

			      
			        	if(data.result.files.length > 0){

			        		jQuery.ajax({
			        			url: "mvc/controller/ajaxController.php",
			        			type: "post",
			        			dataType: "json",
			        			data: {func:"readBarcodes",url: data.result.files[0].name,type: data.result.files[0].type},
			        			success: function(data){
			        				
			        				if(data.result=="OK"){

			        					var barcodestring = "";
			        					barcodes = new Array();
										for(var i =0; i<data.barcodes.length; i++){
											barcodes.push(data.barcodes[i]);
											barcodestring+="<tr><td>"+(i+1)+"</td><td>"+data.barcodes[i]+"</td><td><img src='assets/img/mini-check.png'></td></tr>";
										}
										
										$("#uploadedBarcodessample").hide();
										$("#uploadedBarcodestable").html("<br/><br/><div id='gftnow-scrollbar' style='overflow-y:scroll;height:auto;max-height:240px;'><table class='table' id='gftnow-barcodes-table'>"+barcodestring+"</table></div><br/><br/><a href='javascript:void(0)' class='doneBtn btn btn-success gftnow-btn-rounded'>Done</a>");		
										
										$("#uploadBarcodes .doneBtn").unbind("click").bind("click",function(e){
											
											$("#uploadBarcodes").hide();
											checkvalidation();
											
										});
			        				}
			        			},error: function(err){
			        				
			        				$("#gftnow-alert-modal-content").html(err.responseText);
									$("#gftnow-alert-modal").modal("show");
			        			}
			        		});

			            	$("#mask2").hide();
			            	$("#mask1").show();
			            }
			            
			        },
			        error: function(err){
			        	console.log("error");
			        	console.log(err.responseText);
			        },
			        progressall: function (e, data) {
			        	console.log("uplaoding");
			          
			        }
			    }).prop('disabled', !$.support.fileInput)
			        .parent().addClass($.support.fileInput ? undefined : 'disabled');

				checkvalidation();	     

			}
			function uploadBarCodes(){
				

				//$("#generatedBarcodes").hide();
				$("#uploadBarcodes").show();
				fileupload();
				$("#uploadBarcodesBtn").unbind("click").bind("click",function(){
					$("#fileupload").click();
				});

				checkvalidation();

			}
			function generateBarcodes(){
				var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				var quantity = 0;
				
				 $(".quantity").each(function(){
					quantity += Number($(this).val());
				  });

				barcodes = new Array();
				var barcodestring = "";
				for(var i =0; i<quantity; i++){
					var barcode = generateRandom(5)+"-"+generateRandom(5)+"-"+generateRandom(7);
					barcodes.push(barcode);
					if($("#type").val()=="free-product"){
						barcodestring+="<tr><td>"+(i+1)+"</td><td>"+barcode+"</td><td><img src='assets/img/mini-check.png'></td></tr>";
					}
				}

				console.log(barcodes);

				if($("#type").val()=="free-product"){
					$("#generatedBarcodes").html("<br/><br/><div id='gftnow-scrollbar' style='overflow-y:scroll;height:auto;max-height:240px;'><div class='pull-right'><a href='javascript:void(0);$(\'#generatedBarcodes\').hide();'></a></div><table class='table' id='gftnow-barcodes-table'>"+barcodestring+"</table></div><br/><br/><a href='javascript:void(0)' class='doneBtn btn btn-success gftnow-btn-rounded'>Done</a>");
					$("#generateBarcodes").show();

					$("#generateBarcodesBtn").unbind("click").bind("click",function(){
					    generateBarcodes();
					});

					$("#generateBarcodes .doneBtn").unbind("click").bind("click", function(e){
						$("#generateBarcodes").hide();
						$("#uploadBarcodes").hide();
						checkvalidation();
					});
				}
			}
			function generateRandom(length){
				var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				var string = "";
				for(var i=0;i<length;i++){
					string+=charset.charAt(Math.floor(Math.random() * charset.length));
				}

				return string;

		

			}
			function DropControl(controlDiv, map) {

			        // Set CSS for the control border.
			        var controlUI = document.createElement('div');
			        controlUI.style.backgroundColor = '#fff';
			        controlUI.style.cursor = 'pointer';
			        controlUI.style.marginTop = '10px';
			        controlUI.style.textAlign = 'center';
			        controlDiv.appendChild(controlUI);

			
			        // Set CSS for the control interior.
			        var controlText = document.createElement('div');
			        controlText.style.color = '#514F4F';
			        controlText.style.fontFamily = 'GothamRndRegular';
			        controlText.style.fontSize = '16px';
			        controlText.style.lineHeight = '38px';
			        controlText.style.paddingLeft = '15px';
			        controlText.style.paddingRight = '15px';
			        controlText.innerHTML = 'Drop Pin &nbsp;&nbsp; <img src="assets/img/green_pin.png">';
			        controlUI.appendChild(controlText);

			         // Setup the click event listeners: simply set the map to Chicago.
			        controlUI.addEventListener('click', function() {
			          //add event here
			          if( $("#row-counter-container").is(":visible") ){
			          	$("#add-new-btn2").click();
			          }else{
			          	$("#gftnow-alert-modal-content").html("Please enter address");
						$("#gftnow-alert-modal").modal("show");
			          }
			          		

			        });

			        checkvalidation();

			}


			function init() {

                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York
                    disableDefaultUI: true,
                    mapTypeControl: true,
                    streetViewControl: true,
                    mapTypeControlOptions: {
                    	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    },


                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };

                

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                 map = new google.maps.Map(mapElement, mapOptions);

                var dropControlDiv = document.createElement('div');
        		var dropControl = new DropControl(dropControlDiv, map);

        		dropControlDiv.index = 1;
        		map.controls[google.maps.ControlPosition.TOP_CENTER].push(dropControlDiv);

        		checkvalidation();


        		if( $("#sessiondrops").length>0 )
        		{
        			var sessionDrops = JSON.parse( $("#sessiondrops").val() );

        			if(sessionDrops)
					{
						for(var i = 0; i<sessionDrops.drops.length; i++)
						{	

							addNewDrop( sessionDrops.drops[i] );
						}
					}
        		}else{

        			$.getJSON('https://ipinfo.io/geo', function(response) { 
					    var loc = response.loc.split(',');
					    var coords = {
					        latitude: loc[0],
					        longitude: loc[1]
					    };

					    var initialLocation = new google.maps.LatLng(coords.latitude, coords.longitude);
					    map.setCenter(initialLocation);
					});

        		}
        		
			
				

				$("#add-new-btn2").click();

            }

            function codeAddress(address,id)
			{
				console.log("codeAddress");
				console.log(address);
			    var geocoder = new google.maps.Geocoder();
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			        	
			        	$("#text-"+id).attr("lat", results[0].geometry.location.lat());
				        $("#text-"+id).attr("lng", results[0].geometry.location.lng());
				        $("#lat-"+id).val(results[0].geometry.location.lat());
				        $("#lng-"+id).val(results[0].geometry.location.lng());

			            map.setCenter(results[0].geometry.location);
			           
			            var marker = new google.maps.Marker(
			            {
			                map: map,
			                position: results[0].geometry.location,
			                icon: "assets/img/icon-pin.png",
			                draggable: true,
    						animation: google.maps.Animation.DROP
			            });

			         

			            google.maps.event.addListener(marker, "dragend", function(event) { 
				          var lat = event.latLng.lat(); 
				          var lng = event.latLng.lng(); 

				          
				          $("#text-"+id).attr("lat", lat);
				          $("#text-"+id).attr("lng", lng);
				          $("#lat-"+id).val(lat);
				          $("#lng-"+id).val(lng);

				          var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

				          geocoder.geocode({'location': latlng}, function(results, status) {

				          		if (status === 'OK') {
						            if (results[1]) {
						            var id = marker.get("id");
						            $("#text-"+id).val(results[1].formatted_address);
						            $("#text-"+id).parent().find("#txt-address-clicked-"+id).val(results[1].formatted_address);
						            
						            } else {
						             console.log('No results found');
						            }
						          } else {
						            console.log('Geocoder failed due to: ' + status);
						          }

				          });

				        }); 

			            marker.set("id",id);
			            map.setCenter(marker.getPosition());
			            markers.push(marker);

			        
			           
			        }
			        else
			        {
			        	$("#gftnow-alert-modal-content").text("Geocode was not successful for the following reason: " + status);
						$("#gftnow-alert-modal").modal("show");
			          
			        }
			    });
				
				checkvalidation();

			}


		</script>


	</body>

</html>