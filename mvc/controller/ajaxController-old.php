<?php ob_start();session_start();
	
	if(isset($_POST['func']))
	{
		switch ($_POST['func']) 
		{	
			case "braintree":
				braintree($_POST['payload'],$_POST['amount'],$_POST['id']);
				break;
			case "listGiftcards":
				listGiftcards($_POST['token']);
				break;
			case "getSessionDrops":
				getSessionDrops();
				break;
			case "base64ToPNG":
				base64ToPNG($_POST['base64']);
				break;
			case "readBarcodes":
				readBarcodes($_POST['url'],$_POST['type']);
				break;
			case "listProductCategories":
				listProductCategories();
				break;
			case "signInAnonymous":
				signInAnonymous($_POST['token'], $_POST['company']);
				break;
			case "ajaxLogin":
				ajaxLogin($_POST['token']);
				break;
			case "loginAdmin":
				loginAdmin($_POST['token']);
				break;				
			case "ajaxCreateCompany":
				ajaxCreateCompany($_POST['company']);
				break;
			case "ajaxChangeCompany":
				ajaxChangeCompany($_POST['company']);
				break;
			case "ajaxChangeUserState":
				ajaxChangeUserState($_POST['account']);
				break;
			case "ajaxChangeAccountState":
				ajaxChangeAccountState($_POST['account']);
				break;
			case "ajaxChangeCompanyState":
				ajaxChangeCompanyState($_POST['company']);
				break;
			case "ajaxUpdateSettings":
				ajaxUpdateSettings($_POST['settings']);
				break;
			
			default:
		       return json_encode(array('result'=>'failed','msg'=>'cannot find action..'));
		}
	}

	function getSwaggerAPI_client($token){
		include_once "../../lib/php-client/SwaggerClient-php/vendor/autoload.php";
		$host="https://gftnow.appspot.com/_ah/api";
		$provider="firebase";

		$config = new Swagger\Client\Configuration();
		$config->setHost($host);
		$config->setSSLVerification(false);
		$config->addDefaultHeader('X-GFT-AUTH-TOKEN',$token);
		$config->addDefaultHeader('X-GFT-AUTH-PROVIDER',$provider);
		$config->setDebug(false);
		
		$api_client = new Swagger\Client\ApiClient($config);

		return $api_client;
	}

	function braintree($payload,$amount,$id){
		 require_once "../../assets/braintree-php-3.23.1/lib/Braintree.php";

		 Braintree_Configuration::environment('sandbox');
		 Braintree_Configuration::merchantId('3zh5phbpsnqh9br2');
		 Braintree_Configuration::publicKey('87fzr4fq3n3pygjc');
		 Braintree_Configuration::privateKey('3de4c81547064ba5297b8f4e9e00345d');

		 $result = Braintree_Transaction::sale([
		    'amount' => $amount,
		    'paymentMethodNonce' => $payload['nonce'],
		    'options' => [ 'submitForSettlement' => true ]
		]);

		 if ($result->success) {
		    //print_r("success!: " . $result->transaction->id);
		    echo json_encode(array("result"=>"ok","payload"=>$payload['nonce'],"amount"=>$amount,'braintreeResult'=>$result,"braintreeTranID"=>$result->transaction->id));
		} else if ($result->transaction) {
		    /*print_r("Error processing transaction:");
		    print_r("\n  code: " . $result->transaction->processorResponseCode);
		    print_r("\n  text: " . $result->transaction->processorResponseText);*/
		    echo json_encode(array("result"=>"failed","payload"=>$payload['nonce'],"amount"=>$amount,'braintree'=>$result->transaction->processorResponseText));
		} else {
		    /*print_r("Validation errors: \n");
		    print_r($result->errors->deepAll());*/
		    echo json_encode(array("result"=>"failed","payload"=>$payload['nonce'],"amount"=>$amount,'braintree'=>$result->errors->deepAll()));
		}

		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\Body16;
		$body['product_identifier'] = $id;
		$body['payment_confirmation'] = array("nonce"=>'fake-valid-nonce');
			
		try{
			$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
			$buyAndPublishDraftProduct = $api->buyAndPublishDraftProduct($body);
			echo json_encode(array("result"=>"ok", "buyAndPublishDraftProduct"=>$buyAndPublishDraftProduct));
		} catch(Exception $e){
			echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}

	}

	function listGiftcards($token){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
    	$listGiftcards = $user->listGiftcards($token);
    	echo json_encode(array("result"=>"ok","listGiftcards"=>$listGiftcards));*/
		/*$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		try {
		    $listGiftcards1 = $api->listGiftcards1();
		    echo json_encode(array("result"=>"ok", "listGiftcards1"=>$listGiftcards1));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}*/

		include_once "../../mvc/model/swagger.php";

		$swagger = new _swagger();

		$api_client = $swagger->init($token);

		$identifier;
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		
		$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
		
		try {
		    $listGiftCards1 = $api->listGiftcards1();
		    $giftCards = array();
		    foreach($listGiftCards1['vendors'] as $giftCard){
		    	$newArr = array(
		    		"identifier"=>$giftCard['identifier'],
		    		"creator"=>$giftCard['creator'],
		    		"created"=>$giftCard['created'],
		    		"name"=>$giftCard['name'],
		    		"description"=>$giftCard['description'],
		    		"logo_url"=>$giftCard['logo_url'],
		    		"disclaimer"=>$giftCard['disclaimer'],
		    		"terms_and_conditions"=>$giftCard['terms_and_conditions'],
		    		"valid_values"=>$giftCard['valid_values'],
		    		"catalog_status"=>$giftCard['catalog status']
		    	);
		    	array_push($giftCards, $newArr);
		    }

			echo json_encode(array("result"=>"ok", "token"=>$token,"listGiftcards1"=>$giftCards));

		} catch (Exception $e) {

			echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));

		    //echo 'Exception when calling GftServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
		}
	}
	
	function ajaxUpdateSettings($s){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body9();
		$body['settings'] = array(
			0=>array("name"=>"defaultUserLogo", "type"=>"URL", "value"=>$s['defaultUserLogo']),
			1=>array("name"=>"dropValidityDuration", "type"=>"Duration", "value"=>$s['dropValidityDuration']),
			2=>array("name"=>"dropPutBackDuration", "type"=>"Duration", "value"=>$s['dropPutBackDuration']),
			3=>array("name"=>"pickupCoolDownPeriod", "type"=>"Duration", "value"=>$s['pickupCoolDownPeriod']),
			4=>array("name"=>"visibleDropPickupRadius", "type"=>"Distance", "value"=>$s['visibleDropPickupRadius']),
			5=>array("name"=>"visibleDropOfferRadius", "type"=>"Distance", "value"=>$s['visibleDropOfferRadius']),
			6=>array("name"=>"visibleDropVisibilityRadius", "type"=>"Distance", "value"=>$s['visibleDropVisibilityRadius']),
			7=>array("name"=>"hiddenDropPickupRadius", "type"=>"Distance", "value"=>$s['hiddenDropPickupRadius']),
			8=>array("name"=>"defaultPlan", "type"=>"UUID", "value"=>$s['defaultPlan']),
			9=>array("name"=>"tangoUsername", "type"=>"String", "value"=>$s['tangoUsername']),
			10=>array("name"=>"tangoPassword", "type"=>"String", "value"=>$s['tangoPassword']),
			11=>array("name"=>"tangoCustomerId", "type"=>"String", "value"=>$s['tangoCustomerId']),
			12=>array("name"=>"tangoAccountId", "type"=>"String", "value"=>$s['tangoAccountId']),
			13=>array("name"=>"maximumLogoSizeInBytes", "type"=>"Long", "value"=>$s['maximumLogoSizeInBytes']),
			14=>array("name"=>"emailSenderName", "type"=>"String", "value"=>$s['emailSenderName']),
			15=>array("name"=>"emailSenderEmailAddress", "type"=>"String", "value"=>$s['emailSenderEmailAddress']),
			16=>array("name"=>"emailTemplateCampaignApproved", "type"=>"String", "value"=>addslashes($s['emailTemplateCampaignApproved'])),
			17=>array("name"=>"emailTemplateBusinessUserRegistered", "type"=>"String", "value"=>addslashes($s['emailTemplateBusinessUserRegistered'])),
			18=>array("name"=>"emailTemplateCampaignDeclined", "type"=>"String", "value"=>addslashes($s['emailTemplateCampaignDeclined'])),
			19=>array("name"=>"emailTemplateNewCampaign", "type"=>"String", "value"=>addslashes($s['emailTemplateNewCampaign']))
		);

		try {
		    $updateSettings = $api->updateSettings($body);
		    echo json_encode(array("result"=>"ok", "updateSettings"=>$updateSettings));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxCreateCompany($c){
		include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->createMyCompany($c['name'], $c['website'], $c['address'],$c['about'],$c['logoURL'], $c['category'], $c['identifier']);

		echo json_encode(array("result"=>"ok","company"=>$company['name'],"result"=>$result));
	}

	function ajaxChangeCompany($c){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$raw = array(
			"identifier"=>$c['identifier'],
			"created"=>"",
			"notes"=>"",
			"website"=>$c['website'],
			"address"=>$c['address'],
			"name"=>$c['name'],
			"about"=>$c['about']
		);
		$result = $user->changeAccountInfo($raw);
		echo json_encode(array("result"=>"ok","company"=>$raw,"result"=>$result,"c"=>$raw));*/
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\Body11;

		$account = array();
		$account['identifier'] = $c['identifier'];
		$account['created_at'] = gmdate("Y-m-d\TH:i:s\Z");
		$account['notes'] = $c['notes'];
		$account['logoURL'] = $c['logoURL'];
		$account['state'] = $c['state'];
		$account['title'] = "testtitle";
		$account['about'] = $c['about'];
		$account['website'] = $c['website'];

		$body['identifier'] = $c['identifier'];
		$body['name'] = $c['name'];
		$body['created'] = gmdate("Y-m-d\TH:i:s\Z");
		$body['notes'] = $c['notes'];
		$body['state'] = $c['state'];
		$body['address'] = $c['address'];
		$body['category'] = $c['category'];
		$body['account'] = $account;
		try {
		    $changeMyCompany = $api->changeMyCompany($body);
			echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "result"=>$changeMyCompany));
		} catch (Exception $e) {
		    echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxChangeUserState($c){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->changeUserState($c['identifier'], $c['status']);
		echo json_encode(array("result"=>"ok", "company"=>$c['identifier'],"result"=>$result));*/

		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body3;
		$body['user_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeUserState = $api->changeUserState($body);
		    echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "body"=>$body, "changeUserState"=>$changeUserState));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxChangeAccountState($c){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->changeAccountState($c['identifier'], $c['status']);
		echo json_encode(array("result"=>"ok", "company"=>$c['identifier'],"result"=>$result));*/

		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body4();
		$body['account_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeAccountState = $api->changeAccountState($body);
		    echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "result"=>$changeAccountState));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxChangeCompanyState($c){
		include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->changeCompanyState($c['identifier'], $c['status']);
		echo json_encode(array("result"=>"ok", "company"=>$c['identifier'],"result"=>$result));

		/*$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body5();
		$body['account_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeCompanyState = $api->changeCompanyState($body);
		     echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "result"=>$changeCompanyState));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}*/
	}

	function ajaxLogin($token){
		include_once "../../mvc/model/user.php";
		$user = new user();	
		$result = $user->login($token);

		if(isset($result['result']->userInfo->identifier)){
			$_SESSION['user'] = $result['result'];
			$_SESSION['token'] = $user->token;
			$_SESSION['provider'] = $user->provider;
			echo json_encode(array("result"=>"ok", "result"=>$result,"session"=>$_SESSION));
		}else{
			echo json_encode(array("result"=>$result));
		}

		/*$_SESSION['token'] = $token;

		$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
	
		try {
			$result = $api->login();
			echo json_encode(array("result"=>"OK","response"=>$result, "session"=>$_SESSION));
		} catch (Exception $e){
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}*/

		/*$_SESSION['token'] = $token;

		$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
	
		try {
		    $result = $api->createBusiness();
		   	echo json_encode(array("result"=>"OK","response"=>$result));
		} catch (Exception $e) {
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}*/
	}

	function loginAdmin($token){
		include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->loginAdmin($token); //$token

		if(isset($result['result']->userInfo->identifier)){
			$_SESSION['user'] = $result['result'];
			$_SESSION['token'] = $token;
			echo json_encode(array("result"=>"ok", "result"=>$result,"session"=>$_SESSION));
		}else{
			echo json_encode(array("result"=>"failed"));
		}
		
	}

	function signInAnonymous($token,$company){
		
		$_SESSION['token'] = $token;
		$_SESSION['company'] = $company;

		$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
	
		try {
		    $result = $api->createBusiness();
		    $_SESSION['user'] = $result;
		   	echo json_encode(array("result"=>"OK","response"=>$result));
		} catch (Exception $e) {
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}

	}
	function listProductCategories(){
		include_once "../../mvc/model/user.php";
		$user = new user();
		echo json_encode(array("result"=>"OK","productCategories"=>$user->listCompanyCategories()));
	}
	function getSessionDrops(){
		if(isset($_SESSION['drops']['drops'])){
			echo json_encode(array("result"=>"OK", "drops"=>$_SESSION['drops']['drops'] ));
		}else{
			echo json_encode(array("result"=>"Failed"));
		}
	}
	function readBarcodes($url,$type){
		$filepath = "../../assets/upload/php/files/".$url;
		$file = fopen($filepath,"r");
		$result =array();

		if($type=="text/plain"){
			if ($file) {
			   $result = explode("\n", file_get_contents($filepath));
			}
		}else{
			if ($file) {
			$result = fgetcsv($file);
			}
		}
		
		fclose($file);
		echo json_encode(array("result"=>"OK", "barcodes"=>$result, "type"=>$type));
	}
	function base64ToPNG($base64){
		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64));
		$file = substr(str_shuffle(md5(time())),0,20).".png";
		$filepath = "../../assets/upload/php/files/".$file;
		
		if( file_put_contents($filepath, $data) ){
			echo json_encode(array("result"=>"OK", "file"=>$file));
		}else{
			echo json_encode(array("result"=>"Failed"));
		}

		
	}

?>