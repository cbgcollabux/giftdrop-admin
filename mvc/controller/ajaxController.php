<?php ob_start();session_start();
	
	if(isset($_POST['func']))
	{
		switch ($_POST['func']) 
		{	

			case "ajaxChangeProductState":
				ajaxChangeProductState($_POST['state'],$_POST['product_identifier']);
				break;
			case "stripe":
				stripe($_POST['token'],$_POST['amount']);
				break;
			case "braintree":
				braintree($_POST['payload'],$_POST['amount']);
				break;
			case "listGiftcards":
				listGiftcards($_POST['token']);
				break;
			case "getSessionDrops":
				getSessionDrops();
				break;
			case "base64ToPNG":
				base64ToPNG($_POST['base64']);
				break;
			case "readBarcodes":
				readBarcodes($_POST['url'],$_POST['type']);
				break;
			case "listProductCategories":
				listProductCategories();
				break;
			case "signInAnonymous":
				signInAnonymous($_POST['token'], $_POST['company']);
				break;
			case "ajaxLogin":
				ajaxLogin($_POST['token']);
				break;
			case "loginAdmin":
				loginAdmin($_POST['token']);
				break;				
			case "ajaxCreateCompany":
				ajaxCreateCompany($_POST['company']);
				break;
			case "ajaxChangeCompany":
				ajaxChangeCompany($_POST['company'], $_POST['account']);
				break;
			case "ajaxChangeUserState":
				ajaxChangeUserState($_POST['account']);
				break;
			case "ajaxChangeAccountState":
				ajaxChangeAccountState($_POST['account']);
				break;
			case "ajaxChangeAccountState2":
				ajaxChangeAccountState2($_POST['account']);
				break;
			case "ajaxChangeCompanyState":
				ajaxChangeCompanyState($_POST['company']);
				break;
			case "ajaxUpdateSettings":
				ajaxUpdateSettings($_POST['settings']);
				break;
			case "ajaxCreatePlan":
				ajaxCreatePlan($_POST['name'], $_POST['fees']);
				break;
			case "ajaxDisablePlans":
				ajaxDisablePlans($_POST['plans']);
				break;
			case "ajaxAssignPlanToAccounts":
				ajaxAssignPlanToAccounts($_POST['planIdentifier'], $_POST['accountIdentifiers']);
				break;
			case "ajaxListUsers":
				ajaxListUsers();
				break;
			
			default:
		       return json_encode(array('result'=>'failed','msg'=>'cannot find action..'));
		}
	}

	function stripe($token,$amount){
		require_once('../../lib/stripe/vendor/autoload.php');

		\Stripe\Stripe::setApiKey("sk_test_EIHOlymZUWjdMnz8W0Wdrvxx");

		// Token is created using Stripe.js or Checkout!
		// Get the payment token ID submitted by the form:
		
		// Charge the user's card:
		$charge = \Stripe\Charge::create(array(
		  "amount" => $amount*100,
		  "currency" => "usd",
		  "description" => "Drops Payment",
		  "source" => $token,
		));



		echo json_encode($charge);


	}
	function getSwaggerAPI_client($token){
		include_once "../../lib/php-client/SwaggerClient-php/vendor/autoload.php";
		$host="https://gftnow.appspot.com/_ah/api";
		$provider="firebase";

		$config = new Swagger\Client\Configuration();
		$config->setHost($host);
		$config->setSSLVerification(true);
		$config->addDefaultHeader('X-GFT-AUTH-TOKEN',$token);
		$config->addDefaultHeader('X-GFT-AUTH-PROVIDER',$provider);
		$config->setDebug(false);
		
		$api_client = new Swagger\Client\ApiClient($config);

		return $api_client;
	}

	function braintree($payload,$amount){
		 require_once "../../assets/braintree-php-3.23.1/lib/Braintree.php";

		 Braintree_Configuration::environment('sandbox');
		 Braintree_Configuration::merchantId('3zh5phbpsnqh9br2');
		 Braintree_Configuration::publicKey('87fzr4fq3n3pygjc');
		 Braintree_Configuration::privateKey('3de4c81547064ba5297b8f4e9e00345d');

		 $result = Braintree_Transaction::sale([
		    'amount' => $amount,
		    'paymentMethodNonce' => $payload['nonce'],
		    'options' => [ 'submitForSettlement' => true ]
		]);

		 if ($result->success) {
		    //print_r("success!: " . $result->transaction->id);
		    echo json_encode(array("result"=>"ok","payload"=>$payload['nonce'],"amount"=>$amount,'braintreeResult'=>$result,"braintreeTranID"=>$result->transaction->id));
		} else if ($result->transaction) {
		    /*print_r("Error processing transaction:");
		    print_r("\n  code: " . $result->transaction->processorResponseCode);
		    print_r("\n  text: " . $result->transaction->processorResponseText);*/
		    echo json_encode(array("result"=>"failed","payload"=>$payload['nonce'],"amount"=>$amount,'braintree'=>$result->transaction->processorResponseText));
		} else {
		    /*print_r("Validation errors: \n");
		    print_r($result->errors->deepAll());*/
		    echo json_encode(array("result"=>"failed","payload"=>$payload['nonce'],"amount"=>$amount,'braintree'=>$result->errors->deepAll()));
		}

	}

	function listGiftcards($token){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
    	$listGiftcards = $user->listGiftcards($token);
    	echo json_encode(array("result"=>"ok","listGiftcards"=>$listGiftcards));*/
		/*$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		try {
		    $listGiftcards1 = $api->listGiftcards1();
		    echo json_encode(array("result"=>"ok", "listGiftcards1"=>$listGiftcards1));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}*/

		include_once "../../mvc/model/swagger.php";

		$swagger = new _swagger();

		$api_client = $swagger->init($token);

		$identifier;
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		
		$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
		
		try {
		    $listGiftCards1 = $api->listGiftCards();
		    $giftCards = array();
		    foreach($listGiftCards1['vendors'] as $giftCard){
		    	$newArr = array(
		    		"identifier"=>$giftCard['identifier'],
		    		"creator"=>$giftCard['creator'],
		    		"created"=>$giftCard['created'],
		    		"name"=>$giftCard['name'],
		    		"description"=>$giftCard['description'],
		    		"logo_url"=>$giftCard['logo_url'],
		    		"disclaimer"=>$giftCard['disclaimer'],
		    		"terms_and_conditions"=>$giftCard['terms_and_conditions'],
		    		"valid_values"=>$giftCard['valid_values'],
		    		"catalog_status"=>$giftCard['catalog status']
		    	);
		    	array_push($giftCards, $newArr);
		    }

			echo json_encode(array("result"=>"ok", "token"=>$token,"listGiftcards1"=>$giftCards));

		} catch (Exception $e) {

			echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));

		    //echo 'Exception when calling GftServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
		}
	}
	
	function ajaxUpdateSettings($s){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\UpdateSettingsRequest;
		$body['settings'] = array(
			0=>array("name"=>"defaultUserLogo", "type"=>"URL", "value"=>$s['defaultUserLogo']),
			1=>array("name"=>"incognitoLogo", "type"=>"URL", "value"=>$s['incognitoLogo']),
			2=>array("name"=>"dropValidityDuration", "type"=>"Duration", "value"=>$s['dropValidityDuration']),
			3=>array("name"=>"dropPutBackDuration", "type"=>"Duration", "value"=>$s['dropPutBackDuration']),
			4=>array("name"=>"pickupCoolDownPeriod", "type"=>"Duration", "value"=>$s['pickupCoolDownPeriod']),
			5=>array("name"=>"visibleDropPickupRadius", "type"=>"Distance", "value"=>$s['visibleDropPickupRadius']),
			6=>array("name"=>"visibleDropOfferRadius", "type"=>"Distance", "value"=>$s['visibleDropOfferRadius']),
			7=>array("name"=>"visibleDropVisibilityRadius", "type"=>"Distance", "value"=>$s['visibleDropVisibilityRadius']),
			8=>array("name"=>"defaultPlan", "type"=>"UUID", "value"=>$s['defaultPlan']),
			9=>array("name"=>"tangoUsername", "type"=>"String", "value"=>$s['tangoUsername']),
			10=>array("name"=>"tangoPassword", "type"=>"String", "value"=>$s['tangoPassword']),
			11=>array("name"=>"tangoCustomerId", "type"=>"String", "value"=>$s['tangoCustomerId']),
			12=>array("name"=>"tangoAccountId", "type"=>"String", "value"=>$s['tangoAccountId']),
			13=>array("name"=>"maximumLogoSizeInBytes", "type"=>"Long", "value"=>$s['maximumLogoSizeInBytes']),
			14=>array("name"=>"visibleDropCacheHilbertGranularity", "type"=>"Integer", "value"=>$s['visibleDropCacheHilbertGranularity']),
			15=>array("name"=>"hiddenDropCacheHilbertGranularity", "type"=>"Integer", "value"=>$s['hiddenDropCacheHilbertGranularity']),
			16=>array("name"=>"sendGridAPIKey", "type"=>"String", "value"=>$s['sendGridAPIKey']),
			17=>array("name"=>"stripeAPIKey", "type"=>"String", "value"=>$s['stripeAPIKey']),
			18=>array("name"=>"firebaseUrl", "type"=>"URL", "value"=>$s['firebaseUrl']),
			19=>array("name"=>"firebaseConfig", "type"=>"JSON", "value"=>$s['firebaseConfig']),
			20=>array("name"=>"redissonConfig", "type"=>"JSON", "value"=>$s['redissonConfig']),
			21=>array("name"=>"testPaymentProviderState", "value"=>"String", "value"=>$s['testPaymentProviderState']),
			22=>array("name"=>"testGiftCardProviderState", "value"=>"String", "value"=>$s['testGiftCardProviderState']),
			23=>array("name"=>"emailSenderName", "type"=>"String", "value"=>$s['emailSenderName']),
			24=>array("name"=>"emailSenderEmailAddress", "type"=>"String", "value"=>$s['emailSenderEmailAddress']),
			25=>array("name"=>"emailAdminNamee", "type"=>"String", "value"=>$s['emailAdminNamee']),
			26=>array("name"=>"emailAdminEmailAddress", "type"=>"Email", "value"=>$s['emailAdminEmailAddress']),
			27=>array("name"=>"hiddenDropPickupRadius", "type"=>"Distance", "value"=>$s['hiddenDropPickupRadius'])
		);

		try {
		    $updateSettings = $api->updateSettings($body);
		    echo json_encode(array("result"=>"OK", "updateSettings"=>$updateSettings));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxCreateCompany($c){
		include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->createMyCompany($c['name'], $c['website'], $c['address'],$c['about'],$c['logoURL'], $c['category'], $c['identifier']);

		echo json_encode(array("result"=>"ok","company"=>$company['name'],"result"=>$result));
	}

	function ajaxChangeProductState($c, $a){
	
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\ChangeProductStateRequest;

		$body['product_identifier'] = $a;
		$body['state'] = ucfirst($c);
		
		try {
		    $changeProductState = $api->changeProductState($body);
			echo json_encode(array("result"=>"ok","body"=>$body,"changeProductState"=>$changeProductState));
		} catch (Exception $e) {
		    echo json_encode(array("result"=>"failed","body"=>$body,"message"=>$e->getMessage()));
		}		

	}

	function ajaxChangeCompany($c, $a){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$raw = array(
			"identifier"=>$c['identifier'],
			"created"=>"",
			"notes"=>"",
			"website"=>$c['website'],
			"address"=>$c['address'],
			"name"=>$c['name'],
			"about"=>$c['about']
		);
		$result = $user->changeAccountInfo($raw);
		echo json_encode(array("result"=>"ok","company"=>$raw,"result"=>$result,"c"=>$raw));*/
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\CompanyProfile();

		/*$account = array();
		$account['identifier'] = $a['identifier'];
		$account['created'] = $a['created'];
		$account['notes'] = $a['notes'];
		$account['logoURL'] = $a['logoURL'];
		$account['state'] = $a['state'];
		$account['title'] = $a['title'];
		$account['about'] = $a['about'];
		$account['website'] = $a['website'];*/

		$body['identifier'] = $c['identifier'];
		$body['name'] = $c['name'];
		$body['account'] = array("identifier"=>$a['identifier'], "created"=>$a['created'], "notes"=>$a['notes'], "logoURL"=>$a['logoURL'], "state"=>$a['state'], "title"=>$a['title'], "about"=>$a['about'], "website"=>$a['website']);
		$body['created'] = $c['created'];
		$body['notes'] = $c['notes'];
		$body['state'] = $c['state'];
		$body['address'] = $c['address'];
		$body['category'] = $c['category'];
		
		try {
		    $changeMyCompany = $api->changeMyCompany($body);
			echo json_encode(array("result"=>"ok", "company"=>$c, "account"=>$a, "body"=>$body, "result"=>$changeMyCompany));
		} catch (Exception $e) {
		    echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxChangeUserState($c){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->changeUserState($c['identifier'], $c['status']);
		echo json_encode(array("result"=>"ok", "company"=>$c['identifier'],"result"=>$result));*/

		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\UpdateUserStateRequest;
		$body['user_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeUserState = $api->changeUserState($body);
		    echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "body"=>$body, "changeUserState"=>$changeUserState));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}
	}

	function ajaxChangeAccountState2($c){
		include_once "../../mvc/model/user.php";
	
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\UpdateAccountStateRequest;

		$body['account_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";

		$result = array();
		
		try {
		    $changeAccountState = $api->changeAccountState($body);
		    $result['accountState']=1;
		} catch (Exception $e) {
			$result['accountState']=0;
		}


		echo json_encode(array("c"=>$c,"accountState"=>$result['accountState']));
	}

	function ajaxChangeAccountState($c){
		include_once "../../mvc/model/user.php";
	
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\UpdateAccountStateRequest;

		$body['account_identifier'] = $c['account_identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";

		$result = array();
		
		try {
		    $changeAccountState = $api->changeAccountState($body);
		    $result['accountState']=1;
		} catch (Exception $e) {
			$result['accountState']=0;
		}

		$body = new Swagger\Client\Model\UpdateCompanyStateRequest;

		$body['account_identifier'] = $c['account_identifier'];
		$body['company_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeCompanyState = $api->changeCompanyState($body);
		    $result['companyState']=1;
		} catch (Exception $e) {
			$result['companyState']=0;
		}

		echo json_encode(array("c"=>$c,"accountState"=>$result['accountState'],"companyState"=>$result['companyState']));
	}

	function ajaxChangeCompanyState($c){
		include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->changeCompanyState($c['identifier'], $c['status']);
		echo json_encode(array("result"=>"ok", "company"=>$c['identifier'],"result"=>$result));

		/*$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body5();
		$body['account_identifier'] = $c['identifier'];
		$body['state'] = $c['status'];
		$body['security_alert'] = "";
		
		try {
		    $changeCompanyState = $api->changeCompanyState($body);
		     echo json_encode(array("result"=>"ok", "company"=>$c['identifier'], "changeCompanyState"=>$changeCompanyState));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed","message"=>$e->getMessage()));
		}*/
	}

	function ajaxCreatePlan($n, $f){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\CreatePlanRequest;
		$body['name'] = $n;
		$body['notes'] = "";
		$body['fees'] = $f;

		try {
		    $createPlan = $api->createPlan($body);
		    echo json_encode(array("result"=>"ok", "planName"=>$n, "planFees"=>$f, "createPlan"=>$createPlan));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed", "message"=>$e->getMessage()));
		}
	}

	function ajaxDisablePlans($p){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\DisablePlansRequest;
		$body['plans'] = $p;

		try {
		    $disablePlans = $api->disablePlans($body);
		    echo json_encode(array("result"=>"ok", "plan"=>$p, "disablePlans"=>$disablePlans));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed", "message"=>$e->getMessage()));
		}
	}

	function ajaxAssignPlanToAccounts($p, $a){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\AssignPlanToAccountsRequest;
		$body['plan_identifier'] = $p;
		$body['account_identifiers'] = $a;
		try {
		    $assignPlanToAccounts = $api->assignPlanToAccounts($body);
		    echo json_encode(array("result"=>"ok", "plan"=>$p, "accounts"=>$a, "assignPlanToAccounts"=>$assignPlanToAccounts));
		} catch (Exception $e) {
		   	echo json_encode(array("result"=>"failed", "message"=>$e->getMessage()));
		}
	}

	function ajaxListUsers(){
		/*$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\Body1();
		try{
		 	$listUsers = $api->listUsers($body);
		 	echo json_encode(array("result"=>"ok", "listUsers"=>$listUsers['user']));
		}catch (Exception $e){
			echo json_encode(array("result"=>"failed", "message"=>$e->getMessage()));
		}*/
		include_once "../../mvc/model/user.php";
		$user = new user();
		echo json_encode(array("result"=>"OK","listUsers"=>$user->listUsers()));
	}

	function ajaxFilterListUsers($p){
		$api_client = getSwaggerAPI_client($_SESSION['token']);
		$api = new Swagger\Client\Api\AdminServiceApi($api_client);
		$body = new Swagger\Client\Model\UserListRequest;
		//$body['query_parameters'] = $p;
		try{
			$listUsers = $api->listUsers(/*$body*/);
			echo json_encode(array("result"=>"ok","listUsers"=>$listUsers));
		}catch (Exception $e){
			echo json_encode(array("result"=>"failed", "message"=>$e->getMessage()));
		}
	}

	function ajaxLogin($token){
		/*include_once "../../mvc/model/user.php";
		$user = new user();	
		$result = $user->login($token);

		if(isset($result['result']->userInfo->identifier)){
			$_SESSION['user'] = $result['result'];
			$_SESSION['token'] = $user->token;
			$_SESSION['provider'] = $user->provider;
			echo json_encode(array("result"=>"ok", "login"=>$result, "state"=>$result['result']->userInfo->account->state));
		}else{
			echo json_encode(array("result"=>$result));
		}*/

		/*$_SESSION['token'] = $token;

		$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
	
		try {
			$result = $api->login();
			echo json_encode(array("result"=>"OK","response"=>$result, "session"=>$_SESSION));
		} catch (Exception $e){
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}*/

		$_SESSION['token'] = $token;

		$api_client = getSwaggerAPI_client($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
	
		try {
		    $result = $api->createBusiness();

		    $_SESSION['user'] = array("identifier"=>$result['user_info']['identifier'],
					"name"=>$result['user_info']['name'],
					"first_name"=>$result['user_info']['first_name'],
					"last_name"=>$result['user_info']['last_name'],
					"account"=>array("identifier"=>$result['user_info']['account']['identifier'])
					);

		    $_SESSION['token'] = $token;
			echo json_encode(array("result"=>"ok","state"=>$result['user_info']['account']['state'],"company_state"=>$result['user_info']['state'], "identifier"=>$result['user_info']['identifier']));
		
		   	//echo json_encode(array("result"=>"OK","response"=>$result));
		} catch (Exception $e) {
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}
	}

	function loginAdmin($token){
		/*include_once "../../mvc/model/user.php";
		$user = new user();
		$result = $user->loginAdmin($token); //$token

		if(isset($result['result']->userInfo->identifier)&&$result['result']->userInfo->role=="Admin"){
			$_SESSION['user'] = $result['result'];
			$_SESSION['token'] = $token;
			echo json_encode(array("result"=>"ok", "result"=>$result,"user"=>$result['result']->userInfo,"session"=>$_SESSION));
		}else{
			echo json_encode(array("result"=>"failed"));
		}*/

		include_once "../../mvc/model/swagger.php";
		$swagger = new _swagger();
		$api_client = $swagger->init($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);
		//$_SESSION['token'] = $token;
				
		try {

			$result = $api->createAdmin();

			if(isset($result['user_info']['identifier'])&&$result['user_info']['role']=="Admin"){
				$_SESSION['user'] = array("identifier"=>$result['user_info']['identifier'],
					"name"=>$result['user_info']['name'],
					"first_name"=>$result['user_info']['first_name'],
					"last_name"=>$result['user_info']['last_name'],
					"account"=>array("identifier"=>$result['user_info']['account']['identifier'])
					);
				$_SESSION['role'] = $result['user_info']['role'];
				$_SESSION['token'] = $token;
				echo json_encode(array("result"=>"ok","role"=>$result['user_info']['role'], "identifier"=>$result['user_info']['identifier']));
			}else{
				echo json_encode(array("result"=>"failed"));
			}

		    
		} catch (Exception $e) {
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}
		
	}

	function signInAnonymous($token,$company){
		
		$_SESSION['token'] = $token;
		$_SESSION['company'] = $company;

		include_once "../../mvc/model/swagger.php";
		$swagger = new _swagger();
		$api_client = $swagger->init($token);
		$api = new Swagger\Client\Api\LoginServiceApi($api_client);

		//$api_client = getSwaggerAPI_client($token);
		
		try {
		    $result = $api->createBusiness();
		    $_SESSION['user'] = $result;
		   	echo json_encode(array("result"=>"OK","response"=>$result));
		} catch (Exception $e) {
			echo json_encode(array("result"=>"Failed","message"=>$e->getMessage()));
		}

	}
	function listProductCategories(){
		include_once "../../mvc/model/user.php";
		$user = new user();
		echo json_encode(array("result"=>"OK","productCategories"=>$user->listCompanyCategories()));
	}
	function getSessionDrops(){
		if(isset($_SESSION['drops']['drops'])){
			echo json_encode(array("result"=>"OK", "drops"=>$_SESSION['drops']['drops'] ));
		}else{
			echo json_encode(array("result"=>"Failed"));
		}
	}
	function readBarcodes($url,$type){
		$filepath = "../../assets/upload/php/files/".$url;
		$file = fopen($filepath,"r");
		$result =array();

		if($type=="text/plain"){
			if ($file) {
			   $result = explode("\n", file_get_contents($filepath));
			}
		}else{
			if ($file) {
			$result = fgetcsv($file);
			}
		}
		
		fclose($file);
		echo json_encode(array("result"=>"OK", "barcodes"=>$result, "type"=>$type));
	}
	function base64ToPNG($base64){
		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64));
		$file = substr(str_shuffle(md5(time())),0,20).".png";
		$filepath = "../../assets/upload/php/files/".$file;
		
		if( file_put_contents($filepath, $data) ){
			echo json_encode(array("result"=>"OK", "file"=>$file));
		}else{
			echo json_encode(array("result"=>"Failed"));
		}

		
	}

?>