<?php
	class api{

		var $endpoints;

		function __construct(){
			$this->endpoints = array(
				"login"=>"https://gftnow.appspot.com/_ah/api/loginService/v1/createBusiness",
				"adminLogin"=>"https://gftnow.appspot.com/_ah/api/loginService/v1/createAdmin",
				"findMyCompanyProfile"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/findMyCompanyProfile",
				"listProductCategories"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/listProductCategories",
				"listMyCompanyProducts"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/listMyCompanyProducts",
				"createMyCompany"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/createMyCompany",
				"listCompanyCategories"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/listCompanyCategories",
				"buyProduct"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/buyProduct",
				"listUsers"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/listUsers",
				"collectMyCompanyStatistics"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/collectMyCompanyStatistics",
				"listGiftcards"=>"https://gftnow.appspot.com/_ah/api/gftService/v1/listGiftcards",
				"changeMyCompany"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/changeMyCompany",
				"createDraftProduct"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/createDraftProduct",
				"buyAndPublishDraftProduct"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/buyAndPublishDraftProduct",
				"findProduct"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/findProduct",
				"listCompanies"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/listCompanies",
				"detailedCompanyProfile"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/detailedCompanyProfile",
				"changeUserState"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/changeUserState",
				"changeAccountState"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/changeAccountState",
				"changeCompanyState"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/changeCompanyState",
				"detailedUserProfile"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/detailedUserProfile",
				"createAdmin"=>"https://gftnow.appspot.com/_ah/api/loginService/v1/createAdmin",
				"changeAccountInfo"=>"https://gftnow.appspot.com/_ah/api/gftService/v1/changeAccountInfo",
				"sendEmail"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/sendEmail",
				"changeAccountLogoURL"=>"https://gftnow.appspot.com/_ah/api/gftService/v1/changeAccountLogoURL",
				"listUserLocations"=>"https://gftnow.appspot.com/_ah/api/adminService/v1/listUserLocations",
				"requestTotal"=>"https://gftnow.appspot.com/_ah/api/businessService/v1/requestTotal",
				"createPlayer"=>"https://gftnow.appspot.com/_ah/api/loginService/v1/createPlayer");
		}

		function send($endpoint,$token,$provider,$data_string=''){
			$ch = curl_init();

			$headers = [
			    'X-GFT-AUTH-TOKEN: '.$token,
			    'X-GFT-AUTH-PROVIDER: '.$provider,
			    'Content-type: application/json'
			];

			$optArray = array(
			    CURLOPT_URL => $endpoint,
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_SSL_VERIFYPEER => true,
			    CURLOPT_CUSTOMREQUEST => "POST",
			    CURLOPT_POSTFIELDS => $data_string,
			    CURLOPT_HTTPHEADER => $headers
			);

			curl_setopt_array($ch, $optArray);


			$result = curl_exec($ch);
			$errors = curl_error($ch);
			$response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			return array("result"=>json_decode($result), "errors"=>$errors, "response"=>$response);
		}

	}
?>