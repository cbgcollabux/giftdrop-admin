<?php

	include_once dirname(__FILE__,3)."/lib/php-client/SwaggerClient-php/vendor/autoload.php";

	class _swagger{
		var $host;
		var $provider;

		function __construct(){
			$this->host="https://gftnow.appspot.com/_ah/api";
			$this->provider="firebase";
		}

		public function init($token){
			$config = new Swagger\Client\Configuration();
			$config->setHost($this->host);
			$config->setSSLVerification(true);
			$config->addDefaultHeader('X-GFT-AUTH-TOKEN',$token);
			$config->addDefaultHeader('X-GFT-AUTH-PROVIDER',$this->provider);
			$config->addDefaultHeader('Expect','');
			$config->setDebug(false);
			
			$api_client = new Swagger\Client\ApiClient($config);

			return $api_client;

		
		}
	}
?>