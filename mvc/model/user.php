<?php
	include_once dirname(__FILE__)."/api.php";
	
	class user{

		var $provider;
		var $token;

		function __construct(){
			$this->provider = "Firebase";
			$this->token = "";
		}
		function login($token)
		{
			if(!isset($token)||$token=='')
				$this->token = "business_user_".uniqid();
			else
				$this->token = $token;

			if(isset($token)){
				$api = new api();
				return $api->send($api->endpoints['login'],$this->token,$this->provider);
				//return array("token"=>$token,"provider"=>$provider);
			}else{
				return array("result"=>"Failed","message"=>"Auth not set",'token'=>$token,'provider'=>$provider);
			}
		}

		function loginAdmin($token)
		{

			if(!isset($token))
				$token = "admin_user_".uniqid();

			if(!isset($provider))
				$provider = "Firebase";

			if(isset($token)&&isset($provider)){
				$api = new api();
				return $api->send($api->endpoints['adminLogin'],$token,$provider);
			}else{
				return array("result"=>"Failed","message"=>"Auth not set");
			}

			/*if(!isset($token)||$token=='')
				$this->token = "admin_user_".uniqid();
			else
				$this->token = $token;

			if(isset($token)){
				$api = new api();
				return $api->send($api->endpoints['adminLogin'],$this->token, "Test");
				//return array("token"=>$token,"provider"=>$provider);
			}else{
				return array("result"=>"Failed","message"=>"Auth not set",'token'=>$token,'provider'=>$provider);
			}*/
		}

		function findMyCompanyProfile(){
			$api = new api();
			return $api->send($api->endpoints['findMyCompanyProfile'],$_SESSION['token'], $this->provider);
		}

		function listProductCategories(){
			$api = new api();
			return $api->send($api->endpoints['listProductCategories'],$_SESSION['token'], $this->provider);
		}

		function listGiftcards($token){
			$api = new api();
			return $api->send($api->endpoints['listGiftcards'],$_SESSION['token'], $this->provider);
			//return array("token"=>$token, "provider"=>$this->provider);
		}
		
		function listMyCompanyProducts($from, $to){

			//from and to should be in gm/utc format
			$api = new api();
			return $api->send($api->endpoints['listMyCompanyProducts'],$_SESSION['token'], $this->provider, json_encode(array("from"=>$from, "to"=>$to)));
		}

		function createMyCompany($company){
			
			$data_string = json_encode($company);

			$api = new api();
			return $api->send($api->endpoints['createMyCompany'],$_SESSION['token'], $this->provider, $data_string);
		}

		function changeMyCompany($company){
			
			$data_string = json_encode($company);

			$api = new api();
			return $api->send($api->endpoints['changeMyCompany'],$_SESSION['token'], $this->provider, $data_string);
			
		}

		/*function changeMyCompany($identifier, $created, $name, $address, $notes, $logoURL){
			
			$company = array("identifier"=>$identifier,"created"=>$created,"name"=>$name,"address"=>$address,"notes"=>$notes,"logoURL"=>$logoURL,"category"=>$category);
			$data_string = json_encode($company);

			$api = new api();
			return $api->send($api->endpoints['changeMyCompany'],$_SESSION['token'], $this->provider, $data_string);
			
		}*/

		function listCompanyCategories(){

			$api = new api();
			return $api->send($api->endpoints['listCompanyCategories'],$_SESSION['token'], $this->provider,'');
			
		}

		function buyAndPublishDraftProduct($nonce, $campaign_id){
			$api = new api();
			$data_string = json_encode(array("productIdentifier"=>$campaign_id,"paymentConfirmation"=>array("nonce"=>$nonce)));
			
			return $api->send($api->endpoints['buyAndPublishDraftProduct'],$_SESSION['token'], $this->provider, $data_string);
		}
		function buyProduct($product, $drops){

			$api = new api();
			$data_string = json_encode(array("product"=>$product,"drops"=>$drops));
			
			//return $data_string;
			return $api->send($api->endpoints['buyProduct'],$_SESSION['token'], $this->provider, $data_string);

		}

		function listUsers($start, $end, $from, $to){
			$api = new api();
			$data_string = json_encode(array("itemRangeQuery"=>array('startItemIndexInclusive'=>$start,'endItemIndexInclusive'=>$end), "creation_period"=>array('from'=>$from,'to'=>$to) ));
			return $api->send($api->endpoints['listUsers'],$_SESSION['token'], $this->provider, $data_string);
		}

		function listCompanies($start, $end, $from, $to){
			$api = new api();
			$data_string = json_encode(array("itemRangeQuery"=>array('startItemIndexInclusive'=>$start,'endItemIndexInclusive'=>$end), "creation_period"=>array('from'=>$from,'to'=>$to) ));
			return $api->send($api->endpoints['listCompanies'],$_SESSION['token'], $this->provider, $data_string);
		}

		function collectMyCompanyStatistics($start, $end){
			$api = new api();
			$data_string = json_encode(array("from"=>$start,"to"=>$end));
			return $api->send($api->endpoints['collectMyCompanyStatistics'],$_SESSION['token'], $this->provider, $data_string);
		}

		function createDraftProduct($product, $drops){
		   $api = new api();
		   $data_string = json_encode(array("productAndDrops"=>array("product"=>$product,"drops"=>$drops) ) );
		   
		   return $api->send($api->endpoints['createDraftProduct'],$_SESSION['token'], $this->provider, $data_string);
		}

		function findProduct($campaign_id){
			$api = new api();
			$data_string = json_encode(array("productIdentifier"=>$campaign_id));

			return $api->send($api->endpoints['findProduct'],$_SESSION['token'], $this->provider, $data_string);
		}

		function detailedCompanyProfile($id){
			$api = new api();
			$data_string = json_encode(array("companyIdentifier"=>$id));
			return $api->send($api->endpoints['detailedCompanyProfile'],$_SESSION['token'], $this->provider, $data_string);
		}

		function changeUserState($id, $status){
			$api = new api();
			$data_string = json_encode(array("userIdentifier"=>$id, "securityAlert"=>"", "state"=>$status));
			return $api->send($api->endpoints['changeUserState'],$_SESSION['token'], $this->provider, $data_string);
		}

		function changeAccountState($id, $status){
			$api = new api();
			$data_string = json_encode(array("accountIdentifier"=>$id, "securityAlert"=>"", "state"=>$status));
			return $api->send($api->endpoints['changeAccountState'],$_SESSION['token'], $this->provider, $data_string);
		}

		function changeCompanyState($id, $status){
			$api = new api();
			$data_string = json_encode(array("companyIdentifier"=>$id, "securityAlert"=>"", "state"=>$status));
			return $api->send($api->endpoints['changeCompanyState'],$_SESSION['token'], $this->provider, $data_string);
		}

		function detailedUserProfile($id){
			$api = new api();
			$data_string = json_encode(array("userIdentifier"=>$id));
			return $api->send($api->endpoints['detailedUserProfile'],$_SESSION['token'], $this->provider, $data_string);
		}

		function createAdmin(){
			$api = new api();
			return $api->send($api->endpoints['createAdmin'], $_SESSION['token'], $this->provider, "");
		}

		function changeAccountInfo($id, $created, $notes, $website, $state, $title, $about){
			$api = new api();
			$data_string = json_encode(array("identifier"=>$id, "created"=>$created, "notes"=>$notes, "website"=>$website, "state"=>$state, "title"=>$title, "about"=>$about));
			return $api->send($api->endpoints['changeAccountInfo'], $_SESSION['token'], $this->provider, $data_string);
		}

		function sendEmail($f_address, $f_name, $t_address, $t_name, $title, $body, $contentType){
			$api = new api();
			$data_string = json_encode(array("from"=>array("address"=>$f_address, "name"=>$f_name), "to"=>array("address"=>$t_address, "name"=>$t_name), "title"=>$title, "body"=>$body, "contentType"=>$contentType));
			return $api->send($api->endpoints['sendEmail'], $_SESSION['token'], $this->provider, $data_string);
		}

		function changeAccountLogoURL($data){
			$api = new api();
			$data_string = json_encode(array("data"=>$data, "imageType"=>"Unknown"));
			return $api->send($api->endpoints['changeAccountLogoURL'], $_SESSION['token'], $this->provider, $data_string);
		}

		function listUserLocations($lat, $lng){
			$api = new api();
			$data_string = json_encode(array("center"=>array("latitude"=>$lat, "longitude"=>$lng),"radius"=>1000));
			return $api->send($api->endpoints['listUserLocations'], $_SESSION['token'], $this->provider, $data_string);
		}

		function requestTotal(){
			$api = new api();
			$data_string = json_encode(array("amount"=>0));
			return $api->send($api->endpoints['requestTotal'], $_SESSION['token'], $this->provider, $data_string);
		}

		function createPlayer($company){
			$data_string = json_encode($company);
			$api = new api();
			return $api->send($api->endpoints['createPlayer'],$_SESSION['token'], $this->provider, $data_string);
		}

	}
?>