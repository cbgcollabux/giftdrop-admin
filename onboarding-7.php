<?php
include_once "header.php";
include_once "mvc/model/user.php"; 
include_once "mvc/model/swagger.php";


	if(isset( $_POST['BTtransaction'] ) && isset( $_POST['BTnonce'] ) )
	{
		
		$user = new user();
		$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

		echo "<input type='hidden' id='email' value='".$_SESSION['company']['email']."'/>";
		echo "<input type='hidden' id='password' value='".$_SESSION['company']['password']."'/>";
		echo "<input type='hidden' id='token' value='".$_SESSION['token']."'/>";

		$drops = $_SESSION['drops']['drops'];
		$barcodes = $_SESSION['drops']['barcodes'];

		$newdrops = array();

		for($i=0;$i<count($drops);$i++)
		{
			$quantity = (int)$drops[$i]->quantity;
		
			for($j=0;$j<$quantity;$j++)
			{
				$barcode = getBarcodes($barcodes);
				$drop = array("barcode"=>$barcode['barcode'],"location"=>array("latitude"=>$drops[$i]->lat,"longitude"=>$drops[$i]->lng ));
				$barcodes = $barcode['barcodes'];
				array_push($newdrops, $drop);
			}
		}

		$product = array(
		"visibility"=>"Visible",
		"creator"=>"",
		"state"=>"Unknown",
		"notes"=>/*json_encode(array("newdrops"=>$newdrops))*/'',

		"name"=>$_SESSION['campaign']['name'],
		"message"=>$_SESSION['campaign']['description'],
		"photoURL"=>($_SESSION['campaign']['vendor']==-1)?$protocol.$_SERVER['SERVER_NAME']."/template/".$_SESSION['campaign']['image']:$_SESSION['campaign']['image'],
		"termsAndConditions"=>$_SESSION['campaign']['terms'],
		"category"=>$_SESSION['company']['businesscategory'],
		"vendor"=>( ( $_SESSION['campaign']['vendor'] == -1 )?null:$_SESSION['campaign']['vendor'] ),
		"amount"=>floatval(str_replace('$', '', $_SESSION['drops']['offer']) ),
		"expiration"=>"2027-04-15T23:00:26.669Z",
		"howToRedeem"=>$_SESSION['drops']['redemption_details']);


		include_once "../../mvc/model/swagger.php";

		/*setting up swagger configuration to session
		  should only call once after login
		*/
		$swagger = new _swagger();

		$api_client = $swagger->init($_SESSION['token']);

		$identifier;
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\Body14;

		$body['product_and_drops'] = array("product"=>$product,"drops"=>$newdrops);


		try{
			$createDraftProduct = $api->createDraftProduct($body);
			
			$p_identifier = $createDraftProduct['product_and_drops']['product']['identifier'];
			

			$body1 = new Swagger\Client\Model\Body16;

			$body1['product_identifier'] = $p_identifier;
			$body1['payment_confirmation'] = array("nonce"=>/*$_POST['BTnonce']*/'fake-valid-nonce');

		/*	echo "<pre>";
			print_r($body1);
			echo "</pre>";*/
			
				
			try{

				$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");

				/*echo $timeofrequest;*/
				$buyAndPublishDraftProduct = $api->buyAndPublishDraftProduct($body1);
				/*echo "<pre>";
				print_r($buyAndPublishDraftProduct);
				echo "</pre>";*/

			} catch(Exception $e){
				echo"<pre>";
				print_r($e);
				echo"</pre>";
			}
			

		} catch(Exception $e){

			echo"<pre>";
			print_r($e);
			echo"</pre>";
		}


		/*$campaign = $user->createDraftProduct($product,$newdrops);
		$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");*/

		/*echo "timeofrequest ".$timeofrequest;
		echo "<pre>";
		print_r($campaign);
		echo "</pre>";*/
		
		//$send_email = $user->sendEmail($_SESSION['company']['email'], $_SESSION['company']['name'], "admin@giftdrop.com", "GiftDrop Admin", "title", "body", "contentType");
		
		/*$sendEmail;
		$createDraftProduct;
		$swagger = new _swagger();
		$api_client = $swagger->init($_SESSION['token']);*/

		/*$swagger = new _swagger();
		$api_client = $swagger->init($_SESSION['token']);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);

		$body = new Swagger\Client\Model\Body14;

		$createDraftProductBody['product_and_drops'] = $newdrops;
		$body['product_and_drops'] = array("product"=>$product,"drops"=>$newdrops);
		
		echo"<pre>";
		print_r($body);
		echo"</pre>";

		$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
		echo "timeofrequest: ".$timeofrequest;

		try{
			$createDraftProduct = $api->createDraftProduct($createDraftProductBody);
			echo"<pre>";
			print_r($createDraftProduct);
			echo"</pre>";
		} catch(Exception $e){

			 echo"<pre>";
			print_r($e);
			echo"</pre>";
		}
		/*
		/*echo "<pre>";
		print_r($product);
		echo "</pre>";

		echo "<pre>";
		print_r($newdrops);
		echo "</pre>";

		echo "<pre>";
		print_r($body);
		echo "</pre>";*/
		/*
		
		/*$sendEmailBody = new Swagger\Client\Model\Body18;
		$sendEmailBody = array(
			"from"=>array(
				"address"=>$_SESSION['company']['email'],
				"name"=>$_SESSION['company']['name']
			),
			"to"=>array(
				"address"=>"string",
				"name"=>"string"
			),
			"title"=>"string",
			"body"=>"string",
			"content_type"=>"PlainText"
		);
		try{
			$sendEmail = $api->sendEmail($sendEmailBody);
			echo"<pre>";
			print_r($sendEmail);
			echo"</pre>";
		} catch(Exception $e){
			echo 'Exception when calling BusinessServiceApi->sendEmail: ', $e->getMessage(), PHP_EOL;
		}*/

		$_SESSION['completed'] = 1;

		unset($_SESSION['user']);
		unset($_SESSION['company']);
		unset($_SESSION['hasCompany']);
		unset($_SESSION['campaign']);
		unset($_SESSION['drops']);

	}

	function getBarcodes($barcodes){
		if(isset($barcodes[0])){
			$barcode = $barcodes[0];
			array_splice($barcodes, 0, 1);
			return array("barcode"=>$barcode,"barcodes"=>$barcodes);
		}
	}
	
	?>
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full" >
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/template/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">HOW IT WORKS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
			</div>
			
			<div class="gftnow-bg-confirm">
				<div class="mask"></div>
			</div>
			

			<div class="gftnow-panel" id="confirm-panel" style="width:60%;">
				<center>
					<img src="assets/img/mini-confirm.png" /> <br/><br/>
					<span class="gftnow-font-medium gftnow-font-18">AWESOME!</span>
					<br/><br/>
					<span class="gftnow-font-medium">Your campaigne is almost ready to launch to the world</span>
					<br/><br/><br/><br/><br/><br/><br/>
					
					We sent you an email with the link to confirm your account and get into the Dashboard<br/>
were you’ll be able to make your campaign live.
					<br/><br/><br/><br/>
					
					<br/><br/>
					
					<span class="gftnow-text-default">Didn’t get the email?</span><br/>
					<a id="resent" class="text-warning gftnow-font-medium" href="javascript:void(0);">re-sent conformation email</a>

					<br/><br/>
					<span class="gftnow-text-default">Whats next?</span><br/>
					<a href="javascript:void(0);" class="text-warning gftnow-font-medium" onclick="goto()">Go to Your Dashboard</a>
				</center>
			</div>

			<br/><br/><br/><br/>

			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>
		
		<script type="text/javascript">


			$(document).ready(function() {	

				$("#resent").click(function(){
					var user = firebase.auth().currentUser;

					user.sendEmailVerification().then(function() {
			        	$("#gftnow-alert-modal-content").text("Password reset email sent successfully");
						$("#gftnow-alert-modal").modal("show");
					}, function(error) {
			        	$("#gftnow-alert-modal-content").text("Error sending password reset email:" +error);
						$("#gftnow-alert-modal").modal("show");
					});
				});

				$("#close-confirm-modal").bind("click", function(){
					$("#confirm-modal").fadeOut("fast");
				});

				jQuery(document).ajaxStart(function(){
					$("#mask2").hide();
				});

				jQuery(document).ajaxComplete(function(){	
					$("#mask2").hide();
				});


					
			});

			function goto(){
				var user = firebase.auth().currentUser;

				user.getToken().then(function(data) {
				      
	    			var token = data;
							
					jQuery.ajax({
						url: "mvc/controller/ajaxController.php",
						type: "post",
						dataType : "json",
						data: { func: "ajaxLogin",token:token },
						success: function(data){
							console.log(data);
							if(data.result="ok"){
								window.location = "index.php?p=business/profile";
							}else{
								console.log("cant login");
							}
						},error: function(err){
							console.log(err.responseText);
						}
					});
	  	  			

    			});


			}


		</script>

		

	</body>

</html>