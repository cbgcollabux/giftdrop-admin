<?php 
	include_once "header.php";
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	if(!isset($_SESSION['start'])){
		session_destroy();
		header('Location: business.php?expired_onboarding=true');
	}
	
	$companycategories;
    $swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	try {
	    $companycategories = $api->listCompanyCategories();

	} catch (Exception $e) {
		
		header('Location: business.php?expired_onboarding=true');
	    //echo 'Exception when calling BusinessServiceApi->listCompanyCategories: ', $e->getMessage(), PHP_EOL;
	}
	
    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
?>
	<body style="background-color: #F4F5F8;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">HOW IT WORKS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
				
			</div>
			
			<div class="text-center section row">
				<div class="col-md-12">
					
					<h2 class="gftnow-title">
						About Your Business
					</h2>
					<center>
						Let’s make it happen! What would you like to promote?
					</center>
					
					
					
					
					<br/><br/>
				</div>

				<div class="col-md-12">
					<center>
						
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-3 gftnow-nopadding">
									<div class="onboarding onboarding-left onboarding-0-left" style="height:1200px;margin-top:0;">
										<br/><br/><br/><br/><br/><br/><br/><br/>
										<div class="onboarding-content">
											<div class="wizard">
												<div class="numberCircle active">1</div>
												<div class="numberCircle">2</div>
												<div class="numberCircle">3</div>
											</div>
											<br><br>
											<center>
												<div  class="onboarding-icon">
													<img src="assets/img/icon-house.png" style="width: 80px;height:auto;">
												</div>
											</center>
											<br><br>
												<p><center>
													<h3>Business Information</h3>
													<span class='gftnow-font-14'>This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
												</center></p>
											<br>
										</div>
									</div>
									<br/><br/>
								</div>
						
									<div class="col-md-7 gftnow-nopadding">
										<div class="onboarding onboarding-right" style="height:1200px;margin-top:0;background: url(assets/img/left-shadow.png) left;background-repeat:repeat-y;background-color: #fff;">
											<div class="onboarding-content row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<br/><br/>
													
													<div class="text-left">
														<p>Most Popular</p>
													</div>

													<div class="row">
														<?php
														   	foreach (array_slice($companycategories['product_categories'],1,8) as $companycategory) {
														   		echo
														   		'<div class="col-md-4 col-sm-6 col-xs-12" style="padding: 5px;">
																	<div class="uploadLogo uploadLogo2" data-value="'.$companycategory['identifier'].'">
																		<div class="img">
																			<img src="'.$protocol.''.$companycategory['picture_url'].'"/>
																		</div>
																		<div class="label2">
																			<h4>'.$companycategory['name'].'</h4>
																		</div>
																	</div>
													    		</div>';
														   	}
															foreach (array_slice($companycategories['product_categories'],10,13) as $companycategory) {
														   		echo
														   		'<div class="col-md-4 col-sm-6 col-xs-12" style="padding: 5px;">
																	<div class="uploadLogo uploadLogo2" data-value="'.$companycategory['identifier'].'">
																		<div class="img">
																			<img src="'.$protocol.''.$companycategory['picture_url'].'"/>
																		</div>
																		<div class="label2">
																			<h4>'.$companycategory['name'].'</h4>
																		</div>
																	</div>
													    		</div>';
														   	}
														   	foreach (array_slice($companycategories['product_categories'],9,-3) as $companycategory) {
														   		echo
														   		'<div class="col-md-4 col-sm-6 col-xs-12" style="padding: 5px;">
																	<div class="uploadLogo uploadLogo2" data-value="'.$companycategory['identifier'].'">
																		<div class="img">
																			<img src="'.$protocol.''.$companycategory['picture_url'].'"/>
																		</div>
																		<div class="label2">
																			<h4>'.$companycategory['name'].'</h4>
																		</div>
																	</div>
													    		</div>';
														   	}
														?>
													</div>
													<br/><br/>
													<div class="pull-right">
														<form method="post" action="onboarding-1-1.php" id="form1">
															<input type="hidden" name="businesscategory" id="businesscategory" value="<?php if(isset($_SESSION['company']['businesscategory']))echo $_SESSION['company']['businesscategory'];?>"/>
															<button id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
														</form>
														
													</div>
													
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</div>
								
								<div class="col-md-1"></div>
							</div>
						
					</center>
				</div>
			</div>
			
			<br/><br/>

			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>

		<div class="gftnow-mask" id="mask1">
			<div class="gftnow-modal">
				<div class="gftnow-modal-header text-center">
					<b>Logo</b> <br/><br/>
					<p>Move your image around to fit the orange box</p>
				</div>
				<div class="gftnow-modal-body">
					<img id="croppie-crop-icon" src="assets/img/mini-croppie-icon.png"/>
					<div id="croppie"></div>
					<br/>
					<center>
						<button class="gftnow-btn gftnow-btn-success" id="croppie-done">DONE</button>
					</center>
					<br/>
				</div>
			</div>
		</div>
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">

			
			$(document).ready(function() {	

				$(".uploadLogo.uploadLogo2").each(function(){
					if($("#businesscategory").val() == $(this).attr("data-value") )
						$(this).addClass("active");
				});

				checkvalidation();

				function checkvalidation()
				{
					var err = 0;

					if( $("#businesscategory").val() == "" )
						err++;

				

					if(err==0){
						$("#submit").removeClass("gftnow-btn-default");
						$("#submit").addClass("gftnow-btn-success");
					}else{
						$("#submit").addClass("gftnow-btn-default");
						$("#submit").removeClass("gftnow-btn-success");
					}
				}
				

				 $(".uploadLogo2").click(function(){
				 	$(".uploadLogo2").removeClass("active");
				 	$(this).addClass("active");
				 	$("#businesscategory").val( $(this).attr('data-value') );
				 	checkvalidation();
				 });

				 $("#form1").validate({
					 	
					 	submitHandler: function(form){
					 		if( $("#businesscategory").val() !='' ){
					 			form.submit();
					 		}else{
					 			$("#gftnow-alert-modal-content").text("Please select business category");
								$("#gftnow-alert-modal").modal("show");
					 		}
					 	}
					 });

				 $(".uploadLogo.uploadLogo2[data-value='"+$("#businesscategory").val()+"']").addClass("active");

			});

		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>