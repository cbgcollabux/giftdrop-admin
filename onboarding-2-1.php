<?php
include_once "header.php";
include_once "mvc/model/user.php"; 
include_once "mvc/model/swagger.php";

	
	if(!isset($_SESSION['start'])){
		session_destroy();
		header('Location: business.php?expired_onboarding=true');
	}

	if ($_SESSION['timeout'] + 30 * 60 < time()) {
		session_destroy();
		header('Location: business.php?expired_onboarding=true');
	}
	if(isset($_POST['website']))
	{
		$_SESSION['company']['website'] = $_POST['website'];
		$_SESSION['company']['about'] = $_POST['about'];
		$_SESSION['company']['phone'] = json_encode(array("phone"=>$_POST['phone'],"firstname"=>$_POST['firstname'],"lastname"=>$_POST['lastname']) );
		$_SESSION['company']['address'] = $_POST['address'];
		$_SESSION['company']['city'] = $_POST['city'];
		$_SESSION['company']['zip'] = $_POST['zip'];
		$_SESSION['company']['state'] = $_POST['state'];
		$_SESSION['company']['firstname'] = $_POST['firstname'];
		$_SESSION['company']['lastname'] = $_POST['lastname'];

		$user = new user();
		$raw = array("state"=>2,"notes"=>$_SESSION['company']['phone'],"name"=>$_SESSION['company']['name'], "account"=>array("state"=>2,"notes"=>$_SESSION['company']['phone'],"logoURL"=>$_SESSION['company']['logoURL'], "website"=>$_SESSION['company']['website'],"about"=>$_SESSION['company']['about'],"title"=>$_SESSION['company']['phone']), "address"=>$_SESSION['company']['address'].", ".$_SESSION['company']['city'].", ".$_SESSION['company']['state']." ".$_SESSION['company']['zip'],"category"=>$_SESSION['company']['businesscategory'] );
		
		$company = $user->createMyCompany($raw);

		/*echo "<pre>";
		print_r($company);
		echo "</pre>";
*/
		/*echo "<pre>";
		print_r($company['result']);
		echo "</pre>";*/


		/*$swagger = new _swagger();
		$api_client = $swagger->init($_SESSION['token']);
		$api = new Swagger\Client\Api\AccountServiceApi($api_client);

		$body = new Swagger\Client\Model\UpdateUserRequest;
		$body['user_info'] = array("identifier"=>$company['result']->identifier,
			"first_name"=>$_SESSION['company']['firstname'],
			"last_name"=>$_SESSION['company']['lastname'],
			"account"=>array("identifier"=>$company['result']->account->identifier,
				"notes"=>$company['result']->account->notes));

		echo "<pre>";
		print_r($body);
		echo "</pre>";

		try {
		    $changeUserInfo = $api->changeUserInfo();

		   

		    echo "<pre>";
			print_r($changeUserInfo);
			echo "</pre>";

		} catch (Exception $e) {
			
			$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
			echo "timeofrequest: ".$timeofrequest;

			echo "<pre>";
			print_r($e->getMessage());
			echo "</pre>";
			//header('Location: business.php?expired_onboarding=true');
		    //echo 'Exception when calling BusinessServiceApi->listCompanyCategories: ', $e->getMessage(), PHP_EOL;
		}*/



		/*$swagger = new _swagger();

		$api_client = $swagger->init($_SESSION['token']);
		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\CompanyProfile;
		$body['name'] = $_SESSION['company']['name'];
		$body['state'] = "Disabled";
		$body['address'] = $_SESSION['company']['address'];
		$body['category'] = $_SESSION['company']['businesscategory'];
		$body['account'] = array("identifier"=>"",
			"logo_url"=>$_SESSION['company']['logoURL'],
			"state"=>"Disabled",
			"about"=>$_SESSION['company']['about'],
			"website"=>$_SESSION['company']['website'],
			"notes"=>"",
			"title"=>"");

		echo "<pre>";
		print_r($body);
		echo "</pre>";
		try {
		    $createMyCompany = $api->createMyCompany();

		    echo "<pre>";
			print_r($createMyCompany);
			echo "</pre>";

		} catch (Exception $e) {
			
			echo "<pre>";
			print_r($e->getMessage());
			echo "</pre>";
			//header('Location: business.php?expired_onboarding=true');
		    //echo 'Exception when calling BusinessServiceApi->listCompanyCategories: ', $e->getMessage(), PHP_EOL;
		}*/

		/*echo "<pre>";
		print_r($raw);
		echo "</pre>";

		echo "<pre>";
		print_r($company);
		echo "</pre>";*/

		if($company['response']==200){

			unset($_SESSION['start']);
			session_unset();
		}
		

	}else if(isset($_SESSION['company'])){
		
	}else{
		header('Location: business.php?expired_onboarding=true');
	}

	?>
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full" >
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">HOW IT WORKS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
			</div>
			
			<div class="gftnow-bg-confirm">
				<div class="mask"></div>
			</div>
			

			<div class="gftnow-panel" id="confirm-panel" style="width:60%;">
				<center>
					<img src="assets/img/mini-confirm.png" /> <br/><br/>
					<span class="gftnow-font-medium gftnow-font-18">AWESOME!</span>
					<br/><br/>
					<!-- <span class="gftnow-font-medium">We will send you an email with the link to confirm that your account is approve and get you into the Dashboard<br/>
					were you’ll be able to make your campaign live.</span> -->
					<br/><br/><br/><br/><br/><br/><br/>
					
					You will receive a confirmation email for login to your Dashboard.
					<br/><br/><br/><br/><br/><br/><br/>
					
					<br/><br/>
					
					<!-- <span class="gftnow-text-default">Didn’t get the email?</span><br/>
					<a id="resent" class="text-warning gftnow-font-medium" href="javascript:void(0);">re-sent conformation email</a>
					<br/><br/>-->

					<!-- <span class="gftnow-text-default">Whats next?</span><br/>
					<a href="business.php" class="text-warning gftnow-font-medium">Go to Login</a> -->
					
					<!-- <a href="javascript:void(0);" class="text-warning gftnow-font-medium" onclick="goto()">Go to Your Dashboard</a> -->
				</center>
			</div>

			<br/><br/><br/><br/>

			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>
		
		<script type="text/javascript">


			$(document).ready(function() {	

				$("#resent").click(function(){
					var user = firebase.auth().currentUser;

					user.sendEmailVerification().then(function() {
			        	$("#gftnow-alert-modal-content").text("Password reset email sent successfully");
						$("#gftnow-alert-modal").modal("show");
					}, function(error) {
			        	$("#gftnow-alert-modal-content").text("Error sending password reset email:" +error);
						$("#gftnow-alert-modal").modal("show");
					});
				});

				$("#close-confirm-modal").bind("click", function(){
					$("#confirm-modal").fadeOut("fast");
				});

				jQuery(document).ajaxStart(function(){
					$("#mask2").hide();
				});

				jQuery(document).ajaxComplete(function(){	
					$("#mask2").hide();
				});


					
			});

			function goto(){
				var user = firebase.auth().currentUser;

				user.getToken().then(function(data) {
				      
	    			var token = data;
							
					jQuery.ajax({
						url: "mvc/controller/ajaxController.php",
						type: "post",
						dataType : "json",
						data: { func: "ajaxLogin",token:token },
						success: function(data){
							console.log(data);
							if(data.result="ok"){
								window.location = "index.php?p=business/profile";
							}else{
								console.log("cant login");
							}
						},error: function(err){
							console.log(err.responseText);
						}
					});
	  	  			

    			});


			}


		</script>

		

	</body>

</html>