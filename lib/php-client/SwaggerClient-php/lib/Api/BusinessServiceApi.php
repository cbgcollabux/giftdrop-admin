<?php
/**
 * BusinessServiceApi
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gftnow REST API specification
 *
 * This is the list of all APIs provided by gftnow backend.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Api;

use \Swagger\Client\ApiClient;
use \Swagger\Client\ApiException;
use \Swagger\Client\Configuration;
use \Swagger\Client\ObjectSerializer;

/**
 * BusinessServiceApi Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class BusinessServiceApi
{
    /**
     * API Client
     *
     * @var \Swagger\Client\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Swagger\Client\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Swagger\Client\ApiClient $apiClient = null)
    {
        if ($apiClient === null) {
            $apiClient = new ApiClient();
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Swagger\Client\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Swagger\Client\ApiClient $apiClient set the API client
     *
     * @return BusinessServiceApi
     */
    public function setApiClient(\Swagger\Client\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation buyDraftProduct
     *
     * Make a payment for the draft campaign
     *
     * @param \Swagger\Client\Model\BuyDraftProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    public function buyDraftProduct($body = null)
    {
        list($response) = $this->buyDraftProductWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation buyDraftProductWithHttpInfo
     *
     * Make a payment for the draft campaign
     *
     * @param \Swagger\Client\Model\BuyDraftProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function buyDraftProductWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/buyDraftProduct";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/businessService/v1/buyDraftProduct'
            );

            return [null, $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }

    /**
     * Operation changeMyCompany
     *
     * Updates some fields for current business user
     *
     * @param \Swagger\Client\Model\CompanyProfile $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    public function changeMyCompany($body = null)
    {
        list($response) = $this->changeMyCompanyWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation changeMyCompanyWithHttpInfo
     *
     * Updates some fields for current business user
     *
     * @param \Swagger\Client\Model\CompanyProfile $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function changeMyCompanyWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/changeMyCompany";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/businessService/v1/changeMyCompany'
            );

            return [null, $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }

    /**
     * Operation collectMyCompanyStatistics
     *
     * Returns statistics for current business user based on selected time period
     *
     * @param \Swagger\Client\Model\Period $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\CompanyStatisticsResponse
     */
    public function collectMyCompanyStatistics($body = null)
    {
        list($response) = $this->collectMyCompanyStatisticsWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation collectMyCompanyStatisticsWithHttpInfo
     *
     * Returns statistics for current business user based on selected time period
     *
     * @param \Swagger\Client\Model\Period $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\CompanyStatisticsResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function collectMyCompanyStatisticsWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/collectMyCompanyStatistics";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\CompanyStatisticsResponse',
                '/businessService/v1/collectMyCompanyStatistics'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\CompanyStatisticsResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\CompanyStatisticsResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation createDraftProduct
     *
     * Creates a draft and unpaid campaign for current business account
     *
     * @param \Swagger\Client\Model\CreateDraftProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProductResponse
     */
    public function createDraftProduct($body = null)
    {
        list($response) = $this->createDraftProductWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation createDraftProductWithHttpInfo
     *
     * Creates a draft and unpaid campaign for current business account
     *
     * @param \Swagger\Client\Model\CreateDraftProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProductResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function createDraftProductWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/createDraftProduct";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProductResponse',
                '/businessService/v1/createDraftProduct'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProductResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProductResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation createMyCompany
     *
     * Creates new company for business user and returns CompanyProfile
     *
     * @param \Swagger\Client\Model\CompanyProfile $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\CompanyProfile
     */
    public function createMyCompany($body = null)
    {
        list($response) = $this->createMyCompanyWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation createMyCompanyWithHttpInfo
     *
     * Creates new company for business user and returns CompanyProfile
     *
     * @param \Swagger\Client\Model\CompanyProfile $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\CompanyProfile, HTTP status code, HTTP response headers (array of strings)
     */
    public function createMyCompanyWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/createMyCompany";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\CompanyProfile',
                '/businessService/v1/createMyCompany'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\CompanyProfile', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\CompanyProfile', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation findMyCompanyProfile
     *
     * Returns CompanyProfile that belongs to current user
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\CompanyProfile
     */
    public function findMyCompanyProfile()
    {
        list($response) = $this->findMyCompanyProfileWithHttpInfo();
        return $response;
    }

    /**
     * Operation findMyCompanyProfileWithHttpInfo
     *
     * Returns CompanyProfile that belongs to current user
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\CompanyProfile, HTTP status code, HTTP response headers (array of strings)
     */
    public function findMyCompanyProfileWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/businessService/v1/findMyCompanyProfile";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);


        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\CompanyProfile',
                '/businessService/v1/findMyCompanyProfile'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\CompanyProfile', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\CompanyProfile', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation findProduct
     *
     * Returns campaign info by its identifier
     *
     * @param \Swagger\Client\Model\FindProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProductResponse
     */
    public function findProduct($body = null)
    {
        list($response) = $this->findProductWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation findProductWithHttpInfo
     *
     * Returns campaign info by its identifier
     *
     * @param \Swagger\Client\Model\FindProductRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProductResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function findProductWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/findProduct";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProductResponse',
                '/businessService/v1/findProduct'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProductResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProductResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation listCompanyCategories
     *
     * Returns a list of available categories for the business account
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\CompanyCategoryListResponse
     */
    public function listCompanyCategories()
    {
        list($response) = $this->listCompanyCategoriesWithHttpInfo();
        return $response;
    }

    /**
     * Operation listCompanyCategoriesWithHttpInfo
     *
     * Returns a list of available categories for the business account
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\CompanyCategoryListResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function listCompanyCategoriesWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/businessService/v1/listCompanyCategories";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);


        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\CompanyCategoryListResponse',
                '/businessService/v1/listCompanyCategories'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\CompanyCategoryListResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\CompanyCategoryListResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation listGiftCards
     *
     * Returns the list of all available gift cards that can be purchased by user
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\GiftCardCatalogView
     */
    public function listGiftCards()
    {
        list($response) = $this->listGiftCardsWithHttpInfo();
        return $response;
    }

    /**
     * Operation listGiftCardsWithHttpInfo
     *
     * Returns the list of all available gift cards that can be purchased by user
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\GiftCardCatalogView, HTTP status code, HTTP response headers (array of strings)
     */
    public function listGiftCardsWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/businessService/v1/listGiftCards";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);


        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\GiftCardCatalogView',
                '/businessService/v1/listGiftCards'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\GiftCardCatalogView', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\GiftCardCatalogView', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation listMyCompanyProducts
     *
     * Returns all the campaigns created from current account
     *
     * @param \Swagger\Client\Model\Period $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\ProductListResponse
     */
    public function listMyCompanyProducts($body = null)
    {
        list($response) = $this->listMyCompanyProductsWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation listMyCompanyProductsWithHttpInfo
     *
     * Returns all the campaigns created from current account
     *
     * @param \Swagger\Client\Model\Period $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\ProductListResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function listMyCompanyProductsWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/listMyCompanyProducts";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\ProductListResponse',
                '/businessService/v1/listMyCompanyProducts'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\ProductListResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\ProductListResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation requestTotal
     *
     * During checkout, returns total amount that business will be charged based
     *
     * @param \Swagger\Client\Model\TotalRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\TotalResponse
     */
    public function requestTotal($body = null)
    {
        list($response) = $this->requestTotalWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation requestTotalWithHttpInfo
     *
     * During checkout, returns total amount that business will be charged based
     *
     * @param \Swagger\Client\Model\TotalRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of \Swagger\Client\Model\TotalResponse, HTTP status code, HTTP response headers (array of strings)
     */
    public function requestTotalWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/requestTotal";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Swagger\Client\Model\TotalResponse',
                '/businessService/v1/requestTotal'
            );

            return [$this->apiClient->getSerializer()->deserialize($response, '\Swagger\Client\Model\TotalResponse', $httpHeader), $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Swagger\Client\Model\TotalResponse', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation sendEmail
     *
     * Sending out an email from our noreply address
     *
     * @param \Swagger\Client\Model\SendEmailRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    public function sendEmail($body = null)
    {
        list($response) = $this->sendEmailWithHttpInfo($body);
        return $response;
    }

    /**
     * Operation sendEmailWithHttpInfo
     *
     * Sending out an email from our noreply address
     *
     * @param \Swagger\Client\Model\SendEmailRequest $body  (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return array of null, HTTP status code, HTTP response headers (array of strings)
     */
    public function sendEmailWithHttpInfo($body = null)
    {
        // parse inputs
        $resourcePath = "/businessService/v1/sendEmail";
        $httpBody = '';
        $queryParams = [];
        $headerParams = [];
        $formParams = [];
        $_header_accept = $this->apiClient->selectHeaderAccept(['application/json']);
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(['application/json']);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/businessService/v1/sendEmail'
            );

            return [null, $statusCode, $httpHeader];
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }
}
