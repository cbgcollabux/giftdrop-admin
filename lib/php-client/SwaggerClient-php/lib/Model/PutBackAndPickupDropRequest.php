<?php
/**
 * PutBackAndPickupDropRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gftnow REST API specification
 *
 * This is the list of all APIs provided by gftnow backend.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * PutBackAndPickupDropRequest Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PutBackAndPickupDropRequest implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'PutBackAndPickupDropRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'pickup_product' => 'string',
        'user_location' => '\Swagger\Client\Model\PhysicalLocation',
        'pickup_location' => '\Swagger\Client\Model\PhysicalLocation',
        'put_back_drop' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'pickup_product' => 'uuid',
        'user_location' => null,
        'pickup_location' => null,
        'put_back_drop' => 'uuid'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'pickup_product' => 'pickupProduct',
        'user_location' => 'userLocation',
        'pickup_location' => 'pickupLocation',
        'put_back_drop' => 'putBackDrop'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'pickup_product' => 'setPickupProduct',
        'user_location' => 'setUserLocation',
        'pickup_location' => 'setPickupLocation',
        'put_back_drop' => 'setPutBackDrop'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'pickup_product' => 'getPickupProduct',
        'user_location' => 'getUserLocation',
        'pickup_location' => 'getPickupLocation',
        'put_back_drop' => 'getPutBackDrop'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['pickup_product'] = isset($data['pickup_product']) ? $data['pickup_product'] : null;
        $this->container['user_location'] = isset($data['user_location']) ? $data['user_location'] : null;
        $this->container['pickup_location'] = isset($data['pickup_location']) ? $data['pickup_location'] : null;
        $this->container['put_back_drop'] = isset($data['put_back_drop']) ? $data['put_back_drop'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        return true;
    }


    /**
     * Gets pickup_product
     * @return string
     */
    public function getPickupProduct()
    {
        return $this->container['pickup_product'];
    }

    /**
     * Sets pickup_product
     * @param string $pickup_product
     * @return $this
     */
    public function setPickupProduct($pickup_product)
    {
        $this->container['pickup_product'] = $pickup_product;

        return $this;
    }

    /**
     * Gets user_location
     * @return \Swagger\Client\Model\PhysicalLocation
     */
    public function getUserLocation()
    {
        return $this->container['user_location'];
    }

    /**
     * Sets user_location
     * @param \Swagger\Client\Model\PhysicalLocation $user_location
     * @return $this
     */
    public function setUserLocation($user_location)
    {
        $this->container['user_location'] = $user_location;

        return $this;
    }

    /**
     * Gets pickup_location
     * @return \Swagger\Client\Model\PhysicalLocation
     */
    public function getPickupLocation()
    {
        return $this->container['pickup_location'];
    }

    /**
     * Sets pickup_location
     * @param \Swagger\Client\Model\PhysicalLocation $pickup_location
     * @return $this
     */
    public function setPickupLocation($pickup_location)
    {
        $this->container['pickup_location'] = $pickup_location;

        return $this;
    }

    /**
     * Gets put_back_drop
     * @return string
     */
    public function getPutBackDrop()
    {
        return $this->container['put_back_drop'];
    }

    /**
     * Sets put_back_drop
     * @param string $put_back_drop
     * @return $this
     */
    public function setPutBackDrop($put_back_drop)
    {
        $this->container['put_back_drop'] = $put_back_drop;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


