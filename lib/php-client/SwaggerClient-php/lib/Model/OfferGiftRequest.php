<?php
/**
 * OfferGiftRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gftnow REST API specification
 *
 * This is the list of all APIs provided by gftnow backend.
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * OfferGiftRequest Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OfferGiftRequest implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'OfferGiftRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'drop_identifier' => 'string',
        'contact' => '\Swagger\Client\Model\ContactIdentifier',
        'message' => 'string',
        'sender_visibility' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'drop_identifier' => 'uuid',
        'contact' => null,
        'message' => null,
        'sender_visibility' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'drop_identifier' => 'dropIdentifier',
        'contact' => 'contact',
        'message' => 'message',
        'sender_visibility' => 'senderVisibility'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'drop_identifier' => 'setDropIdentifier',
        'contact' => 'setContact',
        'message' => 'setMessage',
        'sender_visibility' => 'setSenderVisibility'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'drop_identifier' => 'getDropIdentifier',
        'contact' => 'getContact',
        'message' => 'getMessage',
        'sender_visibility' => 'getSenderVisibility'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const SENDER_VISIBILITY_UNKNOWN = 'Unknown';
    const SENDER_VISIBILITY_VISIBLE = 'Visible';
    const SENDER_VISIBILITY_INCOGNITO = 'Incognito';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getSenderVisibilityAllowableValues()
    {
        return [
            self::SENDER_VISIBILITY_UNKNOWN,
            self::SENDER_VISIBILITY_VISIBLE,
            self::SENDER_VISIBILITY_INCOGNITO,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['drop_identifier'] = isset($data['drop_identifier']) ? $data['drop_identifier'] : null;
        $this->container['contact'] = isset($data['contact']) ? $data['contact'] : null;
        $this->container['message'] = isset($data['message']) ? $data['message'] : null;
        $this->container['sender_visibility'] = isset($data['sender_visibility']) ? $data['sender_visibility'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        $allowed_values = $this->getSenderVisibilityAllowableValues();
        if (!in_array($this->container['sender_visibility'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'sender_visibility', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        $allowed_values = $this->getSenderVisibilityAllowableValues();
        if (!in_array($this->container['sender_visibility'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets drop_identifier
     * @return string
     */
    public function getDropIdentifier()
    {
        return $this->container['drop_identifier'];
    }

    /**
     * Sets drop_identifier
     * @param string $drop_identifier
     * @return $this
     */
    public function setDropIdentifier($drop_identifier)
    {
        $this->container['drop_identifier'] = $drop_identifier;

        return $this;
    }

    /**
     * Gets contact
     * @return \Swagger\Client\Model\ContactIdentifier
     */
    public function getContact()
    {
        return $this->container['contact'];
    }

    /**
     * Sets contact
     * @param \Swagger\Client\Model\ContactIdentifier $contact
     * @return $this
     */
    public function setContact($contact)
    {
        $this->container['contact'] = $contact;

        return $this;
    }

    /**
     * Gets message
     * @return string
     */
    public function getMessage()
    {
        return $this->container['message'];
    }

    /**
     * Sets message
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->container['message'] = $message;

        return $this;
    }

    /**
     * Gets sender_visibility
     * @return string
     */
    public function getSenderVisibility()
    {
        return $this->container['sender_visibility'];
    }

    /**
     * Sets sender_visibility
     * @param string $sender_visibility
     * @return $this
     */
    public function setSenderVisibility($sender_visibility)
    {
        $allowed_values = $this->getSenderVisibilityAllowableValues();
        if (!is_null($sender_visibility) && !in_array($sender_visibility, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'sender_visibility', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['sender_visibility'] = $sender_visibility;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


