# BarcodeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drop_identifier** | **string** |  | [optional] 
**barcode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


