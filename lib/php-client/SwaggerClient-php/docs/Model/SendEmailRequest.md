# SendEmailRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**\Swagger\Client\Model\EmailAddress**](EmailAddress.md) |  | [optional] 
**to** | [**\Swagger\Client\Model\EmailAddress**](EmailAddress.md) |  | [optional] 
**title** | **string** |  | [optional] 
**body** | **string** |  | [optional] 
**content_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


