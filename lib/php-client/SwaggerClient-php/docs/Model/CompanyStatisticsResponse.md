# CompanyStatisticsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**spendings** | [**\Swagger\Client\Model\MoneyPerPeriod[]**](MoneyPerPeriod.md) |  | [optional] 
**placed_drops** | [**\Swagger\Client\Model\CountPerPeriod[]**](CountPerPeriod.md) |  | [optional] 
**picked_drops** | [**\Swagger\Client\Model\CountPerPeriod[]**](CountPerPeriod.md) |  | [optional] 
**redeemed_drops** | [**\Swagger\Client\Model\CountPerPeriod[]**](CountPerPeriod.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


