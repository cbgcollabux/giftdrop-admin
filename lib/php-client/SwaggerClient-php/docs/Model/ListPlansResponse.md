# ListPlansResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plans** | [**\Swagger\Client\Model\Plan[]**](Plan.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


