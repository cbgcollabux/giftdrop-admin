# ProductDescription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**amount** | **float** |  | [optional] 
**name** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**how_to_redeem** | **string** |  | [optional] 
**expiration** | [**\DateTime**](\DateTime.md) |  | [optional] 
**terms_and_conditions** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


