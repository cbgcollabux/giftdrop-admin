# CountPerPeriod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**count** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


