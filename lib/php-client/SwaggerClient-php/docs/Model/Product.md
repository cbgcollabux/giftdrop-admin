# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**visibility** | **string** |  | [optional] 
**creator** | **string** |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**payment_state** | **string** |  | [optional] 
**operation_state** | **string** |  | [optional] 
**notes** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**photo_url** | **string** |  | [optional] 
**terms_and_conditions** | **string** |  | [optional] 
**vendor** | **string** |  | [optional] 
**amount** | **float** |  | [optional] 
**expiration** | [**\DateTime**](\DateTime.md) |  | [optional] 
**how_to_redeem** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


