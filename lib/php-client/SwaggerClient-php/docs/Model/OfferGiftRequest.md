# OfferGiftRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drop_identifier** | **string** |  | [optional] 
**contact** | [**\Swagger\Client\Model\ContactIdentifier**](ContactIdentifier.md) |  | [optional] 
**message** | **string** |  | [optional] 
**sender_visibility** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


