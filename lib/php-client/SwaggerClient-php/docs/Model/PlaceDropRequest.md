# PlaceDropRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drop_identifier** | **string** |  | [optional] 
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**incognito** | **string** |  | [optional] 
**visibility** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


