# CompanyCategoryListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_categories** | [**\Swagger\Client\Model\CompanyCategory[]**](CompanyCategory.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


