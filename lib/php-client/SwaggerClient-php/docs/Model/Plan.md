# Plan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**notes** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**fees** | [**\Swagger\Client\Model\Fee[]**](Fee.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


