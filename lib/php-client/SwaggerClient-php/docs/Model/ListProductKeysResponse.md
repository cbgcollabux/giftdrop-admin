# ListProductKeysResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**added_product_keys** | [**\Swagger\Client\Model\PlayerProductKey[]**](PlayerProductKey.md) |  | [optional] 
**removed_product_keys** | [**\Swagger\Client\Model\PlayerProductKey[]**](PlayerProductKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


