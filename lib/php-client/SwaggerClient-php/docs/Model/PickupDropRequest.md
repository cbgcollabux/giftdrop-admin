# PickupDropRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_identifier** | **string** |  | [optional] 
**user_location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**pickup_location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


