# DetailedCompanyProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_profile** | [**\Swagger\Client\Model\CompanyProfile**](CompanyProfile.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


