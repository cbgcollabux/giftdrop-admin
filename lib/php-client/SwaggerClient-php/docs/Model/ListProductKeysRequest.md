# ListProductKeysRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**products** | [**\Swagger\Client\Model\PlayerProductKey[]**](PlayerProductKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


