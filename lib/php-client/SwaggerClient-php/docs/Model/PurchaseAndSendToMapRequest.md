# PurchaseAndSendToMapRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_confirmation** | [**\Swagger\Client\Model\PaymentConfirmation**](PaymentConfirmation.md) |  | [optional] 
**vendor_id** | **string** |  | [optional] 
**amount** | **float** |  | [optional] 
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**sender_visibility** | **string** |  | [optional] 
**product_visibility** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


