# InventoryEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **string** |  | [optional] 
**identifier** | **string** |  | [optional] 
**instant** | [**\DateTime**](\DateTime.md) |  | [optional] 
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**other_user** | [**\Swagger\Client\Model\ShortUserInfo**](ShortUserInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


