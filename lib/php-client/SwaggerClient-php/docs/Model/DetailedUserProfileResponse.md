# DetailedUserProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**player_inventory_view** | [**\Swagger\Client\Model\PlayerInventoryView**](PlayerInventoryView.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


