# UserLocationResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locations** | [**\Swagger\Client\Model\UserLocation[]**](UserLocation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


