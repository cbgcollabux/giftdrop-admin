# GiftInventoryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drop_description** | [**\Swagger\Client\Model\DropDescription**](DropDescription.md) |  | [optional] 
**gift_event** | [**\Swagger\Client\Model\InventoryEvent**](InventoryEvent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


