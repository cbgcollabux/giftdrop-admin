# GiftCardCatalogView

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendors** | [**\Swagger\Client\Model\Vendor[]**](Vendor.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


