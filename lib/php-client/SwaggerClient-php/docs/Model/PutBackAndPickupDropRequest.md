# PutBackAndPickupDropRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pickup_product** | **string** |  | [optional] 
**user_location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**pickup_location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**put_back_drop** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


