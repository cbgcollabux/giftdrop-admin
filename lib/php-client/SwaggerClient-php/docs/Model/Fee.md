# Fee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**range_begin** | **float** |  | [optional] 
**range_end** | **float** |  | [optional] 
**fixed** | **float** |  | [optional] 
**percentage** | **float** |  | [optional] 
**card_fixed** | **float** |  | [optional] 
**card_percentage** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


