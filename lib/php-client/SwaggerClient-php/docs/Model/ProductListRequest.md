# ProductListRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query_parameters** | [**\Swagger\Client\Model\QueryParameters**](QueryParameters.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


