# ResolveGiftRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gift** | **string** |  | [optional] 
**resolution** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


