# BuyDraftProductRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_identifier** | **string** |  | [optional] 
**payment_confirmation** | [**\Swagger\Client\Model\PaymentConfirmation**](PaymentConfirmation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


