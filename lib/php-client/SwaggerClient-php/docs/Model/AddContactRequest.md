# AddContactRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_identifier** | [**\Swagger\Client\Model\ContactIdentifier**](ContactIdentifier.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


