# UserInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**account** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**notes** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**role** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**home_location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**contact_repository** | [**\Swagger\Client\Model\ContactRepository**](ContactRepository.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


