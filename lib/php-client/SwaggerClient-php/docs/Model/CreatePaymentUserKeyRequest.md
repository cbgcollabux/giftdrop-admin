# CreatePaymentUserKeyRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_provider_name** | **string** |  | [optional] 
**api_version** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


