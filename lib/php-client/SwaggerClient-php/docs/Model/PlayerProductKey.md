# PlayerProductKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_identifier** | **string** |  | [optional] 
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**cell_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


