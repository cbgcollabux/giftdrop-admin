# Vendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**creator** | **string** |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**logo_url** | **string** |  | [optional] 
**disclaimer** | **string** |  | [optional] 
**terms_and_conditions** | **string** |  | [optional] 
**valid_values** | **string** |  | [optional] 
**catalog_status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


