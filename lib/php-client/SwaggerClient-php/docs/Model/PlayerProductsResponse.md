# PlayerProductsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_identifiers** | [**map[string,\Swagger\Client\Model\PlayerProduct]**](PlayerProduct.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


