# AssignPlanToAccountsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plan_identifier** | **string** |  | [optional] 
**account_identifiers** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


