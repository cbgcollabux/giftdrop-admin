# UserLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**\Swagger\Client\Model\PhysicalLocation**](PhysicalLocation.md) |  | [optional] 
**user_identifier** | **string** |  | [optional] 
**user_name** | **string** |  | [optional] 
**last_seen** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


