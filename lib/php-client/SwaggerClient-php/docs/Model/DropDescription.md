# DropDescription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**product** | [**\Swagger\Client\Model\ProductDescription**](ProductDescription.md) |  | [optional] 
**vendor** | [**\Swagger\Client\Model\VendorDescription**](VendorDescription.md) |  | [optional] 
**deal_image_url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


