# ItemRangeQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_item_index_inclusive** | **int** |  | [optional] 
**end_item_index_exclusive** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


