# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**notes** | **string** |  | [optional] 
**logo_url** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**about** | **string** |  | [optional] 
**website** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


