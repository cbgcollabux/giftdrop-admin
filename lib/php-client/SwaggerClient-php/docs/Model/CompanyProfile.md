# CompanyProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**account** | [**\Swagger\Client\Model\Account**](Account.md) |  | [optional] 
**created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**activated** | [**\DateTime**](\DateTime.md) |  | [optional] 
**notes** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**category** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


