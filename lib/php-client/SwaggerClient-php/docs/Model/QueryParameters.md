# QueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_range_query** | [**\Swagger\Client\Model\ItemRangeQuery**](ItemRangeQuery.md) |  | [optional] 
**period_filters** | [**\Swagger\Client\Model\PeriodQuery[]**](PeriodQuery.md) |  | [optional] 
**text_filters** | [**\Swagger\Client\Model\TextQuery[]**](TextQuery.md) |  | [optional] 
**state_filters** | [**\Swagger\Client\Model\StateQuery[]**](StateQuery.md) |  | [optional] 
**sorting** | [**\Swagger\Client\Model\SortingQuery[]**](SortingQuery.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


