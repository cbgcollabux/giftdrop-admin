# UserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_info** | [**\Swagger\Client\Model\UserInfo**](UserInfo.md) |  | [optional] 
**balance** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


