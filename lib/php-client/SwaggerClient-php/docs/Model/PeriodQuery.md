# PeriodQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_name** | **string** |  | [optional] 
**period** | [**\Swagger\Client\Model\Period**](Period.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


