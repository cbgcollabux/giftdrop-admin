# OfferGiftResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gift_identifier** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


