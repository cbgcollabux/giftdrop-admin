# PaymentConfirmation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nonce** | **string** |  | [optional] 
**payment_provider_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


