# PlayerProductView

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**player_product_key** | [**\Swagger\Client\Model\PlayerProductKey**](PlayerProductKey.md) |  | [optional] 
**pickup_radius** | **float** |  | [optional] 
**sender_visibility** | **string** |  | [optional] 
**creator_identifier** | **string** |  | [optional] 
**product_name** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**deal_image_url** | **string** |  | [optional] 
**terms_and_conditions** | **string** |  | [optional] 
**amount** | **float** |  | [optional] 
**expiration** | [**\DateTime**](\DateTime.md) |  | [optional] 
**how_to_redeem** | **string** |  | [optional] 
**visibility** | **string** |  | [optional] 
**vendor_identifier** | **string** |  | [optional] 
**vendor_name** | **string** |  | [optional] 
**vendor_description** | **string** |  | [optional] 
**vendor_disclaimer** | **string** |  | [optional] 
**vendor_terms_and_conditions** | **string** |  | [optional] 
**logo_url** | **string** |  | [optional] 
**creator_title** | **string** |  | [optional] 
**creator_about** | **string** |  | [optional] 
**creator_website** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


