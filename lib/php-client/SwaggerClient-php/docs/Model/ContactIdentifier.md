# ContactIdentifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_type** | **string** |  | [optional] 
**contact** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


