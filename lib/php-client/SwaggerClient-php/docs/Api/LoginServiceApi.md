# Swagger\Client\LoginServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAdmin**](LoginServiceApi.md#createAdmin) | **POST** /loginService/v1/createAdmin | Creating user with role: Admin
[**createBusiness**](LoginServiceApi.md#createBusiness) | **POST** /loginService/v1/createBusiness | Creating user with role: Business
[**createPlayer**](LoginServiceApi.md#createPlayer) | **POST** /loginService/v1/createPlayer | Creating user with role: Player
[**login**](LoginServiceApi.md#login) | **POST** /loginService/v1/login | Logging in to backend
[**logout**](LoginServiceApi.md#logout) | **POST** /loginService/v1/logout | Log out for current user


# **createAdmin**
> \Swagger\Client\Model\UserProfile createAdmin()

Creating user with role: Admin



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginServiceApi();

try {
    $result = $api_instance->createAdmin();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginServiceApi->createAdmin: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createBusiness**
> \Swagger\Client\Model\UserProfile createBusiness()

Creating user with role: Business



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginServiceApi();

try {
    $result = $api_instance->createBusiness();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginServiceApi->createBusiness: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPlayer**
> \Swagger\Client\Model\UserProfile createPlayer()

Creating user with role: Player



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginServiceApi();

try {
    $result = $api_instance->createPlayer();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginServiceApi->createPlayer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **login**
> \Swagger\Client\Model\UserProfile login()

Logging in to backend



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginServiceApi();

try {
    $result = $api_instance->login();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginServiceApi->login: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **logout**
> logout()

Log out for current user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginServiceApi();

try {
    $api_instance->logout();
} catch (Exception $e) {
    echo 'Exception when calling LoginServiceApi->logout: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

