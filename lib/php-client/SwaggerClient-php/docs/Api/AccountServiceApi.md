# Swagger\Client\AccountServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addContact**](AccountServiceApi.md#addContact) | **POST** /accountService/v1/addContact | Adding new contact information to current user
[**changeAccountInfo**](AccountServiceApi.md#changeAccountInfo) | **POST** /accountService/v1/changeAccountInfo | Updates account information - title, notes, etc.
[**changeAccountLogoURL**](AccountServiceApi.md#changeAccountLogoURL) | **POST** /accountService/v1/changeAccountLogoURL | Uploads and saves new account picture in cloud storage and returns URL
[**changeUserInfo**](AccountServiceApi.md#changeUserInfo) | **POST** /accountService/v1/changeUserInfo | Updates account information - title, notes, etc.
[**removeAccountLogoURL**](AccountServiceApi.md#removeAccountLogoURL) | **POST** /accountService/v1/removeAccountLogoURL | Sets default account picture and returns URL


# **addContact**
> addContact($body)

Adding new contact information to current user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountServiceApi();
$body = new \Swagger\Client\Model\AddContactRequest(); // \Swagger\Client\Model\AddContactRequest | 

try {
    $api_instance->addContact($body);
} catch (Exception $e) {
    echo 'Exception when calling AccountServiceApi->addContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\AddContactRequest**](../Model/AddContactRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeAccountInfo**
> changeAccountInfo($body)

Updates account information - title, notes, etc.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountServiceApi();
$body = new \Swagger\Client\Model\UpdateAccountRequest(); // \Swagger\Client\Model\UpdateAccountRequest | 

try {
    $api_instance->changeAccountInfo($body);
} catch (Exception $e) {
    echo 'Exception when calling AccountServiceApi->changeAccountInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateAccountRequest**](../Model/UpdateAccountRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeAccountLogoURL**
> \Swagger\Client\Model\UpdateAccountLogoURLResponse changeAccountLogoURL($body)

Uploads and saves new account picture in cloud storage and returns URL



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountServiceApi();
$body = new \Swagger\Client\Model\UpdateAccountLogoURLRequest(); // \Swagger\Client\Model\UpdateAccountLogoURLRequest | 

try {
    $result = $api_instance->changeAccountLogoURL($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountServiceApi->changeAccountLogoURL: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateAccountLogoURLRequest**](../Model/UpdateAccountLogoURLRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UpdateAccountLogoURLResponse**](../Model/UpdateAccountLogoURLResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeUserInfo**
> changeUserInfo($body)

Updates account information - title, notes, etc.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountServiceApi();
$body = new \Swagger\Client\Model\UpdateUserRequest(); // \Swagger\Client\Model\UpdateUserRequest | 

try {
    $api_instance->changeUserInfo($body);
} catch (Exception $e) {
    echo 'Exception when calling AccountServiceApi->changeUserInfo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateUserRequest**](../Model/UpdateUserRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **removeAccountLogoURL**
> \Swagger\Client\Model\UpdateAccountLogoURLResponse removeAccountLogoURL()

Sets default account picture and returns URL



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountServiceApi();

try {
    $result = $api_instance->removeAccountLogoURL();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountServiceApi->removeAccountLogoURL: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UpdateAccountLogoURLResponse**](../Model/UpdateAccountLogoURLResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

