# Swagger\Client\GftServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStripeEphemeralKey**](GftServiceApi.md#createStripeEphemeralKey) | **POST** /gftService/v1/createStripeEphemeralKey | Creates payment key that uniquely identifies customer to payment provider
[**createStripeEphemeralKeyBad**](GftServiceApi.md#createStripeEphemeralKeyBad) | **POST** /gftService/v1/createStripeEphemeralKey_bad | Creates payment key that uniquely identifies customer to payment provider
[**findRecentPickup**](GftServiceApi.md#findRecentPickup) | **POST** /gftService/v1/findRecentPickup | Returns last picked up product with timestamps
[**listGiftCards**](GftServiceApi.md#listGiftCards) | **POST** /gftService/v1/listGiftCards | Returns the list of all available gift cards that can be purchased by user
[**listInventory**](GftServiceApi.md#listInventory) | **POST** /gftService/v1/listInventory | Returns a list of all drops and gifts that user dropped and picked up
[**listPendingGifts**](GftServiceApi.md#listPendingGifts) | **POST** /gftService/v1/listPendingGifts | Returns all the pending gifts that have been sent to user but never accepted/rejected
[**offerGift**](GftServiceApi.md#offerGift) | **POST** /gftService/v1/offerGift | User is sending his unredeemed drop to another user
[**pickupDrop**](GftServiceApi.md#pickupDrop) | **POST** /gftService/v1/pickupDrop | Product pickup by user if withing pickup range
[**placeDrop**](GftServiceApi.md#placeDrop) | **POST** /gftService/v1/placeDrop | User is placing his unredeemed drop to any location on a map
[**purchaseAndSendToMap**](GftServiceApi.md#purchaseAndSendToMap) | **POST** /gftService/v1/purchaseAndSendToMap | Product purchase by user and placing it to the map
[**purchaseAndSendToPlayer**](GftServiceApi.md#purchaseAndSendToPlayer) | **POST** /gftService/v1/purchaseAndSendToPlayer | Product purchase by user and gifting it to another user
[**putBack**](GftServiceApi.md#putBack) | **POST** /gftService/v1/putBack | User is returning his last picked up product back to map where it was
[**putBackAndPickupDrop**](GftServiceApi.md#putBackAndPickupDrop) | **POST** /gftService/v1/putBackAndPickupDrop | User is returning his last picked up product back to map where it was and will pickup another product that was just discovered
[**redeemDrop**](GftServiceApi.md#redeemDrop) | **POST** /gftService/v1/redeemDrop | User is redeeming his drop/gift from his inventory
[**reportProduct**](GftServiceApi.md#reportProduct) | **POST** /gftService/v1/reportProduct | Sends report about bad product to administrator
[**resolveGift**](GftServiceApi.md#resolveGift) | **POST** /gftService/v1/resolveGift | Accepting or declining of pending gifts
[**revealBarcode**](GftServiceApi.md#revealBarcode) | **POST** /gftService/v1/revealBarcode | Reveals and returns barcode for redeemed product from user inventory


# **createStripeEphemeralKey**
> \Swagger\Client\Model\CreatePaymentUserKeyResponse createStripeEphemeralKey($body)

Creates payment key that uniquely identifies customer to payment provider



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\CreatePaymentUserKeyRequest(); // \Swagger\Client\Model\CreatePaymentUserKeyRequest | 

try {
    $result = $api_instance->createStripeEphemeralKey($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->createStripeEphemeralKey: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreatePaymentUserKeyRequest**](../Model/CreatePaymentUserKeyRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreatePaymentUserKeyResponse**](../Model/CreatePaymentUserKeyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createStripeEphemeralKeyBad**
> createStripeEphemeralKeyBad($body)

Creates payment key that uniquely identifies customer to payment provider



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\CreatePaymentUserKeyRequest(); // \Swagger\Client\Model\CreatePaymentUserKeyRequest | 

try {
    $api_instance->createStripeEphemeralKeyBad($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->createStripeEphemeralKeyBad: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreatePaymentUserKeyRequest**](../Model/CreatePaymentUserKeyRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findRecentPickup**
> \Swagger\Client\Model\RecentPickupResponse findRecentPickup()

Returns last picked up product with timestamps



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();

try {
    $result = $api_instance->findRecentPickup();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->findRecentPickup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\RecentPickupResponse**](../Model/RecentPickupResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listGiftCards**
> \Swagger\Client\Model\GiftCardCatalogView listGiftCards()

Returns the list of all available gift cards that can be purchased by user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();

try {
    $result = $api_instance->listGiftCards();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->listGiftCards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\GiftCardCatalogView**](../Model/GiftCardCatalogView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listInventory**
> \Swagger\Client\Model\PlayerInventoryView listInventory()

Returns a list of all drops and gifts that user dropped and picked up



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();

try {
    $result = $api_instance->listInventory();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->listInventory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\PlayerInventoryView**](../Model/PlayerInventoryView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listPendingGifts**
> \Swagger\Client\Model\GiftInventoryView listPendingGifts()

Returns all the pending gifts that have been sent to user but never accepted/rejected



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();

try {
    $result = $api_instance->listPendingGifts();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->listPendingGifts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\GiftInventoryView**](../Model/GiftInventoryView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **offerGift**
> \Swagger\Client\Model\OfferGiftResponse offerGift($body)

User is sending his unredeemed drop to another user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\OfferGiftRequest(); // \Swagger\Client\Model\OfferGiftRequest | 

try {
    $result = $api_instance->offerGift($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->offerGift: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\OfferGiftRequest**](../Model/OfferGiftRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\OfferGiftResponse**](../Model/OfferGiftResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **pickupDrop**
> \Swagger\Client\Model\PickupDropResponse pickupDrop($body)

Product pickup by user if withing pickup range



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PickupDropRequest(); // \Swagger\Client\Model\PickupDropRequest | 

try {
    $result = $api_instance->pickupDrop($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->pickupDrop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PickupDropRequest**](../Model/PickupDropRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PickupDropResponse**](../Model/PickupDropResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **placeDrop**
> placeDrop($body)

User is placing his unredeemed drop to any location on a map



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PlaceDropRequest(); // \Swagger\Client\Model\PlaceDropRequest | 

try {
    $api_instance->placeDrop($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->placeDrop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PlaceDropRequest**](../Model/PlaceDropRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseAndSendToMap**
> purchaseAndSendToMap($body)

Product purchase by user and placing it to the map



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PurchaseAndSendToMapRequest(); // \Swagger\Client\Model\PurchaseAndSendToMapRequest | 

try {
    $api_instance->purchaseAndSendToMap($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->purchaseAndSendToMap: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PurchaseAndSendToMapRequest**](../Model/PurchaseAndSendToMapRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseAndSendToPlayer**
> \Swagger\Client\Model\OfferGiftResponse purchaseAndSendToPlayer($body)

Product purchase by user and gifting it to another user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PurchaseAndSendToPlayerRequest(); // \Swagger\Client\Model\PurchaseAndSendToPlayerRequest | 

try {
    $result = $api_instance->purchaseAndSendToPlayer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->purchaseAndSendToPlayer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PurchaseAndSendToPlayerRequest**](../Model/PurchaseAndSendToPlayerRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\OfferGiftResponse**](../Model/OfferGiftResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **putBack**
> putBack($body)

User is returning his last picked up product back to map where it was



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PutBackRequest(); // \Swagger\Client\Model\PutBackRequest | 

try {
    $api_instance->putBack($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->putBack: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PutBackRequest**](../Model/PutBackRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **putBackAndPickupDrop**
> \Swagger\Client\Model\PickupDropResponse putBackAndPickupDrop($body)

User is returning his last picked up product back to map where it was and will pickup another product that was just discovered



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\PutBackAndPickupDropRequest(); // \Swagger\Client\Model\PutBackAndPickupDropRequest | 

try {
    $result = $api_instance->putBackAndPickupDrop($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->putBackAndPickupDrop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PutBackAndPickupDropRequest**](../Model/PutBackAndPickupDropRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PickupDropResponse**](../Model/PickupDropResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **redeemDrop**
> redeemDrop($body)

User is redeeming his drop/gift from his inventory



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\RedeemDropRequest(); // \Swagger\Client\Model\RedeemDropRequest | 

try {
    $api_instance->redeemDrop($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->redeemDrop: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\RedeemDropRequest**](../Model/RedeemDropRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **reportProduct**
> reportProduct($body)

Sends report about bad product to administrator



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\ReportProductRequest(); // \Swagger\Client\Model\ReportProductRequest | 

try {
    $api_instance->reportProduct($body);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->reportProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ReportProductRequest**](../Model/ReportProductRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resolveGift**
> \Swagger\Client\Model\ResolveGiftResponse resolveGift($body)

Accepting or declining of pending gifts



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\ResolveGiftRequest(); // \Swagger\Client\Model\ResolveGiftRequest | 

try {
    $result = $api_instance->resolveGift($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->resolveGift: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ResolveGiftRequest**](../Model/ResolveGiftRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ResolveGiftResponse**](../Model/ResolveGiftResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **revealBarcode**
> \Swagger\Client\Model\BarcodeResponse revealBarcode($body)

Reveals and returns barcode for redeemed product from user inventory



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\GftServiceApi();
$body = new \Swagger\Client\Model\BarcodeRequest(); // \Swagger\Client\Model\BarcodeRequest | 

try {
    $result = $api_instance->revealBarcode($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GftServiceApi->revealBarcode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\BarcodeRequest**](../Model/BarcodeRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\BarcodeResponse**](../Model/BarcodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

