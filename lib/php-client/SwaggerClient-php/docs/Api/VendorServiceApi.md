# Swagger\Client\VendorServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVendor**](VendorServiceApi.md#createVendor) | **POST** /vendorService/v1/createVendor | Creates a vendor
[**findVendor**](VendorServiceApi.md#findVendor) | **POST** /vendorService/v1/findVendor | Returns Vendor by its id
[**placeProduct**](VendorServiceApi.md#placeProduct) | **POST** /vendorService/v1/placeProduct | Creates and places a product on defined location


# **createVendor**
> \Swagger\Client\Model\Vendor createVendor($body)

Creates a vendor



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\VendorServiceApi();
$body = new \Swagger\Client\Model\Vendor(); // \Swagger\Client\Model\Vendor | 

try {
    $result = $api_instance->createVendor($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VendorServiceApi->createVendor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Vendor**](../Model/Vendor.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Vendor**](../Model/Vendor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findVendor**
> \Swagger\Client\Model\Vendor findVendor($body)

Returns Vendor by its id



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\VendorServiceApi();
$body = new \Swagger\Client\Model\FindByIdRequest(); // \Swagger\Client\Model\FindByIdRequest | 

try {
    $result = $api_instance->findVendor($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VendorServiceApi->findVendor: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\FindByIdRequest**](../Model/FindByIdRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Vendor**](../Model/Vendor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **placeProduct**
> \Swagger\Client\Model\Product placeProduct($body)

Creates and places a product on defined location



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\VendorServiceApi();
$body = new \Swagger\Client\Model\PlaceProductRequest(); // \Swagger\Client\Model\PlaceProductRequest | 

try {
    $result = $api_instance->placeProduct($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VendorServiceApi->placeProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PlaceProductRequest**](../Model/PlaceProductRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Product**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

