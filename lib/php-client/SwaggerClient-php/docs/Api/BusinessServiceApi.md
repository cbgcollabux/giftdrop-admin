# Swagger\Client\BusinessServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**buyDraftProduct**](BusinessServiceApi.md#buyDraftProduct) | **POST** /businessService/v1/buyDraftProduct | Make a payment for the draft campaign
[**changeMyCompany**](BusinessServiceApi.md#changeMyCompany) | **POST** /businessService/v1/changeMyCompany | Updates some fields for current business user
[**collectMyCompanyStatistics**](BusinessServiceApi.md#collectMyCompanyStatistics) | **POST** /businessService/v1/collectMyCompanyStatistics | Returns statistics for current business user based on selected time period
[**createDraftProduct**](BusinessServiceApi.md#createDraftProduct) | **POST** /businessService/v1/createDraftProduct | Creates a draft and unpaid campaign for current business account
[**createMyCompany**](BusinessServiceApi.md#createMyCompany) | **POST** /businessService/v1/createMyCompany | Creates new company for business user and returns CompanyProfile
[**findMyCompanyProfile**](BusinessServiceApi.md#findMyCompanyProfile) | **POST** /businessService/v1/findMyCompanyProfile | Returns CompanyProfile that belongs to current user
[**findProduct**](BusinessServiceApi.md#findProduct) | **POST** /businessService/v1/findProduct | Returns campaign info by its identifier
[**listCompanyCategories**](BusinessServiceApi.md#listCompanyCategories) | **POST** /businessService/v1/listCompanyCategories | Returns a list of available categories for the business account
[**listGiftCards**](BusinessServiceApi.md#listGiftCards) | **POST** /businessService/v1/listGiftCards | Returns the list of all available gift cards that can be purchased by user
[**listMyCompanyProducts**](BusinessServiceApi.md#listMyCompanyProducts) | **POST** /businessService/v1/listMyCompanyProducts | Returns all the campaigns created from current account
[**requestTotal**](BusinessServiceApi.md#requestTotal) | **POST** /businessService/v1/requestTotal | During checkout, returns total amount that business will be charged based
[**sendEmail**](BusinessServiceApi.md#sendEmail) | **POST** /businessService/v1/sendEmail | Sending out an email from our noreply address


# **buyDraftProduct**
> buyDraftProduct($body)

Make a payment for the draft campaign



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\BuyDraftProductRequest(); // \Swagger\Client\Model\BuyDraftProductRequest | 

try {
    $api_instance->buyDraftProduct($body);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->buyDraftProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\BuyDraftProductRequest**](../Model/BuyDraftProductRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeMyCompany**
> changeMyCompany($body)

Updates some fields for current business user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\CompanyProfile(); // \Swagger\Client\Model\CompanyProfile | 

try {
    $api_instance->changeMyCompany($body);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->changeMyCompany: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CompanyProfile**](../Model/CompanyProfile.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **collectMyCompanyStatistics**
> \Swagger\Client\Model\CompanyStatisticsResponse collectMyCompanyStatistics($body)

Returns statistics for current business user based on selected time period



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\Period(); // \Swagger\Client\Model\Period | 

try {
    $result = $api_instance->collectMyCompanyStatistics($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->collectMyCompanyStatistics: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Period**](../Model/Period.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyStatisticsResponse**](../Model/CompanyStatisticsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createDraftProduct**
> \Swagger\Client\Model\ProductResponse createDraftProduct($body)

Creates a draft and unpaid campaign for current business account



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\CreateDraftProductRequest(); // \Swagger\Client\Model\CreateDraftProductRequest | 

try {
    $result = $api_instance->createDraftProduct($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->createDraftProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateDraftProductRequest**](../Model/CreateDraftProductRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductResponse**](../Model/ProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createMyCompany**
> \Swagger\Client\Model\CompanyProfile createMyCompany($body)

Creates new company for business user and returns CompanyProfile



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\CompanyProfile(); // \Swagger\Client\Model\CompanyProfile | 

try {
    $result = $api_instance->createMyCompany($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->createMyCompany: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CompanyProfile**](../Model/CompanyProfile.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyProfile**](../Model/CompanyProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findMyCompanyProfile**
> \Swagger\Client\Model\CompanyProfile findMyCompanyProfile()

Returns CompanyProfile that belongs to current user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();

try {
    $result = $api_instance->findMyCompanyProfile();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CompanyProfile**](../Model/CompanyProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findProduct**
> \Swagger\Client\Model\ProductResponse findProduct($body)

Returns campaign info by its identifier



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\FindProductRequest(); // \Swagger\Client\Model\FindProductRequest | 

try {
    $result = $api_instance->findProduct($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->findProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\FindProductRequest**](../Model/FindProductRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductResponse**](../Model/ProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCompanyCategories**
> \Swagger\Client\Model\CompanyCategoryListResponse listCompanyCategories()

Returns a list of available categories for the business account



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();

try {
    $result = $api_instance->listCompanyCategories();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->listCompanyCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CompanyCategoryListResponse**](../Model/CompanyCategoryListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listGiftCards**
> \Swagger\Client\Model\GiftCardCatalogView listGiftCards()

Returns the list of all available gift cards that can be purchased by user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();

try {
    $result = $api_instance->listGiftCards();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->listGiftCards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\GiftCardCatalogView**](../Model/GiftCardCatalogView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listMyCompanyProducts**
> \Swagger\Client\Model\ProductListResponse listMyCompanyProducts($body)

Returns all the campaigns created from current account



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\Period(); // \Swagger\Client\Model\Period | 

try {
    $result = $api_instance->listMyCompanyProducts($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->listMyCompanyProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\Period**](../Model/Period.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductListResponse**](../Model/ProductListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **requestTotal**
> \Swagger\Client\Model\TotalResponse requestTotal($body)

During checkout, returns total amount that business will be charged based



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\TotalRequest(); // \Swagger\Client\Model\TotalRequest | 

try {
    $result = $api_instance->requestTotal($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->requestTotal: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\TotalRequest**](../Model/TotalRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\TotalResponse**](../Model/TotalResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sendEmail**
> sendEmail($body)

Sending out an email from our noreply address



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BusinessServiceApi();
$body = new \Swagger\Client\Model\SendEmailRequest(); // \Swagger\Client\Model\SendEmailRequest | 

try {
    $api_instance->sendEmail($body);
} catch (Exception $e) {
    echo 'Exception when calling BusinessServiceApi->sendEmail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\SendEmailRequest**](../Model/SendEmailRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

