# Swagger\Client\ScannerServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkBarcode**](ScannerServiceApi.md#checkBarcode) | **POST** /scannerService/v1/checkBarcode | checkBarcode
[**useBarcode**](ScannerServiceApi.md#useBarcode) | **POST** /scannerService/v1/useBarcode | useBarcode


# **checkBarcode**
> \Swagger\Client\Model\CheckBarcodeResponse checkBarcode($body)

checkBarcode



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ScannerServiceApi();
$body = new \Swagger\Client\Model\CheckBarcodeRequest(); // \Swagger\Client\Model\CheckBarcodeRequest | 

try {
    $result = $api_instance->checkBarcode($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ScannerServiceApi->checkBarcode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CheckBarcodeRequest**](../Model/CheckBarcodeRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CheckBarcodeResponse**](../Model/CheckBarcodeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **useBarcode**
> useBarcode($body)

useBarcode



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\ScannerServiceApi();
$body = new \Swagger\Client\Model\UseBarcodeRequest(); // \Swagger\Client\Model\UseBarcodeRequest | 

try {
    $api_instance->useBarcode($body);
} catch (Exception $e) {
    echo 'Exception when calling ScannerServiceApi->useBarcode: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UseBarcodeRequest**](../Model/UseBarcodeRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

