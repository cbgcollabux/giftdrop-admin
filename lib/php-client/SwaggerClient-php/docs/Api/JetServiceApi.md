# Swagger\Client\JetServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listHiddenDrops**](JetServiceApi.md#listHiddenDrops) | **POST** /jetService/v1/listHiddenDrops | Returns list of invisible drop identifiers within some small radius (15meters-) based on user location
[**listHiddenProductKeys**](JetServiceApi.md#listHiddenProductKeys) | **POST** /jetService/v1/listHiddenProductKeys | Returns list of invisible drops within some small radius (15meters-) based on user location
[**listOwnDrops**](JetServiceApi.md#listOwnDrops) | **POST** /jetService/v1/listOwnDrops | Returns list of all the drops owned by user without any distance limitations
[**listVisibleDrops**](JetServiceApi.md#listVisibleDrops) | **POST** /jetService/v1/listVisibleDrops | Returns list of visible drops within some radius (1000+ meters) based on user location
[**listVisibleProductKeys**](JetServiceApi.md#listVisibleProductKeys) | **POST** /jetService/v1/listVisibleProductKeys | Returns list of visible drop identifiers within some radius (1000+ meters) based on user location
[**playerProducts**](JetServiceApi.md#playerProducts) | **POST** /jetService/v1/playerProducts | Returns comprehensive product information based on product identifiers supplied


# **listHiddenDrops**
> \Swagger\Client\Model\PlayerMapView listHiddenDrops($body)

Returns list of invisible drop identifiers within some small radius (15meters-) based on user location



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\PhysicalLocation(); // \Swagger\Client\Model\PhysicalLocation | 

try {
    $result = $api_instance->listHiddenDrops($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->listHiddenDrops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PhysicalLocation**](../Model/PhysicalLocation.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerMapView**](../Model/PlayerMapView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listHiddenProductKeys**
> \Swagger\Client\Model\ListProductKeysResponse listHiddenProductKeys($body)

Returns list of invisible drops within some small radius (15meters-) based on user location



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\ListProductKeysRequest(); // \Swagger\Client\Model\ListProductKeysRequest | 

try {
    $result = $api_instance->listHiddenProductKeys($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->listHiddenProductKeys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ListProductKeysRequest**](../Model/ListProductKeysRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ListProductKeysResponse**](../Model/ListProductKeysResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listOwnDrops**
> \Swagger\Client\Model\PlayerMapView listOwnDrops($body)

Returns list of all the drops owned by user without any distance limitations



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\PhysicalLocation(); // \Swagger\Client\Model\PhysicalLocation | 

try {
    $result = $api_instance->listOwnDrops($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->listOwnDrops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PhysicalLocation**](../Model/PhysicalLocation.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerMapView**](../Model/PlayerMapView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listVisibleDrops**
> \Swagger\Client\Model\PlayerMapView listVisibleDrops($body)

Returns list of visible drops within some radius (1000+ meters) based on user location



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\PhysicalLocation(); // \Swagger\Client\Model\PhysicalLocation | 

try {
    $result = $api_instance->listVisibleDrops($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->listVisibleDrops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PhysicalLocation**](../Model/PhysicalLocation.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerMapView**](../Model/PlayerMapView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listVisibleProductKeys**
> \Swagger\Client\Model\ListProductKeysResponse listVisibleProductKeys($body)

Returns list of visible drop identifiers within some radius (1000+ meters) based on user location



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\ListProductKeysRequest(); // \Swagger\Client\Model\ListProductKeysRequest | 

try {
    $result = $api_instance->listVisibleProductKeys($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->listVisibleProductKeys: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ListProductKeysRequest**](../Model/ListProductKeysRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ListProductKeysResponse**](../Model/ListProductKeysResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **playerProducts**
> \Swagger\Client\Model\PlayerProductsResponse playerProducts($body)

Returns comprehensive product information based on product identifiers supplied



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\JetServiceApi();
$body = new \Swagger\Client\Model\PlayerProductsRequest(); // \Swagger\Client\Model\PlayerProductsRequest | 

try {
    $result = $api_instance->playerProducts($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JetServiceApi->playerProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\PlayerProductsRequest**](../Model/PlayerProductsRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\PlayerProductsResponse**](../Model/PlayerProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

