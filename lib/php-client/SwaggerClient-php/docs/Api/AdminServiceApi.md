# Swagger\Client\AdminServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**assignPlanToAccounts**](AdminServiceApi.md#assignPlanToAccounts) | **POST** /adminService/v1/assignPlanToAccounts | Assigns single plan to multiple accounts.
[**changeAccountState**](AdminServiceApi.md#changeAccountState) | **POST** /adminService/v1/changeAccountState | Changes account state (Enabled or Disabled) and adds security alert to account.
[**changeCompanyState**](AdminServiceApi.md#changeCompanyState) | **POST** /adminService/v1/changeCompanyState | Changes company state (Enabled or Disabled) and adds security alert to company profile.
[**changeProductState**](AdminServiceApi.md#changeProductState) | **POST** /adminService/v1/changeProductState | Changes product state (Enabled or Disabled).
[**changeUserState**](AdminServiceApi.md#changeUserState) | **POST** /adminService/v1/changeUserState | Changes user state (Enabled or Disabled) and adds security alert to user profile.
[**createPlan**](AdminServiceApi.md#createPlan) | **POST** /adminService/v1/createPlan | Creates account fee plan.
[**detailedCompanyProfile**](AdminServiceApi.md#detailedCompanyProfile) | **POST** /adminService/v1/detailedCompanyProfile | Returns detailed company profile by its identifier.
[**detailedUserProfile**](AdminServiceApi.md#detailedUserProfile) | **POST** /adminService/v1/detailedUserProfile | Returns detailed info about user by identifier.
[**disablePlans**](AdminServiceApi.md#disablePlans) | **POST** /adminService/v1/disablePlans | Disables plans from the supplied list and makes them invisible for listActivePlans.
[**importGiftCardCatalog**](AdminServiceApi.md#importGiftCardCatalog) | **POST** /adminService/v1/importGiftCardCatalog | Imports all the available gift cars from TANGO to our database
[**listActivePlans**](AdminServiceApi.md#listActivePlans) | **POST** /adminService/v1/listActivePlans | Returns a list of all active plans.
[**listCompanies**](AdminServiceApi.md#listCompanies) | **POST** /adminService/v1/listCompanies | Returns a filtered list of companies. Filters can be applied: textFilters, stateFilters, sorting.
[**listProducts**](AdminServiceApi.md#listProducts) | **POST** /adminService/v1/listProducts | Returns a filtered list of products.
[**listSettings**](AdminServiceApi.md#listSettings) | **POST** /adminService/v1/listSettings | Lists all settings.
[**listUserLocations**](AdminServiceApi.md#listUserLocations) | **POST** /adminService/v1/listUserLocations | Returns a list of users with their locations.
[**listUsers**](AdminServiceApi.md#listUsers) | **POST** /adminService/v1/listUsers | Returns a filtered list of users. Filters can be applied: textFilters, stateFilters, sorting
[**updateSettings**](AdminServiceApi.md#updateSettings) | **POST** /adminService/v1/updateSettings | Updates settings.


# **assignPlanToAccounts**
> assignPlanToAccounts($body)

Assigns single plan to multiple accounts.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\AssignPlanToAccountsRequest(); // \Swagger\Client\Model\AssignPlanToAccountsRequest | 

try {
    $api_instance->assignPlanToAccounts($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->assignPlanToAccounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\AssignPlanToAccountsRequest**](../Model/AssignPlanToAccountsRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeAccountState**
> changeAccountState($body)

Changes account state (Enabled or Disabled) and adds security alert to account.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UpdateAccountStateRequest(); // \Swagger\Client\Model\UpdateAccountStateRequest | 

try {
    $api_instance->changeAccountState($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->changeAccountState: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateAccountStateRequest**](../Model/UpdateAccountStateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeCompanyState**
> changeCompanyState($body)

Changes company state (Enabled or Disabled) and adds security alert to company profile.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UpdateCompanyStateRequest(); // \Swagger\Client\Model\UpdateCompanyStateRequest | 

try {
    $api_instance->changeCompanyState($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->changeCompanyState: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateCompanyStateRequest**](../Model/UpdateCompanyStateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeProductState**
> changeProductState($body)

Changes product state (Enabled or Disabled).



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\ChangeProductStateRequest(); // \Swagger\Client\Model\ChangeProductStateRequest | 

try {
    $api_instance->changeProductState($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->changeProductState: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ChangeProductStateRequest**](../Model/ChangeProductStateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **changeUserState**
> changeUserState($body)

Changes user state (Enabled or Disabled) and adds security alert to user profile.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UpdateUserStateRequest(); // \Swagger\Client\Model\UpdateUserStateRequest | 

try {
    $api_instance->changeUserState($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->changeUserState: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateUserStateRequest**](../Model/UpdateUserStateRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createPlan**
> \Swagger\Client\Model\CreatePlanResponse createPlan($body)

Creates account fee plan.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\CreatePlanRequest(); // \Swagger\Client\Model\CreatePlanRequest | 

try {
    $result = $api_instance->createPlan($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->createPlan: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreatePlanRequest**](../Model/CreatePlanRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CreatePlanResponse**](../Model/CreatePlanResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **detailedCompanyProfile**
> \Swagger\Client\Model\DetailedCompanyProfileResponse detailedCompanyProfile($body)

Returns detailed company profile by its identifier.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\DetailedCompanyProfileRequest(); // \Swagger\Client\Model\DetailedCompanyProfileRequest | 

try {
    $result = $api_instance->detailedCompanyProfile($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->detailedCompanyProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DetailedCompanyProfileRequest**](../Model/DetailedCompanyProfileRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DetailedCompanyProfileResponse**](../Model/DetailedCompanyProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **detailedUserProfile**
> \Swagger\Client\Model\DetailedUserProfileResponse detailedUserProfile($body)

Returns detailed info about user by identifier.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\DetailedUserProfileRequest(); // \Swagger\Client\Model\DetailedUserProfileRequest | 

try {
    $result = $api_instance->detailedUserProfile($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->detailedUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DetailedUserProfileRequest**](../Model/DetailedUserProfileRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\DetailedUserProfileResponse**](../Model/DetailedUserProfileResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **disablePlans**
> disablePlans($body)

Disables plans from the supplied list and makes them invisible for listActivePlans.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\DisablePlansRequest(); // \Swagger\Client\Model\DisablePlansRequest | 

try {
    $api_instance->disablePlans($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->disablePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\DisablePlansRequest**](../Model/DisablePlansRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importGiftCardCatalog**
> \Swagger\Client\Model\GiftCardCatalogView importGiftCardCatalog()

Imports all the available gift cars from TANGO to our database



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();

try {
    $result = $api_instance->importGiftCardCatalog();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->importGiftCardCatalog: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\GiftCardCatalogView**](../Model/GiftCardCatalogView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listActivePlans**
> \Swagger\Client\Model\ListPlansResponse listActivePlans()

Returns a list of all active plans.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();

try {
    $result = $api_instance->listActivePlans();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listActivePlans: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\ListPlansResponse**](../Model/ListPlansResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listCompanies**
> \Swagger\Client\Model\CompanyListResponse listCompanies($body)

Returns a filtered list of companies. Filters can be applied: textFilters, stateFilters, sorting.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\CompanyListRequest(); // \Swagger\Client\Model\CompanyListRequest | 

try {
    $result = $api_instance->listCompanies($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listCompanies: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CompanyListRequest**](../Model/CompanyListRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\CompanyListResponse**](../Model/CompanyListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listProducts**
> \Swagger\Client\Model\ProductListResponse listProducts($body)

Returns a filtered list of products.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\ProductListRequest(); // \Swagger\Client\Model\ProductListRequest | 

try {
    $result = $api_instance->listProducts($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ProductListRequest**](../Model/ProductListRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ProductListResponse**](../Model/ProductListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listSettings**
> \Swagger\Client\Model\ListSettingsResponse listSettings()

Lists all settings.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();

try {
    $result = $api_instance->listSettings();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\ListSettingsResponse**](../Model/ListSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listUserLocations**
> \Swagger\Client\Model\UserLocationResponse listUserLocations($body)

Returns a list of users with their locations.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UserLocationRequest(); // \Swagger\Client\Model\UserLocationRequest | 

try {
    $result = $api_instance->listUserLocations($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listUserLocations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UserLocationRequest**](../Model/UserLocationRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UserLocationResponse**](../Model/UserLocationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **listUsers**
> \Swagger\Client\Model\UserListResponse listUsers($body)

Returns a filtered list of users. Filters can be applied: textFilters, stateFilters, sorting



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UserListRequest(); // \Swagger\Client\Model\UserListRequest | 

try {
    $result = $api_instance->listUsers($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UserListRequest**](../Model/UserListRequest.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UserListResponse**](../Model/UserListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSettings**
> updateSettings($body)

Updates settings.



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AdminServiceApi();
$body = new \Swagger\Client\Model\UpdateSettingsRequest(); // \Swagger\Client\Model\UpdateSettingsRequest | 

try {
    $api_instance->updateSettings($body);
} catch (Exception $e) {
    echo 'Exception when calling AdminServiceApi->updateSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateSettingsRequest**](../Model/UpdateSettingsRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

