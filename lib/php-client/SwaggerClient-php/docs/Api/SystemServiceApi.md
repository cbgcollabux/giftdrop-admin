# Swagger\Client\SystemServiceApi

All URIs are relative to *http://gftnow.appspot.com:8080/_ah/api/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearCache**](SystemServiceApi.md#clearCache) | **POST** /systemService/v1/clearCache | 
[**initializeDatabase**](SystemServiceApi.md#initializeDatabase) | **POST** /systemService/v1/initializeDatabase | 
[**refreshSettings**](SystemServiceApi.md#refreshSettings) | **POST** /systemService/v1/refreshSettings | 


# **clearCache**
> clearCache()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SystemServiceApi();

try {
    $api_instance->clearCache();
} catch (Exception $e) {
    echo 'Exception when calling SystemServiceApi->clearCache: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **initializeDatabase**
> initializeDatabase()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SystemServiceApi();

try {
    $api_instance->initializeDatabase();
} catch (Exception $e) {
    echo 'Exception when calling SystemServiceApi->initializeDatabase: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refreshSettings**
> refreshSettings()



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\SystemServiceApi();

try {
    $api_instance->refreshSettings();
} catch (Exception $e) {
    echo 'Exception when calling SystemServiceApi->refreshSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

