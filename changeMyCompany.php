<?php ob_start();session_start();
	
	include_once "mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	
	$identifier;
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	
	try {
	    $findMyCompanyProfile = $api->findMyCompanyProfile();

	    $identifier = $findMyCompanyProfile['identifier'];

	    // change company profile error 500
	    $business_api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\Body11;

		$account = array();
		$account['identifier'] = $_SESSION['user']->userInfo->identifier;
		$account['created_at'] = gmdate("Y-m-d\TH:i:s\Z");
		$account['notes'] = "test notes";
		$account['logoURL'] = "http://www.gftnow.com/template/assets/upload/php/files/36e2418c89b2a3b8e276.png";
		$account['state'] = "PH";
		$account['title'] = "test title";
		$account['about'] = "test about";
		$account['website'] = "www.facebook.com";

		$body['identifier'] = $identifier;
		$body['name'] = "chris";
		$body['created'] = gmdate("Y-m-d\TH:i:s\Z");
		$body['notes'] = "chris notes";
		$body['state'] = "chris state";
		$body['address'] = "chris address";
		$body['category'] = "a02a6059-aaf4-4cc1-9494-a31a1715fbbd";
		$body['account'] =$account;


		echo "<pre>";
	    print_r($body);
	    echo "</pre>";

	    
		try {
		    $changeMyCompany = $business_api->changeMyCompany($body);

		    echo "<pre>";
		    print_r($changeMyCompany);
		    echo "</pre>";
		} catch (Exception $e) {
		    echo $e->getMessage();
		}

		

	} catch (Exception $e) {
	    echo 'Exception when calling BusinessServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
	}

	
	


	


?>