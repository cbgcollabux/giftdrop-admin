
<!DOCTYPE html>
<html lang="en-us" style="overflow:none !important;">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />
		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>
			body{
				overflow-x: hidden !important;
			}
			.row{
				margin: 0px !important;
			}
			#text-slider li{
				display: none;
				position: absolute;
			}
			.button-scroll{position:absolute;bottom:10vh;left:calc(50% - 13px);width:26px;height:50px;border-radius:15px;border:2px solid #fff;z-index:9}

			@media only screen and (max-width: 767px) and (min-width: 320px) {
				.gftnow-phone-bg{
					width:240px !important;
					height:450px !important;
				}
				.phone{
					width: 200px !important;
				}
				#animated-pin{
					left:40px !important;
				}
				#small-elipse{
					left:45px !important;
				}
				#big-elipse{
					left:-340px !important;
				}
				#gftnow-landing-discover-description {
					top: -650px !important;
				}
				#gftnow-landing-discover-description h1{
					font-size: 30px !important;
				}
				#gftnow-landing-discover-description p{
					font-size: 20px !important;
				}
				#gftnow-landing-discover .gftnow-phone-bg{
					left: 40px !important;
				}
				#jordan-shoes{
					left: -275px !important;
					z-index:9997 !important;
				}
				#healthy-burger{
					width:180px !important;
					height:130px !important;
					left:200px !important;
					top:620px !important;
					z-index:9998 !important;
				}
				#gftnow-landing-share-description{
					top: -200px !important;
					display: block !important;
					left: -10px !important;
				}
				#gftnow-landing-share-description h1{
					font-size: 30px !important;
				}
				#gftnow-landing-share-description p{
					font-size: 20px !important;
				}					
				#gftnow-landing-share .gftnow-phone-bg{
					top: -450px !important;
					display: block !important;
				}
				#gftnow-landing-give-description{
					top:-950px !important;
					right: 5px !important;
				}
				#gftnow-landing-give-description h1{
					font-size: 30px !important;
				}
				#gftnow-landing-give-description p{
					font-size: 20px !important;
				}
				#girl-and-phone{
					width:512px !important;
					height:567px !important;
					top:270px !important;
					left:-170px !important;
				}
				#gift-card{
					height:90px !important;
					width:210px !important;
					top:630px !important;
					left: 200px !important;
				}
				#gftnow-landing-give .gftnow-phone-bg{
					left: 70px !important;
					top: 260px !important;
				}
				#gftnow-landing-slider-description{
					top: -180px;
				}
				#gftnow-landing-slider-description h1{
					font-size: 30px !important;
				}
				#gftnow-landing-slider-description p{
					font-size: 20px !important;
				}
				#canvas{
					top: 0 !important;
				}
				#green-container{
					width: 320px !important;
					height: 264px !important;
					right: 0 !important;
					top: 200px !important;
				}
				#text-slider{
					top: 370px !important;
					right: 65px !important;
					font-size: 16px !important;
					width: 250px !important;
				}
				#gftnow-landing-slider-images{
					top:-700px;
				}
				#gftnow-landing-download h1{
					font-size: 25px !important;
				}
				#gftnow-landing-download p{
					font-size: 20px !important;
				}
				#gftnow-landing-download-description{
					top: -80px !important;
					z-index: 9999 !important;
				}
				#gftnow-landing-download .gftnow-phone-bg{
					bottom: -250px !important;
					left: -1px !important;
					z-index: 9998 !important;
				}
				.download-img{
					width: 140px !important;
					height: 45px !important;
				}
			}
			@media only screen and (max-width: 1023px) and (min-width: 768px){
				#gftnow-landing-discover-description{
					top: -850px !important;
					left: 430px;
					width: 330px;
				}
				#gftnow-landing-discover-description h1{
					font-size: 35px !important;
				}
				#gftnow-landing-discover-description p{
					font-size: 25px !important;
				}					
				#gftnow-landing-share-description{
					top: -50px !important;
					left: 40px;
					display: block !important;
					width: 330px;
				}
				#gftnow-landing-share-description h1{
					font-size: 35px !important;
				}
				#gftnow-landing-share-description p{
					font-size: 25px !important;
				}				
				#gftnow-landing-share .gftnow-phone-bg{
					top: -600px !important;
					display: block !important;
				}
				#gftnow-landing-give-description{
					top:-850px;
					left:380px;
					width:350px;
				}
				#gftnow-landing-give-description h1{
					font-size: 35px !important;
				}
				#gftnow-landing-give-description p{
					font-size: 25px !important;
				}				
				#gftnow-landing-slider-description{
					top: -50px !important;
					left: 30px;
					width:300px;
				}
				#gftnow-landing-slider-description h1{
					font-size: 35px !important;
				}
				#gftnow-landing-slider-description p{
					font-size: 25px !important;
				}
				#canvas{
					top: 0 !important;
				}
				#green-container{
					width: 398px !important;
					height: 324px !important;
					left: -350px !important;
					top: 50px !important;
				}
				#text-slider{
					top: 280px !important;
					left: -10px !important;
					font-size: 21px !important;
					width:350px !important;
				}
				#gftnow-landing-slider-images{
					top:-700px;
					left: 354px !important;
				}
				#gftnow-landing-download h1{
					font-size: 25px !important;
				}
				#gftnow-landing-download p{
					font-size: 20px !important;
				}
				#gftnow-landing-download-description{
					top: 180px !important;
					right: 20px !important;
					z-index: 9999 !important;
				}
				#gftnow-landing-download .gftnow-phone-bg{
					left: 50px !important;
				}
				.download-img{
					width: 140px !important;
					height: 45px !important;
				}
			}

		@media only screen and (min-device-width : 375px) and (max-device-width : 667px) and (orientation : landscape) { 
			.container-full{
				height: 60%;
			}
		}

		@media only screen and (min-device-width : 320px) and (max-device-width : 568px) {
			.container-full{
				height: 60%;
			}
		}
			
		</style>
	</head>
	<body style="background-color: #ffffff;padding:0;margin:0;height: 100%;margin: 0px;overflow:none !important;">
	
		<div class="container-full" style="background-color:#FFFFFF;">
			<div class="gftnow-landing-video2 hidden-sm hidden-xs">
				<span class="mask" style="z-index:999;height:100vh;">
					<div class="landing-nav">
						<div class="landing-nav-header">
							<br/>
							<div class="row" style="font-size:22px;">
								<div class="col-md-4 col-sm-12 text-center">
									<div class="row">
										<div class="col-md-3 col-sm-3"></div>
										<div class="col-md-9 col-sm-9 text-left"><a href="#" style="color:white;"><img src="assets/img/gftnow-logo-big.png" style="width:60px;height:65px;"> GiftDrop</a></div>
									</div>
								</div>
								<div class="col-md-4 col-sm-12 text-center"></div>
								<div class="col-md-4 col-sm-12 text-center">
									<a href="business.php" class="bttn">For Business</a>
									<br/>
								</div>
							</div>
							
						</div>
					</div>
					<div class="section hidden-sm hidden-xs" id="landing-video">
					
						<div class="row">
							<div class="col-md-12 text-center subheader">
								<br/><br/><br/><br/><br/>
								<h1 class="gftnow-text-white" style="font-size:52px;">DISCOVER FREE GIFTS AROUND YOU</h1>
							</div>
						</div>
						<div class="row">
							<br/><br/>
							<div class="col-md-4"></div>
							<div class="col-md-2 col-sm-6 col-xs-6 text-right">
								<a href="#"><img src="assets/img/google-play.png" /></a>
							</div>
							<div class="col-md-2 col-sm-6 col-xs-6 text-left">
								<a href="#"><img src="assets/img/app-store.png" /></a>
							</div>
							<div class="col-md-4"></div>
						</div>
						<a href="javascript:void(0);" id="button-scroll-click">
							<div class="button-scroll"><center>
								<br/>
								<p id="moving-mousescroll" style="color:white;font-size:10px;"><i class="fa fa-chevron-down"></i></p>
							</center></div>
						</a>
					</div>
				</span>
				<video style="width:100%;height:auto;z-index:0;filter:opacity(.4);;" autoplay loop><source src="assets/videos/Videoplace.mp4"></source></video>
			</div>

			<div class="section hidden-md hidden-lg" style="background-color:black;padding:0;">
				<span class="mask" style="height:60%;z-index:999;position:absolute;background-color: rgba(0,0,0,0.5)">
					<div class="text-center">
						<a href="#" style="color:white;"><img src="assets/img/gftnow-logo-big.png" style="width:60px;height:65px;"> GiftDrop</a>
					</div>
					<div class="row">
						<div class="col-md-12 text-center subheader">
							<h1 class="text-white" style="font-size:24px;">DISCOVER FREE GIFTS AROUND YOU</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"></div>
						<div class="col-md-2 col-sm-6 col-xs-6 text-right">
							<a href="#"><img src="assets/img/google-play.png" /></a>
						</div>
						<div class="col-md-2 col-sm-6 col-xs-6 text-left">
							<a href="#"><img src="assets/img/app-store.png" /></a>
						</div>
						<div class="col-md-4"></div>
					</div>
					<br/>
					<div class="text-center">
						<a href="business.php" class="bttn">For Business</a>
					</div>
				</div>
				<video style="width:auto;height:100%;z-index:0;filter:opacity(.4);" autoplay loop><source src="assets/videos/Videoplace.mp4"></source></video>
			</div>

			<div class="text-center section gftnow-nopadding" id="gftnow-landing-discover">
				<div class="row gftnow-nopadding">
					<div class="col-md-2 text-center"></div>
					<div class="col-md-4 text-center" style="min-height: 700px;">
						<img id="jordan-shoes" src="assets/img/jordan-shoes.png" style="top:600px;left:-150px;bottom:0;margin:auto;position:absolute;height:300px;width:500px;z-index:9998;"/>
						<img id="healthy-burger" src="assets/img/healthy-burger.png" style="top:550px;left:250px;bottom:0;margin:auto;position:absolute;height:180px;width:330px;z-index:9997;"/>

						<div class="gftnow-phone-bg" style="top:0;left:150px;bottom:-350px;margin:auto;height:450px;position:absolute;z-index:9999;height:538px; width:288px;">
							<video class="phone" width="245" style="bottom:0;" autoplay loop>
								<source src="assets/videos/1-Discover.mp4"></source>
							</video>
						</div>
					</div>
					<div class="col-md-6 text-left" id="gftnow-landing-discover-description" style="min-height: 700px; z-index:99999; top:-115px;">
						<div>
							<h1 style="color:#669c4c;font-size:52px;">Discover Free Stuff</h1>
							<p class="gftnow-font-medium" style="font-size:22px;">
								So many free cool items are all around you:<br/>
								from free burger or pairs of shoes to an hour<br/>
								of massage or stay at the hotel</p>
						</div>
					</div>
				</div>
			</div>

			<div class="section section-success text-center gftnow-nopadding" style="height:700px;" id="gftnow-landing-share">
				<div class="row gftnow-nopadding">
					<div class="col-md-2"></div>
					<div class="col-md-4 text-center hidden-sm hidden-xs" id="gftnow-landing-share-description" style="min-height: 700px; z-index:9999">
						<div>
							<h1 class="gftnow-font-medium" style="color:#333333; top:-100px; font-size:52px;">Share With Friends</h1>
							<p class="gftnow-font-medium" style="font-size:22px;">
								Discovered cool stuff but know somebody<br/>
								who needs it more? Make their day but<br/>
								simply regifting it to them
							</p>
						</div>
					</div>
					<div class="col-md-4 text-center" style="min-height: 700px;">
						<br/><br/><br/>
						<div class="gftnow-phone-bg" style="top:120px;position:absolute;right: 50px; height:538px; width:288px;">
							<video class="phone" width="245" autoplay loop>
								<source src="assets/videos/2-Regift.mp4"></source>
							</video>
						</div>
					</div>

					<div class="col-md-2 text-center"></div>
				</div>
			</div>

			<div class="text-center section gftnow-nopadding" id="gftnow-landing-give">
				<div class="row gftnow-nopadding">
					<div class="col-md-2 text-center"></div>
					<div class="col-md-4 text-center" id="gftnow-landing-give" style="min-height: 700px;">
						
						<img id="girl-and-phone" src="assets/img/girl-and-phone.png" style="top:250px;left:-250px;bottom:0;margin:auto;position:absolute;height:677px;width:622px;z-index:9998;"/>
						<img id="gift-card" src="assets/img/gift-card.png" style="top:620px;left:330px;bottom:0;margin:auto;position:absolute;height:137px;width:329px;z-index:9997;"/>

						<div class="gftnow-phone-bg" style="top:250px;left:120px;bottom:0;margin:auto;height:538px; width:288px; position:absolute;z-index:9999;">
							<video class="phone" width="245" autoplay loop>
								<source src="assets/videos/3-Send.mp4"></source>
							</video>
						</div>
				
					</div>
					<div class="col-md-6 text-right" style="min-height: 700px; z-index:9999" id="gftnow-landing-give-description">
						<div style="position:absolute;height:200px;top:0;bottom:0;margin: auto;">
						<h1 style="color:#669c4c;font-size:52px;">The Joy of Giving</h1>
						<p class="gftnow-font-medium" style="font-size:22px;">
							You can pay a roomate back, send friend a<br/>
							gift cardor creat a scavanger hunt by<br/>
							dropping money on the map
						</p>
						</div>
					</div>
				</div>
			</div>

			<div class="section section-success text-center gftnow-nopadding" id="gftnow-landing-slider">
				<div class="row gftnow-nopadding">
					<div class="col-md-1"></div>
					<div class="col-md-5 text-center" id="gftnow-landing-slider-description" style="min-height: 700px; z-index:99999;">
						<div>
						<h1 style="color:#333333; font-size:52px;">Don’t Take Our Word For It</h1>
						<p class="gftnow-font-medium" style="color:#669c4c; font-size:22px;">
							Sounds too good to be true? This is what 
							other people say about this
						</p>
						</div>
					</div>
					<div id="gftnow-landing-slider-images" class="col-md-5 text-center" style="min-height: 700px;">
						<img id="green-container" src="assets/img/green-container.png" style="top:0;right:0;bottom:0;margin:auto;position:absolute;height:444px;width:508px;z-index:9998;"/>
						<!-- <img src="assets/img/bubbles-lg-yellow.png" style="top:0;left:0;bottom:0;margin:auto;position:absolute;z-index:9999;"/> -->
						<ul id="text-slider" style="list-style:none;color:#ebf8a1;font-size:22px;top:200px;right:150px;position:absolute;z-index:9999;width:300px;">
							<li class="gftnow-font-bold"><i>“ This is so much fun! I live exploring the city with friends and finding cools stuff!”</i></li>
							<li class="gftnow-font-bold"><i>“Lorem ipsum dolor sit amet, consectetur adipiscing elit.”</i></li>
							<li class="gftnow-font-bold"><i>“Nullam vel fringilla turpis, ac ultrices quam.”</i></li>
						</ul>
					</div>
					<div class="col-md-1 text-center"></div>
					<div id="canvas" style="position:absolute;right:0;"></div>
				</div>
			</div>

			<div class="section text-center gftnow-nopadding" id="gftnow-landing-download" style="z-index:99999;">
				<div class="row gftnow-nopadding">
					<div class="col-md-1 text-center"></div>
					<div class="col-md-5">
						<div class="gftnow-phone-bg" style="top:400px;left:120px;bottom:0;margin:auto;height:538px; width:288px; position:absolute;z-index:9999;">
							<video class="phone" width="245" autoplay loop>
								<source src="assets/videos/3-Send.mp4"></source>
							</video>
						</div>						
					</div>
					<div class="col-md-5 text-right" id="gftnow-landing-download-description" style="min-height: 700px;">
						<br/><br/><br/><br/>
							<h1 style="font-size:52px;color:#000000;">
							Walking is not only good
							for your health, now it is
							good for your wallet as well!
							</h1>
						<br/>
						<a href="#"><img class="download-img" src="assets/img/google-app-black.png" style="width:200px;height:68px;"/></a>
						<a href="#"><img class="download-img" src="assets/img/app-store-black.png" style="width:200px;height:68px;"/></a>
					</div>
					<div class="col-md-1 text-center"></div>
				</div>
			</div>

			<div class="text-center section section-default row" style="z-index:99999; font-size:22px;">
				<div class="col-md-5 text-center" style="z-index:99999;">
					<br/><br/>
					<img src="assets/img/logo.png"/> &nbsp; <span class="gftnow-font-medium">Gift</span>Drop
				</div>
				<div class="col-md-3 text-left" style="z-index:99999;">
					<span class="gftnow-font-medium">Links</span>
					<br/><br/>
					<a href="#" class="gftnow-font-color-light">FAQ</a> <br/>
					<a href="#" class="gftnow-font-color-light">About</a> <br/>
					<a href="#" class="gftnow-font-color-light">For Business</a> <br/>
					<a href="#" class="gftnow-font-color-light">How It Works</a> <br/>

					<br/><br/>
				</div>
				<div class="col-md-4 text-left " style="z-index:99999;">
					<span class="gftnow-font-medium">Contacts</span>
					<br/><br/>
					<div class="gftnow-font-color-light">
						10700 Beaverton Hillsdale HWY <br/>
						Beaverton, OR <br/>
						97005 <br/>
					</div>
				</div>
				
			</div>

			<div class="text-center section section-warning row gftnow-nopadding" style="z-index:99999;">
				<br/>
				<a href="#"><img src="assets/img/fb-mini.png" /></a> &nbsp;&nbsp;
				<a href="#"><img src="assets/img/insta-mini.png" /></a> &nbsp;&nbsp;
				<a href="#"><img src="assets/img/youtube-mini.png" /></a> &nbsp;&nbsp;
				<a href="#"><img src="assets/img/twitter-mini.png" /></a>
				<br/><br/>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<script src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
		<script src="assets/js/jquery-easing.1.2.js"></script>

		<!-- BOOTSTRAP JS -->
		<script src="assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="assets/js/plugin/fastclick/fastclick.js"></script>

		<!--[if IE 7]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<script src="assets/js/demo.js"></script>

		<script src="assets/MovingBoxes-master/js/jquery.movingboxes.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="assets/js/app.js"></script>
		<script src="assets/js/gftnow.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		
		<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		<script src="assets/js/plugin/flot/jquery.flot.cust.js"></script>
		<script src="assets/js/plugin/flot/jquery.flot.resize.js"></script>
		<script src="assets/js/plugin/flot/jquery.flot.tooltip.js"></script>
		
		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="assets/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="assets/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
		
		<!-- Full Calendar -->
		<script src="{assets/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>

		<!-- Morris -->
		<script src="assets/js/plugin/morris/raphael.2.1.0.min.js"></script>
		<script src="assets/js/plugin/morris/morris.min.js"></script>

		<script src="assets/js/plugin/Chart.js-2.5.0/dist/Chart.min.js"></script>
		<script src="assets/js/plugin/bootstrap-select-1.12.2/dist/js/bootstrap-select.min.js"></script>
		<script src="assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<script src="assets/js/jsbarcode.min.js"></script>


		<script type="text/javascript">
			$(document).ready(function() {
				init();
				movePinUp();
				moveSMelipseRight();
				rotateBGelipse();

				moveMouseScrollUp();
				$("#button-scroll-click").click(function() {
				    $("html, body").animate({
				        scrollTop: $("#gftnow-landing-discover").offset().top
				    }, 1000);
				});
			});

			function moveMouseScrollUp(){
			    $("#moving-mousescroll").animate({top: "+=20"}, 500, moveMouseScrollDown);
			}
			function moveMouseScrollDown(){
			    $("#moving-mousescroll").animate({top: "-=20"}, 500, moveMouseScrollUp);
			}

			function fadeInOut(item) {
				item.fadeIn(1000).delay(3000).fadeOut(1000, function() {
					if (item.next().length) // if there is a next element
					{
						fadeInOut(item.next());
					} // use it
					else {
						fadeInOut(item.siblings(':first'));
					} // if not, then use go back to the first sibling
				});
			}

			fadeInOut(jQuery('#text-slider li:first-child'));

			(function() {
			    var paper, circs, i, nowX, nowY, timer, props = {}, toggler = 0, elie, dx, dy, cur;
			    // Returns a random integer between min and max  
			    // Using Math.round() will give you a non-uniform distribution!  
			    function ran(min, max){  
			        return Math.floor(Math.random() * (max - min + 1)) + min;  
			    } 
			    
			    function moveIt(){
			        for(i = 0; i < circs.length; ++i){            
						// Reset when time is at zero
			            if (! circs[i].time) 
			            {
			                circs[i].time  = ran(30, 100);
			                circs[i].deg   = ran(-179, 180);
			                circs[i].vel   = ran(1, 5);  
			                circs[i].curve = ran(0, 1);
			            }                
						// Get position
			            nowX = circs[i].attr("x");
			            nowY = circs[i].attr("y");   
						// Calc movement
			            dx = circs[i].vel * Math.cos(circs[i].deg * Math.PI/180);
			            dy = circs[i].vel * Math.sin(circs[i].deg * Math.PI/180);
						// Calc new position
			            nowX += dx;
			            nowY += dy;
						// Calc wrap around
			            if (nowX < 0){
			            	if (window.matchMedia('(max-width: 767px)').matches){
			            		nowX = 380 + nowX;
			            	}else{
			            		nowX = 630 + nowX;
			            	}
			            }
			            else{
			            	if (window.matchMedia('(max-width: 767px)').matches){
			            		nowX = nowX % 380;
			            	}else{
			            		nowX = nowX % 630;            
			            	}
			            }
			            if (nowY < 0){
			            	nowY = 480 + nowY;
			            }
			            else{
			            	nowY = nowY % 480;
			            }
			            
						// Render moved particle
			            circs[i].attr({x: nowX, y: nowY});

						// Calc curve
			            if (circs[i].curve > 0) circs[i].deg = circs[i].deg + 2;
			            else                    circs[i].deg = circs[i].deg - 2;

			            // Progress timer for particle
			            circs[i].time = circs[i].time - 1;
			            
						// Calc damping
			            if (circs[i].vel < 1) circs[i].time = 0;
			            else circs[i].vel = circs[i].vel - .05;              
			       
			        } 
			        timer = setTimeout(moveIt, 60);
			    }
			    
			    window.onload = function () {
			    	var sizeRand, imgRand, imgURL, width, height;
			    	if (window.matchMedia('(max-width: 768px)').matches){
			    		paper = Raphael("canvas", 400, 700);
			    	}else{
			        	paper = Raphael("canvas", 650, 700);
			    	}
			        circs = paper.set();
			        for (i = 0; i < 20; ++i){
			        	sizeRand = ran(1,5);
			        	if(sizeRand == 1 || sizeRand == 2){
			        		sizeWH = 250;
			        		imgRand = ran(1,4);
			        		if(imgRand == 1)
			        			imgURL = "assets/img/bubble-lg-blue.png";
			        		if(imgRand == 2)
			        			imgURL = "assets/img/bubble-lg-green.png";
		        			if(imgRand == 3)
		        				imgURL = "assets/img/bubble-lg-purple.png";
		        			if(imgRand == 4)
		        				imgURL = "assets/img/bubble-lg-yellow.png";
			        	}
			        	if(sizeRand == 3 || sizeRand == 4){
			        		sizeWH = 75;
			        		imgRand = ran(1,4);
			        		if(imgRand == 1)
			        			imgURL = "assets/img/bubble-sm-blue.png";
			        		if(imgRand == 2)
			        			imgURL = "assets/img/bubble-sm-green.png";
		        			if(imgRand == 3)
		        				imgURL = "assets/img/bubble-sm-purple.png";
		        			if(imgRand == 4)
		        				imgURL = "assets/img/bubble-sm-yellow.png";			        		
			        	}
			        	if(sizeRand == 5){
			        		sizeWH = 25;
			        		imgRand = ran(1,4);
			        		if(imgRand == 1)
			        			imgURL = "assets/img/bubble-xs-blue.png";
			        		if(imgRand == 2)
			        			imgURL = "assets/img/bubble-xs-green.png";
		        			if(imgRand == 3)
		        				imgURL = "assets/img/bubble-xs-purple.png";
		        			if(imgRand == 4)
		        				imgURL = "assets/img/bubble-xs-yellow.png";				        		
			        	}

			            circs.push(paper.image(imgURL, ran(0,650), ran(0,700), sizeWH, sizeWH));
			            console.log(imgURL+": H: "+sizeWH+" W: "+sizeWH);
			        }

			        moveIt();
			    };
			}());

			function movePinUp(){
			    $("#animated-pin").animate({top: "+=18"}, 1500, movePinDown);
			}
			function movePinDown(){
			    $("#animated-pin").animate({top: "-=18"}, 1500, movePinUp);
			}

			function moveSMelipseRight(){
				$("#small-elipse").animate({left: "+=2"}, 1800, moveSMelipseLeft);	
			}
			function moveSMelipseLeft(){
				$("#small-elipse").animate({left: "-=2"}, 1800, moveSMelipseRight);	
			}

			function rotateBGelipse(){
			    var startAngle = 0;
			    var unit = 10;

			    var animate = function () {
			        var rad = startAngle * (Math.PI / 180);
			        $('#big-elipse').css({
			            left: -300 + Math.cos(rad) * unit + 'px',
			            top: unit * (1 - Math.sin(rad)) + 'px'
			        });
			        startAngle--;
			    }
			    var timer = setInterval(animate, 10);
			}

			function init(){
				var width = $(window).width();
				console.log(width);
				var panelWidth = 0.5;

				if(width<=414)
					panelWidth = 0.95;
				else if(width<=320)
					panelWidth = 1;

				

				$('#slider-one').movingBoxes({
				startPanel   : 2,      // start with this panel
				reducedSize  : 0.5,    // non-current panel size: 80% of panel size
				wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
				buildNav     : false,   // if true, navigation links will be added
				navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
				,panelWidth   : panelWidth,
				initAnimation: true,
				easing       : 'swing',
				completed: function(e, slider, tar){
					
					$("#movingbox-label").text(slider.curPanel+"/3");
				}
				});

				var mb = $('#slider-one').data('movingBoxes');

				$("#movingbox-next").bind("click", function(){
					mb.goForward();
				});

				$("#movingbox-prev").bind("click", function(){
					mb.goBack();
				});
			}


		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>