<?php
include_once "header.php";

$_SESSION['start'] = true;
$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
echo "<input type='hidden' id='protocol' name='protocol' value='".$protocol."'>";
echo "<input type='hidden' id='servername' name='servername' value='".$_SERVER['HTTP_HOST']."'>";
echo "";
if(isset($_SESSION['token'])){

	echo "<input type='hidden' id='hasToken' value=1 />";
} else {
	echo "<input type='hidden' id='hasToken' value=0 />";
}
?>
	<body style="background-color: #F4F5F8;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">HOW IT WORKS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
				
			</div>
			
			<div class="text-center section row">
				<div class="col-md-12">
					
					<center>

						We are so glad you are here! Our desire is to tell more people about how greate You are and to<br/>do that we would love to know a bit more about who you are and what you do.
					</center>
					
					<br>
					
					<h2 class="gftnow-title">
						About Your Business
					</h2>
					<br/><br/>
				</div>

				<div class="col-md-12">
					<center>
						
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-3 gftnow-nopadding">
									<div class="onboarding onboarding-left onboarding-0-left" style="height:750px;margin-top:0;">
										<br/><br/><br/><br/>
										<div class="onboarding-content">
											<div class="wizard">
												<div class="numberCircle active">1</div>
												<div class="numberCircle">2</div>
												<div class="numberCircle">3</div>
											</div>
											<br><br>
											<center>
												<div  class="onboarding-icon">
													<img src="assets/img/icon-house.png" style="width: 80px;height:auto;">
												</div>
											</center>
											<br><br>
												<p><center>
													<h3>Business Information</h3>
													<span class='gftnow-font-14'>This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
												</center></p>
											<br>
										</div>
									</div>
									<br/><br/>
								</div>
								<form method="post" action="onboarding-1.php" id="form1">
									<div class="col-md-5 gftnow-nopadding">
										<div class="onboarding onboarding-right" style="margin-top:0;background: url(assets/img/left-shadow.png) left;background-repeat:repeat-y;background-color: #fff;">
											<div class="onboarding-content row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<br/><br/>
													<div class="row">
														<div class="col-md-5">
																<div class="uploadLogo gftnow-rounded fileinput-button">
																	<?php if(isset($_SESSION['company']['logoURL'])){?>
																		<img class="img-responsive" src="<?php echo $_SESSION['company']['logoURL']; ?>"/>
																		<input id="fileupload" type="file" name="files[]">
																	<?php }else{?>
																		<h4>Upload Logo</h4>
																		<input id="fileupload" type="file" name="files[]">
																	<?php } ?>
																</div>
														</div>
														<div class="col-md-7 text-left gftnow-nopadding text-center">
																<br/><br/>
																<span class="small">We recommend 300x300 px <br/> File size should be under 5mb</span>
														</div>
													</div>
													<input type="hidden" name="clogo" id="clogo"class="form-control" value="<?php if(isset($_SESSION['company']['logoURL']))echo $_SESSION['company']['logoURL']; ?>"/>
													<br><br>
													<p id="company-placeholder" class="placeholder-effect"></p>
													<input class="form-control" type="text" name="company" id="company" placeholder="Company Name" value="<?php if(isset($_SESSION['company']['name']))echo $_SESSION['company']['name']; ?>">
													<br><br>
													<p id="email-placeholder" class="placeholder-effect"></p>
													<input class="form-control" type="text" name="email" id="email" placeholder="Email" value="<?php if(isset($_SESSION['company']['email']))echo $_SESSION['company']['email']; ?>">
													<br><br>
													<p id="password-placeholder" class="placeholder-effect"></p>
													<input class="form-control" type="password" name="password" id="password" placeholder="Password" value="<?php if(isset($_SESSION['company']['password']))echo $_SESSION['company']['password']; ?>">
													<br><br>
													<p id="repassword-placeholder" class="placeholder-effect"></p>
													<input class="form-control" type="password" name="repassword" id="repassword" placeholder="Confirm Password" value="<?php if(isset($_SESSION['company']['repassword']))echo $_SESSION['company']['repassword']; ?>">
													<br><br>
													
													
													<br/><br/><br/><br/><br/>
													<div class="text-right">
														<button type="submit" id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
														<button type="button" id="submit2" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
													</div>
													
												</div>
												<div class="col-md-2"></div>
											</div>
										</div>
									</div>
								</form>
								<div class="col-md-1"></div>
							</div>
						
					</center>
				</div>
			</div>
			
			<br/><br/>

			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>

		<div class="gftnow-mask" id="mask1">
			<div class="gftnow-modal">
				<div class="gftnow-modal-header text-center">
					<b>Logo</b> <br/><br/>
					<p>Move your image around to fit the orange box</p>
				</div>
				<div class="gftnow-modal-body">
					<img id="croppie-crop-icon" src="assets/img/mini-croppie-icon.png"/>
					<div id="croppie"></div>
					<br/>
					<center>
						<img id="loader" src="assets/img/ring-alt.svg" alt="Loading..." style="width:40px;height:40px;margin-left:-40px;display:none;">
						<button class="gftnow-btn gftnow-btn-default" id="croppie-done">DONE</button>
					</center>
					<br/>
				</div>
			</div>
		</div>
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">

			var basic;
			$(document).ready(function() {	
				checkvalidation();
				init();

				jQuery.validator.addMethod("zipcode", function(value, element) {
				  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
				}, "Please provide a valid zipcode.");

				if($("#hasToken").val() == 0){
					$("#submit").removeClass("gftnow-hide");
					$("#submit2").addClass("gftnow-hide");
				}
				if($("#hasToken").val() == 1){
					$("#submit2").removeClass("gftnow-hide");
					$("#submit").addClass("gftnow-hide");
				}

				$("#submit2").click(function(){
					window.location.href = 'onboarding-1.php';
				});

				$.validator.addMethod("pwcheck", function(value) {

				      return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]+$/.test(value)
				       && /[a-z]/.test(value) // has a lowercase letter
				       && /[A-Z]/.test(value) // has a uppercase letter
				       && /\d/.test(value)//has a digit
				       && /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)// has a special character
				      },"Must be at least 8 characters and consist of lowercase letter, uppercase letter, number and special characters");




				$("#form1").validate({
					rules:{
						company: "required",
						email: {
							required: true,
							email: true
						},
						password:{
							required: true,
							pwcheck: true,
							minlength: 8
						},
						repassword: {
							required: true
						}
					},
					submitHandler: function(form){

						$("#mask2").show();
						
						if($("#clogo").val()==""){
							$("#gftnow-alert-modal-content").text("Please upload your logo");
							$("#gftnow-alert-modal").modal("show");
							$("#mask2").hide();
						}else if($("#password").val()!=$("#repassword").val()){
							$("#gftnow-alert-modal-content").text("Password do not match");
							$("#gftnow-alert-modal").modal("show");
							$("#mask2").hide();
						}else{

							//form.submit();
							firebase.auth().createUserWithEmailAndPassword( $("#email").val(), $("#password").val() ).catch(function(error) {
					 
							  var errorCode = error.code;
							  var errorMessage = error.message;

							  console.log(errorCode);
							  console.log(errorMessage);
							 	
							  if(errorCode=="auth/email-already-in-use"){
							  	$("#gftnow-alert-modal-content").text(errorMessage);
								$("#gftnow-alert-modal").modal("show");

								$("#mask2").hide();
							  }
							  
							}).then(function(user){
								if(user){
									
									var cuser = firebase.auth().currentUser;

									cuser.getToken().then(function(data){
										var token = data;

										user.updateProfile({
						  	  				displayName: $("#company").val(),
										  	email: $("#email").val(),
										  	photoURL: $("#clogo").val(),
											}).then(function() {

												var company = {};
												company.name = $("#company").val();
												company.email = $("#email").val();
												company.password = $("#password").val();
												company.logoURL = $("#clogo").val();

												console.log(token);

												jQuery.ajax({
											    	url: "mvc/controller/ajaxController.php",
											    	type: "post",
											    	dataType: "json",
											    	data: {func: "signInAnonymous", token: token, company:company},
											    	success: function(data){
											    		//console.log(data);
											    		window.location = "onboarding-1.php";
											    	},error: function(err){
											    		console.log(err.responseText);
											    	}
											    });
										  		
										  		/*user.sendEmailVerification().then(function() {

												}, function(error) {

												});*/
										  		
											}, function(error) {
												console.log(error);
										});
									});
								}
							});
						}
							
					}
				});

				$("input").focus(function(){
					checkvalidation();
				});

				$("input").keyup(function(){
					checkvalidation();
				});
			});
			
			function checkvalidation()
			{
				var err = 0;

				$("input").each(function(){
					if ( $(this).hasClass("invalid") )
						err++;
				});

				if( $("#company").val() == "" )
					err++;

				if( $("#email").val() == "" )
					err++;

				if( $("#password").val() == "" )
					err++;

				if( $("#password").val() != $("#repassword").val() )
					err++;

				/*if( $("#phone").val() == "" )
					err++;

				if( $("#address").val() == "" )
					err++;

				if( $("#city").val() == "" )
					err++;

				if( $("#state").val() == "" )
					err++;

				if( $("#zip").val() == "" )
					err++;*/

				if( $("#clogo").val() == "" )
					err++;


				if(err==0){
					$("#submit").removeClass("gftnow-btn-default");
					$("#submit").addClass("gftnow-btn-success");
					$("#submit2").removeClass("gftnow-btn-default");
					$("#submit2").addClass("gftnow-btn-success");
					//$("#submit").prop("disabled",false);
				}else{
					$("#submit").addClass("gftnow-btn-default");
					$("#submit").removeClass("gftnow-btn-success");
					$("#submit2").addClass("gftnow-btn-default");
					$("#submit2").removeClass("gftnow-btn-success");
					//$("#submit").prop("disabled",true);
				}
			}


			function init(){
				
				fileupload();

			    basic = $('#croppie').croppie({
				    viewport: {
				        width: 200,
				        height: 200,
				        type: "circle"
				    },
				    customClass: "gftnow-croppie-container",
				    showZoomer: false
				});

				$("#croppie-crop-icon, #croppie-done").click(function(){
					if($("#croppie-done").hasClass("gftnow-btn-success")){
						basic.croppie('result','base64').then(function(base64){
							var cropted = base64;

							jQuery(document).ajaxStart(function(){
								$("#mask2").hide();
								$("#loader").show();
							});
							jQuery(document).ajaxComplete(function(){
								$("#loader").hide();
							});

							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType: "json",
								data: { func: "base64ToPNG", base64: base64 },
								success: function(data){

									if(data.result=="OK")
									{
										$("#clogo").val( $("#protocol").val()+$("#servername").val()+'/assets/upload/php/files/'+data.file);
										$(".uploadLogo").html("<img class='img-responsive' src='assets/upload/php/files/"+data.file+"' /><input id='fileupload' type='file' name='files[]'/'>");
										$("#mask1").hide();
										checkvalidation();
										fileupload();
									}
									
								},error: function(err){
									console.log(err.responseText);
									$("#gftnow-alert-modal-content").html("The requested resource does not allow request data width GET requests, or the amount of data provided in the request exceeds the capacity limit.");
									$("#gftnow-alert-modal").modal("show");
									$("#mask1").fadeOut("fast");
								}
							});
							
						});
					}
				});

			}

			function fileupload(){

				var url = "assets/upload/php/index.php";
				var cropted = "";
				'use strict';

			    $('#fileupload').fileupload({
			        url: url,
			        add: function(e, data) {
		                var uploadErrors = [];
		                var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
		                if(!acceptFileTypes.test(data.originalFiles[0]['type'])) {
		                    uploadErrors.push('Not an accepted file type. Accepted file types are as follows: GIF, JPG, and PNG.');
		                }
		                if(data.originalFiles[0]['size'] > 2000000) {
		                    uploadErrors.push('Filesize is too big. Import at least 2 MB.');
		                }
		                if(uploadErrors.length > 0) {
							$("#gftnow-alert-modal-content").html(uploadErrors.join("\n"));
							$("#gftnow-alert-modal").modal("show");				                    
		                } else {
		                    data.submit();
		                }
		                console.log(data.originalFiles[0]);
                	},
			        dataType: 'json',
			        done: function (e, data) {
			        	if(data.result.files.length > 0){

			        		var filesize = (data.result.files[0].size / (1024*1024)).toFixed(2);
			        		console.log(filesize+" MB");

							basic.croppie('bind', {
							    url: data.result.files[0].url
							    /*,points: [77,469,280,739]*/
							});

			            	$("#mask2").hide();
			            	$("#mask1").show();
			            	$("#croppie-done").removeClass("gftnow-btn-default").addClass("gftnow-btn-success");
			            }
			            
			        },
			        error: function(err){
			        	console.log("error");
			        	console.log(err.responseText);
						$("#gftnow-alert-modal-content").html("Error uploading file!");
						$("#gftnow-alert-modal").modal("show");
			        },
			        progressall: function (e, data) {
			        	console.log("uploading");
			        }
			    }).prop('disabled', !$.support.fileInput)
			        .parent().addClass($.support.fileInput ? undefined : 'disabled');

		        $('button.cancel').click(function (e) {
				    xhr.abort();
				    $("#mask2").hide();
				});
			}


		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>