<nav>
	<ul>
		<li id="admin-menu-overview">
			<a href="dashboard.php?p=admin/realtime_map" title="Overview" style="font-weight:bold;"><img src="assets/img/overview_logo.png"> <span class="menu-item-parent">Overview</span></a>
		</li>
		<li id="admin-menu-users">
			<a href="dashboard.php?p=admin/users" title="Users" style="font-weight:bold;"><img src="assets/img/users.png"> <span class="menu-item-parent">Users</span></a>
		</li>
		<li id="admin-menu-vendors">
			<a href="dashboard.php?p=admin/vendors" title="Vendor" style="font-weight:bold;"><img src="assets/img/vendors.png"> <span class="menu-item-parent">Company</span></a>
		</li>
		<li id="admin-menu-approval">
			<a href="dashboard.php?p=admin/vendors_approval" title="Approval" style="font-weight:bold;"><img src="assets/img/approval.png"> <span class="menu-item-parent">Campaigns</span></a>
		</li>
	</ul>
</nav>

<nav class="nav-bottom">
	<ul>
		<li id="admin-menu-settings">
			<a href="dashboard.php?p=admin/admin_panel_settings_superadmin" title="Overview" style="font-weight:bold;"><img src="assets/img/settings.png"> <span class="menu-item-parent">Settings</span></a>
		</li>
		<li id="admin-menu-support">
			<a href="#" title="Users" style="font-weight:bold;"><img src="assets/img/support.png"> <span class="menu-item-parent">Support</span></a>
		</li>
		<li id="admin-menu-help">
			<a href="#" title="Vendor" style="font-weight:bold;"><img src="assets/img/helpcenter.png"> <span class="menu-item-parent">Help Center</span></a>
		</li>
	</ul>
</nav>