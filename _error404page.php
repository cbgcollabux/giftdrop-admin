<?php
include_once "header.php";
?>
	<style>
		.err-link{
			color: #E3B338;
		}
		.err-link:hover{
			color: #FDD05E;
		}
	</style>
	<body style="background-color:#F4F5F8;padding:0;margin:0;overflow:auto;height:100%;margin:0px;">
			


		<div class="container-full">
			<div class="landing-nav-2">
				<div class="landing-nav-header" style="width:90%;">
					<div class="row">
						<div class="col-md-12 text-center title-logo">
							<br/><br/><br/>
						</div>
					</div>
				</div>
			</div>
			<br/><br/>
			<center>
				<h1 style="color:#E3B338;font-size:70px;"><i class="fa fa-warning"></i></h1>
				<h1 style="font-size:60px;">Error 404</h1>
				<img src="assets/img/sampleupload2-1.png">
				<h3 class="gftnow-font-light">Page doesn't exists or some other error occured.<br/>Go to our <a class="err-link gftnow-font-medium" href="index.php">home page</a> or go back to <a class="err-link gftnow-font-medium" onclick="window.history.back();" href="javascript:void(0);">previous page</a>.<h3>
			</center>
		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>
		
		<script type="text/javascript">


			$(document).ready(function() {	

				$("#resent").click(function(){
					var user = firebase.auth().currentUser;

					user.sendEmailVerification().then(function() {
			        	$("#gftnow-alert-modal-content").text("Password reset email sent successfully");
						$("#gftnow-alert-modal").modal("show");
					}, function(error) {
			        	$("#gftnow-alert-modal-content").text("Error sending password reset email:" +error);
						$("#gftnow-alert-modal").modal("show");
					});
				});

				$("#close-confirm-modal").bind("click", function(){
					$("#confirm-modal").fadeOut("fast");
				});

				jQuery(document).ajaxStart(function(){
					$("#mask2").hide();
				});

				jQuery(document).ajaxComplete(function(){	
					$("#mask2").hide();
				});


					
			});

			function goto(){
				var user = firebase.auth().currentUser;

				user.getToken().then(function(data) {
				      
	    			var token = data;
							
					jQuery.ajax({
						url: "mvc/controller/ajaxController.php",
						type: "post",
						dataType : "json",
						data: { func: "ajaxLogin",token:token },
						success: function(data){
							console.log(data);
							if(data.result="ok"){
								window.location = "index.php?p=business/profile";
							}else{
								console.log("cant login");
							}
						},error: function(err){
							console.log(err.responseText);
						}
					});
	  	  			

    			});


			}


		</script>

		

	</body>

</html>