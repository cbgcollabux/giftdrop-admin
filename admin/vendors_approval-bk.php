<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	/*$user = new user();
	$listCompanyCategories = $user->listCompanyCategories();
	$result = $user->findMyCompanyProfile();*/

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	function findCategory($id){
		$category;
		if($id == "1b25-4e1d-b2a5-adcfb5fbcdeb"){ $category = "Cash"; }
		if($id == "b6dae6b7-d02a-458b-ad22-3b626ddd882d"){ $category = "Food & Drinks"; }
		if($id == "a02a6059-aaf4-4cc1-9494-a31a1715fbbd"){ $category = "Automotive"; }
		if($id == "61360c31-3cae-4f3d-b395-e0f9aa320d47"){ $category = "Clothing"; }
		if($id == "831bd6c2-73c7-4502-a0c5-629b7a0331c6"){ $category = "Electronics"; }
		if($id == "280bc362-960b-4203-9f5e-a108d4b593d9"){ $category = "Entertainment"; }
		if($id == "63452c3b-8145-447b-aa45-ef12479693c2"){ $category = "Health & Beauty"; }
		if($id == "06125b59-0b1c-4c75-9a05-ecd821a82940"){ $category = "Home Improvements"; }
		if($id == "8a8cf347-8fbe-4850-84dd-312c06ee5905"){ $category = "Kids"; }
		if($id == "f9003709-0cd9-4f5d-83af-77b690d83251"){ $category = "Other"; }
		if($id == "ba864d3b-d434-4fbc-b0aa-b09df2af1c35"){ $category = "Pets"; }
		if($id == "8acf3596-eec1-4572-99d8-4e5463c3170b"){ $category = "Sport & Fitness"; }
		if($id == "0360c1e1-008d-4480-9a96-b0a5c95d0339"){ $category = "Travel"; }
		return $category;
	}

	$state_drop_active;
	$time_drop_active;

	$state;
	if(isset($_GET['state'])){
		if($_GET['state'] == "Blocked"){
			$state = 'Disabled';
			$state_drop_active = "Blocked";
		}
		if($_GET['state'] == "Active"){
			$state = 'Enabled';
			$state_drop_active = "Active";
		}
		$time_drop_active = "All Time";
	}else {
		$state = '';
		$state_drop_active = "Show All";
	}

	if(isset($_GET['time'])){
		if($_GET['time'] == 'last7days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 weeks"));
			$time_drop_active = 'Last 7 Days';
		}
		if($_GET['time'] == 'last30days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 months"));
			$time_drop_active = 'Last 30 Days';
		}
		$state_drop_active = "Show All";
	} else {
		$from = gmdate("Y-m-d\TH:i:s\Z");
		$time_drop_active = 'All Time';
	}

	$to = gmdate("Y-m-d\TH:i:s\Z");

	$listCompanies;
    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api_admin = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listCompaniesBody = new Swagger\Client\Model\Body6();
	try {
	    $listCompanies = $api_admin->listCompanies($listCompaniesBody);

	   /* echo "<pre>";
	    print_r($listCompanies);
	    echo "</pre>";*/
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listCompanies: ', $e->getMessage(), PHP_EOL;
	}

	/*echo"<pre>";
	print_r($date_minus_1week);
	echo"</pre>";*/

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Approval";
	$content['script'] = 
	'$(document).ready(function() {
		$("#logout").show();
		$("#admin-menu-approval").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#search-field").keyup(function() {
		    var value = $(this).val();

		    $(".active-list").each(function(index) {
		        var id = $(this).find("div").text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-2").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});

		$(".active-list").css("cursor","pointer");

		$(".active-list").click(function(){
			var identifier = $(this).attr("id");
			window.location.href = "index.php?p=admin/vendors_approval1&id="+identifier;
		});

		$(".active-list").hover(function(){
			$(this).css("background-color", "#f4f5f8");
			$(".active-list").not(this).css("background-color", "#ffffff");
		});

	    pageSize = 10;

	    var pageCount =  $(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $(".active-list").hide();
	        $(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });
	});';

	$content['right-of-title'] = '<div class="row">
		<div id="hidden-col-md-2" class="col-md-2" align="center"></div>
		<div class="col-md-4">
			<div class="drop-rounded-container">
				<div id="drop-rounded-header-1" class="drop-rounded-header">
					<span>'.$state_drop_active.'</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div id="drop-rounded-body-1" class="drop-rounded-body">
					<ul>
						<a href="index.php?p=admin/vendors_approval"><li>Show All</li></a>
						<a href="index.php?p=admin/vendors_approval&state=Blocked"><li>Blocked</li></a>
						<a href="index.php?p=admin/vendors_approval&state=Active"><li>Active</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4">
		 	<div class="drop-rounded-container">
				<div id="drop-rounded-header-2" class="drop-rounded-header">
					<span>'.$time_drop_active.'</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div id="drop-rounded-body-2" class="drop-rounded-body">
					<ul>
						<a href="index.php?p=admin/vendors_approval"><li>All Time</li></a>
						<a href="index.php?p=admin/vendors_approval&time=last30days"><li>Last 30 Days</li></a>
						<a href="index.php?p=admin/vendors_approval&time=last7days"><li>Last 7 Days</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div id="searchField-div" class="col-md-4" style="display:none;">
			<div class="inner-addon left-addon">
			<span class="addon-search"><i class="fa fa-search"></i></span>
			    <input type="text" id="search-field" class="rounded-search form-control gotham-regular-placeholder" placeholder="Search" style="cursor:default;background-color:inherit;" />
			</div>
		</div>
		<div id="searchBtn-div" class="col-md-2" align="center">
			<div id="searchBtn" class="rounded-search" style="height:35px;">
				<i class="fa fa-search"></i>
			</div>
		</div>
	</div>';

	$vendors_content='';

	foreach($listCompanies['companies'] as $company){
		$date = $company['created']->format('n/j/Y');

		$icon;
		if($company['state'] == "Disabled")
			$stateTxt = "Pending Approval";
		if($company['state'] == "Enabled")
			$stateTxt = "Active";

		if($state == ""){
			$vendors_content.='<div id='.$company['identifier'].' class="row active-list"><div class="col-md-3"><p>'.trim_text($company['name'], "...", 12).'</p></div><div class="col-md-3"><p>'.findCategory($company['category']).'</p></div><div class="col-md-2"><p>'.trim_text($company['address'], "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
		} else {
			if($company['state'] == $state){
				$vendors_content.='<div id='.$company['identifier'].' class="row active-list"><div class="col-md-3"><p>'.trim_text($company['name'], "...", 12).'</p></div><div class="col-md-3"><p>'.findCategory($company['category']).'</p></div><div class="col-md-2"><p>'.trim_text($company['address'], "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
			}
		}
	}

	$content['content'] = 
	'<div class="active-main-panel">
		<div class="row active-list-title">
				<div class="col-md-3">
					<strong>Name</strong>
				</div>
				<div class="col-md-3">
					<strong>Category</strong>
				</div>
				<div class="col-md-2">
					<strong>Location</strong>
				</div>
				<div class="col-md-2">
					<strong>Registered</strong>
				</div>
				<div class="col-md-2">
					<strong>State</strong>
				</div>
		</div>
		'.$vendors_content.'
		<center>
			<ul id="pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>