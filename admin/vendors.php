<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	function findCategory($id){
		$category = "Other";
		if($id == "1b25-4e1d-b2a5-adcfb5fbcdeb"){ $category = "Cash"; }
		if($id == "b6dae6b7-d02a-458b-ad22-3b626ddd882d"){ $category = "Food & Drinks"; }
		if($id == "a02a6059-aaf4-4cc1-9494-a31a1715fbbd"){ $category = "Automotive"; }
		if($id == "61360c31-3cae-4f3d-b395-e0f9aa320d47"){ $category = "Clothing"; }
		if($id == "831bd6c2-73c7-4502-a0c5-629b7a0331c6"){ $category = "Electronics"; }
		if($id == "280bc362-960b-4203-9f5e-a108d4b593d9"){ $category = "Entertainment"; }
		if($id == "63452c3b-8145-447b-aa45-ef12479693c2"){ $category = "Health & Beauty"; }
		if($id == "06125b59-0b1c-4c75-9a05-ecd821a82940"){ $category = "Home Improvements"; }
		if($id == "8a8cf347-8fbe-4850-84dd-312c06ee5905"){ $category = "Kids"; }
		if($id == "f9003709-0cd9-4f5d-83af-77b690d83251"){ $category = "Other"; }
		if($id == "ba864d3b-d434-4fbc-b0aa-b09df2af1c35"){ $category = "Pets"; }
		if($id == "8acf3596-eec1-4572-99d8-4e5463c3170b"){ $category = "Sport & Fitness"; }
		if($id == "0360c1e1-008d-4480-9a96-b0a5c95d0339"){ $category = "Travel"; }
		return $category;
	}

	$state_drop_active;
	$time_drop_active;

	$state;
	if(isset($_GET['state'])){
		if($_GET['state'] == "pending"){
			$state = 'Disabled';
			$state_drop_active = "pending";
		}
		if($_GET['state'] == "active"){
			$state = 'Enabled';
			$state_drop_active = "active";
		}
		$time_drop_active = "All Time";
	}else {
		$state = '';
		$state_drop_active = "Show All";
	}

	if(isset($_GET['time'])){
		if($_GET['time'] == 'last7days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 weeks"));
			$time_drop_active = 'Last 7 Days';
		}
		if($_GET['time'] == 'last30days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 months"));
			$time_drop_active = 'Last 30 Days';
		}
		$state_drop_active = "Show All";
	} else {
		$from = gmdate("Y-m-d\TH:i:s\Z");
		$time_drop_active = 'All Time';
	}

	$to = gmdate("Y-m-d\TH:i:s\Z");

	$listCompanies;
    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api_admin = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\CompanyListRequest;
	try {
	    $listCompanies = $api_admin->listCompanies($body);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listCompanies: ', $e->getMessage(), PHP_EOL;
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Company Approval<!--<a href='dashboard.php?p=admin/vendors_overview' style='font-size:15px;color:#5DADE2;'>See overview</a>-->";
	$content['script'] = 
	'
		$(document).ready(function() {

		$(".company-row").click(function(){
			var id = $(this).attr("data-id");
			var account_id = $(this).attr("data-account");
			console.log(id);
			window.location = "dashboard.php?p=admin/vendor_active&id="+id+"&account_id="+account_id;
		});

		$("#stateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/vendors&state="+state+"&time="+time+"&text="+text;
		});
		
		$("#filterText").keydown(function (e){
		    if(e.keyCode == 13){
		    	var state = $("#stateFilter").val();
				var time = $("#dateFilter").val();
				var text = $("#filterText").val();

				window.location = "dashboard.php?p=admin/vendors&state="+state+"&time="+time+"&text="+text;
		    }
		})

		$("#dateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/vendors&state="+state+"&time="+time+"&text="+text;
		});

		$("#table1").dataTable({"bFilter": false,"bInfo": false,"bLengthChange": false});

		$("#logout").show();
		$("#admin-menu-vendors").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#search-field").keyup(function(){
		    var value = $(this).val();

		    $(".active-list").each(function(index) {
		        var id = $(this).find("div").text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-2").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});

		$(".active-list").css("cursor","pointer");

		$(".active-list").click(function(){
			var identifier = $(this).attr("id");
			window.location.href = "dashboard.php?p=admin/vendor_active&id="+identifier;
		});

		$(".active-list").hover(function(){
			$(this).css("background-color", "#f4f5f8");
			$(".active-list").not(this).css("background-color", "#ffffff");
		});

	    pageSize = 10;

	    var pageCount =  $(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $(".active-list").hide();
	        $(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });
	});';

	$content['right-of-title'] = '<div class="row">
		
		<div class="col-md-4">
			<select class="form-control" id="stateFilter">
				<option '.( (isset($_GET['state']))? ($_GET['state']=="all")?"selected":"" : ""  ).' value="all">All</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="pending")?"selected":"" : ""  ).' value="pending">Pending Approval</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="active")?"selected":"" : ""  ).' value="active">Active</option>
			</select>
		</div>
		<div class="col-md-4">
			<select class="form-control" id="dateFilter">
				<option value="all" '.( (isset($_GET['time']))? ($_GET['time']=="all")?"selected":"" : ""  ).'>All</option>
				<option value="last30days" '.( (isset($_GET['time']))? ($_GET['time']=="last30days")?"selected":"" : ""  ).'>Last 30 Days</option>
				<option value="last7days" '.( (isset($_GET['time']))? ($_GET['time']=="last7days")?"selected":"" : ""  ).'>Last 7 Days</option>
			</select>
		</div>
		<div class="col-md-4">
			<input type="text" class="form-control" id="filterText" value="'.( isset($_GET['text'])?$_GET['text']:'' ).'" />
		</div>
	</div>';

	$vendors_content='';

	foreach($listCompanies['companies'] as $company){
		$date = $company['created']->format('d-m-Y');

		if($company['activated'] == "")
			$stateTxt = "Pending Approval";
		else
			$stateTxt = "Active";

		$included=0;

		//state filter
		if(isset($_GET['state'])){
			if($_GET['state']=="pending"){
				if($company['activated']=="")
					if($company['state']=="Disabled")
						$included=1;
			}elseif($_GET['state']=="active"){
				if($company['activated']!="")
					if($company['state']=="Enabled")
						$included=1;
			}else{
				$included=1;
			}
		}else{
			$included=1;
		}

		//date filter
		if($included==1)
		{
			if(isset($_GET['time']))
			{
				if($_GET['time']=="last30days"){
					$minus = gmdate("d-m-Y", strtotime("-30 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}

				}else if($_GET['time']=="last7days"){
					$minus = gmdate("d-m-Y", strtotime("-7 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}
				}
			}
		}

		if($included==1){
			if(isset($_GET['text'])){
				if($_GET['text']!=""){
					if (strpos( strtolower( $company['name'] ), strtolower( $_GET['text'] ) ) !== false) {
					    $included=1;
					}else{
						$included=0;
					}
				}
			}
		}

		if($included==1)
			$vendors_content .="<tr class='company-row' data-account='".$company['account']['identifier']."' data-id='".$company['identifier']."'><td>".trim_text($company['name'], "...", 12)."</td><td>".findCategory($company['category'])."</td><td>".trim_text($company['address'], "...", 12)."</td><td>".$date."</td><td>".$stateTxt."</td></tr>";
	}

	$content['content'] = '
	<style>
		.company-row{
			cursor: pointer;
		}
		#table1 th, #table1 td,table.dataTable.no-footer{
			background-color:#ffff;
			border: none !important;
		}
		.table tr, .table th, .table td{
			border: none !important;
			padding-top: 20px !important;
			padding-bottom: 20px !important;
		}
		.dataTables_paginate{
			float: none !important;
			text-align:center !important;
			padding-top: 25px !important;
		}
		.paginate_button.current{
			background: none !important;
			background-color: #619534 !important;
			
			border-radius: 50px !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button.current{
			color: #fff !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
			background:none !important;
			border: 1px solid #fff !important;
			color: #514F4F !important;
		}
		.active-main-panel{
			padding-top:30px;
			padding-bottom: 30px;
		}
		.company-row:hover td{
			background-color: #f4f5f8 !important;
		}
	</style>

	<div class="active-main-panel">
		<table id="table1" class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Category</th>
					<th>Location</th>
					<th>Registered</th>
					<th>State</th>
				</tr>
			</thead>
			<tbody>
				'.$vendors_content.'
			</tbody>
		</table>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>