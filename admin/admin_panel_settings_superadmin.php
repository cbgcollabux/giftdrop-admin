<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	$listSettings;

    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUsersBody = new Swagger\Client\Model\UserListRequest();
	$listActivePlansBody = new Swagger\Client\Model\DisablePlansRequest();
	
	try {
	    $listUsers = $api->listUsers($listUsersBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

	try {
	    $listSettings = $api->listSettings();
	} catch (Exception $e) {
		echo 'Exception when calling AdminServiceApi->listSettings: ', $e->getMessage(), PHP_EOL;
	}

	try {
		$listActivePlans = $api->listActivePlans($listActivePlansBody);
	} catch (Exception $e){
		echo 'Exception when calling AdminServiceApi->listActivePlansBody: ', $e->getMessage(), PHP_EOL;
	}

	function findMySetting($name, $ls){
		$mySettings = null;
		foreach($ls['settings'] as $settings){
			if($settings['name'] == $name){
				$mySettings = $settings['value'];
				break;
			}
		}
		return $mySettings;
	}

	$admin = '';
	foreach(array_reverse($listUsers['users']) as $user){
		if($user['role'] == "Admin"){
			$admin .= '<div class="row active-list">
							<div class="col-md-3">
								'.$user['role'].'
							</div>
							<div class="col-md-6">
								'.$user['name'].'
							</div>
							<div class="col-md-3">
								<p style="cursor:pointer;" class="choose-action-btn">Choose Action <i class="fa fa-chevron-down"></i></p>
								<div class="drop-rounded-body choose-action">
									<ul>
										<li style="cursor:pointer;">Make Admin</li>
										<li style="cursor:pointer;">Make Manager</li>
										<li style="cursor:pointer;">Freeze Access</li>
										<li style="cursor:pointer;">Delete</li>
									</ul>
								</div>
							</div>
						<hr>
						</div>';
		}
	}

	$alert_active;
	if(isset($_GET["plan_removed"])){ $alert_active = '<input type="hidden" id="alert_active" value="plan_removed">'; }
	elseif(isset($_GET['plan_added'])){ $alert_active = '<input type="hidden" id="alert_active" value="plan_added">'; }
	elseif(isset($_GET["plan_removed"]) && isset($_GET['plan_added'])){ $alert_active = '<input type="hidden" id="alert_active" value="none">'; }
	else{ $alert_active = '<input type="hidden" id="alert_active" value="none">'; }

	$plans = '';
	foreach($listActivePlans['plans'] as $plan){
		$fees = '';
		foreach($plan['fees'] as $fee){
			$fees .= '<li>Range Begin: '.number_format($fee['range_begin']).'</li>';
			$fees .= '<li>Range End: '.number_format($fee['range_end'],2).'</li>';
			$fees .= '<li>Fixed: '.number_format($fee['fixed'],2).'</li>';
			$fees .= '<li>Percentage: $'.number_format($fee['percentage'],2).'</li>';
			$fees .= '<li>Card Fixed: $'.number_format($fee['card_fixed'],2).'</li>';
			$fees .= '<li>Card Percentage: $'.number_format($fee['card_percentage'],2).'</li>';
		}

		$button = '<button data-id="'.$plan['identifier'].'" class="btn btn-warning gftnow-btn-warning gftnow-btn disable-plan-btn">Remove</button>';

		$plans .= '<div class="row active-list">
						<div class="col-md-3">
							'.$plan['name'].'
						</div>
						<div class="col-md-6">
							<ul>'.$fees.'</ul>
						</div>
						<div class="col-md-3">
							'.$button.'<br/><br/><button data-id="'.$plan['identifier'].'" data-name="'.$plan['name'].'" class="btn gftnow-btn-success gftnow-btn assign-plan-btn">Assign</button>
						</div>
						<hr>
					</div>';
	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';
	$content['title'] = 'Settings';
	$content['right-of-title'] = '';
	$content['script'] = 
	'$(document).ready(function(){
		var feeCount = 0;

		$("#logout").show();
		$("#admin-menu-settings").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		pageSize = 10;

	    var pageCount =  $("#admin-row").find(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $("#admin-row").find(".active-list").hide();
	        $("#admin-row").find(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });

		$("#add-admin-btn").click(function(){
			$(this).blur();
			$("#add-admin-modal").fadeIn("fast");
		});

		$("#close-add-admin-modal").click(function(){
			$("#add-admin-modal").fadeOut("fast");
		});

		checkvalidation();

		$.validator.addMethod("pwcheck", function(value) {
			 return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]+$/.test(value)
			&& /[a-z]/.test(value) // has a lowercase letter
			&& /[A-Z]/.test(value) // has a uppercase letter
			&& /\d/.test(value)//has a digit
			&& /[!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]/.test(value)
		},"Must be at least 8 characters and consist of lowercase letter, uppercase letter, number and special characters");

		$("#form1").validate({
			rules:{
				email: {
					required: true,
					email: true
				},
				password:{
					required: true,
					pwcheck: true,
					minlength: 8
				}
			},
			submitHandler: function(form){

				$("#mask2").show();

				var email = $("#email").val();
				var password = $("#password").val();

				var haserror = 0;

				firebase.auth().createUserWithEmailAndPassword( email, password ).catch(function(error) {
		 
					var errorCode = error.code;
					var errorMessage = error.message;

					console.log(errorCode);
					console.log(errorMessage);
				 	
					if(errorCode=="auth/invalid-email"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/weak-password"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/email-already-in-use"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
				  
				}).then(function(user){
					if(haserror == 0){
						
						var cuser = firebase.auth().currentUser;

						cuser.getToken().then(function(data){
							var token = data;

							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType : "json",
								data: { func: "loginAdmin",token:token },
								success: function(data){
									console.log(data);

									location.reload();
									/*if(data.result.result.userInfo.identifier){
										$("#add-admin-modal").fadeIn("fast");
										location.reload();
									}else{
										console.log("cant add");
									}*/
								},error: function(err){
									console.log(err.responseText);
								}
							});
						});
					} else {
						$("#mask2").hide();
					}
				});
			}
		});

		$("input").focus(function(){
			checkvalidation();
		});

		$("input").keyup(function(){
			checkvalidation();
		});

		function checkvalidation(){
			var err = 0;
			$("input").each(function(){
				if ( $(this).hasClass("invalid") )
					err++;
			});

			if( $("#email").val() == "" )
				err++;

			if( $("#password").val() == "" )
				err++;

			if(err==0){
				$("#submit").removeClass("gftnow-btn-default");
				$("#submit").addClass("gftnow-btn-success");
				$("#submit").attr("type", "submit");
			}else{
				$("#submit").addClass("gftnow-btn-default");
				$("#submit").removeClass("gftnow-btn-success");
				$("#submit").attr("type", "button");
			}
		}


		$(".choose-action-btn").click(function(){
			$(this).parent().find(".choose-action").slideToggle("fast");
		});

		$(".gftnow-checkbox").click(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
			} else {
				$(this).addClass("active");
			}
		});



		//Plans alert
		if($("#alert_active").val() == "plan_removed"){
			$("#plan-removed-alert").show();
			$(function() {
				$("html, body").animate({
	                scrollTop: $("#plan-removed-alert").offset().top - 150
	            }, 500);
			});
		}
		if($("#alert_active").val() == "plan_added"){
			$("#plan-added-alert").show();
			$(function() {
				$("html, body").animate({
	                scrollTop: $("#plan-added-alert").offset().top - 150
	            }, 500);
			});
		}


		// Plans pagination
		var plansPageSize = 5

	    var plansPageCount =  $("#plans-row").find(".active-list").length / plansPageSize;
	      
	    for(var i = 0 ; i<plansPageCount;i++){
	      $("#plans-pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#plans-pagination li").first().find("a").addClass("current");
	    
	    showPlanPage = function(page) {
	        $("#plans-row").find(".active-list").hide();
	        $("#plans-row").find(".active-list").each(function(n) {
	            if (n >= plansPageSize * (page - 1) && n < plansPageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPlanPage(1);

	    $("#plans-pagination li a").click(function() {
	        $("#plans-pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPlanPage(parseInt($(this).text()))
	    });


		// Create Plan	

		$(".add-plan-inputs").keyup(function(e)
		{	
			planValidation();
		});

		function planValidation()
		{
			var error=0;

     		if( $("#planName").val() ==""){
     			error++;
     		}

     		if( $("#planDescription").val() ==""){
     			error++;
     		}


     		$(".add-plan-inputs").each(function(){
     			if($(this).val()==""){
     				error++;
     			}
     		});

			if(feeCount==0)
				error++;

     		if(error==0){
     			$("#submit-add-plan").prop("disabled",false);
     		}else{
     			$("#submit-add-plan").prop("disabled",true);
     		}
		}

		$("#add-plan-btn").click(function(){
			$(this).blur();
			$("#add-plan-modal").fadeIn("fast");
		});

		$("#numFees").keypress(function(e){
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		     	return false;
		     }
		});

		$("#add-planNameNumFees-btn").click(function(){
			var gftnowScrollbarHTML = "<label class=\'badge\'>FEE "+(feeCount+1)+"</label><div class=\'row\'>";

			gftnowScrollbarHTML +=	
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'rangeBegin"+(feeCount)+"\' name=\'rangeBegin"+feeCount+"\' placeholder=\'Range Begin\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'rangeEnd"+feeCount+"\' name=\'rangeEnd"+feeCount+"\' placeholder=\'Range End\' /></div><br/><br/>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'fixed"+feeCount+"\' name=\'fixed"+feeCount+"\' placeholder=\'Fixed\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'cardFixed"+feeCount+"\' name=\'cardFixed"+feeCount+"\' placeholder=\'Card Fixed\' /></div><br/><br/>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'percentage"+feeCount+"\' name=\'percentage"+feeCount+"\' placeholder=\'Percentage\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'cardPercentage"+feeCount+"\' name=\'cardPercentage"+feeCount+"\' placeholder=\'Card Percentage\' /></div>"+
					"<br/><br/><br/>";

			gftnowScrollbarHTML += "</div>";

			$("#gftnow-scrollbar").append(gftnowScrollbarHTML);

			feeCount++;


			$(".add-plan-inputs").keyup(function(e)
			{	
				planValidation();
			});

			$(".add-planFee-inputs").keypress(function(e){
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		     		return false;
		     	}
			});

		});

		$("#submit-add-plan").click(function(){
			var err=0;
			$(".add-plan-inputs").each(function(){
				if($(this).val() == ""){
					err++;
				}
			});

			if(err == 0){
				var fees = new Array();
				//var feeCount = $(".add-planFee-inputs").length / 6;

				for(i = 0; i < feeCount; i++){
					var fee = {};
					fee.rangeBegin = parseInt($("#rangeBegin"+i).val());
					fee.rangeEnd = parseInt($("#rangeEnd"+i).val());
					fee.fixed = parseInt($("#fixed"+i).val());
					fee.cardFixed = parseInt($("#cardFixed"+i).val());
					fee.percentage = parseInt($("#percentage"+i).val());
					fee.cardPercentage = parseInt($("#cardPercentage"+i).val());
					fees.push(fee);
				}

				console.log(fees);

				jQuery.ajax({
					url:"mvc/controller/ajaxController.php",
					type: "post",
					dataType: "json",
					data: {name: $("#planName").val(), fees: fees, func: "ajaxCreatePlan"},
					success: function(data){
						//location.reload();
						window.location.href = "dashboard.php?p=admin/admin_panel_settings_superadmin&plan_added";
						console.log(data);
					},error: function(err){
						console.log(err.responseText);
					}
				});

			} else {
				$("#add-plan-error").show();
			}
			
		});

		$("#close-add-plan-modal").click(function(){
			$("#add-plan-modal").fadeOut("fast");
			$("#planName").val("");
			$("#numFees").val("");
			$("#add-planNameNumFees-btn").removeClass("gftnow-btn gftnow-btn-default btn btn-success").addClass("gftnow-btn gftnow-btn-success btn btn-success");
			$("#numFees").prop("disabled", false);
			$("#add-plan-error").hide();
			$("#gftnow-scrollbar").html("");
		});


		// Assign Plan
		$("#search-field").keyup(function(){
		    var value = $(this).val();

		    $(".list-users").each(function(index){
		        var id = $(this).text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});
		
		$(".assign-plan-btn").click(function(){
			console.log("id: "+$(this).attr("data-id")+" name: "+$(this).attr("data-name"));
			$("#plan-name").html($(this).attr("data-name"));
			$("#plan-id").val($(this).attr("data-id"));

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {func: "ajaxListUsers"},
				success: function(data){
					console.log(data);

					gftnowScrollbar1HTML = "<ul style=\'list-style: none;\'>";

					for(i = 0; i < data.listUsers.result.users.length; i++){
						console.log(data.listUsers.result.users[i]);

						gftnowScrollbar1HTML += "<li class=\'list-users\'>"+
													"<div class=\'row\'>"+
														"<div class=\'gftnow-inline\'>"+
															"<div class=\'col-md-2\'>"+
																"<div class=\'gftnow-form\'>"+
																	"<div class=\'gftnow-checkbox selectUserForPlan\' data-id=\'"+data.listUsers.result.users[i].identifier+"\'></div>"+
																"</div>"+
															"</div>"+
															"<div class=\'col-md-2\'>"+
																"<img style=\'height:50px;width:50px;\' src=\'"+data.listUsers.result.users[i].account.logoURL+"\'>"+
															"</div>"+
															"<div class=\'col-md-8\'>"+
																"<label class=\'gftnow-font-medium\'>"+data.listUsers.result.users[i].name+"</label>"+
															"</div>"+
														"</div>"+
													"</div>"+
												"</li>";
					}

					gftnowScrollbar1HTML += "</ul>";

					$("#gftnow-scrollbar1").html(gftnowScrollbar1HTML);

					$("#assign-plan-modal").fadeIn("fast");
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$(document).on("click", ".selectUserForPlan", function () {
			$(this).toggleClass("active");
		});

		$("#submit-assign-plan").click(function(){
			var plan_id = $("#plan-id").val();
			var ids = new Array();
			var unchecked = 0;

			$(".selectUserForPlan").each(function(){
				if($(this).hasClass("active")){
					ids.push($(this).attr("data-id"));
				} else {
					unchecked++;
				}
			});
			
			if($(".selectUserForPlan").length == unchecked){
				$("#assign-plan-error").show();
			} else {
				jQuery.ajax({
					url:"mvc/controller/ajaxController.php",
					type: "post",
					dataType: "json",
					data: {planIdentifier: plan_id, accountIdentifiers: ids, func: "ajaxAssignPlanToAccounts"},
					success: function(data){
						//location.reload();
						console.log(data);
					},error: function(err){
						console.log(err.responseText);
					}
				});
				console.log(plan_id);
				console.log(ids);
			}
		});

		$("#close-assign-plan-modal").click(function(){
			$("#assign-plan-modal").fadeOut("fast");
		});


		// Disable Plan
		$(".disable-plan-btn").click(function(){
			var id = new Array(); 
			id.push($(this).attr("data-id"));

			console.log(id);

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {plans:id, func: "ajaxDisablePlans"},
				success: function(data){
					window.location.href = "dashboard.php?p=admin/admin_panel_settings_superadmin&plan_removed";
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});



		//Firebase Config
		$("#firebaseConfig-btn").click(function(){
			$("#edit-firebaseConfig-modal").fadeIn("fast");
		});

		$("#save-firebaseConfig-btn").click(function(){
			var fbc = {};
			fbc.type = $("#firebaseconfig-type").val();
			fbc.project_id = $("#firebaseconfig-project_id").val();
			fbc.private_key_id = $("#firebaseconfig-private_key_id").val();
			fbc.private_key = $("#firebaseconfig-private_key").val();
			fbc.client_email = $("#firebaseconfig-client_email").val();
			fbc.client_id = $("#firebaseconfig-client_id").val();
			fbc.auth_uri = $("#firebaseconfig-auth_uri").val();
			fbc.token_uri = $("#firebaseconfig-token_uri").val();
			fbc.auth_provider_x509_cert_url = $("#firebaseconfig-auth_provider_x509_cert_url").val();
			fbc.client_x509_cert_url = $("#firebaseconfig-client_x509_cert_url").val();

			$("#firebaseConfig").val(JSON.stringify(fbc));
			$("#edit-firebaseConfig-modal").fadeOut("fast");
		});

		$("#close-firebaseConfig-modal").click(function(){
			$("#edit-firebaseConfig-modal").fadeOut("fast");
		});


		
		//Redisson Config
		$("#redissonConfig-btn").click(function(){
			$("#edit-redissonConfig-modal").fadeIn("fast");
		});

		$("#dnsMonitoring-radio-true").click(function(){
			$("#dnsMonitoring-radio-false").removeClass("active")
			$("#redissonConfig-dnsMonitoring").val("true");
		});
		$("#dnsMonitoring-radio-false").click(function(){
			$("#dnsMonitoring-radio-true").removeClass("active")
			$("#redissonConfig-dnsMonitoring").val("false");
		});
		$("#useLinuxNativeEpoll-radio-true").click(function(){
			$("#useLinuxNativeEpoll-radio-false").removeClass("active");
			$("#redissonConfig-useLinuxNativeEpoll").val("true");
		});
		$("#useLinuxNativeEpoll-radio-false").click(function(){
			$("#useLinuxNativeEpoll-radio-true").removeClass("active");
			$("#redissonConfig-useLinuxNativeEpoll").val("false");
		});

		$("#save-redissonConfig-btn").click(function(){
			var ssc = {};
			ssc.idleConnectionTimeout = $("#redissonConfig-idleConnectionTimeout").val();
			ssc.pingTimeout = $("#redissonConfig-pingTimeout").val();
			ssc.connectTimeout = $("#redissonConfig-connectTimeout").val();
			ssc.timeout = $("#redissonConfig-timeout").val();
			ssc.retryAttempts = $("#redissonConfig-retryAttempts").val();
			ssc.retryInterval = $("#redissonConfig-retryInterval").val();
			ssc.reconnectionTimeout = $("#redissonConfig-reconnectionTimeout").val();
			ssc.failedAttempts = $("#redissonConfig-failedAttempts").val();
			ssc.password = $("#redissonConfig-password").val();
			ssc.subscriptionsPerConnection = $("#redissonConfig-subscriptionsPerConnection").val();
			ssc.clientName = $("#redissonConfig-clientName").val();
			ssc.address = $("#redissonConfig-address").val();
			ssc.subscriptionConnectionMinimumIdleSize = $("#redissonConfig-subscriptionConnectionMinimumIdleSize").val();
			ssc.subscriptionConnectionPoolSize = $("#redissonConfig-subscriptionConnectionPoolSize").val();
			ssc.connectionMinimumIdleSize = $("#redissonConfig-connectionMinimumIdleSize").val();
			ssc.connectionPoolSize = $("#redissonConfig-connectionPoolSize").val();
			ssc.database = $("#redissonConfig-database").val();
			ssc.dnsMonitoring = $("#redissonConfig-dnsMonitoring").val();
			ssc.dnsMonitoringInterval = $("#redissonConfig-dnsMonitoringInterval").val();

			var rc = {};
			rc.singleServerConfig = ssc;
			rc.threads = $("#redissonConfig-threads").val();
			rc.nettyThreads = $("#redissonConfig-nettyThreads").val();
			rc.codec = $("#redissonConfig-codec").val();
			rc.useLinuxNativeEpoll = $("#redissonConfig-useLinuxNativeEpoll").val();

			$("#redissonConfig").val(JSON.stringify(rc));
			$("#edit-redissonConfig-modal").fadeOut("fast");
		});

		$("#close-redissonConfig-modal").click(function(){
			$("#edit-redissonConfig-modal").fadeOut("fast");
		});


		var imgType;
		var basic;
		basic = $("#croppie").croppie({
				    viewport: {
				        width: 200,
				        height: 200,
				        type: "circle"
				    },
				    customClass: "gftnow-croppie-container",
				    showZoomer: false
				});

		$("#close-mask1").click(function(){
			$("#mask1").hide();
		});

		$("#croppie-crop-icon, #croppie-done").click(function(){
			basic.croppie("result","base64").then(function(base64){
				var cropted = base64;

				jQuery.ajax({
					url: "mvc/controller/ajaxController.php",
					type: "post",
					dataType: "json",
					data: { func: "base64ToPNG", base64: base64 },
					success: function(data){
						if(data.result=="OK"){
							$("#logoURL").val("http://www.gftnow.com/template/assets/upload/php/files/"+data.file);

							if(imgType==0){
								$("#defaultUserLogoContainer").html("<input id=\'defaultUserLogo\' type=\'hidden\' value=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' /><img style=\'width:100%;height:auto;\' id=\'profilepic\' class=\'img-responsive\' src=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' alt=\'no profile\' /><a href=\'javascript:void(0);\'><input id=\'fileupload\' type=\'file\' name=\'files[]\' /><img src=\'assets/img/icon_camera.png\' style=\'width: 36px;height:auto;margin-top: 25px;\'></a>");
							}
							if(imgType==1){
								$("#incognitoLogoContainer").html("<input id=\'incognitoLogo\' type=\'hidden\' value=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' /><img style=\'width:100%;height:auto;\' id=\'profilepic\' class=\'img-responsive\' src=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' alt=\'no profile\' /><a href=\'javascript:void(0);\'><input id=\'fileupload1\' type=\'file\' name=\'files[]\' /><img src=\'assets/img/icon_camera.png\' style=\'width: 36px;height:auto;margin-top: 25px;\'></a>");							
							}

							$("#mask1").hide();
						}
					},error: function(err){
						console.log(err.responseText);
					}
				});
			});
		});

		fileupload();
		fileupload1();

		function fileupload(){
			var url = "assets/upload/php/index.php";
			var cropted = "";

		    $("#fileupload").fileupload({
		        url: url,
		        dataType: "json",
		        done: function (e, data) {
		        	if(data.result.files.length > 0){
		        		imgType = 0;
		            	basic.croppie("bind", {
						    url: data.result.files[0].url
						});
		            	$("#mask2").hide();
		            	$("#mask1").show();
		            }
		        },
		        error: function(err){
		        	console.log("error");
		        	console.log(err.responseText);
		        },
		        progressall: function (e, data) {
		        	console.log("uploading");
		        }
		    }).prop("disabled", !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : "disabled");

	        $("button.cancel").click(function (e) {
			    xhr.abort();
			    $("#mask2").hide();
			});
		}

		function fileupload1(){
			var url = "assets/upload/php/index.php";
			var cropted = "";

		    $("#fileupload1").fileupload({
		        url: url,
		        dataType: "json",
		        done: function (e, data) {
		        	if(data.result.files.length > 0){
		        		imgType = 1;
		            	basic.croppie("bind", {
						    url: data.result.files[0].url
						});
		            	$("#mask2").hide();
		            	$("#mask1").show();
		            }
		        },
		        error: function(err){
		        	console.log("error");
		        	console.log(err.responseText);
		        },
		        progressall: function (e, data) {
		        	console.log("uploading");
		        }
		    }).prop("disabled", !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : "disabled");

	        $("button.cancel").click(function (e) {
			    xhr.abort();
			    $("#mask2").hide();
			});
		}

		function updateSettings(){
			var settings = {};
			settings.defaultUserLogo = $("#defaultUserLogo").val();
			settings.incognitoLogo = $("#incognitoLogo").val();
			settings.dropValidityDuration = $("#dropValidityDuration").val();
			settings.dropPutBackDuration = $("#dropPutBackDuration").val();
			settings.pickupCoolDownPeriod = $("#pickupCoolDownPeriod").val();
			settings.visibleDropPickupRadius = $("#visibleDropPickupRadius").val();
			settings.visibleDropOfferRadius = $("#visibleDropOfferRadius").val();
			settings.visibleDropVisibilityRadius = $("#visibleDropVisibilityRadius").val();
			settings.defaultPlan = $("#defaultPlan").val();
			settings.tangoUsername = $("#tangoUsername").val();
			settings.tangoPassword = $("#tangoPassword").val();
			settings.tangoCustomerId = $("#tangoCustomerId").val();
			settings.tangoAccountId = $("#tangoAccountId").val();
			settings.maximumLogoSizeInBytes = $("#maximumLogoSizeInBytes").val();
			settings.visibleDropCacheHilbertGranularity = $("#visibleDropCacheHilbertGranularity").val();
			settings.hiddenDropCacheHilbertGranularity = $("#hiddenDropCacheHilbertGranularity").val();
			settings.sendGridAPIKey = $("#sendGridAPIKey").val();
			settings.stripeAPIKey = $("#stripeAPIKey").val();
			settings.firebaseUrl = $("#firebaseUrl").val();
			settings.firebaseConfig = $("#firebaseConfig").val();
			settings.redissonConfig = $("#redissonConfig").val();
			settings.testPaymentProviderState = $("#testPaymentProviderState").val();
			settings.testGiftCardProviderState = $("#testGiftCardProviderState").val();
			settings.emailSenderName = $("#emailSenderName").val();
			settings.emailSenderEmailAddress = $("#emailSenderEmailAddress").val();
			settings.emailAdminNamee = $("#emailAdminNamee").val();
			settings.emailAdminEmailAddress = $("#emailAdminEmailAddress").val();
			settings.hiddenDropPickupRadius = $("#hiddenDropPickupRadius").val();

			jQuery(document).ajaxComplete(function(){	
				$("#mask2").show();
			});

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {settings: settings, func: "ajaxUpdateSettings"},
				success: function(data){
					if(data.result=="OK"){
						console.log(data);
						location.reload();
					}
				},error: function(err){
					console.log(err.responseText);
				}
			});

			setTimeout(function(){
			   location.reload();
			}, 1000);
		}

		$("#save").click(function(){
			updateSettings();
		});

	});';

	$content['content'] = $alert_active.
	'<input type="hidden" id="temp-plan-fees" name="temp-plan-fees" />

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Admin Roles</h1>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Role</strong>
			</div>
			<div class="col-md-6">
				<strong>Name</strong>
			</div>
			<div class="col-md-3">
				<strong>Action</strong>
			</div>
		</div>

		<div id="admin-row">'.$admin.'</div>
		
		<center>
			<ul id="pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>

		<br>
		<center><button id="add-admin-btn" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Plans and Payments</h1>
			</div>
			<div class="col-md-12">
				<div id="plan-removed-alert" class="alert alert-warning alert-dismissable gftnow-hide"><a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>Plan successfully removed.</div>
				<div id="plan-added-alert" class="alert alert-success alert-dismissable gftnow-hide"><a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>Plan successfully added.</div>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Plan Title</strong>
			</div>
			<div class="col-md-6">
				<strong>Details</strong>
			</div>
			<div class="col-md-3">

			</div>
		</div>

		<div id="plans-row">'.$plans.'</div>

		<center>
			<ul id="plans-pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>

		<br>
		<center><button id="add-plan-btn" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>

	<div class="active-main-panel">
	   <div class="row">
	       <div class="col-md-6">

	       		<div class="row">
	       			<center>
	       			<div class="col-md-1"></div>
		       		<div class="col-md-5">
						<div class="form-group">
						    <label for="email">Default User Logo:</label><br/>
						    <div id="defaultUserLogoContainer" class="profile-img-container fileinput-button" style="width:100px;height:100px;">
						    	<input id="defaultUserLogo" type="hidden" value="'.findMySetting("defaultUserLogo", $listSettings).'" />
								<img style="width:100%;height:auto;" id="profilepic" src="'.findMySetting("defaultUserLogo", $listSettings).'" alt="no profile" />
								<a href="javascript:void(0);"><input id="fileupload" type="file" name="files[]"><img src="assets/img/icon_camera.png" style="width: 36px;height:auto;margin-top: 25px;"></a>
							</div>
					    </div>
				    </div>
				    <div class="col-md-5">
						<div class="form-group">
						    <label for="email">Incognito Logo:</label><br/>
						    <div id="incognitoLogoContainer" class="profile-img-container fileinput-button" style="width:100px;height:100px;">
						    	<input id="incognitoLogo" type="hidden" value="'.findMySetting("incognitoLogo", $listSettings).'" />
								<img style="width:100%;height:auto;" id="profilepic" src="'.findMySetting("incognitoLogo", $listSettings).'" alt="no profile" />
								<a href="javascript:void(0);"><input id="fileupload1" type="file" name="files[]"><img src="assets/img/icon_camera.png" style="width: 36px;height:auto;margin-top: 25px;"></a>
							</div>
					    </div>
				    </div>
				    <div class="col-md-1"></div>
				    </center>
			    </div>

	       		<div class="form-group">
				    <label for="email">Drop Validity Duration:</label>
				    <input type="email" value="'.findMySetting("dropValidityDuration", $listSettings).'" class="form-control" id="dropValidityDuration">
			    </div>

			    <div class="form-group">
				    <label for="email">Drop Put Back Duration:</label>
				    <input type="email" value="'.findMySetting("dropPutBackDuration", $listSettings).'" class="form-control" id="dropPutBackDuration">
			    </div>

			    <div class="form-group">
				    <label for="email">Pickup Cool Down Period:</label>
				    <input type="email" value="'.findMySetting("pickupCoolDownPeriod", $listSettings).'" class="form-control" id="pickupCoolDownPeriod">
			    </div>

			    <div class="form-group">
				    <label for="email">Visible Drop Pickup Radius:</label>
				    <input type="email" value="'.findMySetting("visibleDropPickupRadius", $listSettings).'" class="form-control" id="visibleDropPickupRadius">
			    </div>

			    <div class="form-group">
				    <label for="email">Visible Drop Offer Radius:</label>
				    <input type="email" value="'.findMySetting("visibleDropOfferRadius", $listSettings).'" class="form-control" id="visibleDropOfferRadius">
			    </div>

			    <div class="form-group">
				    <label for="email">Visible Drop Visibility Radius:</label>
				    <input type="email" value="'.findMySetting("visibleDropVisibilityRadius", $listSettings).'" class="form-control" id="visibleDropVisibilityRadius">
			    </div>

			    <div class="form-group">
				    <label for="email">Default Plan:</label>
				    <input type="email" class="form-control" value="'.findMySetting("defaultPlan", $listSettings).'" id="defaultPlan">
			    </div>

			    <div class="form-group">
				    <label for="email">Tango Username:</label>
				    <input type="email" value="'.findMySetting("tangoUsername", $listSettings).'" class="form-control" id="tangoUsername">
			    </div>
			    
	       		<div class="form-group">
				    <label for="email">Tango Password:</label>
				    <input type="email" value="'.findMySetting("tangoPassword", $listSettings).'" class="form-control" id="tangoPassword">
			    </div>

			    <div class="form-group">
				    <label for="email">Tango Customer Id:</label>
				    <input type="email" value="'.findMySetting("tangoCustomerId", $listSettings).'" class="form-control" id="tangoCustomerId">
			    </div>

			    <div class="form-group">
				    <label for="email">Tango Account Id:</label>
				    <input type="email" class="form-control" value="'.findMySetting("tangoAccountId", $listSettings).'" id="tangoAccountId">
			    </div>

	       </div>
	       <div class="col-md-6">

	       		<div class="form-group">
				    <label for="email">Maximum Logo Size In Bytes:</label>
				    <input type="email" class="form-control" value="'.findMySetting("maximumLogoSizeInBytes", $listSettings).'" id="maximumLogoSizeInBytes">
			    </div>

			    <div class="form-group">
				    <label for="email">Visible Drop Cache Hilbert Granularity:</label>
				    <input type="email" class="form-control" value="'.findMySetting("visibleDropCacheHilbertGranularity", $listSettings).'" id="visibleDropCacheHilbertGranularity">
			    </div>

			    <div class="form-group">
				    <label for="email">Hidden Drop Cache Hilbert Granularity:</label>
				    <input type="email" class="form-control" value="'.findMySetting("hiddenDropCacheHilbertGranularity", $listSettings).'" id="hiddenDropCacheHilbertGranularity">
			    </div>

			    <div class="form-group">
				    <label for="email">Send Grid API Key:</label>
				    <input type="email" class="form-control" value="'.findMySetting("sendGridAPIKey", $listSettings).'" id="sendGridAPIKey">
			    </div>

			    <div class="form-group">
				    <label for="email">Stripe API Key:</label>
				    <input type="email" class="form-control" value="'.findMySetting("stripeAPIKey", $listSettings).'" id="stripeAPIKey">
			    </div>

			    <div class="form-group">
				    <label for="email">Firebase Url:</label>
				    <input type="email" class="form-control" value="'.findMySetting("firebaseUrl", $listSettings).'" id="firebaseUrl">
			    </div>

			    <div class="row">
				    <div class="col-md-6 form-group">
					    <label for="email">Firebase Config:</label>
					    <button class="btn gftnow-btn gftnow-btn-success" id="firebaseConfig-btn" style="height:32px;padding-top:3px;display:block;width:100%;"><i class="fa fa-edit"></i> Edit</button>
				    	<input type="hidden" id="firebaseConfig" value=\''.findMySetting("firebaseConfig", $listSettings).'\'>
				    </div>
				    <div class="col-md-6 form-group">
					    <label for="email">Redisson Config:</label>
					    <button class="btn gftnow-btn gftnow-btn-success" id="redissonConfig-btn" style="height:32px;padding-top:3px;display:block;width:100%;"><i class="fa fa-edit"></i> Edit</button>
				    	<input type="hidden" id="redissonConfig" value=\''.findMySetting("redissonConfig", $listSettings).'\'>
				    </div>
			    </div>

			    <div class="form-group">
				    <label for="email">Test Payment Provider State:</label>
				    <input type="email" class="form-control" value="'.findMySetting("testPaymentProviderState", $listSettings).'" id="testPaymentProviderState">
			    </div>

			    <div class="form-group">
				    <label for="email">Test Gift Card Provider State:</label>
				    <input type="email" class="form-control" value="'.findMySetting("testGiftCardProviderState", $listSettings).'" id="testGiftCardProviderState">
			    </div>

			    <div class="form-group">
				    <label for="email">Email Sender Name:</label>
				    <input type="email" class="form-control" value="'.findMySetting("emailSenderName", $listSettings).'" id="emailSenderName">
			    </div>

			    <div class="form-group">
				    <label for="email">Email Sender Email Address:</label>
				    <input type="email" class="form-control" value="'.findMySetting("emailSenderEmailAddress", $listSettings).'" id="emailSenderEmailAddress">
			    </div>

			    <div class="form-group">
				    <label for="email">Email Admin Name:</label>
				    <input type="email" class="form-control" value="'.findMySetting("emailAdminNamee", $listSettings).'" id="emailAdminNamee">
			    </div>

			    <div class="form-group">
				    <label for="email">Email Admin Email Address:</label>
				    <input type="email" class="form-control" value="'.findMySetting("emailAdminEmailAddress", $listSettings).'" id="emailAdminEmailAddress">
			    </div>

			    <div class="form-group">
				    <label for="email">Hidden Drop Pickup Radius:</label>
				    <input type="email" class="form-control" value="'.findMySetting("hiddenDropPickupRadius", $listSettings).'" id="hiddenDropPickupRadius">
			    </div>

	       </div>
       </div>

       <hr>

       <div class="row">
       		<div class="col-md-12 text-center">
       			<button id="save" class="btn gftnow-btn gftnow-btn-success">
       				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       			</button>
       		</div>
       </div>
	</div>

	<div class="gftnow-mask" id="mask1">
		<div class="gftnow-modal">
			<div class="gftnow-modal-header text-center">
				<b>Logo</b>
				<a href="javascript:void(0);" id="close-mask1"><b><i class="pull-right fa fa-times"></i></b></a>
				<br/><br/>
				<p>Move your image around to fit the orange box</p>
			</div>
			<div class="gftnow-modal-body">
				<img id="croppie-crop-icon" src="assets/img/mini-croppie-icon.png"/>
				<div id="croppie"></div>
				<br/>
				<center>
					<button class="gftnow-btn gftnow-btn-success" id="croppie-done">DONE</button>
				</center>
				<br/>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="add-admin-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Add Admin</h1>
				<br/><br/>
				<form method="post" id="form1">
				<div class="custom-input">
					<p id="email-placeholder" class="placeholder-effect"></p>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email" />
					<br/><br/>
					<p id="password-placeholder" class="placeholder-effect"></p>
					<input type="password" class="form-control" name="password" id="password" placeholder="Password" />
					<br/><br/>
				</div>
				<div class="gftnow-hide" id="errorCode">
					<div class="alert alert-danger">
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-6">
						<input onclick="blur()" id="submit" class="btn gftnow-btn-success gftnow-btn pull-left" value="ADD" style="width:85%;" />
						</form>
					</div>
					<div class="col-md-6">
						<a href="javascript:void(0);" id="close-add-admin-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
					</div>
				</div>
			</center>
		</div>
	</div>

	<div class="gftnow-mask" id="assign-plan-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width:600px;width:600px;min-height:425px;height:425px;top:90px;left:0;right:0;margin:0 auto;">
			<div class="row">
				<div class="col-md-12">
					<center>
						<h1>Assign Plan</h1><p id="plan-name"></p><input type="hidden" id="plan-id" />
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<div class="inner-addon left-addon">
									<span class="addon-search"><i class="fa fa-search"></i></span>
							   		<input type="text" id="search-field" class="rounded-search form-control gotham-regular-placeholder" placeholder="Search" style="cursor:default;background-color:inherit;" />
								</div>
							</div>
							<div class="col-md-3"></div>
						</div>
					</center>
				</div>
			</div>
			<br/>
			
			<div class="active-main-panel" id="gftnow-scrollbar1" style="margin-top:0px;overflow-y:auto;max-height:185px;height:185px;"></div>

			<br/><br/>
			<div class="row">
				<div class="col-md-5">
					<button id="submit-assign-plan" class="btn gftnow-btn gftnow-btn-success pull-left">Assign</button>
				</div>
				<div class="col-md-2"><span id="assign-plan-error" class="gftnow-font-medium gftnow-hide" style="color:red;">Error!</span></div>
				<div class="col-md-5">
					<a href="javascript:void(0);" id="close-assign-plan-modal" class="btn gftnow-btn-success gftnow-btn pull-right">Close</a>
				</div>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="edit-firebaseConfig-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;height:540px;width:540px;top:60px;left:0;right:0;margin:0 auto;">
			<center><h1>Edit Firebase Config</h1></center>
			<br/><br/>
			<div class="active-main-panel" id="gftnow-scrollbar" style="margin-top:0px;overflow-x:hidden;overflow-y:auto;max-height:350px;height:350px;padding:0px;">
				<form>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
							    <label for="email">Type:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['type'].'" id="firebaseconfig-type">
						    </div>
						    <div class="form-group">
							    <label for="email">Project ID:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['project_id'].'" id="firebaseconfig-project_id">
						    </div>
						    <div class="form-group">
							    <label for="email">Private Key ID:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['private_key_id'].'" id="firebaseconfig-private_key_id">
						    </div>
						    <div class="form-group">
							    <label for="email">Private Key:</label>
								<textarea class="form-control gftnow-scrollbar" style="resize:none;display:block;width:100%;height:173px;" id="firebaseconfig-private_key">
									'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['private_key'].'
								</textarea>					   
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							    <label for="email">Client Email:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['client_email'].'" id="firebaseconfig-client_email">
						    </div>
						    <div class="form-group">
							    <label for="email">Client ID:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['client_id'].'" id="firebaseconfig-client_id">
						    </div>
						    <div class="form-group">
							    <label for="email">Auth Uri:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['auth_uri'].'" id="firebaseconfig-auth_uri">
						    </div>
						    <div class="form-group">
							    <label for="email">Token Uri:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['token_uri'].'" id="firebaseconfig-token_uri">
						    </div>
						    <div class="form-group">
							    <label for="email">Auth Provider x509 Cert Url:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['auth_provider_x509_cert_url'].'" id="firebaseconfig-auth_provider_x509_cert_url">
						    </div>
						    <div class="form-group">
							    <label for="email">Client x509 Cert Url:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("firebaseConfig", $listSettings), true)['client_x509_cert_url'].'" id="firebaseconfig-client_x509_cert_url">
						    </div>
						</div>
					</div>
				</form>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-6">
					<input onclick="blur()" type="button" id="save-firebaseConfig-btn" class="btn gftnow-btn-success gftnow-btn pull-left" value="Save" style="width:85%;" />
				</div>
				<div class="col-md-6">
					<a href="javascript:void(0);" id="close-firebaseConfig-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
				</div>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="edit-redissonConfig-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;height:540px;width:540px;top:60px;left:0;right:0;margin:0 auto;">
			<center><h1>Edit Redisson Config</h1></center>
			<br/><br/>
			<div class="active-main-panel" id="gftnow-scrollbar" style="margin-top:0px;overflow.x:auto;overflow-x:hidden;overflow-y:auto;max-height:350px;height:350px;padding:0px;">
				<form>
					<center><p class="gftnow-font-medium">Single Server Config</p></center>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								    <label for="email">Idle Connection Timeout:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['idleConnectionTimeout'].'" id="redissonConfig-idleConnectionTimeout">
							    </div>
							    <div class="form-group">
								    <label for="email">Ping Timeout:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['pingTimeout'].'" id="redissonConfig-pingTimeout">
							    </div>
							    <div class="form-group">
								    <label for="email">Connect Timeout:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['connectTimeout'].'" id="redissonConfig-connectTimeout">
							    </div>
							    <div class="form-group">
								    <label for="email">Timeout:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['timeout'].'" id="redissonConfig-timeout">
							    </div>
							    <div class="form-group">
								    <label for="email">Retry Attempts:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['retryAttempts'].'" id="redissonConfig-retryAttempts">
							    </div>
							    <div class="form-group">
								    <label for="email">Retry Interval:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['retryInterval'].'" id="redissonConfig-retryInterval">
							    </div>
							    <div class="form-group">
								    <label for="email">Reconnection Timeout:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['reconnectionTimeout'].'" id="redissonConfig-reconnectionTimeout">
							    </div>
							    <div class="form-group">
								    <label for="email">Failed Attempts:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['failedAttempts'].'" id="redissonConfig-failedAttempts">
							    </div>
							    <div class="form-group">
								    <label for="email">Password:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['password'].'" id="redissonConfig-password">
							    </div>
						    </div>
						    <div class="col-md-6">
							    <div class="form-group">
								    <label for="email">Subscriptions Per Connection:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['subscriptionsPerConnection'].'" id="redissonConfig-subscriptionsPerConnection">
							    </div>

							    <input type="hidden" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['clientName'].'" id="redissonConfig-clientName">

							    <div class="form-group">
								    <label for="email">Address:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['address'].'" id="redissonConfig-address">
							    </div>
							    <div class="form-group">
								    <label for="email">Subscription Connection Min Idle Size:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['subscriptionConnectionMinimumIdleSize'].'" id="redissonConfig-subscriptionConnectionMinimumIdleSize">
							    </div>
							    <div class="form-group">
								    <label for="email">Subscription Connection Pool Size:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['subscriptionConnectionPoolSize'].'" id="redissonConfig-subscriptionConnectionPoolSize">
							    </div>
							    <div class="form-group">
								    <label for="email">Connection Minimum Idle Size:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['connectionMinimumIdleSize'].'" id="redissonConfig-connectionMinimumIdleSize">
							    </div>
							    <div class="form-group">
								    <label for="email">Connection Pool Size:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['connectionPoolSize'].'" id="redissonConfig-connectionPoolSize">
							    </div>
							    <div class="form-group">
								    <label for="email">Database:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['database'].'" id="redissonConfig-database">
							    </div>
							    <div class="form-group">
								    <label for="email">DNS Monitoring:</label>
								    <div class="gftnow-inline">
										<div class="gftnow-form">
											<div class="gftnow-checkbox '.(json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['dnsMonitoring'] == "true" ? "active" : "").'" id="dnsMonitoring-radio-true"></div>
											<label class="gftnow-font-medium">True</label>
										</div>

										<div class="gftnow-form">
											<div class="gftnow-checkbox '.(json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['dnsMonitoring'] == "false" ? "active" : "").'" id="dnsMonitoring-radio-false"></div>
											<label class="gftnow-font-medium">False</label>
										</div>
									</div>
								    <input type="hidden" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['dnsMonitoring'].'" id="redissonConfig-dnsMonitoring">
							    </div>
							    <div class="form-group">
								    <label for="email">DNS Monitoring Interval:</label>
								    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['singleServerConfig']['dnsMonitoringInterval'].'" id="redissonConfig-dnsMonitoringInterval">
							    </div>
							</div>
						</div>

					    <hr>

					    <div class="row">
						    <div class="col-md-3 form-group">
							    <label for="email">Threads:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['threads'].'" id="redissonConfig-threads">
						    </div>

						    <div class="col-md-3 form-group">
							    <label for="email">Netty Threads:</label>
							    <input type="email" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['nettyThreads'].'" id="redissonConfig-nettyThreads">
						    </div>

						    <input type="hidden" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['codec'].'" id="redissonConfig-codec">

						    <div class="col-md-6 form-group">
							    <label for="email">Use Linux Native Epoll:</label>
								<div class="gftnow-inline">
									<div class="gftnow-form">
										<div class="gftnow-checkbox '.(json_decode(findMySetting("redissonConfig", $listSettings), true)['useLinuxNativeEpoll'] == "true" ? "active" : "").'" id="useLinuxNativeEpoll-radio-true"></div>
										<label class="gftnow-font-medium">True</label>
									</div>

									<div class="gftnow-form">
										<div class="gftnow-checkbox '.(json_decode(findMySetting("redissonConfig", $listSettings), true)['useLinuxNativeEpoll'] == "false" ? "active" : "").'" id="useLinuxNativeEpoll-radio-false"></div>
										<label class="gftnow-font-medium">False</label>
									</div>
								</div>
							    <input type="hidden" class="form-control" value="'.json_decode(findMySetting("redissonConfig", $listSettings), true)['useLinuxNativeEpoll'].'" id="redissonConfig-useLinuxNativeEpoll">
						    </div>
					    </div>
				</form>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-6">
					<input onclick="blur()" type="button" id="save-redissonConfig-btn" class="btn gftnow-btn-success gftnow-btn pull-left" value="Save" style="width:85%;" />
				</div>
				<div class="col-md-6">
					<a href="javascript:void(0);" id="close-redissonConfig-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
				</div>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="add-plan-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width:800px;width:800px;min-height:540px;height:540px;top:30px;left:0;right:0;margin:0 auto;">
			<center><h1>Add Plan</h1></center>
			<br/><br/>
			<div class="row">
				<div class="col-md-12">
					<p id="planName-placeholder" class="placeholder-effect"></p>
					<input type="text" class="form-control add-plan-inputs" name="planName" id="planName" placeholder="Name" />
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-12">
					<p id="planDescription-placeholder" class="placeholder-effect"></p>
					<textarea type="text" class="form-control add-plan-inputs" name="planDescription" id="planDescription" placeholder="Description"></textarea>
				</div>
			</div>
			
			<br/><br/>
			<div class="active-main-panel" id="gftnow-scrollbar" style="margin-top:0px;overflow.x:auto;overflow-y:auto;max-height:200px;height:200px;padding:0px;">
			</div>

			<br/><br/>
			<div class="row">
				<div class="col-md-12"><center><span id="add-plan-error" class="gftnow-font-medium gftnow-hide" style="color:red;">Please input all fields!</span></center></div>
				<div class="col-md-4">
					<button id="add-planNameNumFees-btn" style="width:100%;" class="gftnow-btn gftnow-btn-success btn btn-success btn-block">Add Fees</button>
				</div>
				<div class="col-md-4">
					<button id="submit-add-plan" disabled class="btn gftnow-btn gftnow-btn-success btn-block">Save</button>
				</div>

				<div class="col-md-4">
					<a href="javascript:void(0);" id="close-add-plan-modal" class="btn gftnow-btn-success gftnow-btn btn-block">Close</a>
				</div>
			</div>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>