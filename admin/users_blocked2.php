<?php
	$content = array();
	$content['title'] = "Users";
	$content['script'] = '';

	$content['right-of-title'] = '<div class="row">
		<div class="col-md-5">
			<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Local Awareness</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Campaign</li></a>
						<a href="#"><li>Local Awareness</li></a>
						<a href="#"><li>Christmas Deal</li></a>
						<a href="#"><li>New Flavor</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-5">
		 	<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Last 30 Days</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Time</li></a>
						<a href="#"><li>Last 30 Days</li></a>
						<a href="#"><li>Last 7 Days</li></a>
						<a href="#"><li>Custom</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="rounded-search">
				<i class="fa fa-search" style="font-size: 1.2em;"></i>
			</div>
		</div>
	</div>';

	$content['content'] = '<div class="active-main-panel">
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1">
				<h3><i class="fa fa-chevron-left"></i></h3>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="active-name">
					<p>Quili Ou</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11">
				<center>
					<img src="assets/img/avatars/male.png" width="100px" class="img-circle active-profile-active">
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<p><strong>Email</strong></p>
						<p>quiliou@designhouse5.com</p>
					</div>
					<!-- <div class="col-md-6">
						<p><strong>Phone</strong></p>
						<p>(503) 123-4567</p>
					</div> -->
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Registered</strong></p>
						<p>1/17/2017</p>
					</div>
					<div class="col-md-6">
						<p><strong>Current Location</strong></p>
						<p>Vancouver, WA</p>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Activity</strong></p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="active-buttons">
					<button class="btn gft-btn gft-btn-success gft-btn-active btn-block" data-toggle="modal" data-target="#remoteModal">Activate</button>
					<button class="btn gft-btn gft-btn-default btn-block" data-toggle="modal" data-target="#blockModal">Block</button>
				</div>
			</div>
		</div><br><br>
		<div class="row active-list-title">
			<div class="col-md-1">
				<div class="col-md-2">
					<strong>Action</strong>
				</div>
				<div class="col-md-2">
					<strong>Date</strong>
				</div>
				<div class="col-md-2">
					<strong>Time</strong>
				</div>
				<div class="col-md-2">
					<strong>Location</strong>
				</div>
				<div class="col-md-2">
					<strong>Item</strong>
				</div>
				<div class="col-md-2">
					<strong>Vendor</strong>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>Received</p>
				</div>
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-2">
					<p>09:11 am</p>
				</div>
				<div class="col-md-2">
					<strong>Beavertron, OR</strong>
				</div>
				<div class="col-md-2">
					<strong>Free small pizza</strong>
				</div>
				<div class="col-md-2">
					<strong>Domino Pizza</strong>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>Dropped</p>
				</div>
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-2">
					<p>04:37 am</p>
				</div>
				<div class="col-md-2">
					<strong>New York, NY</strong>
				</div>
				<div class="col-md-2">
					<strong>$5 cash</strong>
				</div>
				<div class="col-md-2">
					<strong></strong>
				</div>
			</div>
		</div>

	</div>


	
<!-- Modal -->
<div class="modal fade gftnow-modal" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">  
	<div class="modal-dialog">  
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
			</div>
			<div class="modal-body">
				
				<div class="title">
					Activate User
				</div>
				
				<p>
					Are you sure you want to activate Quili Ou ? <br>User will not be notificated b email.
				</p>

			</div>
			<div class="modal-footer">
			<center>
				<button type="button" class="btn gft-btn-success gft-btn" data-dismiss="modal">
					Close
				</button>
				<button type="button" class="btn gft-btn-success gft-btn">
					Activate
				</button>
			</center>
			</div>
		</div>  
	</div>  
</div>
<!-- End of Modal -->


<!-- Modal -->
<div class="modal fade gftnow-modal" id="blockModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" style="display: none;">  
	<div class="modal-dialog">  
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
			</div>
			<div class="modal-body">
				
				<div class="title">
					Block User
				</div>
				
				<p>
					Are you sure you want to block Lex Valishvili ? This will not delete all of the information bu permanently deactivated it.<br> User will not be notificated b email.
				</p>

			</div>
			<div class="modal-footer">
			<center>
				<button type="button" class="btn gft-btn-success gft-btn" data-dismiss="modal">
					Close
				</button>
				<button type="button" class="btn gft-btn-success gft-btn">
					Block
				</button>
			</center>
			</div>
		</div>  
	</div>  
</div>
<!-- End of Modal -->';

	$content['menu'] = file_get_contents('menu1.php');
?>