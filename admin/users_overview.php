<?php
	$content = array();
	$content['title'] = "Users Overview";
	$content['script'] = 'if ($("#users-graph").length)
		{
					var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

					var lmorris = Morris.Line({
					  element: "users-graph",
					  data: [
					    { month: "2016-01",xlabel:"Jan", value: 1230 },
					    { month: "2016-02",xlabel:"Jan", value: 2340 },
					    { month: "2016-03",xlabel:"Jan", value: 2340 },
					    { month: "2016-04",xlabel:"Jan", value: 2345 },
					    { month: "2016-05",xlabel:"Jan", value: 2335 },
					    { month: "2016-06",xlabel:"Jan", value: 2324 },
					    { month: "2016-07",xlabel:"Jan", value: 2454 },
					    { month: "2016-08",xlabel:"Jan", value: 2345 },
					    { month: "2016-09",xlabel:"Jan", value: 1123 },
					    { month: "2016-10",xlabel:"Jan", value: 2324 },
					    { month: "2016-11",xlabel:"Jan", value: 3456 },
					    { month: "2016-12",xlabel:"Jan", value: 1234 },
					  ],
					  xkey: "month",
					  ykeys: ["value"],
					  xLabelFormat: function(x){
					    var month = months[x.getMonth()];
    					return month;
					  },
					  hideHover: "auto",
					  resize: "true",
					  lineColors: ["#8bc24a"],
					  labels : ["Users"],
					  pointSize : 5
					});

					$(window).on("resize", function(){
				      lmorris.redraw();
				    });

		}

		if ($("#locations-graph").length)
		{
			var locationsgraph = document.getElementById("locations-graph").getContext("2d");
			
			var locationsgraphdata = {
			    labels: [
			        "Portland, OR",
			        "Seattle, WA",
			        "New York, NY",
			        "Los Angeles, CA",
			        "Houston, TX",
			        "Las Vegas, NV",
			    ],
			    datasets: [{
			        label: "Primary Locations",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: "#34A6FF",
			        hoverBackgroundColor: ["#34A6FF"]
			    }]
			};

			var locationsgraphchart = new Chart(locationsgraph, {
			    type: "horizontalBar",
			    data: locationsgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	ticks: {
			            		beginAtZero: true,
			            		callback: function(value) {
				               		return value + "%";
				            	}
			                }
			            }],
			            yAxes: [{
			            	stacked: true
			            }]
			        },legend: {
			        	display:false
			    	}

			    }
			});

		}';

	$content['right-of-title'] = '';
	$content['content'] = '<div class="row">
		<div class="col-md-12">
			
			<div class="col-md-3 col-sm-6 text-center">
				<h3>Total Users <br/>
					<b><span>192,226</span></b> <i class="fa fa-arrow-up text-success"></i>
				</h3>
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<h3>New Users (month)<br/>
					<b><span>34,008</span></b> <i class="fa fa-arrow-up text-success"></i>
				</h3>
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<h3>New Users (week)<br/>
					<b><span>12,184</span></b> <i class="fa fa-arrow-down text-warning"></i>
				</h3>
			</div>
			<div class="col-md-3 col-sm-6 text-center">
				<h3>New Users (day) <br/>
					<b><span>1,903</span></b> <i class="fa fa-arrow-up text-success"></i>
				</h3>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div id="users-graph"></div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<h3>Primary Locations <br/><br/>
			<canvas id="locations-graph" height="200"></canvas>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>