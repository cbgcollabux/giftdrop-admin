<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	$listUserLocations;
	$listUsers;
    $swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUserLocationsBody = new Swagger\Client\Model\UserLocationRequest;
	$listUsersBody = new Swagger\Client\Model\UserListRequest;

	$listUsersBody = array("radius"=>1000000,"center"=>array("latitude"=>45.485168,"longitude"=>-122.804489));
	
	try {
	    $listUserLocations = $api->listUserLocations($listUserLocationsBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUserLocations: ', $e->getMessage(), PHP_EOL;
	}
	try {
	    $listUsers = $api->listUsers($listUsersBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

	$active_users_count = 0;
	$active_drops_count = 0;
	$active_companies_count = 0;
	$gftnow_infobox_wrapper_content = '';

	foreach($listUsers['users'] as $user){
		if($user['role'] == "Player"){
			if($user['state'] == "Enabled"){
				$active_drops_count++;
			}
			if($user['account']['state'] == "Enabled"){
				$active_users_count++;
			}
			$gftnow_infobox_wrapper_content .= '<div id="'.$user['identifier'].'" class="gftnow-infobox">'.$user['name'].'</div>';
		}
		if($user['role'] == "Business"){
			if($user['account']['state'] == "Enabled"){
				$active_companies_count++;
			}
		}
		echo'<input type="hidden" class="markers" data-id="'.$user['identifier'].'" data-lat="'.$user['home_location']['latitude'].'" data-lng="'.$user['home_location']['longitude'].'" data-logo="'.$user['account']['logo_url'].'">';
	}

	$body = new Swagger\Client\Model\CompanyListRequest;
	try {
		
	    $listCompanies = $api->listCompanies($body);
	   
	} catch (Exception $e) {

	}

	$body = new Swagger\Client\Model\ProductListRequest;

	try {
	    $listProducts = $api->listProducts($body);
	  
	} catch (Exception $e) {

	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Statistics";
	$content['script'] = '
		$("#logout").show();
		$("#title").removeClass("col-md-3 col-lg-3").addClass("col-md-10 col-lg-10");
		$("#right-of-title").css("display", "none");
		$("#admin-menu-overview").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");
		
		google.maps.event.addDomListener(window, "load", init);

		function codeAddress(id, lat, lng, logo){
			var getRandomLocation = function (latitude, longitude, radiusInMeters){
			    var getRandomCoordinates = function (radius, uniform) {
			        // Generate two random numbers
			        var a = Math.random(),
			            b = Math.random();

			        // Flip for more uniformity.
			        if (uniform) {
			            if (b < a) {
			                var c = b;
			                b = a;
			                a = c;
			            }
			        }

			        // It\'s all triangles.
			        return [
			            b * radius * Math.cos(2 * Math.PI * a / b),
			            b * radius * Math.sin(2 * Math.PI * a / b)
			        ];
			    };

			    var randomCoordinates = getRandomCoordinates(radiusInMeters, true);

			    // Earths radius in meters via WGS 84 model.
			    var earth = 6378137;

			    // Offsets in meters.
			    var northOffset = randomCoordinates[0],
			        eastOffset = randomCoordinates[1];

			    // Offset coordinates in radians.
			    var offsetLatitude = northOffset / earth,
			        offsetLongitude = eastOffset / (earth * Math.cos(Math.PI * (latitude / 180)));

			    // Offset position in decimal degrees.
			    return {
			        latitude: latitude + (offsetLatitude * (180 / Math.PI)),
			        longitude: longitude + (offsetLongitude * (180 / Math.PI))
			    }
			};

			var newlatlng = getRandomLocation(parseFloat(lat),parseFloat(lng),1000);
			console.log(newlatlng);
			
			var overlay = new ImageMarker(new google.maps.LatLng(newlatlng.latitude, newlatlng.longitude),map,{}, logo);
			var infowindow = new google.maps.InfoWindow({
				content: document.getElementById(id)
			});

			var bounds = new google.maps.LatLngBounds();

			google.maps.event.addListener(infowindow, \'domready\', function() {
				var iwOuter = $(\'.gm-style-iw\');
				var iwBackground = iwOuter.prev();
				iwBackground.children(\':nth-child(2)\').css({\'display\' : \'none\'});
				iwBackground.children(\':nth-child(4)\').css({\'display\' : \'none\'});
				iwBackground.children(\':nth-child(1)\').css({\'display\' : \'none\'});
				iwBackground.children(\':nth-child(3)\').css({\'display\' : \'none\'});
			});

			overlay.addListener(\'click\', function() {
				infowindow.open(map, overlay);
			});
		}

		function init() {
	        var mapOptions = {
	            zoom: 1, disableDefaultUI: true,
	            mapTypeControl: true,
	            streetViewControl: true,
	            mapTypeControlOptions: {
	            	style: google.maps.MapTypeControlStyle.DEFAULT
	            },
				zoomControl: true,
				scaleControl: true,
				streetViewControl: true,
				rotateControl: true,
				fullscreenControl: true,
	            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
	        };

	        var mapElement = document.getElementById("map");

	        map = new google.maps.Map(mapElement, mapOptions);
	        var locations = new Array();

			$(".markers").each(function(){

				codeAddress( $(this).attr("data-id"), $(this).attr("data-lat"), $(this).attr("data-lng"), $(this).attr("data-logo"));
			});
	  
			$.getJSON(\'https://ipinfo.io/geo\', function(response) { 
			    var loc = response.loc.split(\',\');
			    var coords = {
			        latitude: loc[0],
			        longitude: loc[1]
			    };

			    var initialLocation = new google.maps.LatLng(45.484983477962, -122.78700962951);
			    map.setCenter(initialLocation);
			});
	    }

		if ($("#topusers-location-graph").length){
			var locationsgraph = document.getElementById("topusers-location-graph").getContext("2d");

			var color_gradient = locationsgraph.createLinearGradient(0, 0, 300, 0);
			color_gradient.addColorStop(0, \'#81D4FA\');
			color_gradient.addColorStop(1, \'#02477D\');
			
			var locationsgraphdata = {
			    labels: [
			        "Portland, OR",
			        "Seattle, WA",
			        "New York, NY",
			        "Los Angeles, CA",
			        "Houston, TX",
			        "Las Vegas, NV",
			    ],
			    datasets: [{
			        label: "Primary Locations",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: color_gradient,
			        hoverBackgroundColor: color_gradient
			    }]
			};		

			var locationsgraphchart = new Chart(locationsgraph, {
			    type: "horizontalBar",
			    data: locationsgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	display: false,
			            }],
			            yAxes: [{
			            	stacked: true,
			                gridLines: {
			                    display: false,
			                },
			                ticks: {
								fontColor: "black",
                        		fontStyle: "bold",
                        		fontFamily: "GothamRndMedium",
			                }
			            }],
			        	},
			        	legend: {
			        		display:false
			    		}
					}
			});
		}

		if ($("#topdrops-location-graph").length){
			var locationsgraph = document.getElementById("topdrops-location-graph").getContext("2d");

			var color_gradient = locationsgraph.createLinearGradient(0, 0, 300, 0);
			color_gradient.addColorStop(0, \'#81D4FA\');
			color_gradient.addColorStop(1, \'#02477D\');
			
			var locationsgraphdata = {
			    labels: [
			        "Portland, OR",
			        "Seattle, WA",
			        "New York, NY",
			        "Los Angeles, CA",
			        "Houston, TX",
			        "Las Vegas, NV",
			    ],
			    datasets: [{
			        label: "Primary Locations",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: color_gradient,
			        hoverBackgroundColor: color_gradient
			    }]
			};

			var locationsgraphchart = new Chart(locationsgraph, {
			    type: "horizontalBar",
			    data: locationsgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	display: false,
			            }],
			            yAxes: [{
			            	stacked: true,
			                gridLines: {
			                    display: false,
			                },
			                ticks: {
								fontColor: "black",
                        		fontStyle: "bold",
                        		fontFamily: "GothamRndMedium",
			                }
			            }],
			        	},
			        	legend: {
			        		display:false
			    		}
					}
			});
		}

		if ($("#overview-graph").length){
		var lmorris = Morris.Line({
			element: "overview-graph",
			data : [{
				period : "2010 Q1",
				view : 2666,
				spent : null,
				drop : 2647
			}, {
				period : "2010 Q2",
				view : 2778,
				spent : 2294,
				drop : 2441
			}, {
				period : "2010 Q3",
				view : 4912,
				spent : 1969,
				drop : 2501
			}, {
				period : "2010 Q4",
				view : 3767,
				spent : 3597,
				drop : 5689
			}, {
				period : "2011 Q1",
				view : 6810,
				spent : 1914,
				drop : 2293
			}, {
				period : "2011 Q2",
				view : 5670,
				spent : 4293,
				drop : 1881
			}, {
				period : "2011 Q3",
				view : 4820,
				spent : 3795,
				drop : 1588
			}, {
				period : "2011 Q4",
				view : 15073,
				spent : 5967,
				drop : 5175
			}, {
				period : "2012 Q1",
				view : 10687,
				spent : 4460,
				drop : 2028
			}, {
				period : "2012 Q2",
				view : 8432,
				spent : 5713,
				drop : 1791
			}],

				xkey : "period",
				ykeys : ["view", "spent", "drop"],
				labels : ["Drop Views", "Profile Views", "Money Spent"],
				pointSize : 5,
				hideHover : "auto",
				lineColors: ["#fdd05e","#8bc24a","#75C5F9"],
				xLabelFormat: function(x){
					//var month = months[x.getMonth()];
					return "";
				},
			});

			lmorris.options.labels.forEach(function(label, i) {
			    var legendItem = $("<span></span>").text(label).prepend("<br><span>&nbsp;</span>");
			    legendItem.find("span")
			      .css("backgroundColor", lmorris.options.lineColors[i])
			      .css("width", "10px")
			      .css("height", "10px")
			      .css("display", "inline-block")
			      .css("border-radius", "5px")
			      .css("margin", "10px")
			      .css("font-size", "10px");
			    $("#overview-graph-legend").append(legendItem)
			});

			$(window).on("resize", function(){
				lmorris.redraw();
			});

		}';

	$content['right-of-title'] = '';
	$content['content'] = 

	'<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default" style="border-radius:10px;border:0px solid;">
			  <div class="panel-body" style="padding:0;">
			
				<div class="row">
					<div class="col-md-2">
						<p style="font-weight:bold;font-family:GothamRndLight;font-size:14px;margin-left:25px;margin-bottom:-20px;margin-top:25px;">Users</p>
						<h3 style="margin-left:25px;margin-bottom:25px;"><b><span>'.$active_users_count.'</span></b> <!--<i class="fa fa-arrow-up text-success"></i>--></h3>
					</div>
					<div class="col-md-2">
						<p style="font-weight:bold;font-family:GothamRndLight;font-size:14px;margin-left:25px;margin-bottom:-20px;margin-top:25px;">Companies</p>
						<h3 style="margin-left:25px;margin-bottom:25px;"><b><span>'.( isset($listCompanies['companies'])?count($listCompanies['companies']) : 0 ).'</span></b> <!--<i class="fa fa-arrow-up text-success"></i>--></h3>
					</div>
					<div class="col-md-2">
						<p style="font-weight:bold;font-family:GothamRndLight;font-size:14px;margin-left:25px;margin-bottom:-20px;margin-top:25px;">Campaigns</p>
						<h3 style="margin-left:25px;margin-bottom:25px;"><b><span>'.( isset($listProducts['products'])?count($listProducts['products']) : 0 ).'</span></b> <!--<i class="fa fa-arrow-down text-warning"></i>--></h3>
					</div>
				</div>
				
				<div id="map" class="gftnow-map gftnow-nopadding"></div>
				
			  </div>
			</div>
		</div>

		<!--<div class="col-md-6">
			<div class="panel panel-default" style="border:0px solid;">
			  <div class="panel-body" style="text-align:left;">
				<p style="font-size:20px;font-family:GothamRndRegular;">Top Users Location</p><br/>
				<canvas id="topusers-location-graph" height="130"></canvas>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default" style="border:0px solid;">
			  <div class="panel-body" style="text-align:left;">
				<p style="font-size:20px;font-family:GothamRndRegular;">Top Drops Location</p><br/>
				<canvas id="topdrops-location-graph" height="130"></canvas>
				</div>
			</div>
		</div>-->

		<div class="gftnow-infobox-wrapper">'.$gftnow_infobox_wrapper_content.'</div>
	</div>

	<!--<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="panel panel-success">
			  <div class="panel-heading">Drops</div>
			  <div class="panel-body">
			  	<div id="drops-graph"></div>
			  </div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="panel panel-success">
			  <div class="panel-heading">Users</div>
			  <div class="panel-body">
			  	<div id="users-graph"></div>
			  </div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="panel panel-success">
			  <div class="panel-heading">Campaigns</div>
			  <div class="panel-body">
			  	<div id="campaigns-graph"></div>
			  </div>
			</div>
		</div>
	</div>-->

	<!--<div class="row">
		<div class="col-md-8 col-sm-12 col-xs-12">
			<div id="overview-graph"></div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div id="overview-graph-legend"></div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="highlight" style="height:250px;max-height:250px;">
				<div class="row">
					<div class="col-md-12 text-left">
						<h2 class="gftnow-font-regular">Views</h2>
					</div>
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Total Drop Views</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">14,230</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Total Profile Views</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">14,230</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Total Licks Clicks</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">14,230</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="highlight" style="height:250px;max-height:250px;">
				<div class="row">
					<div class="col-md-12 text-left">
						<h2 class="gftnow-font-regular">Spent</h2>
					</div>
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Total to date</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">$491</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Last 30 days</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">$248</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Last 7 days</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">$73</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-6">
			<div class="highlight" style="height:250px;max-height:250px;">
				<div class="row">
					<div class="col-md-12 text-left">
						<h2 class="gftnow-font-regular">Drops</h2>
					</div>
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Total drops</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">115</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Picked up</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">98</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-left">
						<h3 class="gftnow-font-light">Reedeemed</h3>
					</div>
					<div class="col-md-6 text-right">
						<h3 class="gftnow-font-regular">64</h3>
					</div>
				</div>
			</div>
		</div>
	</div>-->';

	$content['menu'] = file_get_contents('menu1.php');
?>