<?php
	include_once "mvc/model/swagger.php";

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	function findCategory($id){
		$category;
		if($id == "1b25-4e1d-b2a5-adcfb5fbcdeb"){ $category = "Cash"; }
		if($id == "b6dae6b7-d02a-458b-ad22-3b626ddd882d"){ $category = "Food & Drinks"; }
		if($id == "a02a6059-aaf4-4cc1-9494-a31a1715fbbd"){ $category = "Automotive"; }
		if($id == "61360c31-3cae-4f3d-b395-e0f9aa320d47"){ $category = "Clothing"; }
		if($id == "831bd6c2-73c7-4502-a0c5-629b7a0331c6"){ $category = "Electronics"; }
		if($id == "280bc362-960b-4203-9f5e-a108d4b593d9"){ $category = "Entertainment"; }
		if($id == "63452c3b-8145-447b-aa45-ef12479693c2"){ $category = "Health & Beauty"; }
		if($id == "06125b59-0b1c-4c75-9a05-ecd821a82940"){ $category = "Home Improvements"; }
		if($id == "8a8cf347-8fbe-4850-84dd-312c06ee5905"){ $category = "Kids"; }
		if($id == "f9003709-0cd9-4f5d-83af-77b690d83251"){ $category = "Other"; }
		if($id == "ba864d3b-d434-4fbc-b0aa-b09df2af1c35"){ $category = "Pets"; }
		if($id == "8acf3596-eec1-4572-99d8-4e5463c3170b"){ $category = "Sport & Fitness"; }
		if($id == "0360c1e1-008d-4480-9a96-b0a5c95d0339"){ $category = "Travel"; }
		return $category;
	}

	$state_drop_active;
	$time_drop_active;

	$state;
	if(isset($_GET['state'])){
		if($_GET['state'] == "pending"){
			$state = 'Disabled';
			$state_drop_active = "Blocked";
		}
		if($_GET['state'] == "active"){
			$state = 'Enabled';
			$state_drop_active = "Active";
		}
		$time_drop_active = "All Time";
	}else {
		$state = '';
		$state_drop_active = "Show All";
	}

	if(isset($_GET['time'])){
		if($_GET['time'] == 'last7days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 weeks"));
			$time_drop_active = 'Last 7 Days';
		}
		if($_GET['time'] == 'last30days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 months"));
			$time_drop_active = 'Last 30 Days';
		}
		$state_drop_active = "Show All";
	} else {
		$from = gmdate("Y-m-d\TH:i:s\Z");
		$time_drop_active = 'All Time';
	}

	$to = gmdate("Y-m-d\TH:i:s\Z");

	$listCompanies;
    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api_admin = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\ProductListRequest;

	try {
	    $listProducts = $api_admin->listProducts($body);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listCompanies: ', $e->getMessage(), PHP_EOL;
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Campaign Approval";
	$content['script'] = 
	'$(document).ready(function() {
		$(".campaign-row").click(function(){
			var id = $(this).attr("data-id");

			var form = $(document.createElement("form"));
			$(form).attr("action", "dashboard.php?p=admin/vendors_approval1");
			$(form).attr("method", "POST");

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "id")
			    .val( $(this).attr("data-id")  );

			$(form).append($(input));

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "name")
			    .val( $(this).attr("data-name")  );

			$(form).append($(input));

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "terms_and_conditions")
			    .val( $(this).attr("data-terms")  );

			$(form).append($(input));

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "how_to_redeem")
			    .val( $(this).attr("data-redeem")  );

			$(form).append($(input));

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "photo_url")
			    .val( $(this).attr("data-photo")  );

			$(form).append($(input));

			var input = $("<input>")
			    .attr("type", "hidden")
			    .attr("name", "state")
			    .val( $(this).attr("data-state")  );

			$(form).append($(input));

			$(form).appendTo( document.body );
			$(form).submit();
		});

		$("#stateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/vendors_approval&state="+state+"&time="+time+"&text="+text;
		});
		
		$("#filterText").keydown(function (e){
		    if(e.keyCode == 13){
		    	var state = $("#stateFilter").val();
				var time = $("#dateFilter").val();
				var text = $("#filterText").val();

				window.location = "dashboard.php?p=admin/vendors_approval&state="+state+"&time="+time+"&text="+text;
		    }
		})

		$("#dateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/vendors_approval&state="+state+"&time="+time+"&text="+text;
		});

		$("#table1").dataTable({"bFilter": false,"bInfo": false,"bLengthChange": false});

		$("#logout").show();
		$("#admin-menu-approval").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#search-field").keyup(function() {
		    var value = $(this).val();

		    $(".active-list").each(function(index) {
		        var id = $(this).find("div").text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-2").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});

		$(".active-list").css("cursor","pointer");

		$(".active-list").click(function(){
			var identifier = $(this).attr("id");
			window.location.href = "dashboard.php?p=admin/vendors_approval1&id="+identifier;
		});

		$(".active-list").hover(function(){
			$(this).css("background-color", "#f4f5f8");
			$(".active-list").not(this).css("background-color", "#ffffff");
		});

	    pageSize = 10;

	    var pageCount =  $(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $(".active-list").hide();
	        $(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });
	});';

	$content['right-of-title'] =
	'<div class="row">
		<div class="col-md-4">
			<select class="form-control" id="stateFilter">
				<option '.( (isset($_GET['state']))? ($_GET['state']=="all")?"selected":"" : ""  ).' value="all">All</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="pending")?"selected":"" : ""  ).' value="pending">Pending Approval</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="active")?"selected":"" : ""  ).' value="active">Active</option>
			</select>
		</div>
		<div class="col-md-4">
			<select class="form-control" id="dateFilter">
				<option value="all" '.( (isset($_GET['time']))? ($_GET['time']=="all")?"selected":"" : ""  ).'>All</option>
				<option value="last30days" '.( (isset($_GET['time']))? ($_GET['time']=="last30days")?"selected":"" : ""  ).'>Last 30 Days</option>
				<option value="last7days" '.( (isset($_GET['time']))? ($_GET['time']=="last7days")?"selected":"" : ""  ).'>Last 7 Days</option>
			</select>
		</div>
		<div class="col-md-4">
			<input type="text" class="form-control" id="filterText" value="'.( isset($_GET['text'])?$_GET['text']:'' ).'" />
		</div>
	</div>';

	$vendors_content='';

	foreach($listProducts['products'] as $product){
		$date = $product['created']->format('d-m-Y');

		if($product['operation_state'] == "Enabled")
			$stateTxt = "Active";
		else
			$stateTxt = "Pending Approval";

		$included=0;

		//state filter
		if(isset($_GET['state'])){

			if($_GET['state']=="pending"){
				if($product['operation_state']=="Disabled")
					$included=1;
			}else if($_GET['state']=="active"){
				if($product['operation_state']!="Disabled")
					$included=1;
			}else{
					$included=1;
			}
		}else{
			$included=1;
		}

		//date filter
		if($included==1)
		{
			if(isset($_GET['time']))
			{
				if($_GET['time']=="last30days"){
					$minus = gmdate("d-m-Y", strtotime("-30 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}

				}else if($_GET['time']=="last7days"){
					$minus = gmdate("d-m-Y", strtotime("-7 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}
				}
			}
		}

		if($included==1){
			if(isset($_GET['text'])){
				if($_GET['text']!=""){
					if (strpos( strtolower( $product['name'] ), strtolower( $_GET['text'] ) ) !== false) {
					    $included=1;
					}else{
						$included=0;
					}
				}
			}
		}

		if($included==1)
			$vendors_content .="<tr class='campaign-row' data-state='".$product['operation_state']."' data-photo='".$product['photo_url']."' data-redeem='".$product['how_to_redeem']."' data-terms='".$product['terms_and_conditions']."' data-name='".$product['name']."' data-id='".$product['identifier']."'><td><img src='".( ($product['vendor']=='')?''.$product['photo_url']: $product['photo_url'] )."' style='width: 100px;height:auto;'/></td><td>".trim_text($product['name'], "...", 12)."</td><td>".$product['created']->format('m/d/Y')."</td><td>".$product['operation_state']."</td></tr>";
	}

	$content['content'] = 
	'<style>
		.campaign-row{
			cursor: pointer;
		}
		#table1 th, #table1 td,table.dataTable.no-footer{
			background-color:#ffff;
			border: none !important;
		}
		.table tr, .table th, .table td{
			border: none !important;
			padding-top: 20px !important;
			padding-bottom: 20px !important;
		}
		.dataTables_paginate{
			float: none !important;
			text-align:center !important;
			padding-top: 25px !important;
		}
		.paginate_button.current{
			background: none !important;
			background-color: #619534 !important;
			
			border-radius: 50px !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button.current{
			color: #fff !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
			background:none !important;
			border: 1px solid #fff !important;
			color: #514F4F !important;
		}
		.active-main-panel{
			padding-top:30px;
			padding-bottom: 30px;
		}
		.company-row:hover td{
			background-color: #f4f5f8 !important;
		}
	</style>
	<div class="active-main-panel">
		<table id="table1" class="table">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Created</th>
					<th>State</th>
				</tr>
			</thead>
			<tbody>
				'.$vendors_content.'
			</tbody>
		</table>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>