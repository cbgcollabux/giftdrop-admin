<?php
	if(isset($_POST['id']))
		$_SESSION['campaign_id_view'] = $_POST['id'];

	if(isset($_POST['state']))
		$_SESSION['campaign_state_view'] = $_POST['state'];

	if(isset($_POST['name']))
		$_SESSION['campaign_name_view'] = $_POST['name'];

	if(isset($_POST['terms_and_conditions']))
		$_SESSION['campaign_terms_and_conditions_view'] = $_POST['terms_and_conditions'];

	if(isset($_POST['how_to_redeem']))
		$_SESSION['campaign_how_to_redeem_view'] = $_POST['how_to_redeem'];

	if(isset($_POST['photo_url']))
		$_SESSION['campaign_photo_url_view'] = $_POST['photo_url'];

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Campaign Approval";
	$content['script'] = 
	'$(document).ready(function() {

		$(".setState").click(function(){
			var id = $(this).attr("data-id");
			var product_id = $(this).attr("data-product");
			console.log(id);
			console.log(product_id);

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {state:id,product_identifier: product_id , func: "ajaxChangeProductState"},
				success: function(data){
					//location.reload();
					window.location = "dashboard.php?p=admin/vendors_approval";
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#logout").show();
		$("#admin-menu-approval").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$(".flavor-campaigns").click(function(){
			$(".campaign-drops-desc", this).slideToggle("fast");
			$(".campaign-drops-desc", this).bind("click", function(e){
				e.stopPropagation();
			});
		});

		$(".drops").click(function(){
			var drop = $(".drop", this).attr("data-barcode");

			JsBarcode("#barcode", drop, {format: "CODE128", width: 1});

			if($(this).find("i").hasClass("text-success")){
				$("#drop-info-checked").fadeIn("fast");
			}
			if($(this).find("i").hasClass("text-default")){
				$("#drop-info-unchecked").fadeIn("fast");
			}
		});
		
		$("#close-drop-info-checked").click(function(){
			$("#drop-info-checked").fadeOut("fast");
		});

		$("#close-drop-info-unchecked").click(function(){
			$("#drop-info-unchecked").fadeOut("fast");
		});


		if($("#status").val() == "Enabled"){
			$("#activate-btn").removeClass("btn btn-success gftnow-btn gftnow-btn-success").addClass("btn btn-default gftnow-btn gftnow-btn-default");
			$("#activate-btn").click(function(){
				$(this).blur();
			});
			$("#block-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-warning gftnow-btn gftnow-btn-warning");
			$("#block-btn").css({"background-color": "#E3B339", "border-color": "#E3B339"});
			$("#block-btn").hover(
				function(){
					$(this).css({"background-color": "#c09853", "border-color": "#c09853"});
				},
				function(){
					$(this).css({"background-color": "#E3B339", "border-color": "#E3B339"});
			});
			$("#block-btn").click(function(){
				$("#block-modal").fadeIn("fast");
			});
		}

		if($("#status").val() == "Disabled"){
			$("#block-btn").removeClass("btn btn-warning gftnow-btn gftnow-btn-warning").addClass("btn btn-default gftnow-btn gftnow-btn-default");
			$("#block-btn").click(function(){
				$(this).blur();
			});
			$("#activate-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-success gftnow-btn gftnow-btn-success");
			$("#activate-btn").click(function(){
				$("#activate-modal").fadeIn("fast");
			});
		}

		$("#activate").click(function(){
			$(this).blur();
			$("#status").attr("value", "Enabled");

			var company = {};
			company.identifier = $("#identifier").val();
			company.status = $("#status").val();

			console.log(company);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {company:company, func: "ajaxChangeCompanyState"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-activate-modal").click(function(){
			$("#activate-modal").fadeOut("fast");
		});
		
		$("#block").click(function(){
			$(this).blur();
			$("#status").attr("value", "Disabled");

			var company = {};
			company.identifier = $("#identifier").val();
			company.status = $("#status").val();

			console.log(company);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {company:company, func: "ajaxChangeProductState"},
				success: function(data){
					//location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-block-modal").click(function(){
			$("#block-modal").fadeOut("fast");
		});
	});';
	
	$content['right-of-title'] = '';

	$content['content'] =
	'<div class="row">
		<div class="col-md-12">
			<div class="col-md-12 custom-campaign-title">
				<h1 class="gftnow-font-regular">'.( isset($_SESSION['campaign_name_view'])?$_SESSION['campaign_name_view']:'' ).'</h1>
			</div>
		</div>

		<div class="col-md-12">
			<div class="gftnow-wizard-panel">
				<div class="step1">
					<div class="row">
						<div class="col-md-1 col-sm-1 col-xs-1">
							<h3><a href="index.php?p=admin/vendors_approval"><i class="fa fa-chevron-left nextWizard"></i></a></h3>
						</div>
						<div class="col-md-10 col-sm-12 col-xs-12">
							<div class="active-name">
								<p><img src="'.( isset($_SESSION['campaign_photo_url_view'])?$_SESSION['campaign_photo_url_view']:'' ).'"></p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							
							<div class="row">
								<div class="col-md-4">
									<p class="gftnow-font-medium">Terms & Conditions</p>
								</div>
								<div class="col-md-12">
									'.( isset($_SESSION['campaign_terms_and_conditions_view'])?$_SESSION['campaign_terms_and_conditions_view']:'' ).'
								</div>
							</div>
								
							<br/>

							<div class="row">
								<div class="col-md-4">
									<p class="gftnow-font-medium">How To Redeem</p>
								</div>
								<div class="col-md-12">
									'.( isset($_SESSION['campaign_how_to_redeem_view'])?$_SESSION['campaign_how_to_redeem_view']:'' ).'
								</div>
							</div>
							
						</div>
						
						<div class="text-center">
							<br/><br/><br/><br/>
							<button '.( ($_SESSION['campaign_state_view']=="Disabled") ? "" : "disabled" ).' class="btn btn-success gftnow-btn gftnow-btn-success setState" data-id="enabled" data-product="'.( isset($_SESSION['campaign_id_view'])? $_SESSION['campaign_id_view']:'' ).'">Approve</button>
							<button '.( ($_SESSION['campaign_state_view']=="Disabled") ? "disabled" : "" ).' class="btn btn-warning gftnow-btn gftnow-btn-warning setState" data-id="disabled" data-product="'.( isset($_SESSION['campaign_id_view'])? $_SESSION['campaign_id_view']:'' ).'">&nbsp;&nbsp;Block&nbsp;&nbsp;</button>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="drop-info-unchecked" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;top:0;bottom:0;margin:auto auto;">
			<center>
				<h4 style="font-family:GothamRndMedium;"><b>Drop Information</b></h4>
				<br/>
				<svg id="barcode"></svg>			
				<div id="drop-info-content"></div>
				<br/><br/>
				<p style="text-align:center;padding:0px;">The drop is still available.</p>
				<span style="color:#8bc24a;cursor:pointer;font-size:1.3em;">Mark as redeemed</span>
				<br/><br/><br/><br/>
				<label type="button" id="close-drop-info-unchecked" class="btn btn-success gftnow-btn-rounded">&nbsp;&nbsp;Close&nbsp;&nbsp;</label>
			</center>
		</div>
	</div>

	<div class="gftnow-mask" id="drop-info-checked" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;height:430px;top:90px;left:0;right:0;top:0;bottom:0;margin: auto auto;">
			<center>
				<h4 style="font-family:GothamRndMedium;"><b>Drop Information</b></h4>
				<br/>
				<svg id="barcode"></svg>
				<div id="drop-info-content"></div>

				<div class="row" style="padding:30px;">
					<div class="col-md-2">
						<div><img src="assets/img/man.png" class="img-circle" width="100px"></div>
					</div>
					<div class="col-md-1"></div>
					<div class="col-md-8">
					<br>
						<div style="text-align:left;">John Sweanson redeemed the drop on February 18, 2017 in 3892 SW 21st Street Tigard, OR 98210 </div>
					</div>
				</div>

				<label type="button" id="close-drop-info-checked" class="btn btn-success gftnow-btn-rounded">&nbsp;&nbsp;Close&nbsp;&nbsp;</label>
			</center>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>