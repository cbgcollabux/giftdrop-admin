<?php
	$content = array();
	$content['title'] = "Vendors";
	$content['script'] = '';

	$content['right-of-title'] = '<div class="row">
		<div class="col-md-5">
			<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Local Awareness</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Campaign</li></a>
						<a href="#"><li>Local Awareness</li></a>
						<a href="#"><li>Christmas Deal</li></a>
						<a href="#"><li>New Flavor</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-5">
		 	<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Last 30 Days</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Time</li></a>
						<a href="#"><li>Last 30 Days</li></a>
						<a href="#"><li>Last 7 Days</li></a>
						<a href="#"><li>Custom</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="rounded-search">
				<i class="fa fa-search" style="font-size: 1.2em;"></i>
			</div>
		</div>
	</div>';

	$content['content'] = '<div class="active-main-panel">
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1">
				<h3><i class="fa fa-chevron-left"></i></h3>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="active-name">
					<p>Nike</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11">
				<center>
					<img src="assets/img/nike.png" width="100px" class="active-profile-active">
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<p><strong>Email</strong></p>
						<p>lex@designhouse5.com</p>
					</div>
					<div class="col-md-6">
						<p><strong>Phone</strong></p>
						<p>(503) 123-4567</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Address</strong></p>
						<p>1234 NW 56th Active, <br> Portland, OR 97234</p>
					</div>
					<div class="col-md-6">
						<p><strong>Website</strong></p>
						<p>nike.com</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Activity</strong></p>
						<p>1234 NW 56th Active, <br> Portland, OR 97234</p>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="active-buttons">
					<button class="btn gft-btn gft-btn-default gft-btn-active btn-block">Active</button>
					<button class="btn gft-btn gft-btn-warning btn-block">Block</button>
				</div>
			</div>
		</div><br><br>
		<div class="row active-list-title">
			<div class="col-md-12">
				<div class="col-md-2">
					<strong>Start Date</strong>
				</div>
				<div class="col-md-4">
					<strong>Campaign Name</strong>
				</div>
				<div class="col-md-3">
					<strong>Item</strong>
				</div>
				<div class="col-md-3">
					<strong>Status</strong>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-4">
					<p>2017 Running Shoe Launch</p>
				</div>
				<div class="col-md-3">
					<p>Free small pizza</p>
				</div>
				<div class="col-md-3">
					<p>
						ongoing
						<i class="pull-right fa fa-refresh text-primary"></i>
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-4">
					<p>2017 Running Shoe Launch</p>
				</div>
				<div class="col-md-3">
					<p>Free small pizza</p>
				</div>
				<div class="col-md-3">
					<p>
						Active
						<!-- <i class="pull-right fa fa-refresh text-primary"></i> -->
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-4">
					<p>2017 Running Shoe Launch</p>
				</div>
				<div class="col-md-3">
					<p>Free small pizza</p>
				</div>
				<div class="col-md-3">
					<p>
						On Hold
						<i class="pull-right fa fa-warning text-warning"></i>
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-2">
					<p>1/17/2017</p>
				</div>
				<div class="col-md-4">
					<p>2017 Running Shoe Launch</p>
				</div>
				<div class="col-md-3">
					<p>Free small pizza</p>
				</div>
				<div class="col-md-3">
					<p>
						complete
						<i class="pull-right fa fa-check text-success"></i>
					</p>
				</div>
			</div>
		</div>

	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>