<?php
	$content = array();
	$content['title'] = "Real Time Map";
	$content['script'] = '

		if ($("#locations-graph").length)
		{
			var locationsgraph = document.getElementById("locations-graph").getContext("2d");
			
			var locationsgraphdata = {
			    labels: [
			        "Portland, OR",
			        "Seattle, WA",
			        "New York, NY",
			        "Los Angeles, CA",
			        "Houston, TX",
			        "Las Vegas, NV",
			    ],
			    datasets: [{
			        label: "Primary Locations",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: "#34A6FF",
			        hoverBackgroundColor: ["#34A6FF"]
			    }]
			};

			var locationsgraphchart = new Chart(locationsgraph, {
			    type: "horizontalBar",
			    data: locationsgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	ticks: {
			            		beginAtZero: true,
			            		callback: function(value) {
				               		return value + "%";
				            	}
			                }
			            }],
			            yAxes: [{
			            	stacked: true
			            }]
			        },legend: {
			        	display:false
			    	}

			    }
			});

		}';

	$content['right-of-title'] = '';
	$content['content'] = '<div class="row">
		<div class="col-md-12">
			
			<div class="col-md-3 col-sm-6 text-center text-left">
				<h3>Active Users <br/>
					<b><span>192,226</span></b> <i class="fa fa-arrow-up text-success"></i>
				</h3>
			</div>
			
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="gftnow-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d89512.46277246125!2d-122.87551424520208!3d45.4846189482437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5495082476f88863%3A0x10e0cf158aacbd08!2sBeaverton%2C+OR%2C+USA!5e0!3m2!1sen!2sph!4v1490107405592" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<h3>Primary Locations <br/><br/>
			<canvas id="locations-graph" height="200"></canvas>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>