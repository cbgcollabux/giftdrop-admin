<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	function getaddress($latlng){
		$location = @file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latlng.'&sensor=false');
		if (strpos($http_response_header[0], "200")) { 
		   
		   $location = json_decode( $location );
		   return isset( $location->results[0]->formatted_address ) ? $location->results[0]->formatted_address : "" ;
		} else { 
		   return "Cant Find Address";
		}	
	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$state_drop_active;
	$time_drop_active;

	$state;
	if(isset($_GET['state'])){
		if($_GET['state'] == "Blocked"){
			$state = 'Disabled';
			$state_drop_active = "Blocked";
		}
		if($_GET['state'] == "Active"){
			$state = 'Enabled';
			$state_drop_active = "Active";
		}
		$time_drop_active = "All Time";
	}else {
		$state = '';
		$state_drop_active = "Show All";
	}
	
	if(isset($_GET['time'])){
		if($_GET['time'] == 'last7days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 weeks"));
			$time_drop_active = 'Last 7 Days';
		}
		if($_GET['time'] == 'last30days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 months"));
			$time_drop_active = 'Last 30 Days';
		}
		$state_drop_active = "Show All";
	} else {
		$from = gmdate("Y-m-d\TH:i:s\Z");
		$time_drop_active = 'All Time';
	}

	$listUsers;
    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUsersBody = new Swagger\Client\Model\UserListRequest();

	//$stateFilters = array("fieldName"=>"state","")
	/*$textFilters = array("fieldName"=>"name","pattern"=>$_GET['text']);
	$itemRangeQuery = array("startItemIndexInclusive"=>1,"endItemIndexExclusive"=>10);
	$periodFilters = array("fieldName"=>"created","period"=>array("from"=>$from,"to"=>gmdate("Y-m-d\TH:i:s\Z")));
	*/
	
	$query_parameters = array();
	$hasfilter = 0;

	if(isset($_GET['state'])){
		$stateFilters = array("fieldName"=>"state","state"=>($_GET['state']!="Active")?"Disabled":"Enabled");
		$query_parameters['state_filters'] = $stateFilters;
		$hasfilter++;
	}

	if(isset($_GET['text'])){
		$textFilters = array("fieldName"=>"name","pattern"=>$_GET['text']);
		$query_parameters['text_filters'] = $textFilters;
		$hasfilter++;
	}
	
	if($hasfilter>0)
		$listUsersBody['query_parameters'] = $query_parameters;
	
	try {
	    $listUsers = $api->listUsers($listUsersBody);
	   /* echo "<pre>";
	    print_r($listUsers);
	    echo "</pre>";*/
	    
	} catch (Exception $e) {
	
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Users Approval";
	$content['script'] = 
	
	'$(document).ready(function() {

		$(".user-row").click(function(){
			var id = $(this).attr("data-id");
			var latitude = $(this).attr("data-latitude");
			var longitude = $(this).attr("data-longitude");
			var email = $(this).attr("data-email");
			
			window.location = "dashboard.php?p=admin/users_active&id="+id+"&email="+email+"&latitude="+latitude+"&longitude="+longitude;
		});

		$("#stateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/users&state="+state+"&time="+time+"&text="+text;
		});

		
		$("#filterText").keydown(function (e){
		    if(e.keyCode == 13)
		    {
		    	var state = $("#stateFilter").val();
				var time = $("#dateFilter").val();
				var text = $("#filterText").val();

				window.location = "dashboard.php?p=admin/users&state="+state+"&time="+time+"&text="+text;
		    
		    }
		})

		$("#dateFilter").change(function(){
			var state = $("#stateFilter").val();
			var time = $("#dateFilter").val();
			var text = $("#filterText").val();

			window.location = "dashboard.php?p=admin/users&state="+state+"&time="+time+"&text="+text;
		});

		$("#table1").dataTable({"bFilter": false,"bInfo": false,"bLengthChange": false});

		$("#logout").show();
		$("#admin-menu-users").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$(".active-list").css("cursor", "pointer");

		$(".active-list").click(function(){
			var identifier = $(this).attr("data-id");
			window.location.href = "dashboard.php?p=admin/users_active&id="+identifier;
		});

		$(".active-list").hover(function(){
			$(this).css("background-color", "#f4f5f8");
			$(".active-list").not(this).css("background-color", "#ffffff");
		});

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#search-field").keyup(function(){
		    var value = $(this).val();

		    $(".active-list").each(function(index){
		        var id = $(this).find("div").text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-1").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});

	    pageSize = 10;

	    var pageCount =  $(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $(".active-list").hide();
	        $(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });
	});';

	$content['right-of-title'] = 
	'<div class="row">
		<div class="col-md-4">
			<select class="form-control" id="stateFilter">
				<option '.( (isset($_GET['state']))? ($_GET['state']=="all")?"selected":"" : ""  ).' value="all">All</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="pending")?"selected":"" : ""  ).' value="pending">Pending Approval</option>
				<option '.( (isset($_GET['state']))? ($_GET['state']=="active")?"selected":"" : ""  ).' value="active">Active</option>
			</select>
		</div>
		<div class="col-md-4">
			<select class="form-control" id="dateFilter">
				<option value="all" '.( (isset($_GET['time']))? ($_GET['time']=="all")?"selected":"" : ""  ).'>All</option>
				<option value="last30days" '.( (isset($_GET['time']))? ($_GET['time']=="last30days")?"selected":"" : ""  ).'>Last 30 Days</option>
				<option value="last7days" '.( (isset($_GET['time']))? ($_GET['time']=="last7days")?"selected":"" : ""  ).'>Last 7 Days</option>
			</select>
		</div>
		<div class="col-md-4">
			<input type="text" class="form-control" id="filterText" value="'.( isset($_GET['text'])?$_GET['text']:'' ).'" />
		</div>
	</div>';

	$users_content='';

	foreach($listUsers['users'] as $user){
		$date = $user['created']->format('n/j/Y');
		$user_name = $user['name'];

		if($user['account']['state'] == "Disabled")
			$stateTxt = "Pending Approval";
		if($user['account']['state'] == "Enabled")
			$stateTxt = "Active";

		$included=0;

		if(isset($_GET['state'])){
			if($_GET['state']=="pending"){
				if($user['account']['state']=="Disabled")
					$included=1;
			}else{
				if($user['account']['state']!="Disabled")
					$included=1;
			}
		}else{
			$included=1;
		}

		if($included==1){
			if(isset($_GET['time']))
			{
				if($_GET['time']=="last30days"){
					$minus = gmdate("d-m-Y", strtotime("-30 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}

				}else if($_GET['time']=="last7days"){
					$minus = gmdate("d-m-Y", strtotime("-7 days"));

					$now = gmdate("d-m-Y");
					$from=strtotime($minus);
					$to=strtotime($now);

					if( strtotime($date) >= $from && strtotime($date) <= $to ){
						$included=1;
					}else{
						$included=0;
					}
				}


			}
		}

		if($included==1){
			if(isset($_GET['text'])){
				if($_GET['text']!=""){

					if($user['role']=="Business"){
						$firstname = isset(json_decode($user['account']['title'])->firstname)?json_decode($user['account']['title'])->firstname:"";
						$lastname = isset(json_decode($user['account']['title'])->lastname)?json_decode($user['account']['title'])->lastname:"";
					}else{
						/*$firstname = $user['first_name'];
						$lastname = $user['last_name'];*/
						$firstname = $user['first_name'];
						$lastname = $user['last_name'];
					}

					if (strpos( strtolower( $user['name'] ), strtolower( $_GET['text'] ) ) !== false) {
					    $included=1;
					}else{
						$included=0;
					}
				}
			}
		}

		if($included==1){
			if($user['role']=="Business"){
				/*$firstname = isset(json_decode($user['account']['title'])->firstname)?json_decode($user['account']['title'])->firstname:"";
				$lastname = isset(json_decode($user['account']['title'])->lastname)?json_decode($user['account']['title'])->lastname:"";
				*/
				$firstname = $user['first_name'];
				$lastname = $user['last_name'];
			}else{
				$firstname = $user['first_name'];
				$lastname = $user['last_name'];
			}


			$users_content .="<tr class='user-row' data-longitude='".( isset($user['home_location']['longitude'])? $user['home_location']['longitude'] :"" )."' data-latitude='".( isset($user['home_location']['latitude'])? $user['home_location']['longitude'] :"" )."' data-email='".( isset($user['contact_repository']['contacts'][0]['contact'])?$user['contact_repository']['contacts'][0]['contact']:"" )."' data-id='".$user['identifier']."'><td>".(isset($firstname)?$firstname:"" )."</td><td>".(isset($lastname)?$lastname:"" )."</td><td>".( getaddress( $user["home_location"]['latitude'].','. $user["home_location"]['longitude'] ) )."</td><td>".( isset($user['contact_repository']['contacts'][0]['contact'])?trim_text($user['contact_repository']['contacts'][0]['contact'], "...", 12):"" )."</td><td>".$date."</td><td>".$stateTxt."</td></tr>";
		}
	}

	$content['content'] = 
	'<input type="hidden" name="start_item_index_inclusive" id="start_item_index_inclusive" />
	<input type="hidden" name="end_item_index_exclusive" id="end_item_index_exclusive" />
	<input type="hidden" name="field_name" id="field_name" />
	<input type="hidden" name="from" id="from" />
	<input type="hidden" name="to" id="to" />
	<input type="hidden" name="pattern" id="pattern" />
	<input type="hidden" name="state" id="state" />
	<input type="hidden" name="sorting_order" id="sorting_order" />

	<style>
		.user-row{
			cursor: pointer;
		}
		#table1 th, #table1 td,table.dataTable.no-footer{
			background-color:#ffff;
			border: none !important;
		}
		.table tr, .table th, .table td{
			border: none !important;
			padding-top: 20px !important;
			padding-bottom: 20px !important;
		}
		.dataTables_paginate{
			float: none !important;
			text-align:center !important;
			padding-top: 25px !important;
		}
		.paginate_button.current{
			background: none !important;
			background-color: #619534 !important;
			border-radius: 50px !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button.current{
			color: #fff !important;
		}
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
			background:none !important;
			border: 1px solid #fff !important;
			color: #514F4F !important;
		}
		.active-main-panel{
			padding-top:30px;
			padding-bottom: 30px;
		}
		.user-row:hover td{
			background-color: #f4f5f8 !important;
		}
	</style>
	<div class="active-main-panel">
		<table id="table1" class="table">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Location</th>
					<th>Email</th>
					<th>Registered</th>
					<th>State</th>
				</tr>
			</thead>
			<tbody>
				'.$users_content.'
			</tbody>
		</table>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>