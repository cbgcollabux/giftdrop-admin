<?php
	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$content = array();
	$content['title'] = "Vendors Overview <a href='index.php?p=admin/vendors' style='font-size:15px;color:#5DADE2;'>See full list</a>";
	$content['script'] = 
	'$(document).ready(function() {
		$("#logout").show();
		$("#title").removeClass("col-md-3 col-lg-3").addClass("col-md-10 col-lg-10");
		$("#right-of-title").css("display", "none");
		$("#admin-menu-vendors").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-2").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});

		if ($("#vendors-graph").length){
			var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

			var lmorris = Morris.Line({
			  element: "vendors-graph",
			  data: [
			    { month: "2016-01", value: 1230 },
			    { month: "2016-02", value: 2340 },
			    { month: "2016-03", value: 2340 },
			    { month: "2016-04", value: 2345 },
			    { month: "2016-05", value: 2335 },
			    { month: "2016-06", value: 2324 },
			    { month: "2016-07", value: 2454 },
			    { month: "2016-08", value: 2345 },
			    { month: "2016-09", value: 1123 },
			    { month: "2016-10", value: 2324 },
			    { month: "2016-11", value: 3456 },
			    { month: "2016-12", value: 1234 },
			  ],
			  xkey: "month",
			  ykeys: ["value"],
			  xLabelFormat: function(x){
			    var month = months[x.getMonth()];
				return month;
			  },\
			  resize: "true",
			  lineColors: ["#FFD23A"],
			  labels : ["Vendors"],
			});

			lmorris.options.labels.forEach(function(label, i) {
			    var legendItem = $("<span></span>").text(label).prepend("<br><span>&nbsp;</span>");
			    legendItem.find("span")
			      .css("backgroundColor", lmorris.options.lineColors[i])
			      .css("width", "10px")
			      .css("height", "10px")
			      .css("display", "inline-block")
			      .css("border-radius", "5px")
			      .css("margin", "10px")
			      .css("font-size", "10px");
			    $("#vendors-graph-legend").append(legendItem)
			});

			$(window).on("resize", function(){
		      lmorris.redraw();
		    });
		}

		if ($("#locations-graph").length){
			var locationsgraph = document.getElementById("locations-graph").getContext("2d");
	
			var color_gradient = locationsgraph.createLinearGradient(0, 0, 300, 0);
			color_gradient.addColorStop(0, \'#81D4FA\');
			color_gradient.addColorStop(1, \'#02477D\');

			var locationsgraphdata = {
			    labels: [
			        "Portland, OR",
			        "Seattle, WA",
			        "New York, NY",
			        "Los Angeles, CA",
			        "Houston, TX",
			        "Las Vegas, NV",
			    ],
			    datasets: [{
			        label: "Primary Locations",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: color_gradient,
			        hoverBackgroundColor: color_gradient,
			    }]
			};

			var locationsgraphchart = new Chart(locationsgraph, {
			    type: "horizontalBar",
			    data: locationsgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	display: false,
			            }],
			            yAxes: [{
			            	stacked: true,
			                gridLines: {
			                    display: false,
			                },
			                ticks: {
								fontColor: "black",
                        		fontStyle: "bold",
                        		fontFamily: "GothamRndMedium",
			                }
			            }],
			        	},
			        	legend: {
			        		display:false
			    		}
					}
			});

		}


		if ($("#categories-graph").length){
			var categoriesgraph = document.getElementById("categories-graph").getContext("2d");

			var color_gradient = categoriesgraph.createLinearGradient(0, 0, 300, 0);
			color_gradient.addColorStop(0, \'#81D4FA\');
			color_gradient.addColorStop(1, \'#02477D\');
			
			var categoriesgraphdata = {
			    labels: [
			        "Fast Food",
			        "Restaurant",
			        "Apparel",
			        "Coffee",
			        "Electronics",
			        "Automotive",
			    ],
			    datasets: [{
			        label: "Primary Categories",
			        data: [27, 19, 16, 9, 4, 2],
			        backgroundColor: color_gradient,
			        hoverBackgroundColor: color_gradient,
			    }]
			};

			var categoriesgraphchart = new Chart(categoriesgraph, {
			    type: "horizontalBar",
			    data: categoriesgraphdata,
			    options: {
			        scales: {
			            xAxes: [{
			            	display: false,
			            }],
			            yAxes: [{
			            	stacked: true,
			                gridLines: {
			                    display: false,
			                },
			                ticks: {
								fontColor: "black",
                        		fontStyle: "bold",
                        		fontFamily: "GothamRndMedium",
			                }
			            }],
			        	},
			        	legend: {
			        		display:false
			    		}
					}
			});

		}
	});';

	$content['right-of-title'] = '';
	$content['content'] = 
	'<div class="row">	
		<div class="col-md-12">
			<div class="active-main-panel">
				<div class="row">
					<div class="col-md-12">
						
						<div class="col-md-2 col-sm-4 text-center">
							Total Vendors
							<h4 style="margin-top:-4px;"><b><span>45,801</span></b> <i class="fa fa-arrow-up text-success"></i></h4>
						</div>
						<div class="col-md-2 col-sm-4 text-center">
							Total Drops
							<h4 style="margin-top:-4px;"><b><span>134,008</span></b> <i class="fa fa-arrow-up text-success"></i></h4>
						</div>
						<div class="col-md-2 col-sm-4 text-center">
							Total Spent 
							<h4 style="margin-top:-4px;"><b><span>$460,227</span></b> <i class="fa fa-arrow-down text-warning"></i></h4>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<div id="vendors-graph"></div>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div id="vendors-graph-legend"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default" style="border:0px solid;">
			  <div class="panel-body" style="text-align:left;">
				<p style="font-size:20px;font-family:GothamRndRegular;">Primerly Locations</p><br/>
				<canvas id="locations-graph" height="130"></canvas>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default" style="border:0px solid;">
			  <div class="panel-body" style="text-align:left;">
				<p style="font-size:20px;font-family:GothamRndRegular;">Primerly Categories</p><br/>
				<canvas id="categories-graph" height="130"></canvas>
				</div>
			</div>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>