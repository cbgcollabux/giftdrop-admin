<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	/*$user = new user();
	$result = $user->findMyCompanyProfile();
	$listUsers = $user->listUsers('', '', $from, $to);*/

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$state_drop_active;
	$time_drop_active;

	$state;
	if(isset($_GET['state'])){
		if($_GET['state'] == "Blocked"){
			$state = 'Disabled';
			$state_drop_active = "Blocked";
		}
		if($_GET['state'] == "Active"){
			$state = 'Enabled';
			$state_drop_active = "Active";
		}
		$time_drop_active = "All Time";
	}else {
		$state = '';
		$state_drop_active = "Show All";
	}

	
	if(isset($_GET['time'])){
		if($_GET['time'] == 'last7days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 weeks"));
			$time_drop_active = 'Last 7 Days';
		}
		if($_GET['time'] == 'last30days'){
			$from = gmdate("Y-m-d\TH:i:s\Z", strtotime("-1 months"));
			$time_drop_active = 'Last 30 Days';
		}
		$state_drop_active = "Show All";
	} else {
		$from = gmdate("Y-m-d\TH:i:s\Z");
		$time_drop_active = 'All Time';
	}

	$listUsers;
    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUsersBody = new Swagger\Client\Model\Body1();
	try {
	    $listUsers = $api->listUsers($listUsersBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

	/*echo"<pre>";
	print_r($listUsers['users']);
	echo"</pre>";*/

	/*if($user->token == ''){
		//echo '<input type="hidden" name="token" id="token" value="-1">';
		header("Location: admin-login.php");
	}*/

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Users";
	$content['script'] = 
	
	'$(document).ready(function() {
		$("#logout").show();
		$("#admin-menu-users").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");


		$(".active-list").css("cursor", "pointer");

		$(".active-list").click(function(){
			var identifier = $(this).attr("data-id");
			window.location.href = "index.php?p=admin/users_active&id="+identifier;
		});

		$(".active-list").hover(function(){
			$(this).css("background-color", "#f4f5f8");
			$(".active-list").not(this).css("background-color", "#ffffff");
		});

		$("#drop-rounded-header-1").unbind("click").bind("click", function(){
			$("#drop-rounded-body-1").slideToggle("fast");
		});

		$("#drop-rounded-header-2").unbind("click").bind("click", function(){
			$("#drop-rounded-body-2").slideToggle("fast");
		});

		$("#search-field").keyup(function(){
		    var value = $(this).val();

		    $(".active-list").each(function(index){
		        var id = $(this).find("div").text();
		        $(this).toggle(id.toLowerCase().indexOf(value.toLowerCase()) !== -1);
		    });
		});

		$("#searchBtn").click(function(){
			$("#hidden-col-md-1").hide();
			$("#searchField-div").fadeIn("fast");
			$("#searchBtn-div").hide();
		});


	    pageSize = 10;

	    var pageCount =  $(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $(".active-list").hide();
	        $(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });
	});';

	$content['right-of-title'] = 
	'<div class="row" style="margin-top:11px;">
		<div id="hidden-col-md-1" class="col-md-1" align="center"></div>
		<div class="col-md-4">
			<div class="drop-rounded-container">
				<div id="drop-rounded-header-1" class="drop-rounded-header">
					<span>'.$state_drop_active.'</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div id="drop-rounded-body-1" class="drop-rounded-body">
					<ul>
						<a href="index.php?p=admin/users"><li>Show All</li></a>
						<a href="index.php?p=admin/users&state=Blocked"><li>Blocked</li></a>
						<a href="index.php?p=admin/users&state=Active"><li>Active</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-4">
		 	<div class="drop-rounded-container">
				<div id="drop-rounded-header-2" class="drop-rounded-header">
					<span>'.$time_drop_active.'</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div id="drop-rounded-body-2" class="drop-rounded-body">
					<ul>
						<a href="index.php?p=admin/users"><li>All Time</li></a>
						<a href="index.php?p=admin/users&time=last30days"><li>Last 30 Days</li></a>
						<a href="index.php?p=admin/users&time=last7days"><li>Last 7 Days</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div id="searchField-div" class="col-md-4" style="display:none;">
			<div class="inner-addon left-addon">
			<span class="addon-search"><i class="fa fa-search"></i></span>
			    <input type="text" id="search-field" class="rounded-search form-control gotham-regular-placeholder" placeholder="Search" style="cursor:default;background-color:inherit;" />
			</div>
		</div>
		<div id="searchBtn-div" class="col-md-2" align="center">
			<div id="searchBtn" class="rounded-search" style="height:35px;">
				<i class="fa fa-search"></i>
			</div>
		</div>
	</div>';

	//<button id="searchBtn" class="btn btn-default rounded-search" style="height:35px;""><i class="fa fa-search"></i></button>

	$users_content='';

	foreach($listUsers['users'] as $user){
		$email = $user['contact_repository']['contacts'][0]['contact'];;
		$stateTxt;
		$date = $user['created']->format('n/j/Y');

		/*foreach($user['contact_repository']['contacts'] as $contact){
			if($contact['contact_type']=="Email")
			{
				$email = $contact['contact'];
				break;
			}
		}*/

		if($user['state'] == "Disabled")
			$stateTxt = "Blocked";
		if($user['state'] == "Enabled")
			$stateTxt = "Active";

		$user_name = explode(" ", $user['name']);

		if($state == ""){
			if (count($user_name) > 1)
				$users_content.='<div data-id="'.$user['identifier'].'" class="row active-list"><div class="col-md-2"><p>'.trim_text($user_name[0], "...", 12).'</p></div><div class="col-md-2"><p>'.trim_text($user_name[1], "...", 12).'</p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p>'.trim_text($email, "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
			else
				$users_content.='<div data-id="'.$user['identifier'].'" class="row active-list"><div class="col-md-2"><p>'.trim_text($user_name[0], "...", 12).'</p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p>'.trim_text($email, "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
		}else{
			if($user['state'] == $state){
				if (count($user_name) > 1)
					$users_content.='<div data-id="'.$user['identifier'].'" class="row active-list"><div class="col-md-2"><p>'.trim_text($user_name[0], "...", 12).'</p></div><div class="col-md-2"><p>'.trim_text($user_name[1], "...", 12).'</p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p>'.trim_text($email, "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
				else
					$users_content.='<div data-id="'.$user['identifier'].'" class="row active-list"><div class="col-md-2"><p>'.trim_text($user_name[0], "...", 12).'</p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p></p></div><div class="col-md-2"><p>'.trim_text($email, "...", 12).'</p></div><div class="col-md-2"><p>'.$date.'</p></div><div class="col-md-2">'.$stateTxt.'</div></div>';
			}
		}
	}

	$content['content'] = 
	'<div class="active-main-panel">
		<div class="row active-list-title">
			<div class="col-md-2">
				<strong>First Name</strong>
			</div>
			<div class="col-md-2">
				<strong>Last Name</strong>
			</div>
			<div class="col-md-2">
				<strong>Location</strong>
			</div>
			<div class="col-md-2">
				<strong>Email</strong>
			</div>
			<div class="col-md-2">
				<strong>Registered</strong>
			</div>
			<div class="col-md-2">
				<strong>State</strong>
			</div>
		</div>
		'.$users_content.'

		<center>
			<ul id="pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>