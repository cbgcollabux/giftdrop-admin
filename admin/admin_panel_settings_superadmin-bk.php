<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	/*$user = new user();
	$listUsers = $user->listUsers();*/

    $swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUsersBody = new Swagger\Client\Model\Body1();
	$listActivePlansBody = new Swagger\Client\Model\Body11();
	
	try {
	    $listUsers = $api->listUsers($listUsersBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

	try {
	    $listSettings = $api->listSettings();

	    echo "<pre>";
	    print_r($listSettings);
	    echo "</pre>";
	} catch (Exception $e) {
		echo 'Exception when calling AdminServiceApi->listSettings: ', $e->getMessage(), PHP_EOL;
	}

	try {
		$listActivePlans = $api->listActivePlans($listActivePlansBody);

	} catch (Exception $e){
		echo 'Exception when calling AdminServiceApi->listActivePlansBody: ', $e->getMessage(), PHP_EOL;
	}

	$admin = '';
	foreach(array_reverse($listUsers['users']) as $user){
		if($user['role'] == "Admin"){
			$admin .= '<div class="row active-list">
							<div class="col-md-3">
								'.$user['role'].'
							</div>
							<div class="col-md-6">
								'.$user['name'].'
							</div>
							<div class="col-md-3">
								<p style="cursor:pointer;" class="choose-action-btn">Choose Action <i class="fa fa-chevron-down"></i></p>
								<div class="drop-rounded-body choose-action">
									<ul>
										<li style="cursor:pointer;">Make Admin</li>
										<li style="cursor:pointer;">Make Manager</li>
										<li style="cursor:pointer;">Freeze Access</li>
										<li style="cursor:pointer;">Delete</li>
									</ul>
								</div>
							</div>
						<hr>
						</div>';
		}
	}

	$plans = '';
	foreach($listActivePlans['plans'] as $plan){
		$fees = '';
		foreach($plan['fees'] as $fee){
			/*if($fees['range_begin'] == "" or $fees['range_begin'] == null){
				$fees .= '<li>Payment range: $'.$fee['range_end'].'</li>';
			} else {
				$fees .= '<li>Payment range: $'.$fee['range_begin'].' - '.$fee['range_end'].'</li>';
			}*/

			$fees .= '<li>Range Begin: '.$fee['range_begin'].'</li>';
			$fees .= '<li>Range End: '.$fee['range_end'].'</li>';
			$fees .= '<li>Fixed: '.$fee['fixed'].'</li>';
			$fees .= '<li>Percentage: $'.$fee['percentage'].'</li>';
			$fees .= '<li>Card Fixed: $'.$fee['card_fixed'].'</li>';
			$fees .= '<li>Card Percentage: $'.$fee['card_percentage'].'</li>';
		}

		$button = '';
		if($plan['state'] == "Active")
			$button .= '<button data-id="'.$plan['identifier'].'" class="btn btn-warning gftnow-btn-warning gftnow-btn disable-plan-btn">Disable</button>';
		else
			$button .= '<button data-id="'.$plan['identifier'].'" class="btn gftnow-btn-success gftnow-btn activate-plan-btn">Disable</button>';

		$plans .= '<div class="row active-list">
						<div class="col-md-3">
							'.$plan['name'].'
						</div>
						<div class="col-md-6">
							<ul>'.$fees.'</ul>
						</div>
						<div class="col-md-3">
							'.$button.'<br/><br/><button data-id="'.$plan['identifier'].'" data-name="'.$plan['name'].'" class="btn gftnow-btn-success gftnow-btn assign-plan-btn">Assign</button>
						</div>
						<hr>
					</div>';
	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';
	$content['title'] = 'Settings';
	$content['right-of-title'] = '';
	$content['script'] = 
	'$(document).ready(function(){

		var feeCount = 0;

		$("#logout").show();
		$("#admin-menu-settings").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		pageSize = 10;

	    var pageCount =  $("#admin-row").find(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $("#admin-row").find(".active-list").hide();
	        $("#admin-row").find(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });

		$("#add-admin-btn").click(function(){
			$(this).blur();
			$("#add-admin-modal").fadeIn("fast");
		});

		$("#close-add-admin-modal").click(function(){
			$("#add-admin-modal").fadeOut("fast");
		});

		checkvalidation();

		$.validator.addMethod("pwcheck", function(value) {
			 return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]+$/.test(value)
			&& /[a-z]/.test(value) // has a lowercase letter
			&& /[A-Z]/.test(value) // has a uppercase letter
			&& /\d/.test(value)//has a digit
			&& /[!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]/.test(value)
		},"Must be at least 8 characters and consist of lowercase letter, uppercase letter, number and special characters");

		$("#form1").validate({
			rules:{
				email: {
					required: true,
					email: true
				},
				password:{
					required: true,
					pwcheck: true,
					minlength: 8
				}
			},
			submitHandler: function(form){

				$("#mask2").show();

				var email = $("#email").val();
				var password = $("#password").val();

				var haserror = 0;

				firebase.auth().createUserWithEmailAndPassword( email, password ).catch(function(error) {
		 
					var errorCode = error.code;
					var errorMessage = error.message;

					console.log(errorCode);
					console.log(errorMessage);
				 	
					if(errorCode=="auth/invalid-email"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/weak-password"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/email-already-in-use"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
				  
				}).then(function(user){
					if(haserror == 0){
						
						var cuser = firebase.auth().currentUser;

						cuser.getToken().then(function(data){
							var token = data;

							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType : "json",
								data: { func: "loginAdmin",token:token },
								success: function(data){
									console.log(data);
									if(data.result.result.userInfo.identifier){
										$("#add-admin-modal").fadeIn("fast");
										location.reload();
									}else{
										console.log("cant add");
									}
								},error: function(err){
									console.log(err.responseText);
								}
							});
						});
					} else {
						$("#mask2").hide();
					}
				});
			}
		});

		$("input").focus(function(){
			checkvalidation();
		});

		$("input").keyup(function(){
			checkvalidation();
		});

		function checkvalidation(){
			var err = 0;
			$("input").each(function(){
				if ( $(this).hasClass("invalid") )
					err++;
			});

			if( $("#email").val() == "" )
				err++;

			if( $("#password").val() == "" )
				err++;

			if(err==0){
				$("#submit").removeClass("gftnow-btn-default");
				$("#submit").addClass("gftnow-btn-success");
				$("#submit").attr("type", "submit");
			}else{
				$("#submit").addClass("gftnow-btn-default");
				$("#submit").removeClass("gftnow-btn-success");
				$("#submit").attr("type", "button");
			}
		}


		$(".choose-action-btn").click(function(){
			$(this).parent().find(".choose-action").slideToggle("fast");
		});

		$(".gftnow-checkbox").click(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
			} else {
				$(this).addClass("active");
			}
		});


		// Plans pagination
		var plansPageSize = 10

	    var plansPageCount =  $("#plans-row").find(".active-list").length / plansPageSize;
	      
	    for(var i = 0 ; i<plansPageCount;i++){
	      $("#plans-pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#plans-pagination li").first().find("a").addClass("current");
	    
	    showPlanPage = function(page) {
	        $("#plans-row").find(".active-list").hide();
	        $("#plans-row").find(".active-list").each(function(n) {
	            if (n >= plansPageSize * (page - 1) && n < plansPageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPlanPage(1);

	    $("#plans-pagination li a").click(function() {
	        $("#plans-pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPlanPage(parseInt($(this).text()))
	    });

		$("#submit-assign-plan").click(function(){
			var ids = new Array();

			$(".selectUserForPlan").each(function(){

				if($(this).is(":checked")){
					ids.push($(this).attr("data-id"));
				}

			});

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {planIdentifier: currentPlanID, accountIdentifiers: ids, func: "ajaxAssignPlanToAccounts"},
				success: function(data){
					//location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
			
			console.log(currentPlanID);
			console.log(ids);
		});


		// Create Plan	

		$(".add-plan-inputs").keyup(function(e)
		{	
			planValidation();
		});

		function planValidation()
		{
			var error=0;

     		if( $("#planName").val() ==""){
     			error++;
     		}

     		if( $("#planDescription").val() ==""){
     			error++;
     		}


     		$(".add-plan-inputs").each(function(){
     			if($(this).val()==""){
     				error++;
     			}
     		});

			if(feeCount==0)
				error++;

     		if(error==0){
     			$("#submit-add-plan").prop("disabled",false);
     		}else{
     			$("#submit-add-plan").prop("disabled",true);
     		}
		}

		$("#add-plan-btn").click(function(){
			$(this).blur();
			$("#add-plan-modal").fadeIn("fast");
		});

		$("#numFees").keypress(function(e){
		     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		     	return false;
		     }
		});

		$("#add-planNameNumFees-btn").click(function(){
			var gftnowScrollbarHTML = "<label class=\'badge\'>FEE "+(feeCount+1)+"</label><div class=\'row\'>";

			gftnowScrollbarHTML +=	
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'rangeBegin"+(feeCount)+"\' name=\'rangeBegin"+feeCount+"\' placeholder=\'Range Begin\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'rangeEnd"+feeCount+"\' name=\'rangeEnd"+feeCount+"\' placeholder=\'Range End\' /></div><br/><br/>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'fixed"+feeCount+"\' name=\'fixed"+feeCount+"\' placeholder=\'Fixed\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'cardFixed"+feeCount+"\' name=\'cardFixed"+feeCount+"\' placeholder=\'Card Fixed\' /></div><br/><br/>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'percentage"+feeCount+"\' name=\'percentage"+feeCount+"\' placeholder=\'Percentage\' /></div>"+
					"<div class=\'col-md-6\'><input class=\'form-control add-plan-inputs add-planFee-inputs\' type=\'text\' id=\'cardPercentage"+feeCount+"\' name=\'cardPercentage"+feeCount+"\' placeholder=\'Card Percentage\' /></div>"+
					"<br/><br/><br/>";

			gftnowScrollbarHTML += "</div>";

			$("#gftnow-scrollbar").append(gftnowScrollbarHTML);

			feeCount++;


			$(".add-plan-inputs").keyup(function(e)
			{	
				planValidation();
			});

			$(".add-planFee-inputs").keypress(function(e){
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		     		return false;
		     	}
			});

		});

		$("#submit-add-plan").click(function(){
			var err=0;
			$(".add-plan-inputs").each(function(){
				if($(this).val() == ""){
					err++;
				}
			});

			if(err == 0){
				var fees = new Array();
				//var feeCount = $(".add-planFee-inputs").length / 6;

				for(i = 0; i < feeCount; i++){
					var fee = {};
					fee.rangeBegin = parseInt($("#rangeBegin"+i).val());
					fee.rangeEnd = parseInt($("#rangeEnd"+i).val());
					fee.fixed = parseInt($("#fixed"+i).val());
					fee.cardFixed = parseInt($("#cardFixed"+i).val());
					fee.percentage = parseInt($("#percentage"+i).val());
					fee.cardPercentage = parseInt($("#cardPercentage"+i).val());
					fees.push(fee);
				}

				console.log(fees);

				jQuery.ajax({
					url:"mvc/controller/ajaxController.php",
					type: "post",
					dataType: "json",
					data: {name: $("#planName").val(), fees: fees, func: "ajaxCreatePlan"},
					success: function(data){
						location.reload();
						console.log(data);
					},error: function(err){
						console.log(err.responseText);
					}
				});

			} else {
				$("#add-plan-error").show();
			}
			
		});

		$("#close-add-plan-modal").click(function(){
			$("#add-plan-modal").fadeOut("fast");
			$("#planName").val("");
			$("#numFees").val("");
			$("#add-planNameNumFees-btn").removeClass("gftnow-btn gftnow-btn-default btn btn-success").addClass("gftnow-btn gftnow-btn-success btn btn-success");
			$("#numFees").prop("disabled", false);
			$("#add-plan-error").hide();
			$("#gftnow-scrollbar").html("");
		});


		// Assign Plan
		
		$(".assign-plan-btn").click(function(){
			currentPlanID = $(this).attr("data-id");
			console.log("id: "+$(this).attr("data-id")+" name: "+$(this).attr("data-name"));
			$("#plan-name").html($(this).attr("data-name"));

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {func: "ajaxListUsers"},
				success: function(data){
					console.log(data);

					gftnowScrollbar1HTML = "<ul style=\'list-style: none;\'>";

					for(i = 0; i < data.listUsers.result.users.length; i++){
						console.log(data.listUsers.result.users[i]);

						gftnowScrollbar1HTML += "<li>"+
													"<div class=\'row\'>"+
														"<div class=\'gftnow-inline\'>"+
															"<div class=\'col-md-2\'>"+
																"<div class=\'gftnow-form\'>"+
																	"<input style=\'width:25px;\' type=\'checkbox\' data-id=\'"+data.listUsers.result.users[i].identifier+"\' class=\'form-control selectUserForPlan\' data-name=\'type\' data-value=\'gift-card\' />"+
																"</div>"+
															"</div>"+
															"<div class=\'col-md-2\'>"+
																"<img style=\'height:50px;width:50px;\' src=\'"+data.listUsers.result.users[i].account.logoURL+"\'>"+
															"</div>"+
															"<div class=\'col-md-8\'>"+
																"<label class=\'gftnow-font-medium\'>"+data.listUsers.result.users[i].name+"</label>"+
															"</div>"+
														"</div>"+
													"</div>"+
												"</li>";
					}

					gftnowScrollbar1HTML += "</ul>";

					$("#gftnow-scrollbar1").html(gftnowScrollbar1HTML);

					$("#assign-plan-modal").fadeIn("fast");
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-assign-plan-modal").click(function(){
			$("#assign-plan-modal").fadeOut("fast");
		});


		// Disable Plan
		$(".disable-plan-btn").click(function(){
			var id = new Array(); 
			id.push($(this).attr("data-id"));

			console.log(id);

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {plans:id, func: "ajaxDisablePlans"},
				success: function(data){
					//location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});



		var basic;
		basic = $("#croppie").croppie({
				    viewport: {
				        width: 200,
				        height: 200,
				        type: "circle"
				    },
				    customClass: "gftnow-croppie-container",
				    showZoomer: false
				});

		$("#croppie-crop-icon, #croppie-done").click(function(){
			basic.croppie("result","base64").then(function(base64){
				var cropted = base64;

				jQuery.ajax({
					url: "mvc/controller/ajaxController.php",
					type: "post",
					dataType: "json",
					data: { func: "base64ToPNG", base64: base64 },
					success: function(data){
						if(data.result=="OK"){
							$("#logoURL").val("http://www.gftnow.com/template/assets/upload/php/files/"+data.file);
							$(".profile-img-container").html("<input id=\'defaultUserLogo\' type=\'hidden\' value=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' /><img style=\'width:100%;height:auto;\' id=\'profilepic\' class=\'img-responsive\' src=\'http://www.gftnow.com/template/assets/upload/php/files/"+data.file+"\' alt=\'no profile\' /><a href=\'javascript:void(0);\'><input id=\'fileupload\' type=\'file\' name=\'files[]\' /><img src=\'assets/img/icon_camera.png\' style=\'width: 36px;height:auto;margin-top: 25px;\'></a>");
							$("#mask1").hide();
						}
					},error: function(err){
						console.log(err.responseText);
					}
				});
			});
		});

		fileupload();

		function fileupload(){
			var url = "assets/upload/php/index.php";
			var cropted = "";

		    $("#fileupload").fileupload({
		        url: url,
		        dataType: "json",
		        done: function (e, data) {
		        	if(data.result.files.length > 0){
		            	basic.croppie("bind", {
						    url: data.result.files[0].url
						});
		            	$("#mask2").hide();
		            	$("#mask1").show();
		            }
		        },
		        error: function(err){
		        	console.log("error");
		        	console.log(err.responseText);
		        },
		        progressall: function (e, data) {
		        	console.log("uploading");
		        }
		    }).prop("disabled", !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : "disabled");

	        $("button.cancel").click(function (e) {
			    xhr.abort();
			    $("#mask2").hide();
			});
		}

		tinymce.init({ selector:"#emailTemplateCampaignApproved", plugins: ["code"] });
		tinymce.init({ selector:"#emailTemplateBusinessUserRegistered", plugins: ["code"] });
		tinymce.init({ selector:"#emailTemplateCampaignDeclined", plugins: ["code"] });
		tinymce.init({ selector:"#emailTemplateNewCampaign", plugins: ["code"] });

		function updateSettings(){
			var settings = {};
			settings.defaultUserLogo = $("#defaultUserLogo").val();
			settings.dropValidityDuration = $("#dropValidityDuration").val();
			settings.dropPutBackDuration = $("#dropPutBackDuration").val();
			settings.pickupCoolDownPeriod = $("#pickupCoolDownPeriod").val();
			settings.visibleDropPickupRadius = $("#visibleDropPickupRadius").val();
			settings.visibleDropOfferRadius = $("#visibleDropOfferRadius").val();
			settings.visibleDropVisibilityRadius = $("#visibleDropVisibilityRadius").val();
			settings.hiddenDropPickupRadius = $("#hiddenDropPickupRadius").val();
			settings.defaultPlan = $("#defaultPlan").val();
			settings.tangoUsername = $("#tangoUsername").val();
			settings.tangoPassword = $("#tangoPassword").val();
			settings.tangoCustomerId = $("#tangoCustomerId").val();
			settings.tangoAccountId = $("#tangoAccountId").val();
			settings.maximumLogoSizeInBytes = $("#maximumLogoSizeInBytes").val();
			settings.emailSenderName = $("#emailSenderName").val();
			settings.emailSenderEmailAddress = $("#emailSenderEmailAddress").val();
			settings.emailTemplateCampaignApproved = $("#emailTemplateCampaignApproved").val();
			settings.emailTemplateBusinessUserRegistered = $("#emailTemplateBusinessUserRegistered").val();
			settings.emailTemplateCampaignDeclined = $("#emailTemplateCampaignDeclined").val();
			settings.emailTemplateNewCampaign = $("#emailTemplateNewCampaign").val();

			console.log(settings);

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {settings: settings, func: "ajaxUpdateSettings"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		}

		$("#save").click(function(){
			updateSettings();
		});

	});';

	$content['content'] = 
	'<input type="hidden" id="temp-plan-fees" name="temp-plan-fees" />

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Admin Roles</h1>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Role</strong>
			</div>
			<div class="col-md-6">
				<strong>Name</strong>
			</div>
			<div class="col-md-3">
				<strong>Action</strong>
			</div>
		</div>

		<div id="admin-row">'.$admin.'</div>
		
		<center>
			<ul id="pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>

		<br>
		<center><button id="add-admin-btn" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Plans and Payments</h1>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Plan Title</strong>
			</div>
			<div class="col-md-6">
				<strong>Details</strong>
			</div>
			<div class="col-md-3">

			</div>
		</div>

		<!--<div class="row active-list">
			<div class="col-md-3">
				Basic
			</div>
			<div class="col-md-6">
				Payment range: $1-99 Fee: $4
			</div>
			<div class="col-md-3">
				<img src="assets/img/edit-icon-invert.png"/>
				<img src="assets/img/delete-icon-invert.png"/>
			</div>
		</div>-->

		<div id="plans-row">'.$plans.'</div>

		<center>
			<ul id="plans-pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>

		<br>
		<center><button id="add-plan-btn" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>
	<!--<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Notifications</h1>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<p class="gftnow-font-medium">General</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is activity or an important update</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of notifications every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-12">
				<p class="gftnow-font-medium">Registrations</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is a new vendor registration</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of all vendor registrations every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get summary of all vendor registrations for the week</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is a new user registration</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of all user registrations every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get summary of all user registrations for the week</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>
	</div>-->

	<div class="active-main-panel">
	   <div class="row">
       <div class="col-md-6">
			<div class="form-group">
			    <label for="email">Default User Logo:</label><br/>
			    <div class="profile-img-container fileinput-button" style="width:100px;height:100px;">
			    	<input id="defaultUserLogo" type="hidden" value="'.$listSettings["settings"][0]["value"].'" />
					<img style="width:100%;height:auto;" id="profilepic" src="'.$listSettings["settings"][0]["value"].'" alt="no profile" />
					<a href="javascript:void(0);"><input id="fileupload" type="file" name="files[]"><img src="assets/img/icon_camera.png" style="width: 36px;height:auto;margin-top: 25px;"></a>
				</div>
		    </div>
       		<div class="form-group">
			    <label for="email">Drop Validity Duration:</label>
			    <input type="email" value="'.$listSettings["settings"][1]["value"].'" class="form-control" id="dropValidityDuration">
		    </div>

		    <div class="form-group">
			    <label for="email">Drop Put Back Duration:</label>
			    <input type="email" value="'.$listSettings["settings"][2]["value"].'" class="form-control" id="dropPutBackDuration">
		    </div>

		    <div class="form-group">
			    <label for="email">Pickup Cool Down Period:</label>
			    <input type="email" value="'.$listSettings["settings"][3]["value"].'" class="form-control" id="pickupCoolDownPeriod">
		    </div>

		    <div class="form-group">
			    <label for="email">Visible Drop Pickup Radius:</label>
			    <input type="email" value="'.$listSettings["settings"][4]["value"].'" class="form-control" id="visibleDropPickupRadius">
		    </div>

		    <div class="form-group">
			    <label for="email">Visible Drop Offer Radius:</label>
			    <input type="email" value="'.$listSettings["settings"][5]["value"].'" class="form-control" id="visibleDropOfferRadius">
		    </div>

		    <div class="form-group">
			    <label for="email">Visible Drop Visibility Radius:</label>
			    <input type="email" value="'.$listSettings["settings"][6]["value"].'" class="form-control" id="visibleDropVisibilityRadius">
		    </div>

		    <div class="form-group">
			    <label for="email">hidden Drop Pickup Radius:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][7]["value"].'" id="hiddenDropPickupRadius">
		    </div>

		    <div class="form-group">
			    <label for="email">Default Plan:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][8]["value"].'" id="defaultPlan">
		    </div>
       </div>
       <div class="col-md-6">
       <div class="form-group">
		    <label for="email">Tango Username:</label>
		    <input type="email" value="'.$listSettings["settings"][9]["value"].'" class="form-control" id="tangoUsername">
	    </div>
       	<div class="form-group">
			    <label for="email">Tango Password:</label>
			    <input type="email" value="'.$listSettings["settings"][10]["value"].'" class="form-control" id="tangoPassword">
		    </div>

		    <div class="form-group">
			    <label for="email">Tango Customer Id:</label>
			    <input type="email" value="'.$listSettings["settings"][11]["value"].'" class="form-control" id="tangoCustomerId">
		    </div>

		    <div class="form-group">
			    <label for="email">Tango Account Id:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][12]["value"].'" id="tangoAccountId">
		    </div>

		    <div class="form-group">
			    <label for="email">Maximum Logo Size InBytes:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][13]["value"].'" id="maximumLogoSizeInBytes">
		    </div>

		    <div class="form-group">
			    <label for="email">Email Sender Name:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][17]["value"].'" id="emailSenderName">
		    </div>

		    <div class="form-group">
			    <label for="email">Email Sender Email Address:</label>
			    <input type="email" class="form-control" value="'.$listSettings["settings"][18]["value"].'" id="emailSenderEmailAddress">
		    </div>
       </div>
       </div>
       <div class="row">
			<div class="col-md-12">
   				<div class="form-group">
				    <label for="email">Email Template Campaign Approved:</label>
				    <textarea class="form-control" id="emailTemplateCampaignApproved" style="height:250px;">'.stripslashes($listSettings["settings"][21]["value"]).'</TextArea>
			    </div>
       		</div>

       		<div class="col-md-12">
   				<div class="form-group">
				    <label for="email">Email Template Business User Registered:</label>
				    <textarea class="form-control" id="emailTemplateBusinessUserRegistered" style="height:250px;">'.stripslashes($listSettings["settings"][22]["value"]).'</TextArea>
			    </div>
       		</div>

			<div class="col-md-12">
   				<div class="form-group">
				    <label for="email">Email Template Campaign Declined:</label>
				    <textarea class="form-control" id="emailTemplateCampaignDeclined" style="height:250px;">'.stripslashes($listSettings["settings"][20]["value"]).'</TextArea>
			    </div>
       		</div>

			<div class="col-md-12">
   				<div class="form-group">
				    <label for="email">Email Template New Campaign:</label>
				    <textarea class="form-control" id="emailTemplateNewCampaign" style="height:250px;">'.stripslashes($listSettings["settings"][19]["value"]).'</TextArea>
			    </div>
       		</div>
       </div>
       <div class="row">
       		<div class="col-md-12 text-right"><button id="save" class="btn gftnow-btn gftnow-btn-success">Save</button></div>
       </div>
	</div>

	<div class="gftnow-mask" id="mask1">
		<div class="gftnow-modal">
			<div class="gftnow-modal-header text-center">
				<b>Logo</b> <br/><br/>
				<p>Move your image around to fit the orange box</p>
			</div>
			<div class="gftnow-modal-body">
				<img id="croppie-crop-icon" src="assets/img/mini-croppie-icon.png"/>
				<div id="croppie"></div>
				<br/>
				<center>
					<button class="gftnow-btn gftnow-btn-success" id="croppie-done">DONE</button>
				</center>
				<br/>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="add-admin-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Add Admin</h1>
				<br/><br/>
				<form method="post" id="form1">
				<div class="custom-input">
					<p id="email-placeholder" class="placeholder-effect"></p>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email" />
					<br/><br/>
					<p id="password-placeholder" class="placeholder-effect"></p>
					<input type="password" class="form-control" name="password" id="password" placeholder="Password" />
					<br/><br/>
				</div>
				<div class="gftnow-hide" id="errorCode">
					<div class="alert alert-danger">
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-6">
						<input onclick="blur()" id="submit" class="btn gftnow-btn-success gftnow-btn pull-left" value="ADD" style="width:85%;" />
						</form>
					</div>
					<div class="col-md-6">
						<a href="javascript:void(0);" id="close-add-admin-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
					</div>
				</div>
			</center>
		</div>
	</div>

	<div class="gftnow-mask" id="assign-plan-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width:600px;width:600px;min-height:425px;height:425px;top:90px;left:0;right:0;margin:0 auto;">
			<div class="row"><div class="col-md-12"><center><h1>Assign Plan</h1><p id="plan-name"></p></center></div></div>
			<br/>
			
			<div class="active-main-panel" id="gftnow-scrollbar1" style="margin-top:0px;overflow-y:auto;max-height:220px;height:220px;"></div>

			<br/><br/>
			<div class="row">
				<div class="col-md-6">
					<button id="submit-assign-plan" class="btn gftnow-btn gftnow-btn-success pull-left" style="width:85%;">Assign</button>
				</div>
				<div class="col-md-6">
					<a href="javascript:void(0);" id="close-assign-plan-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
				</div>
			</div>
		</div>
	</div>

	<div class="gftnow-mask" id="add-plan-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width:800px;width:800px;min-height:625px;height:625px;top:30px;left:0;right:0;margin:0 auto;">
			<center><h1>Add Plan</h1></center>
			<br/><br/>
			<div class="row">
				<div class="col-md-12">
					<p id="planName-placeholder" class="placeholder-effect"></p>
					<input type="text" class="form-control add-plan-inputs" name="planName" id="planName" placeholder="Name" />
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-md-12">
					<p id="planDescription-placeholder" class="placeholder-effect"></p>
					<textarea type="text" class="form-control add-plan-inputs" name="planDescription" id="planDescription" placeholder="Description"></textarea>
				</div>
			</div>
			
			<br/><br/><br/>
			<div class="active-main-panel" id="gftnow-scrollbar" style="margin-top:0px;overflow-y:auto;max-height:200px;height:200px;padding:0px;">
			</div>

			<br/><br/>
			<div class="row">
				<div class="col-md-12"><center><span id="add-plan-error" class="gftnow-font-medium gftnow-hide" style="color:red;">Please input all fields!</span></center></div>
				<div class="col-md-4">
					<button id="add-planNameNumFees-btn" style="width:100%;" class="gftnow-btn gftnow-btn-success btn btn-success btn-block">Add Fees</button>
				</div>
				<div class="col-md-4">
					<button id="submit-add-plan" disabled class="btn gftnow-btn gftnow-btn-success btn-block">Save</button>
				</div>

				<div class="col-md-4">
					<a href="javascript:void(0);" id="close-add-plan-modal" class="btn gftnow-btn-success gftnow-btn btn-block">Close</a>
				</div>
			</div>
		
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>