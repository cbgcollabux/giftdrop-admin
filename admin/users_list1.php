<?php
	$content = array();
	$content['title'] = "Users";
	$content['script'] = '';

	$content['right-of-title'] = '<div class="row">
		<div class="col-md-5">
			<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Local Awareness</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Campaign</li></a>
						<a href="#"><li>Local Awareness</li></a>
						<a href="#"><li>Christmas Deal</li></a>
						<a href="#"><li>New Flavor</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-5">
		 	<div class="drop-rounded-container">
				<div class="drop-rounded-header">
					<span>Last 30 Days</span> <i class="fa fa-angle-down pull-right"></i>
				</div>
				<div class="drop-rounded-body">
					<ul>
						<a href="#"><li>All Time</li></a>
						<a href="#"><li>Last 30 Days</li></a>
						<a href="#"><li>Last 7 Days</li></a>
						<a href="#"><li>Custom</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="rounded-search">
				<i class="fa fa-search" style="font-size: 1.2em;"></i>
			</div>
		</div>
	</div>';

	$content['content'] = '<div class="active-main-panel">
		<div class="row active-list-title">
			<div class="col-md-12">
				<div class="col-md-4">
					<strong>Name</strong>
				</div>
				<div class="col-md-3">
					<strong>Category</strong>
				</div>
				<div class="col-md-3">
					<strong>Location</strong>
				</div>
				<div class="col-md-2">
					<strong>Reistered</strong>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Toyota of Potland</p>
				</div>
				<div class="col-md-3">
					<p>Automotive</p>
				</div>
				<div class="col-md-3">
					<p>Portland, OR</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<!-- <i class="pull-right fa fa-refresh text-primary"></i> -->
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Nike</p>
				</div>
				<div class="col-md-3">
					<p>Apparel</p>
				</div>
				<div class="col-md-3">
					<p>Portland, OR</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<!-- <i class="pull-right fa fa-refresh text-primary"></i> -->
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Toys R Us</p>
				</div>
				<div class="col-md-3">
					<p>Kids & Family</p>
				</div>
				<div class="col-md-3">
					<p>Loas Angeles, LA</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<i class="pull-right fa fa-warning text-warning"></i>
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Home Deposit</p>
				</div>
				<div class="col-md-3">
					<p>Home Improve</p>
				</div>
				<div class="col-md-3">
					<p>San Francisco, CA</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<!-- <i class="pull-right fa fa-refresh text-primary"></i> -->
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Portland Gear</p>
				</div>
				<div class="col-md-3">
					<p>Apparel</p>
				</div>
				<div class="col-md-3">
					<p>San Francisco, CA</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<!-- <i class="pull-right fa fa-refresh text-primary"></i> -->
					</p>
				</div>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<div class="col-md-4">
					<p>Chipotle</p>
				</div>
				<div class="col-md-3">
					<p>Food</p>
				</div>
				<div class="col-md-3">
					<p>Seattle, WA</p>
				</div>
				<div class="col-md-2">
					<p>
						1/17/2017
						<i class="pull-right fa fa-times-circle text-danger"></i>
					</p>
				</div>
			</div>
		</div>

		<center>
			<ul class="pagination pagination-alt gftnow-pagination">
				<li>
					<a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a>
				</li>
				<li>
					<a href="javascript:void(0);">1</a>
				</li>
				<li>
					<a href="javascript:void(0);">2</a>
				</li>
				<li>
					<a href="javascript:void(0);">3</a>
				</li>
				<li class="active">
					<a href="javascript:void(0);">4</a>
				</li>
				<li>
					<a href="javascript:void(0);">5</a>
				</li>
				<li>
					<a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a>
				</li>
			</ul>
		</center>
		

	</div>
	';

	$content['menu'] = file_get_contents('menu1.php');
?>