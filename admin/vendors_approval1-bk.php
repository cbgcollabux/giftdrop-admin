<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	/*$user = new user();
	$detailedCompanyProfile = $user->detailedCompanyProfile($id);
	$listCompanyCategories = $user->listCompanyCategories();*/
	$id = $_GET['id'];

	$detailedCompanyProfile;
	$listUsers;
	$swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$detailedCompanyProfileBody = new Swagger\Client\Model\Body7();
	$detailedCompanyProfileBody["company_identifier"] = $id;
	$listUsersBody = new Swagger\Client\Model\Body1();
	try{
		$detailedCompanyProfile = $api->detailedCompanyProfile($detailedCompanyProfileBody);
	}catch(Exception $e){
		echo 'Exception when calling AdminServiceApi->detailedCompanyProfile: ', $e->getMessage(), PHP_EOL;
	}
	try{
		 $listUsers = $api->listUsers($listUsersBody);
	}catch (Exception $e){
		echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}	

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	function findCategory($id){
		$category;
		if($id == "1b25-4e1d-b2a5-adcfb5fbcdeb"){ $category = "Cash"; }
		if($id == "b6dae6b7-d02a-458b-ad22-3b626ddd882d"){ $category = "Food & Drinks"; }
		if($id == "a02a6059-aaf4-4cc1-9494-a31a1715fbbd"){ $category = "Automotive"; }
		if($id == "61360c31-3cae-4f3d-b395-e0f9aa320d47"){ $category = "Clothing"; }
		if($id == "831bd6c2-73c7-4502-a0c5-629b7a0331c6"){ $category = "Electronics"; }
		if($id == "280bc362-960b-4203-9f5e-a108d4b593d9"){ $category = "Entertainment"; }
		if($id == "63452c3b-8145-447b-aa45-ef12479693c2"){ $category = "Health & Beauty"; }
		if($id == "06125b59-0b1c-4c75-9a05-ecd821a82940"){ $category = "Home Improvements"; }
		if($id == "8a8cf347-8fbe-4850-84dd-312c06ee5905"){ $category = "Kids"; }
		if($id == "f9003709-0cd9-4f5d-83af-77b690d83251"){ $category = "Other"; }
		if($id == "ba864d3b-d434-4fbc-b0aa-b09df2af1c35"){ $category = "Pets"; }
		if($id == "8acf3596-eec1-4572-99d8-4e5463c3170b"){ $category = "Sport & Fitness"; }
		if($id == "0360c1e1-008d-4480-9a96-b0a5c95d0339"){ $category = "Travel"; }
		return $category;
	}

	$identifier = $detailedCompanyProfile['company_profile']['identifier'];
	$status = $detailedCompanyProfile['company_profile']['state'];
	$name = $detailedCompanyProfile['company_profile']['name'];
	$created = $detailedCompanyProfile['company_profile']['created']->date;
	$address = $detailedCompanyProfile['company_profile']['address'];
	$logoURL = $detailedCompanyProfile['company_profile']['account']['logo_url'];
	$about = $detailedCompanyProfile['company_profile']['account']['about'];
	$website = $detailedCompanyProfile['company_profile']['account']['website'];
	$category = findCategory($detailedCompanyProfile['company_profile']['category']);
	$phone = $detailedCompanyProfile['company_profile']['notes'];
	$email;

	foreach($listUsers['users'] as $user){
		if($detailedCompanyProfile['company_profile']['account']['identifier'] == $user['account']['identifier']){
			$email = $user['contact_repository']['contacts'][0]['contact'];
		}
	}

	/*foreach($detailedCompanyProfile['result']->companyProfile->category as $cat){
		if($detailedCompanyProfile['result']->companyProfile->category == $cat->identifier){
			$category.=$cat->name;
		}
	}*/

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';	
	$content['title'] = "Approval";
	$content['script'] = 
	'$(document).ready(function() {
		$("#logout").show();
		$("#admin-menu-approval").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		$("#drop-rounded-header-1").css("background-color","#ffffff");
		$("#drop-rounded-header-2").css("background-color","#ffffff");
		$("#searchBtn").css("background-color","#ffffff");
		$("#search-field").css("background-color","#ffffff");
		$(".fa.fa-angle-down").css("color", "#d3d2d2");

		if($("#status").val() == "Enabled"){
			$("#activate-btn").removeClass("btn btn-success gftnow-btn gftnow-btn-success").addClass("btn btn-default gftnow-btn gftnow-btn-default");
			$("#activate-btn").click(function(){
				$(this).blur();
			});
			$("#block-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-warning gftnow-btn gftnow-btn-warning");
			$("#block-btn").css({"background-color": "#E3B339", "border-color": "#E3B339"});
			$("#block-btn").hover(
				function(){
					$(this).css({"background-color": "#c09853", "border-color": "#c09853"});
				},
				function(){
					$(this).css({"background-color": "#E3B339", "border-color": "#E3B339"});
			});
			$("#block-btn").click(function(){
				$("#block-modal").fadeIn("fast");
			});
		}

		if($("#status").val() == "Disabled"){
			$("#block-btn").removeClass("btn btn-warning gftnow-btn gftnow-btn-warning").addClass("btn btn-default gftnow-btn gftnow-btn-default");
			$("#block-btn").click(function(){
				$(this).blur();
			});
			$("#activate-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-success gftnow-btn gftnow-btn-success");
			$("#activate-btn").click(function(){
				$("#activate-modal").fadeIn("fast");
			});
		}

		$("#activate").click(function(){
			$(this).blur();
			$("#status").attr("value", "Enabled");

			var company = {};
			company.identifier = $("#identifier").val();
			company.status = $("#status").val();

			console.log(company);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {company:company, func: "ajaxChangeCompanyState"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-activate-modal").click(function(){
			$("#activate-modal").fadeOut("fast");
		});
		
		$("#block").click(function(){
			$(this).blur();
			$("#status").attr("value", "Disabled");

			var company = {};
			company.identifier = $("#identifier").val();
			company.status = $("#status").val();

			console.log(company);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {company:company, func: "ajaxChangeCompanyState"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-block-modal").click(function(){
			$("#block-modal").fadeOut("fast");
		});
	});';
	
	$content['right-of-title'] = '';

	$content['content'] =
	'<input type="hidden" name="identifier" id="identifier" value="'.$identifier.'">
	<input type="hidden" name="status" id="status" value="'.$status.'">

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1">
				<h3><a href="index.php?p=admin/vendors_approval"><i class="fa fa-chevron-left"></i></a></h3>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="active-name">
					<p><strong>'.$name.'</strong></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11">
				<center>
					<img src="'.$logoURL.'" width="100px" class="img-circle active-profile-active">
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<p><strong>Category</strong></p>
						<p>'.$category.'</p>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Website</strong></p>
						<p>'.$website.'</p>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Email</strong></p>
						<p>'.$email.'</p>
					</div>
					<div class="col-md-6">
						<p><strong>Phone</strong></p>
						<p>'.$phone.'</p>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Address</strong></p>
						<p>'.$address.'</p>
					</div>
				</div><br>
				<div class="row">
					<div class="col-md-12">
						<p><strong>About The Company</strong></p>
						<p>'.$about.'</p>
					</div>
				</div>
				<br><br><br>
				<div class="row">
					<div class="col-md-12">
						<center>
							<div class="col-md-5">
								<div class="active-buttons">
									<button id="activate-btn" class="btn btn-success btn-nobg gftnow-btn gftnow-btn-success">Approve</button>
								</div>
							</div>
							<div class="col-md-2">
								
							</div>
							<div class="col-md-5">
								<div class="active-buttons">
									<button id="block-btn" class="btn btn-warning btn-nobg gftnow-btn gftnow-btn-warning">&nbsp;Decline&nbsp;</button>
								</div>
							</div>
						</center>
					</div>
				</div>
			</div>
		</div>

	</div>


	
	<div class="gftnow-mask" id="activate-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Approve</h1>
				<br/><br/>
				<p style="font-family:GothamRndRegular;font-size:15px;margin-left:30px;margin-right:30px;">Are you sure you want to approve '.$name.'? User will not be notificated by email.</p>
				<br/><br/><br/>
				<div class="row">
					<div class="col-md-6">
						<button id="close-activate-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:80%;">Close</button>
					</div>
					<div class="col-md-6">
						<button id="activate" class="btn gftnow-btn-success gftnow-btn pull-left" style="width:80%;">Approve</button>
					</div>					
				</div><br/>
			</center>
		</div>
	</div>

	<div class="gftnow-mask" id="block-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Decline</h1>
				<br/><br/>
				<p style="font-family:GothamRndRegular;font-size:15px;margin-left:30px;margin-right:30px;">Are you sure you want to decline '.$name.'? This will not delete all of the information but permanently deactivate it. User will not be notificated by email.</p>
				<br/><br/><br/>
				<div class="row">
					<div class="col-md-6">
						<button id="close-block-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:80%;">Close</button>
					</div>
					<div class="col-md-6">
						<button id="block" class="btn gftnow-btn-success gftnow-btn pull-left" style="width:80%;">Decline</button>
					</div>					
				</div><br/>
			</center>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>