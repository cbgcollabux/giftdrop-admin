<?php
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	$id = $_GET['id'];

	echo "<input type='hidden' id='userID' value='".$id."' />";
	$latitude=$_GET['latitude'];
	$longitude = $_GET['longitude'];

	$listUsers;
	$detailedUserProfile;
	$detailedCompanyProfile;
	$swagger = new _swagger();
	$api_client = $swagger->init($_SESSION['token']);
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$listUsersBody = new Swagger\Client\Model\UserListRequest;
	$detailedUserProfileBody = new Swagger\Client\Model\DetailedUserProfileRequest;
	$detailedUserProfileBody['user_identifier'] = $id;
	$detailedCompanyProfileBody = new Swagger\Client\Model\DetailedCompanyProfileRequest;
	$detailedCompanyProfileBody["company_identifier"] = $id;

	try{
		 $listUsers = $api->listUsers($listUsersBody);
	}catch (Exception $e){
		echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}
	try {
	    $detailedUserProfile = $api->detailedUserProfile($detailedUserProfileBody);
	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->detailedUserProfile: ', $e->getMessage(), PHP_EOL;
	}

	$vendor;
	$name;
	$logoURL;
	$created;
	$email;
	$status;
	$acct_id;
	$phone='';

	foreach($listUsers['users'] as $user){
		if($user['identifier'] == $id){
			$vendor = $user['account']['title'];
			$name = $user['name'];
			$logoURL = $user['account']['logo_url'];
			$created = $user['created']->format('n/j/Y');
			$email = isset(  $user['contact_repository']['contacts'][0]['contact'] ) ? $user['contact_repository']['contacts'][0]['contact'] : "";
			$status = $user['account']['state'];
			$acct_id = $user['account']['identifier'];
			break;
		}
	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	function getaddress($latlng){
		$location = @file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latlng.'&sensor=true');
		if (strpos($http_response_header[0], "200")) { 
		   $location = json_decode($location);
		   return isset( $location->results[0]->formatted_address ) ? $location->results[0]->formatted_address : "" ;
		} else { 
		   return "Cant Find Address";
		}	
	}

	$activities = '';
	foreach($detailedUserProfile['player_inventory_view']['inventory'] as $inventory){
		$activities .= '<div class="row active-list">
							<div class="col-md-12">
								<div class="col-md-2">
									<p>Received</p>
								</div>
								<div class="col-md-2">
									<p>'.$inventory['drop_events'][0]['instant']->format('n/j/Y').'</p>
								</div>
								<div class="col-md-2">
									<p>'.$inventory['drop_events'][0]['instant']->format('h:i a').'</p>
								</div>
								<div class="col-md-2">
									'.getaddress($inventory['drop_events'][0]['location']['latitude'].','.$inventory['drop_events'][0]['location']['longitude']).'
								</div>
								<div class="col-md-2">
									'.$inventory['drop_description']['product']['name'].'
								</div>
								
							</div>
						</div>';
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';
	$content['title'] = "Users";
	$content['script'] = 
	'$(document).ready(function() {
		$("#logout").show();
		$("#admin-menu-users").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		if($("#status").val() == "Enabled"){
			$("#activate-btn").removeClass("btn btn-success gftnow-btn gftnow-btn-success").addClass("btn btn-default gftnow-btn gftnow-btn-default").attr("value","Activated");
			$("#activate-btn").click(function(){
				$(this).blur();
			});
			$("#block-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-warning gftnow-btn gftnow-btn-warning").attr("value","Block");
			$("#block-btn").css({"background-color": "#E3B339", "border-color": "#E3B339"});
			$("#block-btn").hover(
				function(){
					$(this).css({"background-color": "#c09853", "border-color": "#c09853"});
				},
				function(){
					$(this).css({"background-color": "#E3B339", "border-color": "#E3B339"});
			});
			$("#block-btn").click(function(){
				$("#block-modal").fadeIn("fast");
			});
		}

		if($("#status").val() == "Disabled"){
			$("#block-btn").removeClass("btn btn-warning gftnow-btn gftnow-btn-warning").addClass("btn btn-default gftnow-btn gftnow-btn-default").attr("value","Blocked");
			$("#block-btn").click(function(){
				$(this).blur();
			});
			$("#activate-btn").removeClass("btn btn-default gftnow-btn gftnow-btn-default").addClass("btn btn-success gftnow-btn gftnow-btn-success").attr("value","Activate");
			$("#activate-btn").click(function(){
				$("#activate-modal").fadeIn("fast");
			});
		}

		$("#activate").click(function(){
			$(this).blur();
			$("#status").attr("value", "Enabled");

			var identifier = $("#identifier").attr("value");
			var status = $("#status").attr("value");

			var account = {};
			account.identifier = identifier;
			account.status = status;

			console.log(account);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {account:account, func: "ajaxChangeAccountState2"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});

		$("#close-activate-modal").click(function(){
			$("#activate-modal").fadeOut("fast");
		});

		$("#block").click(function(){
			$(this).blur();
			$("#status").attr("value", "Disabled");

			var identifier = $("#identifier").attr("value");
			var status = $("#status").attr("value");

			var account = {};
			account.identifier = identifier;
			account.status = status;

			console.log(account);

			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
			jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });

			jQuery.ajax({
				url:"mvc/controller/ajaxController.php",
				type: "post",
				dataType: "json",
				data: {account:account, func: "ajaxChangeAccountState2"},
				success: function(data){
					location.reload();
					console.log(data);
				},error: function(err){
					console.log(err.responseText);
				}
			});
		});
		
		$("#close-block-modal").click(function(){
			$("#block-modal").fadeOut("fast");
		});
	});';

	$content['right-of-title'] = '';

	$content['content'] = 
	'<input type="hidden" name="identifier" id="identifier" value="'.$acct_id.'">
	<input type="hidden" name="name" id="name" value="'.$name.'">
	<input type="hidden" name="vendor" id="vendor" value="'.$vendor.'">
	<input type="hidden" name="email" id="email" value="'.$email.'">
	<input type="hidden" name="logoURL" id="logoURL" value="'.$logoURL.'">
	<input type="hidden" name="status" id="status" value="'.$status.'">

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1">
				<a href="dashboard.php?p=admin/users"><img src="assets/img/chevron-left.png" /></a>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="active-name">
					<p>'.$name.'</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11">
				<center>
					<img src="'.$logoURL.'" width="100px" class="img-circle active-profile-active">
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<p><strong>Email</strong></p>
						<p>'.$email.'</p>
					</div>
					<div class="col-md-6">
						<p><strong>Current Location</strong></p>
						<p>'.getaddress('45.485168,-122.80448').'</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p><strong>Registered</strong></p>
						<p>'.date("n/j/Y",strtotime($created)).'</p>
					</div>
					
				</div>
			</div>
			<div class="col-md-2">
				<input type="button" id="activate-btn" class="btn btn-success gftnow-btn gftnow-btn-success" style="width:185px; margin-left:-30px;" /><br/><br/>
				<input type="button" id="block-btn" class="btn btn-warning gftnow-btn gftnow-btn-warning" style="width:185px; margin-left:-30px;" />
				<!-- <button id="activate-btn" class="btn btn-default gftnow-btn gftnow-btn-default">Activate</button><br/><br/>
				<button id="block-btn" class="btn btn-warning gftnow-btn gftnow-btn-warning">&nbsp;&nbsp;Block&nbsp;&nbsp;</button> -->
			</div>
		</div>
	</div>
	
	<div class="active-main-panel" style="padding-top:1px;">
		<div class="row active-list-title">
			<div class="row">
				<div class="col-md-1 col-sm-1 col-xs-1">
					<h1 class="gftnow-font-light">Activity</h1>
				</div>
			</div>
			<br/><br/>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-2">
						<strong>Status</strong>
					</div>
					<div class="col-md-2">
						<strong>Date</strong>
					</div>
					<div class="col-md-2">
						<strong>Time</strong>
					</div>
					<div class="col-md-2">
						<strong>Location</strong>
					</div>
					<div class="col-md-2">
						<strong>Item</strong>
					</div>
					
				</div>
			</div>
		</div>
		'.$activities.'
	</div>

	<div class="gftnow-mask" id="activate-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Activate</h1>
				<br/><br/>
				<p style="font-family:GothamRndRegular;font-size:18px;margin-left:30px;margin-right:30px;">Are you sure you want to activate '.$name.'?</p>
				<br/><br/><br/>
				<div class="row">
					<div class="col-md-6">
						<button id="close-activate-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:80%;">Close</button>
					</div>
					<div class="col-md-6">
						<button id="activate" class="btn gftnow-btn-success gftnow-btn pull-left" style="width:80%;">Activate</button>
					</div>					
				</div><br/>
			</center>
		</div>
	</div>

	<div class="gftnow-mask" id="block-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:0;right:0;margin:0 auto;">
			<center>
				<h1>Block</h1>
				<br/><br/>
				<p style="font-family:GothamRndRegular;font-size:18px;margin-left:30px;margin-right:30px;">Are you sure you want to block '.$name.'?</p>
				<br/><br/><br/>
				<div class="row">
					<div class="col-md-6">
						<button id="close-block-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:80%;">Close</button>
					</div>
					<div class="col-md-6">
						<button id="block" class="btn gftnow-btn-success gftnow-btn pull-left" style="width:80%;">Block</button>
					</div>
				</div><br/>
			</center>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>