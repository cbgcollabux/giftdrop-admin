<?php include_once "header.php";?>

	<?php
		if ($_SESSION['timeout'] + 30 * 60 < time()) {
			session_destroy();
			header('Location: onboarding.php');
		}
		if(isset($_POST['key5'])){
			
			$_SESSION['campaign']['gftamount'] =$_POST['offer'];

			$_SESSION['drops'] = array(
				"offer"=>$_POST['offer'],
				"date1"=>$_POST['date1'],
				"time1"=>$_POST['time1'],
				"redemption_details"=>$_POST['redemption_details'],
				"barcodetype"=>$_POST['barcodetype'],
				"barcodes"=>explode(",", $_POST['barcodes']),
				"drops"=>json_decode($_POST['drops']));
		}

		if(!isset($_SESSION['drops'])){
			header("Location:onboading.php");
		}

		/*echo"<pre>";
		print_r($_SESSION);
		echo"</pre>";*/

	?>
	
	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/template/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
					</div>
				</center>
			</div>
			
			<div class="row onboarding-container" style="height: 2000px;">
				<div class="col-md-3 onboarding-left-panel">
					<div class="onboarding-nav-container">
						<nav id="onboarding-nav">
							<ul>
								<li>
									<a href="onboarding.php" title="Service"> <div class="numberCircle">1</div> <span class="menu-item-parent">Service</span></a>
								</li>
								<li>
									<a href="onboarding-4.php" title="Campaign"> <div class="numberCircle">2</div> <span class="menu-item-parent">Campaign</span></a>
								</li>
								<?php
								if(isset($_SESSION['drops'])){
									echo '<li><a href="onboarding-5.php" title="Drops"> <div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a href="onboarding-6.php" title="Detail"> <div class="numberCircle active">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								} else {
									echo '<li><a class="inactive"><div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a></li>
										<li><a class="inactive"><div class="numberCircle active">4</div> <span class="menu-item-parent">Confirm</span></a></li>';
								}
								?>
							</ul>
						</nav>
					
						<br/><br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<p><center>
									<h3>Business Information</h3>
									<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
								</center></p>
							</div>
							<div class="col-md-1"></div>
							
						</div>
					</div>	
				</div>

				<div class="col-md-9 onboarding-right-panel ">
					<div class="row">
						<div class="col-md-11">
							<center>
								<h2 class="gftnow-title">
									Drops Details
								</h2>
								Please check and confirm the campaign details
							</center>

							<div class="onboarding" style="box-shadow:none;">
								<div class="onboarding-content">
									<div class="row">
										<div class="col-md-12">
											<b><?php echo $_SESSION['campaign']['name'];?></b>
											<br/><br/>
											<div class="row" style="width:825px;">
												<div class="col-md-3" style="background-color: #F4F5F9;min-height: 304px;">
													<br/></br>
													<center>
														<img style="width: 80%;"src="<?php echo $_SESSION['company']['logoURL'];?>" class="img-responsive" />
														<h3 style="font-size: 24px;"><?php echo $_SESSION['company']['name'];?></h3>
													</center>
												</div>
												<div class="col-md-9" style="background: linear-gradient(to bottom, rgba(0,0,0,0.3) 0%,rgba(0,0,0,0.3) 100%),url(<?php echo $_SESSION['campaign']['image']; ?>); background-repeat:no-repeat; background-size: cover; background-position:center center; min-height:304px;">
													<h3 style="font-size: 22px;position:absolute; bottom: 10px;left: 15px;" class="text-white"><?php echo $_SESSION['campaign']['name'];?></h3>
												</div>
											</div>
										</div>
									</div>
									<br/>
									
									
									
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Description</p>
											<div class="row">
												<div class="col-md-12 gftnow-nopadding">
												<span class="gftnow-font-medium">
													<?php echo $_SESSION['campaign']['description'];?>
												</span>
												</div>
												
											</div>
										</div>
									</div>	
									
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Terms & Conditions</p>
											<div class="row">
												<div class="col-md-12 gftnow-nopadding">
												<input type="hidden" id="tmp-terms" value="<?php echo $_SESSION['campaign']['terms'];?>">
												<span class="gftnow-font-medium" id="gft-terms-container">
													<?php echo ( strlen($_SESSION['campaign']['terms']) > 1000 ) ? substr($_SESSION['campaign']['terms'], 0, 1000)."..." : $_SESSION['campaign']['terms'];?>
												</span>
												</div>
												
											</div>
										</div>
									</div>	
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Location (s)</p>
											<div class="row">
												<div class="col-md-12 gftnow-nopadding">
													<div id="map" class="google_maps"></div>
												</div>
											</div>
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Value of the Offer</p>
											<span class="gftnow-font-medium">$ <?php if(isset($_SESSION['drops']['offer'])){echo $_SESSION['drops']['offer'];}else{echo 0;}?></span>
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Start Date</p>
											<img src="assets/img/icon-calendar.png" style="height: 18px;width:auto;"/> &nbsp; <span class="gftnow-font-medium"><?php if( isset($_SESSION['drops']['date1'])){ echo date('F d, Y',strtotime($_SESSION['drops']['date1'])); }else{} ?> <?php if( isset($_SESSION['drops']['time1'])){ echo '&nbsp;&nbsp;&nbsp;'.date("g:s a",strtotime($_SESSION['drops']['time1'])); }else{} ?></span>
										</div>
									</div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<p>Redemptation Details</p>
											<span class="gftnow-font-medium"><?php if( isset($_SESSION['drops']['redemption_details'])){ echo $_SESSION['drops']['redemption_details']; }else{} ?></span>
										</div>
									</div>
								</div>
								
								<div style="margin-top:100px;">
									<div class="pull-left">
										<a href="onboarding-5.php" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
									</div>


									<div class="pull-right">
										<a href="onboarding-6-1.php" class="btn btn-default gftnow-btn gftnow-btn-success">CONFIRM &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></a>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="gftnow-infobox-wrapper">  
			    <div id="gftnow-infobox" data-zip="<?php echo $_SESSION['company']['zip'];?>" data-state="<?php echo $_SESSION['company']['state'];?>" data-address="<?php echo $_SESSION['company']['address'];?>" data-city="<?php echo $_SESSION['company']['city'];?>">  
					<?php echo $_SESSION['company']['address'];?> 
					<?php echo $_SESSION['company']['city'];?> 
					<?php echo $_SESSION['company']['state'];?> 
					<?php echo $_SESSION['company']['zip'];?>
				</div>  
			</div>

			
			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		<?php include_once "footer.php";?>
		
		<script type="text/javascript">
			var map;

			$(document).ready(function() {


				$("#add-drop").click(function(){
					$("#tmp-add-drop").css("display","none");
					$("#tmp-with-panel").css("display","initial");
				});

				
				google.maps.event.addDomListener(window, 'load', init);
				
				
				$(".drop-rounded-header").click(function(){
					 	$(this).parent().find(".drop-rounded-body").show();
				 });

				 $(".drop-rounded-body a").click(function(){
				 	var text = $(this).text();
				 	$(this).parent().parent().find(".drop-rounded-header span").text(text);
				 	$(this).parent().parent().parent().find(".drop-rounded-header span").text(text);
					$(this).parent().parent().hide();
				 });

			});

			function DropControl(controlDiv, map) {

			        // Set CSS for the control border.
			        var controlUI = document.createElement('div');
			        controlUI.style.backgroundColor = '#fff';
			        controlUI.style.cursor = 'pointer';
			        controlUI.style.marginTop = '10px';
			        controlUI.style.textAlign = 'center';
			        controlDiv.appendChild(controlUI);

			
			        // Set CSS for the control interior.
			        var controlText = document.createElement('div');
			        controlText.style.color = '#514F4F';
			        controlText.style.fontFamily = 'GothamRndRegular';
			        controlText.style.fontSize = '16px';
			        controlText.style.lineHeight = '38px';
			        controlText.style.paddingLeft = '15px';
			        controlText.style.paddingRight = '15px';
			        controlText.innerHTML = 'Drop Pin &nbsp;&nbsp; <img src="assets/img/green_pin.png">';
			        controlUI.appendChild(controlText);

			         // Setup the click event listeners: simply set the map to Chicago.
			        controlUI.addEventListener('click', function() {
			          //add event here
			        });

			      }

			function codeAddress(drop)
			{	
				console.log(drop);
			    var address = $("#gftnow-infobox").text();
			    var geocoder = new google.maps.Geocoder();
			   
			    geocoder.geocode( { 'address': drop.address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			        	var overlay = new CustomMarker(
								results[0].geometry.location, 
								map,
								{},drop.quantity
						);

						var infowindow = new google.maps.InfoWindow({
				          content: document.getElementById("gftnow-infobox")
				         });

		        		 google.maps.event.addListener(infowindow, 'domready', function() {
							var iwOuter = $('.gm-style-iw');
							var iwBackground = iwOuter.prev();
							iwBackground.children(':nth-child(2)').css({'display' : 'none'});
							iwBackground.children(':nth-child(4)').css({'display' : 'none'});
							iwBackground.children(':nth-child(1)').css({'display' : 'none'});
							iwBackground.children(':nth-child(3)').css({'display' : 'none'});
						});

		        		 overlay.addListener('click', function() {
				         	 infowindow.open(map, overlay);
				         });

						map.setCenter(results[0].geometry.location);
			            /*map.setCenter(results[0].geometry.location);
			            var marker = new google.maps.Marker(
			            {
			                map: map,
			                position: results[0].geometry.location,
			                icon: "http://localhost/~chanu/gftnow/assets/img/pin_with_num.png"
			            });

			           */
			        }
			        else
			        {
			        	$("#gftnow-alert-modal-content").text("Geocode was not successful for the following reason: " + status);
						$("#gftnow-alert-modal").modal("show");
			          
			        }
			    });
			}
			function init() {

				geocoder = new google.maps.Geocoder();
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York
                    disableDefaultUI: true,

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
                };

                 var mapElement = document.getElementById('map');

                 map = new google.maps.Map(mapElement, mapOptions);

              
				
				jQuery.ajax({
	        		url: "mvc/controller/ajaxController.php",
	        		type: "post",
	        		dataType: "json",
	        		data: {func: "getSessionDrops"},
	        		success: function(data){

	        			if(data.result=="OK")
	        			{
	        				var result;
	        				for(var i=0;i<data.drops.length;i++)
	        				{
	        					codeAddress(data.drops[i]);
	        				}
	        				
	        			}

	        		},error: function(err){
	        			$("#gftnow-alert-modal-content").text(err.responseText);
						$("#gftnow-alert-modal").modal("show");
	        		}
	        	});
				


            }


		</script>

		

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>