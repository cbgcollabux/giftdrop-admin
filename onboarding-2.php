<?php ob_start();session_start();

	include_once "header.php";
	include_once "mvc/model/user.php";
	include_once "mvc/model/swagger.php";

	if ($_SESSION['timeout'] + 30 * 60 < time()) {
		session_destroy();
		header('Location: onboarding.php');
	}
	if(isset($_POST['website']))
	{
		$_SESSION['company']['website'] = $_POST['website'];
		$_SESSION['company']['about'] = $_POST['about'];
		$_SESSION['company']['phone'] = $_POST['phone'];
		$_SESSION['company']['address'] = $_POST['address'];
		$_SESSION['company']['city'] = $_POST['city'];
		$_SESSION['company']['zip'] = $_POST['zip'];
		$_SESSION['company']['state'] = $_POST['state'];

		/*$swagger = new _swagger();

		$api_client = $swagger->init($_SESSION['token']);

		$api = new Swagger\Client\Api\BusinessServiceApi($api_client);

		try {
		 	$business_api = new Swagger\Client\Api\BusinessServiceApi($api_client);
			$body = new Swagger\Client\Model\Body11;

			$account = array();
			$account['identifier'] = $_SESSION['user']->userInfo->identifier;
			$account['created_at'] = gmdate("Y-m-d\TH:i:s\Z");
			$account['notes'] = $_SESSION['company']['phone'];
			$account['logoURL'] = $_SESSION['company']['logoURL'];
			$account['state'] = "Disabled";
			$account['title'] = $_SESSION['company']['name'];
			$account['about'] = $_SESSION['company']['about'];
			$account['website'] = $_SESSION['company']['website'];

			$body['identifier'] = $identifier;
			$body['name'] = $_SESSION['company']['name'];
			$body['created'] = gmdate("Y-m-d\TH:i:s\Z");
			$body['notes'] = $_SESSION['company']['phone'];
			$body['state'] = "Disabled";
			$body['address'] = $_SESSION['company']['address'];
			$body['category'] = $_SESSION['company']['businesscategory'];
			

			$body['account'] =$account;

		    
			try {
			    $createMyCompany = $business_api->createMyCompany($body);

			    echo "<pre>";
			    print_r($createMyCompany);
			    echo "</pre>";
			} catch (Exception $e) {
			    echo "<pre>";
			    print_r($e);
			    echo "</pre>";
			}
			
		

		} catch (Exception $e) {
		    echo 'Exception when calling BusinessServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
		}*/

		$user = new user();
		$raw = array("notes"=>$_SESSION['company']['phone'],"name"=>$_SESSION['company']['name'], "account"=>array("notes"=>$_SESSION['company']['phone'],"logoURL"=>$_SESSION['company']['logoURL'], "website"=>$_SESSION['company']['website'],"about"=>$_SESSION['company']['about'],"title"=>$_SESSION['company']['name']), "address"=>$_SESSION['company']['address'].", ".$_SESSION['company']['city'].", ".$_SESSION['company']['state']." ".$_SESSION['company']['zip'],"category"=>$_SESSION['company']['businesscategory'] );
		
		$company = $user->createMyCompany($raw);


	}else if(isset($_SESSION['company'])){
		
	}else{
		header("Location:onboarding.php");
	}

	echo "<input type='hidden' id='name' name='name' value='".$_SESSION['company']['name']."'/>";
	echo "<input type='hidden' id='email' name='email' value='".$_SESSION['company']['email']."'/>";
	echo "<input type='hidden' id='password' name='password' value='".$_SESSION['company']['password']."'/>";
	echo "<input type='hidden' id='logoURL' name='logoURL' value='".$_SESSION['company']['logoURL']."'/>";
	
?>
	<body style="background-color: #F4F5F8;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/business.php';?>"><img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GiftDrop</h2><br/></a>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
			</div>
		
			<div class="text-center section row">
				<div class="col-md-12">
					<center>
						We are so glad you are here! Our desire is to tell more people about how great you are and to<br/>do that we would love to know a bit more about who you are and what you do.
					</center>
					
					<br>
					<p>
						<h2 class="gftnow-title">
							About Your Business
						</h2>
					</p>

					<br/><br/>
				</div>

				<div class="col-md-12">
					<center>

						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-3 gtfnow-nopadding">
								<div class="onboarding onboarding-left onboarding-2-left" style="height:650px;min-height:650px;margin-top:0px;">
									<div class="onboarding-content">
										<div class="wizard">
											<div class="numberCircle">1</div>
											<div class="numberCircle">2</div>
											<div class="numberCircle active">3</div>
										</div>
										<br>
										<center>
											<div class="onboarding-icon">
												<img src="assets/img/scissor-icon.png" style="width:80px;height:auto;">
											</div>
										</center>
										<br><br>
										<p>
											<h3>Business Information</h3>
											<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
										</p>
										<br>
											
									</div>
								</div>
							</div>
							<div class="col-md-5 gftnow-nopadding"> 
								<div class="onboarding onboarding-right" style="margin-top:0;background: url(assets/img/left-shadow.png) left;background-repeat:repeat-y;height:650px;min-height:650px;background-color:#fff;">
									<div class="onboarding-content row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<br/><br/>
											<div class="good-to-go">
												<b style="font-weight: 600;">Good To Go!</b><br/>
												<span>We sent you an email confirmation but you can start by creating your first gift drops.</span>
											</div>
											<br>
											<br/><br/><br/><br/><br/><br/>
											<div class="row">
												<div class="pull-left">
													<a href="onboarding-1-1.php" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg"><i class="fa fa-chevron-left"></i> &nbsp;&nbsp; BACK</a>
												</div>


												<div class="pull-right">
													<a href="onboarding-4.php" class="btn btn-default gftnow-btn gftnow-btn-success">&nbsp; START &nbsp;</a>
												</div>
											</div>

										</div>
										<div class="col-md-1"></div>
										
									</div>
								</div>

							</div>
							<div class="col-md-2"></div>
						</div>
					</center>
				</div>
			</div>
			
			<br/><br/>

			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-white">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php"; ?>


		<script type="text/javascript">
			$(document).ready(function() {	
				

			});
		</script>

		

	</body>

</html>