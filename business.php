<!DOCTYPE html>
<html lang="en-us" style="overflow:none !important;">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" />
		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
 -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		<style>
			.row{
				margin: 0px !important;
			}
			#text-slider li,#text-slider2 li{
				display: none;
			}
			
			#slider-one{
				background-color: #F4F5F8 !important;
			}
			.button-scroll{position:absolute;bottom:10vh;left:calc(50% - 13px);width:26px;height:50px;border-radius:15px;border:2px solid #fff;z-index:9}
			
			@media only screen and (min-device-width : 375px) and (max-device-width : 667px) and (orientation : landscape) { 
				.container-full, .gftnow-landing-video{
					height: 90%;
				}

				.gftnow-landing-video{
					background-size: cover !important;
				}
				.gftnow-landing-video span.mask{
					min-height: 100%;
				}
			}

			@media only screen and (min-device-width : 320px) and (max-device-width : 568px) {
				.container-full, .gftnow-landing-video{
					height: 90%;
				}

				.gftnow-landing-video{
					background-size: cover !important;
				}

				.gftnow-landing-video span.mask{
					min-height: 100%;
				}
			}

		</style>
	</head>
	<body style="background-color: #ffffff;padding:0;margin:0;height: 100%;margin: 0px;overflow:none !important;">
		

		<div class="container-full">
			<div class="gftnow-landing-video hidden-sm hidden-xs">
				<span class="mask" style="height:100vh;">
					<br/>
					<div class="landing-nav">
						<div class="landing-nav-header">
							<div class="row">
								<div class="col-md-5 col-sm-12 text-center"><br/><h4 class="gftnow-font-medium"><img src="assets/img/gftnow-logo-big.png" style="width:40px;height:45px;">GiftDrop</h4><br/></div>
								<div class="col-md-2 col-sm-12 text-center"></div>
								<div class="col-md-5 col-sm-12 text-center">
									<br/>
									<a href="javascript:void(0)" onclick="$('.gftnow-signbox').fadeToggle();" class="gftnow-text-white">Sign In</a> &nbsp;&nbsp;&nbsp; <a href="onboarding.php" class="bttn-rounded"><b>Get Started</b></a>
									<br/>
									<div class="gftnow-signbox arrow_box_top arrow_box_top_center">
										<form method="post" id="loginform">
											<input type="hidden" name="key" value="<?php echo uniqid();?>" />
											<div class="form-group">
												<input class="form-control" type="text" name="email" id="email" placeholder="Email"/>
											</div>
											<div class="form-group">
												<input class="form-control" type="password" name="password" id="password" placeholder="Password"/>
											</div>
											<br/>
											<div class="form-group">
												<button type="submit" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg">Sign In</button>
											</div>
											<div class="gftnow-hide" style="padding: 5px;" id="errorCode">
												<div class="alert alert-danger"></div>
											</div>
										</form>
										<hr>
										<div class="form-group text-center">
											<a href="javascript:void(0);" id="forgot-pw-link" class="text-black" style="font-size: 14px;">Forgot Password</a>
										</div>
										<form method="post" id="forgotPasswordForm" class="gftnow-hide">
											<div class="form-group">
												<input class="form-control" type="text" name="email" id="email" placeholder="Email"/>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg">Create New Password</button>
											</div>
											<div class="gftnow-hide" style="padding: 5px;" id="errorCode1">
												<div class="alert alert-danger"></div>
											</div>
										</form>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="section" id="landing-video">
						<div class="row">
							<div class="col-md-12 text-center">
								<center>

								<span style="border-style: solid;border-color:#fff;border-width:5px;padding:10px;display:inline-block;">
									<h1 class="gftnow-font-bold" style="font-size:42px; color:#fff;">WELCOME TO GIFTDROP</h1>
								</span>
								<br/><br/><br/>
								<ul id="text-slider" style="list-style:none;color:white;font-size:80px;" class="hidden-sm hidden-xs">
									<li class="gftnow-font-medium">Choose Your <b style='color:#f1c40f;'>Location</b></li>
									<li class="gftnow-font-medium"><b style='color:#f1c40f;'>Drop</b> Your Ads</li>
									<li class="gftnow-font-medium">Get More <b style='color:#f1c40f;'>Business</b></li>
								</ul>
								</center>
							</div>
						</div>
						<center><a href="javascript:void(0);" id="button-scroll-click" style="bottom:0;">
							<div class="button-scroll">
								<br/>
								<p id="moving-mousescroll" style="color:white;font-size:10px;"><i class="fa fa-chevron-down"></i></p>
							</div>
						</a></center>
					</div>

				</span>
			</div>

			<div class="gftnow-landing-video hidden-md hidden-lg">
				<span class="mask" style="min-height:none;height:60%;z-index:999;position:absolute;background-color: rgba(0,0,0,0.5)">
					<center>
						<a href="#" style="color:white;"><img src="assets/img/gftnow-logo-big.png" style="width:60px;height:65px;"> GiftDrop</a>
						<span style="border-style: solid;border-color:#fff;border-width:5px;padding:10px;display:inline-block;">
							<h1 class="gftnow-font-bold" style="font-size:18px; color:#fff;">WELCOME TO GIFTDROP</h1>
						</span>
						<br/><br/>
						<ul id="text-slider2" style="padding:0;list-style:none;color:white;font-size:24px;">
							<li class="gftnow-font-medium">Choose Your <b style='color:#f1c40f;'>Location</b></li>
							<li class="gftnow-font-medium"><b style='color:#f1c40f;'>Drop</b> Your Ads</li>
							<li class="gftnow-font-medium">Get More <b style='color:#f1c40f;'>Business</b></li>
						</ul>

						<div class="text-center">
							<a href="javascript:void();" class="bttn" id="mobileShowlogin">Login</a>
						</div>
						<br/>
						<div class="text-center">
							<a href="onboarding.php" class="bttn">Get Started</a>
						</div>

					</center>
				</span>
			</div>

			<div id="mobileLogin" class="form hidden-lg hidden-md" style="background-color:black;padding:10px;display:none;">
				<br/>
				<form method="post" id="loginform2">
					<input type="hidden" name="key" value="<?php echo uniqid();?>" />
					<div class="form-group">
						<input class="form-control" type="text" name="email" id="email2" placeholder="Email"/>
					</div>
					<div class="form-group">
						<input class="form-control" type="password" name="password" id="password2" placeholder="Password"/>
					</div>
					
					<div class="form-group text-center">
						<button type="submit" class="btn btn-default btn-nobg gftnow-btn gftnow-btn-success gftnow-btn-nobg">Sign In</button>
					</div>
					<div class="gftnow-hide" style="padding: 5px;" id="errorCode2">
						<div class="alert alert-danger"></div>
					</div>
				</form>
				
			</div>
			<div class="text-center section" id="gftnow-landing-how">
				<h1><b>How It Works</b></h1>

				<br/>
				<center>
					<h4><i class="fa fa-angle-left" id="movingbox-prev"></i> &nbsp;&nbsp; <span id="movingbox-label">2/3</span> &nbsp;&nbsp; <i class="fa fa-angle-right" id="movingbox-next"></i></h4>
				</center>
			</div>

			 
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<ul id="slider-one">
				<li>
					<div class="how-container">
						<center>
							<img src="assets/img/how1.png" class="img-responsive">
							<h3>DropGifts On The Map</h3>
							Choose specific locations around<br/>your store or spread 
							randomly<br/>around the city
						</center>
					</div>
				</li>

				<li>
					<div class="how-container">
						<center>
							<img src="assets/img/how2.png" class="img-responsive">
							<h3>People Will Find It<br/>And Come To Redeem It</h3>
							Either your physical store or an online page
						</center>
					</div>
				</li>

				<li>
					<div class="how-container">
						<center>
							<img src="assets/img/how3.png" class="img-responsive">
							<h3>DropGifts On The Map</h3>
							Choose specific locations around<br/>your store or spread 
							randomly<br/>around the city
						</center>
					</div>
				</li>
				
			</ul>
				</div>
				<div class="col-md-1"></div>
			</div>
			

			<div class="text-center section row">
				<div class="col-md-12 gftnow-font-18 gftnow-font-light" style="line-height:28px;">
					
						Let’s face it, traditional customer aquasition became less effective. People tune our, skip<br/>and simply ingnore traditional banners and sponsored ads. What if you could reach new<br/>customers in a way they would enjoy too?
						<br/><br/><br/>
						<br/><br/><br/>
						<br/><br/><br/>
				</div>
			</div>

			<div class="section section-success text-center gftnow-nopadding">
				<div class="row gftnow-nopadding">
					<div class="col-md-2 text-center"></div>
					
					<div class="col-md-4 text-left hidden-sm hidden-xs" id="gftnow-landing-share-description">
						<div style="position:absolute;height:200px;top:0;bottom:0;margin: auto;">
						<h1>From Clients To Friends</h1>
						<p class="gftnow-font-light">Ads are not something people get excited about.
Thats why we created a fun playful way for people
to discover and get to know your brand</p>
					</div>
						
					</div>
					<div class="col-md-4 text-center" id="gftnow-landing-share">
						<div class="gftnow-phone-bg" style="position:absolute;margin-top:-140px;right: 50px;">
							<video width="200" autoplay loop>
								<source src="assets/videos/1-Discover.mp4"></source>
							</video>
						</div>
						
					</div>

					<div class="col-md-4 text-left hidden-md hidden-lg" id="gftnow-landing-share-description2">
							<br/><br/>
						<h1>Share With Friends</h1>
						<p class="gftnow-font-light text-center">Discovered cool stuff but know somebody 
who needs it more? Make their day but simply 
regifting it to them</p>
					</div>

					<div class="col-md-2 text-center"></div>
				</div>
			</div>

			<div class="text-center section hidden-xs hidden-sm" >
				<h1><b>Powerful Tools That Easy To Use</b></h1>
			</div>

			<div id="myCarousel" class="carousel slide hidden-xs hidden-sm" data-ride="carousel">
				<div class="text-center section row" style="min-height: 700px;max-height: 700px;">
					<div class="col-md-1"></div>
					<div class="col-md-1 col-sm-1 col-xs-1" style="min-height: 700px;max-height: 700px;">
						<a class="left carousel-control" style="background-image:none;position:absolute;height:12px;top:0;bottom:0;margin:auto;" href="#myCarousel" data-slide="prev">
						    <span class="glyphicon"><img src="assets/img/slider-prev.png" /></span>
						</a>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-8" style="min-height: 700px;max-height: 700px;">
						  <div class="carousel-inner">
							    <div class="item active">
							      <img src="assets/img/tools-to-use.png" alt="Los Angeles">
							      <br/><br/><br/><br/><br/><br/>
							       <div class="carousel-caption">
							        <p class="gftnow-font-light gftnow-font-18" style="text-shadow:none;color: #514F4F;">Full control over where, when and how you want to be discovered</p>
							      </div>
							    </div>
							    <div class="item">
							      <img src="assets/img/tools-to-use.png" alt="Los Angeles">
							      <br/><br/><br/><br/><br/><br/>
							       <div class="carousel-caption">
							        <p class="gftnow-font-light gftnow-font-18" style="text-shadow:none;color: #514F4F;">Full control over where, when and how you want to be discovered</p>
							      </div>
							    </div>
							   
						  </div>
					</div>
					<div class="col-md-1 col-sm-1 col-xs-1" style="min-height: 700px;max-height: 700px;">
						<a class="right carousel-control" style="background-image:none;position:absolute;height:12px;top:0;bottom:0;margin:auto;" href="#myCarousel" data-slide="next">
						    <span class="glyphicon"><img src="assets/img/slider-next.png" /></span>
						</a>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>

			<br/><br/><br/><br/><br/><br/>
			<div class="text-center section row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<a href="onboarding.php" class="gft-btn gft-btn-warning" style="padding: 20px;padding-left:30px;padding-right:30px;width:250px;border-radius:50px;"><b>Get Started</b></a>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>

			<br/><br/><br/>

			<div class="text-center section section-default row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-black">2017 Copyright All Rights Reserved</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>

		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>


		<script type="text/javascript">
			$(document).ready(function() {	
				init();

				$("#mobileShowlogin").click(function(){
					$("#mobileLogin").toggle();
				});
			
				moveMouseScrollUp();
				$("#button-scroll-click").click(function() {
					console.log("scroll up");
					$("html, body").animate({
				        scrollTop: $("#gftnow-landing-how").offset().top
				    }, 1000);
				});
				
				function fadeInOut(item) {
					item.fadeIn(1000).delay(3000).fadeOut(1000, function() {
						if (item.next().length) // if there is a next element
						{
							fadeInOut(item.next());
						} // use it
						else {
							fadeInOut(item.siblings(':first'));
						} // if not, then use go back to the first sibling
					});
				}
				fadeInOut(jQuery('#text-slider li:first-child'));
				fadeInOut(jQuery('#text-slider2 li:first-child'));

				$("#forgot-pw-link").click(function(){
					$("#forgotPasswordForm").toggle();
				});

				$("#forgotPasswordForm").validate({
					errorPlacement: function(){
						return false;
					},
					rules:{
						email: "required"
					},
					submitHandler: function(form){
		            	$("#mask1").show();
						console.log("submit");
						console.log($(form.email).val());

						var email = $(form.email).val();
						var haserror = 0;

						firebase.auth().sendPasswordResetEmail(email).catch(function(error){
							if(errorCode=="auth/invalid-email"){
								$("#errorCode1 .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode1").show();
							}
							if(errorCode=="auth/user-not-found"){
								$("#errorCode1 .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode1").show();
							}
						}).then(function(){
							if(haserror==0){
								$("#mask2").hide();
								$("#gftnow-alert-modal-content").html("Password reset email sent.");
								$("#gftnow-alert-modal").css("position", "fixed").modal("show");
							}
						});
					}
				});

				$("#loginform").validate({
					errorPlacement: function(){
						return false;
					},
					rules:{
						email: "required",
						password: "required"
					},
					submitHandler: function(form){
						console.log("submit");
						
						console.log($(form.email).val());
						console.log($(form.password).val());

						var email = $(form.email).val();
						var password = $(form.password).val();
						var haserror = 0;

						firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
						  // Handle Errors here.
						  var errorCode = error.code;
						  var errorMessage = error.message;

						  if(errorCode=="auth/wrong-password")
						  {
							  	$("#errorCode .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode").show();
						  }
						  
						  // ...
						}).then(function(user){

							if(haserror==0){
								console.log(user);

								var user = firebase.auth().currentUser;

								user.getToken().then(function(data) {
				      
					    			var token = data;

					    			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
									jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });
	  	  							
	  	  							jQuery.ajax({
										url: "mvc/controller/ajaxController.php",
										type: "post",
										dataType : "json",
										data: { func: "ajaxLogin",token:token },
										success: function(data){
											jQuery(document).ajaxComplete(function(){ $("#mask2").hide(); });
											if(data.result=="ok"){
												if(data.state=="Enabled"){
													window.location = "index.php?p=business/profile";
												}else{
													$("#errorCode .alert").text("Cannot login. This user is blocked or do not exist.");
													$("#errorCode").show();
												}
											}else{
												console.log("cant login");
												$("#errorCode .alert").text("Cannot login. This user is blocked or do not exist.");
												$("#errorCode").show();
											}
										},error: function(err){
											console.log(err.responseText);
										}
									});
				    			});
							}
						});
					}
				});

				$("#loginform2").validate({
					errorPlacement: function(){
						return false;
					},
					rules:{
						email: "required",
						password: "required"
					},
					submitHandler: function(form){
						console.log("submit");
						
						console.log($(form.email).val());
						console.log($(form.password).val());

						var email = $(form.email).val();
						var password = $(form.password).val();
						var haserror = 0;

						firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
						  // Handle Errors here.
						  var errorCode = error.code;
						  var errorMessage = error.message;

						  if(errorCode=="auth/wrong-password")
						  {
							  

							  	$("#errorCode2 .alert").text(errorMessage);
							  	haserror = 1;
							  	$("#errorCode2").show();
						  }
						  
						  // ...
						}).then(function(user){

							if(haserror==0){
								console.log(user);

								var user = firebase.auth().currentUser;

								user.getToken().then(function(data) {
				      
					    			var token = data;

					    			jQuery(document).ajaxStart(function(){ $("#mask2").show(); });
									jQuery(document).ajaxComplete(function(){ $("#mask2").show(); });
	  	  							
	  	  							jQuery.ajax({
										url: "mvc/controller/ajaxController.php",
										type: "post",
										dataType : "json",
										data: { func: "ajaxLogin",token:token },
										success: function(data){
											jQuery(document).ajaxComplete(function(){ $("#mask2").hide(); });
											$("#mask2").hide();
											if(data.result=="ok"){
												if(data.state=="Enabled"){
													window.location = "index.php?p=business/profile";
												}else{
													$("#errorCode2 .alert").text("Cannot login. This user is blocked or do not exist.");
													$("#errorCode2").show();
												}
											}else{
												console.log("cant login");
												$("#errorCode2 .alert").text("Cannot login. This user is blocked or do not exist.");
												$("#errorCode2").show();
											}
										},error: function(err){
											console.log(err.responseText);
										}
									});
				    			});
							}
						});
					}
				});

			});

			function moveMouseScrollUp(){
			    $("#moving-mousescroll").animate({top: "+=20"}, 500, moveMouseScrollDown);
			}
			function moveMouseScrollDown(){
			    $("#moving-mousescroll").animate({top: "-=20"}, 500, moveMouseScrollUp);
			}

			function init(){
				var width = $(window).width();
				console.log(width);
				var panelWidth = 0.5;

				if(width<=414)
					panelWidth = 0.95;
				else if(width<=320)
					panelWidth = 1;

				

				$('#slider-one').movingBoxes({
				startPanel   : 2,      // start with this panel
				reducedSize  : 0.5,    // non-current panel size: 80% of panel size
				wrap         : false,   // if true, the panel will "wrap" (it really rewinds/fast forwards) at the ends
				buildNav     : false,   // if true, navigation links will be added
				navFormatter : function(){ return "&#9679;"; } // function which returns the navigation text for each panel
				,panelWidth   : panelWidth,
				initAnimation: true,
				easing       : 'swing',
				completed: function(e, slider, tar){
					
					$("#movingbox-label").text(slider.curPanel+"/3");
				}
				});

				var mb = $('#slider-one').data('movingBoxes');

				$("#movingbox-next").bind("click", function(){
					mb.goForward();
				});

				$("#movingbox-prev").bind("click", function(){
					mb.goBack();
				});
			}


		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			/*var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();*/

		</script>

		

	</body>

</html>