<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);

	$identifier;
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\UpdateSettingsRequest;

	/*$body['from'] = array('address'=>'noreply@gftnow.appspotmail.com';,'name'=>'GiftDrop Team');
	$body['to'] = array('address'=>'gonzales.christian.bedia@gmail.com','name'=>'christian');
	*/

	$firebaseConfig = json_encode(
		array(
			"type"=>"service_account",
			"project_id"=>"gftnow2",
			"private_key_id"=>"dbf53e8bf116ff6e96b86f3129b551b0fd29850e",
			"private_key"=>"-----BEGIN PRIVATE KEY-----
			MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCoPpkTCuUJwta8
			LOljGlT4RlkkNf1rPr9X90xItIU715HZgj5IYSl4CBx02NefS9GzpxPBuUb9ZJo8
			m7s+kS3QyfOgBamDz9z9z5UbGkvsF0nK7dhznxqWyBfEZNb6Wr1SImU1PtiWa29J
			mQ9mAKO8g0mRBoeJi4DvoAvOSGgnfbSktDDsdGVMe6pxKPyxV7059hG+WjOWVDak
			hCGhQrF9FQtIttyXthQJ2GxEQSK3LrJriKVMlry7kX9aWffIAtY8ad6GfqOd2MvE
			HLhHNmxxUX3Vj0F7NoaHoepxfPr65dTzdZiFS8mDMI1VdQwePOdkJVJsSa+OesAH
			w9LpztePAgMBAAECggEAepuj0kzOPR7QfrH8I0y9P3GE5O6zj8RajTCbVyC3N7UR
			hQF1ginMnrzKgBzGhkAqwlGwBC2RnngCLskO27nROaby9Fsr/fMtiYoNj4LKcXNH
			Q6uxYhFAv0FykJ0sTaZy8bg/KV5zWKtSnr/8iMlOicLjidzISWRwwjBQajQ6Mwwp
			0ACqj6n9HUKylNfcFnS9DeW8yGSCBCJzEyLWFFSRFpinAl4YByyzZ1P/4qVPDnr5
			Ej1YAkpgkBbf4iKdMBQ6VgVI+yUSPNVGhwZlSr0izIXA39M8zxs2ZTJS6LnAB/Pa
			qQfOkAxtSXKSrOQOSSnqYRW48QhWJdmc1T94fdopAQKBgQDdKuJKyCJjoRjFZPxq
			ncooldmWp0e1yfW0dKgf4Gl7GSIyhfe7/QpP5c9GGDrUlLEHN7A9kNynzdB12dSc
			dMn2FQBzbwPho2lvYon6uIjmaPha6PDczcr0EBzoBY0IyFN7wjObEaVBbnVNCL3q
			Y3NUIofdHEa3n26cZo0Cmf+i2QKBgQDCvfHU5uPnuv77kAUzZ2yLG3opXmte9qA/
			3OVONvi+b2N3GrYRDZEfiqdBfpXymzCzG4pOhW05PFuqmc6v30OgNMrva59kZsxP
			G5uX/MiNRUd7vnTnBfgxS9HlCCDImAHpNCkUDQUkOOx/2i/TL7kXA4XR5kKQxYox
			9Ufkeu78pwKBgCxR5KkRs/p9FOaxlveSiSla5hCPkwo133v7CJnisaCOZ9DyEbPl
			QETEi6XDQz7IUTVeDn434k/qKYUEUtvLd5FNohp/u6piwcUlXPsWMbDihipJlcLZ
			pnsEI5bHTfzdkAUz5H2FqrUS+oSmZNBsf3bzuIAxo2Y2l8bVsho8XAL5AoGAdCuJ
			Mr8Vn2wOpZSxdYux1X3rrvfzRzAKQm6Mn87tLfUWubtGdv/H3bABvHE+E/5926qH
			I/Y2RNl7uVaNVJ70IYK8uHxUFjm4kUxf0hfqr1NvpCLma9ZEy2t2Z5EktWTwZuRF
			yP9HXOdUNaS7uNiUKmVgk+kvNluQ5+JxI5wuKqsCgYBs+/Jx+6cQ/yJyvHkweIkP
			PJg+Xwp2uyJIdCCDcZSSt8dnEW+oqC4d8mzBkOVXg3yAVjs11Ct9G8YpeURSbLce
			qP3w2hohbO0U3QhIz0OE5yC2K4OQiXGbjvuHs9vFLA3KyPkxB+1uQ7pDSW9ReLRP
			eV3Ei6wKG/8PU2aAHWfotg==
			-----END PRIVATE KEY-----",
			"client_email"=>"servlet@gftnow.iam.gserviceaccount.com",
			"client_id"=>"101872066163529470735",
			"auth_uri"=>"https://accounts.google.com/o/oauth2/auth",
			"token_uri"=>"https://accounts.google.com/o/oauth2/token",
			"auth_provider_x509_cert_url"=>"https://www.googleapis.com/oauth2/v1/certs",
			"client_x509_cert_url"=>"https://www.googleapis.com/robot/v1/metadata/x509/servlet%40gftnow.iam.gserviceaccount.com"
		)
	);

	$redissonConfig = json_encode(
		array(
			"singleServerConfig"=>array(
				"idleConnectionTimeout"=>10000,
				"pingTimeout"=>1000,
				"connectTimeout"=>10000,
				"timeout"=>3000,
				"retryAttempts"=>3,
				"retryInterval"=>1500,
				"reconnectionTimeout"=>3000,
				"failedAttempts"=>3,
				"password"=>"VB7NWG]kaS7?1WHY",
				"subscriptionsPerConnection"=>5,
				"clientName"=>null,
				"address"=>"redis://35.184.82.38:6379",
				"subscriptionConnectionMinimumIdleSize"=>1,
				"subscriptionConnectionPoolSize"=>50,
				"connectionMinimumIdleSize"=>10,
				"connectionPoolSize"=>64,
				"database"=>0,
				"dnsMonitoring"=>"false",
				"dnsMonitoringInterval"=>5000
			),
			"threads"=>0,
			"nettyThreads"=>0,
			"codec"=>null,
			"useLinuxNativeEpoll"=>"false"
		)
	);

	$body['settings'] = array(
		0=>array("name"=>"defaultUserLogo", "type"=>"URL", "value"=>"https://storage.googleapis.com/gftnow.appspot.com/stock/user_drop.gif"),
		1=>array("name"=>"incognitoLogo", "type"=>"URL", "value"=>"https://storage.googleapis.com/gftnow.appspot.com/stock/incognito.png"),
		2=>array("name"=>"dropValidityDuration", "type"=>"Duration", "value"=>"P365D"),
		3=>array("name"=>"dropPutBackDuration", "type"=>"Duration", "value"=>"PT3H"),
		4=>array("name"=>"pickupCoolDownPeriod", "type"=>"Duration", "value"=>"PT3H"),
		5=>array("name"=>"visibleDropPickupRadius", "type"=>"Distance", "value"=>"30"),
		6=>array("name"=>"visibleDropOfferRadius", "type"=>"Distance", "value"=>"10"),
		7=>array("name"=>"visibleDropVisibilityRadius", "type"=>"Distance", "value"=>"1500"),
		8=>array("name"=>"defaultPlan", "type"=>"UUID", "value"=>"A0478E24-C913-45E7-BF18-E57DDCBCDEB6"),
		9=>array("name"=>"tangoUsername", "type"=>"String", "value"=>"designhouse5test"),
		10=>array("name"=>"tangoPassword", "type"=>"String", "value"=>"Fe&LYgTIXVBGNZbWTXYSGKgWOeKPLkjoUPDCEXQqptM"),
		11=>array("name"=>"tangoCustomerId", "type"=>"String", "value"=>"dh5customer"),
		12=>array("name"=>"tangoAccountId", "type"=>"String", "value"=>"dh5customeraccount"),
		13=>array("name"=>"maximumLogoSizeInBytes", "type"=>"Long", "value"=>"2000000"),
		14=>array("name"=>"visibleDropCacheHilbertGranularity", "type"=>"Integer", "value"=>"12"),
		15=>array("name"=>"hiddenDropCacheHilbertGranularity", "type"=>"Integer", "value"=>"15"),
		16=>array("name"=>"sendGridAPIKey", "type"=>"String", "value"=>"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"),
		17=>array("name"=>"stripeAPIKey", "type"=>"String", "value"=>"sk_test_Wp1Ziuq5Sz509Vf7XIGkiyX7"),
		18=>array("name"=>"firebaseUrl", "type"=>"URL", "value"=>"https://gftnow.firebaseio.com/"),
		19=>array("name"=>"firebaseConfig", "type"=>"JSON", "value"=>$firebaseConfig),
		20=>array("name"=>"redissonConfig", "type"=>"JSON", "value"=>$redissonConfig),
		21=>array("name"=>"testPaymentProviderState", "value"=>"String", "value"=>"disabled"),
		22=>array("name"=>"testGiftCardProviderState", "value"=>"String", "value"=>"disabled"),
		23=>array("name"=>"emailSenderName", "type"=>"String", "value"=>"GiftDrop Team"),
		24=>array("name"=>"emailSenderEmailAddress", "type"=>"String", "value"=>"noreply@gftnow.appspotmail.com"),
		25=>array("name"=>"emailAdminNamee", "type"=>"String", "value"=>"GiftDrop Administrator"),
		26=>array("name"=>"emailAdminEmailAddress", "type"=>"Email", "value"=>"ars+dropadmin@saranin.net"),
		27=>array("name"=>"hiddenDropPickupRadius", "type"=>"Distance", "value"=>"10")
	);

	$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
	echo "timeofrequest: ".$timeofrequest;
	
	try {
	    $updateSettings = $api->updateSettings($body);

		echo "<pre>";
	    print_r($updateSettings);
	    echo "</pre>";

	} catch (Exception $e) {

		echo "<pre>";
	    print_r($e);
	    echo "</pre>";

	}

?>