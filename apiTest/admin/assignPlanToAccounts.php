<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);

	$identifier;
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\Body12;
	$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
	echo "timeofrequest: ".$timeofrequest;

	$body['plan_identifier'] = "d076088d-b78b-46ab-99c0-2b5ede24b554";
	$body['account_identifiers'] = array("50d2a0a7-7ece-48b1-bdba-38bf0be1f05b", "79e6bf54-1f55-4796-83cf-aa992beff520");
	
	try {
	    $assignPlanToAccounts = $api->assignPlanToAccounts($body);

		echo "<pre>";
	    print_r($assignPlanToAccounts);
	    echo "</pre>";

	} catch (Exception $e) {

		echo "<pre>";
	    print_r($e);
	    echo "</pre>";

	}


?>