<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);

	$identifier;
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\Body;
	$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
	echo "timeofrequest: ".$timeofrequest;

	$body['center'] = array("latitude"=>13.139062,"longitude"=>123.743800);
	$body['radius'] = 10000000000;
	
	
	try {
	    $listSettings = $api->listUserLocations($body);

		echo "<pre>";
	    print_r($listSettings);
	    echo "</pre>";

	} catch (Exception $e) {

		echo "<pre>";
	    print_r($e);
	    echo "</pre>";

	}


?>