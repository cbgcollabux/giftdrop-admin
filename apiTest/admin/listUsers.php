<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	
	$identifier;
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\UserListRequest();
	
	try {
	    $listUsers = $api->listUsers($body);

		echo "<pre>";
	    print_r($listUsers);
	    echo "</pre>";

	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listUsers: ', $e->getMessage(), PHP_EOL;
	}

?>