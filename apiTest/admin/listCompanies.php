<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	
	$identifier;
	$api = new Swagger\Client\Api\AdminServiceApi($api_client);
	$body = new Swagger\Client\Model\CompanyListRequest();
	$body['query_parameters'] = array(
		"item_range_query"=>array(
			"start_item_index_inclusive"=>0,
			"end_item_index_exclusive"=>0
		),
		"period_filters"=>array(
			array(
				"field_name"=>"",
				"period"=>array(
					"from"=>"2017-09-20T08:08:23.064Z",
					"to"=>"2017-09-20T08:08:23.064Z"
				)
			)
		),
		"text_filters"=>array(
			array(
				"field_name"=>"",
				"pattern"=>""
			)
		),
		"state_filters"=>array(
			array(
				"field_name"=>"",
				"state"=>"Unknown"
			)
		),
		"sorting"=>array(
			array(
				"field_name"=>"",
				"sorting_order"=>"Unknown"
			)
		)
	);
	
	try {
	    $listCompanies = $api->listCompanies($body);

		echo "<pre>";
	    print_r($listCompanies);
	    echo "</pre>";

	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->listCompanies: ', $e->getMessage(), PHP_EOL;
	}

?>