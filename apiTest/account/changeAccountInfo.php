<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);
	
	$identifier;
	$api = new Swagger\Client\Api\AccountServiceApi($api_client);
	$body = new Swagger\Client\Model\UpdateAccountRequest();
	$body['account'] = array(
		"user_info"=>array(
			"identifier"=>"a83335e5-5769-4d60-92e1-954ea4f3b9b2",
			"name"=>"111@adidas.com",
			"first_name"=>"Adi",
			"last_name"=>"das",
			"account"=>array(
				"identifier"=>"023ad424-5fc0-4d1d-9162-df3f8a6686df",
				"created"=>"023ad424-5fc0-4d1d-9162-df3f8a6686df",
				"notes"=>"",
				"logo_url"=>"http:\/\/localhost\/assets\/upload\/php\/files\/18e7bac6840fcd02960a.png",
				"state"=>"Enabled",
				"title"=>"{\"phone\":\"21234567890\",\"firstname\":\"Test\",\"lastname\":\"Test\"}",
				"about"=>"Test",
				"website"=>"https:\/\/www.Test.com"
			),
			"created"=>"2017-09-21T05:31:18+00:00",
			"notes"=>"",
			"company"=>"6706e524-3d99-49f1-9ab6-c5166703d030",
			"role"=>"Business",
			"state"=>"Enabled",
			"home_location"=>array(
				"latitude"=>3.557390192714974e-8,
				"longitude"=>3.557390192714974e-8
			),
			"contact_repository"=>array(
				"contacts"=>array(
					array(
						"contact_type"=>"Email",
						"contact"=>"111@adidas.com"
					)
				)
			)
		)
	);
	
	try {
	    $changeAccountInfo = $api->changeAccountInfo($body);

		echo "<pre>";
	    print_r($changeAccountInfo);
	    echo "</pre>";

	} catch (Exception $e) {
	    echo 'Exception when calling AdminServiceApi->changeAccountInfo: ', $e->getMessage(), PHP_EOL;
	}

?>