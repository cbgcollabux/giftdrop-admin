<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	//echo $_SESSION['token']."<br/>";
	//$api_client = $swagger->init("eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc0NWM3MTI4Y2JhMTBlMjUxYjlmZTcxMmFlZDUyNjEzMzg4YTY2OTkifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZ2Z0bm93IiwibmFtZSI6Impjby5vcmciLCJwaWN0dXJlIjoiaHR0cDovL2xvY2FsaG9zdC9hc3NldHMvdXBsb2FkL3BocC9maWxlcy9iMjJkNGRhZGU0MmFhMDY4ZjMxYi5wbmciLCJhdWQiOiJnZnRub3ciLCJhdXRoX3RpbWUiOjE1MDU3MTQ4MjAsInVzZXJfaWQiOiJ0ZlU4ZmNUZjFkWG9jcGZtdHV1bDZhcWFNRTMzIiwic3ViIjoidGZVOGZjVGYxZFhvY3BmbXR1dWw2YXFhTUUzMyIsImlhdCI6MTUwNTcxNDgyMCwiZXhwIjoxNTA1NzE4NDIwLCJlbWFpbCI6IjFAamNvLm9yZyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyIxQGpjby5vcmciXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.XpEn_GzWH5gI9NZ18k2TMNkEpi0-K21ObvSwf0cMNKKmX0eUS2tnQCB28CPTpBLtqdEhAnTZJujiyf4xYYN9dpbf7I9SoiQQDYIkRwmsTgaOX3ttIjp-1cEC9JHsbFVkLSjwizRPoz45xdsoa0ssKHEg51GLvFNACd5Z_B6oLmsQkxX4MBlOF1P0MkK2gKOsVFfyt3cVnkIQ61njLVd49Md9gR3KvXsL3U90vZv0KjM76w_PV7gosel8yklHPRn_Ic3DodP5w3QRh3SYqpqusz9-KbdRi4ITxpYDWBKPEZm1cugKqa1U1lVpWSngxWNNTVhWPUk0dKK1uWghSfDkZg");
	$api_client = $swagger->init("eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc0NWM3MTI4Y2JhMTBlMjUxYjlmZTcxMmFlZDUyNjEzMzg4YTY2OTkifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZ2Z0bm93IiwibmFtZSI6IkpNTyIsInBpY3R1cmUiOiJodHRwOi8vZGV2LmdpZnRkcm9wLmNvbS9hc3NldHMvdXBsb2FkL3BocC9maWxlcy9jOTExMTYxYmU3ZWNlNjJjMWYwOS5wbmciLCJhdWQiOiJnZnRub3ciLCJhdXRoX3RpbWUiOjE1MDU3MzE2OTcsInVzZXJfaWQiOiJvZUZXcFhjcE1jYmIwWElhUDZsck5nYTgyT2wyIiwic3ViIjoib2VGV3BYY3BNY2JiMFhJYVA2bHJOZ2E4Mk9sMiIsImlhdCI6MTUwNTczMTY5NywiZXhwIjoxNTA1NzM1Mjk3LCJlbWFpbCI6IjRAamNvLm9yZyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyI0QGpjby5vcmciXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.I2wRu8A4MmrsjjW6y12C-VOf17l24MNWnTzfHXlAsbd7kpj3TnjObiqcQXw7mjM7jYpROgToyy0cyqW_YyzdWyERs3V2RKdoYjcXr9xrGq1p-hTqhwdCnMtkoE9xd5ps6c3dkDDDkFlx2l3pyiLFeBo6xajXpTtazMMHtttKV3Akvx4q6EoBRsWb3YN3bLunwfNdkW1Y1-XmaG2GA5qwOFHRKJU34LQsw5yCzh9GRcVq_g4g1u2ccq3R6kq5fi8fG1Uc7NRNen-FLhUAt-47clymGq9tkEbUfxhOzp9W1tcB_Zv033Z68X7zCCMsg4Qr8A9vOKNzuswzdKZtQaiJDQ");
	
	$identifier;
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	
	try {
	   
	    // change company profile error 500
	    $business_api = new Swagger\Client\Api\BusinessServiceApi($api_client);
		$body = new Swagger\Client\Model\CreateDraftProductRequest;

		echo "--------------------------------------";
		echo "REQUEST";
		echo "--------------------------------------";

		$newdrops = array(
			array("barcode"=>"9T814-12Q6X-Q1MAVNA","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNB","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNC","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPND","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNE","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNF","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNG","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNH","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNI","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNJ","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNK","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) ),
			array("barcode"=>"01QRB-I1T9Q-J2PMPNL","location"=>array("latitude"=>13.3240963,"longitude"=>123.64513899999997) )
			
			);
		$product = array(
		"visibility"=>"Visible",
		"creator"=>"",
		"state"=>"Draft",
		"notes"=>'',
		"name"=>"FREE FREE",
		"message"=>"description",
		"photoURL"=>"http://localhost/assets/upload/php/files/a74aa8347048239939fa.png",
		"termsAndConditions"=>"terms",
		"vendor"=>"",
		"amount"=>3,
		"expiration"=>"2027-04-15T23:00:26.669Z",
		"howToRedeem"=>"redeem");

		

		$body['product_and_drops'] = array("product"=>$product,"drops"=>$newdrops);

		echo "<pre>";
		print_r($body);
		echo "</pre>";

		echo "--------------------------------------";
		echo "RESPONSE";
		echo "--------------------------------------";
	   

		try {
		    $createDraftProduct = $api->createDraftProduct($body);

		    echo "<pre>";
		    print_r($createDraftProduct);
		    echo "</pre>";
		} catch (Exception $e) {
		    echo "<pre>";
		    print_r($e);
		    echo "</pre>";
		}

		

	} catch (Exception $e) {
	    echo 'Exception when calling BusinessServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
	}

	
	


	


?>