<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/

	 echo $_SESSION['token'];
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);

	$identifier;
	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	$body = new Swagger\Client\Model\TotalRequest; 

	echo "<pre>";
    print_r($body);
    echo "</pre>";

	$body['quantity'] = 10;
	$body['product_identifier'] = "705f0a44-6f2d-4d97-901d-4122655f0e91";

	echo "<pre>";
    print_r($body);
    echo "</pre>";
	
	$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
	echo "timeofrequest: ".$timeofrequest;
	
	try {
	    $requestTotal = $api->requestTotal($body);

		echo "<pre>";
	    print_r($requestTotal);
	    echo "</pre>";

	} catch (Exception $e) {

		echo "<pre>";
	    print_r($e);
	    echo "</pre>";

	    //echo 'Exception when calling GftServiceApi->findMyCompanyProfile: ', $e->getMessage(), PHP_EOL;
	}

?>