<?php ob_start();session_start();
	
	include_once "../../mvc/model/swagger.php";

	/*setting up swagger configuration to session
	  should only call once after login
	*/
	$swagger = new _swagger();

	$api_client = $swagger->init($_SESSION['token']);

	$api = new Swagger\Client\Api\BusinessServiceApi($api_client);
	$body = new Swagger\Client\Model\Body11();

	$to = date("Y-m-d\TH:i:s\Z");
	$from = date("Y-m-d\TH:i:s\Z", strtotime('-1 year', time()));
	$body = array("from"=>$from, "to"=>$to);

	$timeofrequest = gmdate("Y-m-d\TH:i:s\Z");
	echo "timeofrequest: ".$timeofrequest;
	
	try {
	    $collectMyCompanyStatistics = $api->collectMyCompanyStatistics($body);

		echo "<pre>";
	    print_r($collectMyCompanyStatistics);
	    echo "</pre>";

	} catch (Exception $e) {

		echo "<pre>";
	    print_r($e);
	    echo "</pre>";

	}
	
?>