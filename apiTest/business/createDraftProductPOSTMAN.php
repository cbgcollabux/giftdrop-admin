<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://gftnow.appspot.com/_ah/api/businessService/v1/createDraftProduct",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 300,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"productAndDrops\":{\"product\":{\"visibility\":\"Visible\",\"creator\":\"\",\"state\":\"Draft\",\"notes\":\"\",\"name\":\"FREE FREE\",\"message\":\"description\",\"photoURL\":\"http:\\/\\/localhost\\/assets\\/upload\\/php\\/files\\/a74aa8347048239939fa.png\",\"termsAndConditions\":\"terms\",\"vendor\":\"\",\"amount\":3,\"expiration\":\"2027-04-15T23:00:26.669Z\",\"howToRedeem\":\"redeem\"},\"drops\":[{\"barcode\":\"9T814-12Q6X-Q1MAVNA\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNB\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNC\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPND\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNE\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNF\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNG\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNH\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNI\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNJ\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNK\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}},{\"barcode\":\"01QRB-I1T9Q-J2PMPNL\",\"location\":{\"latitude\":13.3240963,\"longitude\":123.645139}}]}}\r\n                            ",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 8d5e36df-19da-8116-8b23-47e2aaf0d65d",
    "x-gft-auth-provider: firebase",
    "x-gft-auth-token: eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc0NWM3MTI4Y2JhMTBlMjUxYjlmZTcxMmFlZDUyNjEzMzg4YTY2OTkifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vZ2Z0bm93IiwibmFtZSI6IkpNTyIsInBpY3R1cmUiOiJodHRwOi8vZGV2LmdpZnRkcm9wLmNvbS9hc3NldHMvdXBsb2FkL3BocC9maWxlcy9jOTExMTYxYmU3ZWNlNjJjMWYwOS5wbmciLCJhdWQiOiJnZnRub3ciLCJhdXRoX3RpbWUiOjE1MDU3MzE2OTcsInVzZXJfaWQiOiJvZUZXcFhjcE1jYmIwWElhUDZsck5nYTgyT2wyIiwic3ViIjoib2VGV3BYY3BNY2JiMFhJYVA2bHJOZ2E4Mk9sMiIsImlhdCI6MTUwNTczMTY5NywiZXhwIjoxNTA1NzM1Mjk3LCJlbWFpbCI6IjRAamNvLm9yZyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyI0QGpjby5vcmciXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.I2wRu8A4MmrsjjW6y12C-VOf17l24MNWnTzfHXlAsbd7kpj3TnjObiqcQXw7mjM7jYpROgToyy0cyqW_YyzdWyERs3V2RKdoYjcXr9xrGq1p-hTqhwdCnMtkoE9xd5ps6c3dkDDDkFlx2l3pyiLFeBo6xajXpTtazMMHtttKV3Akvx4q6EoBRsWb3YN3bLunwfNdkW1Y1-XmaG2GA5qwOFHRKJU34LQsw5yCzh9GRcVq_g4g1u2ccq3R6kq5fi8fG1Uc7NRNen-FLhUAt-47clymGq9tkEbUfxhOzp9W1tcB_Zv033Z68X7zCCMsg4Qr8A9vOKNzuswzdKZtQaiJDQ"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}