<?php 
	include_once "header.php";
    include_once "mvc/model/user.php";

    /*echo "<pre>";
    print_r($_SESSION);
    echo "</pre>";*/
    $user = new user();
    $companycategories = $user->listCompanyCategories();
    $protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';

?>

	<body style="background-color: #ffffff;padding:0;margin:0;overflow: auto;height: 100%;margin: 0px;">
		

		<div class="container-full">
			<div class="landing-nav-2">
				<center>
					<div class="landing-nav-header" style="width:90%;">
						<div class="row">
							<div class="col-md-6 col-sm-12 text-center title-logo">
								<img src="assets/img/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h2>GFTdrop</h2><br/>
							</div>
							<!-- <div class="col-md-4 col-sm-12 text-center"><img src="assets/img/logo.png"><br/></div> -->
							<div class="col-md-5 col-sm-12 text-center gftnow-font-13" style="line-height: 70px;">
								<a href="#">How It Works</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"><strong>LIVE CHAT</strong>&nbsp;&nbsp;&nbsp;&nbsp;<img src="assets/img/live-chat.png" width="15px"></a>
								<br/>
							</div>
						</div>
						
					</div>
				</center>
			</div>
			
			<div class="row onboarding-container" style="height: 1300px;">
				<div class="col-md-3 onboarding-left-panel">
					<div class="onboarding-nav-container">
						<nav id="onboarding-nav">
							<ul>
								<li>
									<a href="#" title="Service"> <div class="numberCircle active">1</div> <span class="menu-item-parent">Service</span></a>
								</li>
								<li>
									<a href="#" title="Campaign"> <div class="numberCircle">2</div> <span class="menu-item-parent">Campaign</span></a>
								</li>
								<li>
									<a href="#" title="Drops"> <div class="numberCircle">3</div> <span class="menu-item-parent">Drops</span></a>
								</li>

								<li>
									<a href="#" title="Detail"> <div class="numberCircle">4</div> <span class="menu-item-parent">Confirm</span></a>
								</li>
							</ul>
						</nav>
					
						<br/><br/>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
										<p><center>
											<h3>Business Information</h3>
											<span class="gftnow-font-14">This information will remain <br/>confidential and will be used for <br/>internal contact use only.</span>
										</center></p>
							</div>
							<div class="col-md-1"></div>
							
						</div>
					</div>	
				</div>

				<div class="col-md-9 onboarding-right-panel">
					<div class="row">
						<div class="col-md-11">
							<center>
								<h2 class="gftnow-title">
									About Your Business
								</h2>
								Let’s make it happen! What would you like to promote?
							</center>

							<div class="onboarding" style="box-shadow:none;">
								<div class="text-left">
									<p>Most Popular</p>
								</div>
								<div class="row">
									
									<?php
									foreach (array_slice($companycategories['result']->productCategories, 1, 8) as $categories) {
									?>
										<div class="col-md-3 col-sm-6 col-xs-12" style="padding: 5px;">
											<div class="uploadLogo uploadLogo2" data-value="<?php echo $categories->identifier;?>">
												<div class="img">
													<img src="<?php echo $protocol; echo $categories->pictureURL;?>"/>
												</div>
												<div class="label2">
													<h4><?php echo $categories->name;?></h4>
												</div>
												
											</div>
										</div>
									<?php
									}

									foreach (array_slice($companycategories['result']->productCategories, 10, 13) as $categories) {
									?>
										<div class="col-md-3 col-sm-6 col-xs-12" style="padding: 5px;">
											<div class="uploadLogo uploadLogo2" data-value="<?php echo $categories->identifier;?>">
												<div class="img">
													<img src="<?php echo $protocol; echo $categories->pictureURL;?>"/>
												</div>
												<div class="label2">
													<h4><?php echo $categories->name;?></h4>
												</div>
												
											</div>
										</div>
									<?php
									}

									foreach (array_slice($companycategories['result']->productCategories, 9, -3) as $categories) {
									?>
										<div class="col-md-3 col-sm-6 col-xs-12" style="padding: 5px;">
											<div class="uploadLogo uploadLogo2" data-value="<?php echo $categories->identifier;?>">
												<div class="img">
													<img src="<?php echo $protocol; echo $categories->pictureURL;?>"/>
												</div>
												<div class="label2">
													<h4><?php echo $categories->name;?></h4>
												</div>
												
											</div>
										</div>
									<?php
									}
									?>
									
								</div>
								<br/><br/>
								<div class="pull-right">
									<form method="post" action="onboarding-4.php" id="form1">
										<input type="hidden" name="businesscategory" id="businesscategory" value="<?php if(isset($_SESSION['company']['businesscategory']))echo $_SESSION['company']['businesscategory'];?>"/>
										<button id="submit" class="btn btn-default gftnow-btn gftnow-btn-default">NEXT &nbsp;&nbsp; <i class="fa fa-chevron-right"></i></button>
									</form>
									
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			

			
			<div class="text-center section section-default row" id="onboarding-footer">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<center>
						<p class="text-black">2017 Copyright All Rights Received</p>
					</center>
				</div>
				<div class="col-md-4"></div>
			</div>


		</div>
		
		<!--================================================== -->
		
		
		<?php include_once "footer.php";?>

		<script type="text/javascript">
			$(document).ready(function() {	

				$(".uploadLogo.uploadLogo2").each(function(){
					if($("#businesscategory").val() == $(this).attr("data-value") )
						$(this).addClass("active");
				});

				checkvalidation();

				function checkvalidation()
				{
					var err = 0;

					if( $("#businesscategory").val() == "" )
						err++;

				

					if(err==0){
						$("#submit").removeClass("gftnow-btn-default");
						$("#submit").addClass("gftnow-btn-success");
					}else{
						$("#submit").addClass("gftnow-btn-default");
						$("#submit").removeClass("gftnow-btn-success");
					}
				}
				

				 $(".uploadLogo2").click(function(){
				 	$(".uploadLogo2").removeClass("active");
				 	$(this).addClass("active");
				 	$("#businesscategory").val( $(this).attr('data-value') );
				 	checkvalidation();
				 });

				 $("#form1").validate({
					 	
					 	submitHandler: function(form){
					 		if( $("#businesscategory").val() !='' ){
					 			form.submit();
					 		}else{
					 			$("#gftnow-alert-modal-content").text("Please select business category");
								$("#gftnow-alert-modal").modal("show");
					 		}
					 	}
					 });

				 $(".uploadLogo.uploadLogo2[data-value='"+$("#businesscategory").val()+"']").addClass("active");

			});


		</script>

		

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>

		

	</body>

</html>