<?php
	include_once "mvc/model/user.php";

	$user = new user();

	$listUsers = $user->listUsers();

	/*echo"<pre>";
	print_r($listUsers['result']->users);
	echo"</pre>";*/

	$admin = '';

	foreach(array_reverse($listUsers['result']->users) as $user){
		if($user->role == "Admin"){
			$admin .= '<div class="row active-list">
							<div class="col-md-3">
								'.$user->role.'
							</div>
							<div class="col-md-6">
								'.$user->name.'
							</div>
							<div class="col-md-3">
								<p style="cursor:pointer;" class="choose-action-btn">Choose Action <i class="fa fa-chevron-down"></i></p>
								<div class="drop-rounded-body choose-action">
									<ul>
										<li style="cursor:pointer;">Make Admin</li>
										<li style="cursor:pointer;">Make Manager</li>
										<li style="cursor:pointer;">Freeze Access</li>
										<li style="cursor:pointer;">Delete</li>
									</ul>
								</div>
							</div>
						<hr>
						</div>';
		}
	}

	if($_SESSION['token'] == ""){
		header("Location:admin-login.php");
	}

	$content = array();
	$content['ribbon_image'] = '<img style="height:25px;width:25px;" src="assets/img/man.png"/>';
	$content['title'] = 'Settings';
	$content['right-of-title'] = '';
	$content['script'] = 
	'$(document).ready(function(){
		$("#logout").show();
		$("#admin-menu-settings").addClass("active");
		$("#content").css("background-color", "#f4f5f8");
		$("#left-panel").css("background-color", "#ffffff");
		$("nav").css("background-color", "#ffffff");
		$(".active-main-panel").css("border-radius", "0px");
		$(".active-list").css("background-color", "#ffffff");

		pageSize = 10;

	    var pageCount =  $("#admin-row").find(".active-list").length / pageSize;
	      
	    for(var i = 0 ; i<pageCount;i++){
	      $("#pagination").append("<li><a href=\'javascript:void(0);\' class=\'page\'>"+(i+1)+"</a></li>");
	    }

	    $("#pagination li").first().find("a").addClass("current");
	    
	    showPage = function(page) {
	        $("#admin-row").find(".active-list").hide();
	        $("#admin-row").find(".active-list").each(function(n) {
	            if (n >= pageSize * (page - 1) && n < pageSize * page)
	                $(this).show();
	        });        
	    }
	      
	    showPage(1);

	    $("#pagination li a").click(function() {
	        $("#pagination li a").removeClass("active");
	        $(this).addClass("active");
	        showPage(parseInt($(this).text()))
	    });

		$("#add-admin-btn").click(function(){
			$(this).blur();
			$("#add-admin-modal").fadeIn("fast");
		});

		$("#close-add-admin-modal").click(function(){
			$("#add-admin-modal").fadeOut("fast");
		});

		checkvalidation();

		$.validator.addMethod("pwcheck", function(value) {
			 return /^[a-zA-Z0-9!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]+$/.test(value)
			&& /[a-z]/.test(value) // has a lowercase letter
			&& /[A-Z]/.test(value) // has a uppercase letter
			&& /\d/.test(value)//has a digit
			&& /[!@#$%^&*()_=\[\]{};\':"\\|,.<>\/?+-]/.test(value)
		},"Must be at least 8 characters and consist of lowercase letter, uppercase letter, number and special characters");

		$("#form1").validate({
			rules:{
				email: {
					required: true,
					email: true
				},
				password:{
					required: true,
					pwcheck: true,
					minlength: 8
				}
			},
			submitHandler: function(form){

				$("#mask2").show();

				var email = $("#email").val();
				var password = $("#password").val();

				var haserror = 0;

				firebase.auth().createUserWithEmailAndPassword( email, password ).catch(function(error) {
		 
					var errorCode = error.code;
					var errorMessage = error.message;

					console.log(errorCode);
					console.log(errorMessage);
				 	
					if(errorCode=="auth/invalid-email"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/weak-password"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
					if(errorCode=="auth/email-already-in-use"){
						$("#errorCode .alert").text(errorMessage);
						haserror = 1;
						$("#errorCode").show();
					}
				  
				}).then(function(user){
					if(haserror == 0){
						
						var cuser = firebase.auth().currentUser;

						cuser.getToken().then(function(data){
							var token = data;

							jQuery.ajax({
								url: "mvc/controller/ajaxController.php",
								type: "post",
								dataType : "json",
								data: { func: "loginAdmin",token:token },
								success: function(data){
									console.log(data);
									if(data.result.result.userInfo.identifier){
										$("#add-admin-modal").fadeIn("fast");
										location.reload();
									}else{
										console.log("cant add");
									}
								},error: function(err){
									console.log(err.responseText);
								}
							});
						});
					} else {
						$("#mask2").hide();
					}
				});
			}
		});

		$("input").focus(function(){
			checkvalidation();
		});

		$("input").keyup(function(){
			checkvalidation();
		});

		function checkvalidation(){
			var err = 0;
			$("input").each(function(){
				if ( $(this).hasClass("invalid") )
					err++;
			});

			if( $("#email").val() == "" )
				err++;

			if( $("#password").val() == "" )
				err++;

			if(err==0){
				$("#submit").removeClass("gftnow-btn-default");
				$("#submit").addClass("gftnow-btn-success");
				$("#submit").attr("type", "submit");
			}else{
				$("#submit").addClass("gftnow-btn-default");
				$("#submit").removeClass("gftnow-btn-success");
				$("#submit").attr("type", "button");
			}
		}

		$(".choose-action-btn").click(function(){
			$(this).parent().find(".choose-action").slideToggle("fast");
		});

		$(".gftnow-checkbox").click(function(){
			if($(this).hasClass("active")){
				$(this).removeClass("active");
			} else {
				$(this).addClass("active");
			}
		});

	});';

	$content['content'] = 
	'<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Admin Roles</h1>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Role</strong>
			</div>
			<div class="col-md-6">
				<strong>Name</strong>
			</div>
			<div class="col-md-3">
				<strong>Action</strong>
			</div>
		</div>

		<div id="admin-row">'.$admin.'</div>
		
		<center>
			<ul id="pagination" class="pagination pagination-alt gftnow-pagination"></ul>
		</center>

		<br>
		<center><button id="add-admin-btn" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>

	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Plans and Payments</h1>
			</div>
		</div>

		<div class="row active-list-title">
			<div class="col-md-3">
				<strong>Plan Title</strong>
			</div>
			<div class="col-md-6">
				<strong>Details</strong>
			</div>
			<div class="col-md-3">

			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-3">
				Basic
			</div>
			<div class="col-md-6">
				Payment range: $1-99 Fee: $4
			</div>
			<div class="col-md-3">
				<img src="assets/img/edit-icon-invert.png"/>
				<img src="assets/img/delete-icon-invert.png"/>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-3">
				Advanced
			</div>
			<div class="col-md-6">
				Payment range: $100-499 Fee: $10
			</div>
			<div class="col-md-3">
				<img src="assets/img/edit-icon-invert.png"/>
				<img src="assets/img/delete-icon-invert.png"/>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-3">
				Advanced
			</div>
			<div class="col-md-6">
				Payment range: $500+ Fee: 4%
			</div>
			<div class="col-md-3">
				<img src="assets/img/edit-icon-invert.png"/>
				<img src="assets/img/delete-icon-invert.png"/>
			</div>
		</div>
		<hr>
		<br>
		<center><button onclick="blur()" class="btn btn-default btn-nobg gftnow-btn-circle gftnow-btn-success gftnow-btn-nobg" style="margin-left:-15px;"><i class="fa fa-plus"></i></button></center>
		<br><br>
	</div>
	<div class="active-main-panel">
		<div class="row">
			<div class="col-md-12">
				<h1 class="gftnow-font-light" style="margin-left:21px;">Notifications</h1>
			</div>
		</div>

		<div class="row active-list">
			<div class="col-md-12">
				<p class="gftnow-font-medium">General</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is activity or an important update</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of notifications every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-12">
				<p class="gftnow-font-medium">Registrations</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is a new vendor registration</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of all vendor registrations every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get summary of all vendor registrations for the week</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>
		<hr>
		<div class="row active-list">
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a notification each time there is a new user registration</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get a summary of all user registrations every 24 hours</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Get summary of all user registrations for the week</p>
			</div>
			<div class="col-md-12 gftnow-form">
				<div class="gftnow-checkbox"></div>
				<p>Off</p>
			</div>
		</div>

	</div>

	<div class="gftnow-mask" id="add-admin-modal" style="overflow-y: auto;">
		<div class="gftnow-panel" style="position:absolute;z-index:999;min-width: 600px;width: 500px;top:100px;left:350px;padding:30px;">
			<center>
				<h1>Add Admin</h1>
				<br/><br/>
				<form method="post" id="form1">
				<div class="custom-input">
					<p id="email-placeholder" class="placeholder-effect"></p>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email" />
					<br/><br/>
					<p id="password-placeholder" class="placeholder-effect"></p>
					<input type="password" class="form-control" name="password" id="password" placeholder="Password" />
					<br/><br/>
				</div>
				<div class="gftnow-hide" id="errorCode">
					<div class="alert alert-danger">
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-6">
						<input onclick="blur()" id="submit" class="btn gftnow-btn-success gftnow-btn pull-left" value="ADD" style="width:85%;" />
						</form>
					</div>
					<div class="col-md-6">
						<a href="javascript:void(0);" id="close-add-admin-modal" class="btn gftnow-btn-success gftnow-btn pull-right" style="width:85%;">Close</a>
					</div>
				</div>
			</center>
		</div>
	</div>';

	$content['menu'] = file_get_contents('menu1.php');
?>