<?php ob_start();session_start();?>


<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<title> GiftDrop </title>
		<meta name="description" content="">
		<meta name="author" content="">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-production_unminified.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/smartadmin-skins.css">
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/gftnow.css">
		<!-- <link type="text/css" href="assets/css/fancymoves.css" media="screen" charset="utf-8" rel="stylesheet"  />
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox-1.3.4.css" media="screen" /> -->
		<link rel="stylesheet" type="text/css" media="screen" href="assets/css/demo.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="assets/Croppie-2.4.1/croppie.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="assets/jQuery-File-Upload-9.18.0/css/jquery.fileupload.css" />


		<!-- FAVICONS -->
		<link rel="shortcut icon" href="assets/img/logo.png" type="image/x-icon">
		<link rel="icon" href="assets/img/logo.png" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- MORRIS FONT -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

		<link rel="stylesheet" type="text/css" media="screen" href="assets/js/plugin/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
		<link href="assets/MovingBoxes-master/css/movingboxes.css" rel="stylesheet">
		
		<style>
			.row{
				margin: 0px !important;
			}
		</style>
	</head>