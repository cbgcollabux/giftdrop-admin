<nav>
	<ul>
		<li id="business-menu-company-profile">
			<a href="index.php?p=business/profile" title="Company Profile"><img src="assets/img/overview_logo.png"> <span class="menu-item-parent">Company Profile</span></a>
		</li>
		<li id="business-menu-dashboard">
			<a href="index.php?p=business/dashboard&sort=All_Time" title="Dashboard"><img src="assets/img/users.png"> <span class="menu-item-parent">Dashboard</span></a>
		</li>
		<li id="business-menu-campaign">
			<a href="index.php?p=business/campaigns" title="Campaigns"><img src="assets/img/vendors.png"> <span class="menu-item-parent">Campaigns</span></a>
		</li>
	</ul>
</nav>

<nav class='nav-bottom'>
	<ul>
		<li>
			<a href="#" title="Overview"><img src="assets/img/settings.png"> <span class="menu-item-parent">Settings</span></a>
		</li>
		<li>
			<a href="#" title="Users"><img src="assets/img/support.png"> <span class="menu-item-parent">Support</span></a>
		</li>
		<li>
			<a href="#" title="Vendor"><img src="assets/img/helpcenter.png"> <span class="menu-item-parent">Help Center</span></a>
		</li>
	</ul>
</nav>